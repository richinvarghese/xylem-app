package com.owebso.svs.xylemlern.ActivitExam

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.chart.common.listener.Event
import com.anychart.chart.common.listener.ListenersInterface
import com.anychart.enums.Align
import com.anychart.enums.LegendLayout
import com.owebso.svs.xylemlern.ActivitExam.ExamResultsView
import com.owebso.svs.xylemlern.R
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions
import com.owebso.svs.xylemlern.models.ExamResult
import com.owebso.svs.xylemlern.profile.ProfileUtils
import com.owebso.svs.xylemlern.profile.UserSession
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ExamResultsView : AppCompatActivity() {
    var progressBar6: ProgressBar? = null
    var tvTotalQuestions: TextView? = null
    var tvAnswered: TextView? = null
    var tvSkipped: TextView? = null
    var tvTotalScore: TextView? = null
    var tvYourScore: TextView? = null
    var tvRightAnswer: TextView? = null
    var tvWrongAnswers: TextView? = null
    var tvNegativePoints: TextView? = null
    var tvPoints: TextView? = null
    var EXAM_ID = ""
    var userSession: UserSession? = null
    var mContext: Context = this@ExamResultsView
    var API_RESPONSE_GET_EXAM_RESULT = ""
    var TOTAL_SCORE_FROM_CURRENT_EXAM = ""
    var examResult: ExamResult? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam_results_view_view)
        initActionBar()
        changeStatusBarColor()
        initViews()
        intentValues
        examResults
    }

    fun changeStatusBarColor() {
        val window = this@ExamResultsView.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@ExamResultsView, R.color.accentColor1)
    }

    fun initActionBar() {
        val actionBarLayout = layoutInflater.inflate(
                R.layout.action_bar_04,
                null) as ViewGroup
        // Set up your ActionBar
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(false)
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.customView = actionBarLayout
        // You customization
        val actionBarColor = resources.getColor(R.color.accentColor1)
        actionBar.setBackgroundDrawable(ColorDrawable(actionBarColor))
        val parent = actionBarLayout.parent as Toolbar
        //        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);
        val actionBarTitle = findViewById<View>(R.id.actionBarTitle) as TextView
        actionBarTitle.text = "Exam Result"
        val togBackButton = findViewById<View>(R.id.togBackButton) as ImageView
        togBackButton.setOnClickListener { finish() }
    }

    val intentValues: Unit
        get() {
            EXAM_ID = intent.getStringExtra("EXAM_ID")
            TOTAL_SCORE_FROM_CURRENT_EXAM = intent.getStringExtra("TOTAL_SCORE")
        }

    fun initViews() {
        val profileUtils = ProfileUtils(mContext)
        userSession = profileUtils.userSession
        // progressBar6 = (ProgressBar)findViewById(R.id.progressBar6);
        tvTotalQuestions = findViewById<View>(R.id.tvTotalQuestions) as TextView
        tvAnswered = findViewById<View>(R.id.tvAnswered) as TextView
        tvSkipped = findViewById<View>(R.id.tvSkipped) as TextView
        tvTotalScore = findViewById<View>(R.id.tvTotalScore) as TextView
        tvYourScore = findViewById<View>(R.id.tvYourScore) as TextView
        tvRightAnswer = findViewById<View>(R.id.tvRightAnswer) as TextView
        tvWrongAnswers = findViewById<View>(R.id.tvWrongAnswers) as TextView
        tvPoints = findViewById<View>(R.id.tvPoints) as TextView
        tvNegativePoints = findViewById<View>(R.id.tvNegativePoints) as TextView
    }

    private fun setValues() {
        val COUNT_ANSWERED = examResult!!.right_answer.toInt() + examResult!!.wrong_answer.toInt()
        tvTotalQuestions!!.text = examResult!!.total_questions
        tvAnswered!!.text = "" + COUNT_ANSWERED
        tvRightAnswer!!.text = examResult!!.right_answer
        tvWrongAnswers!!.text = examResult!!.wrong_answer
        tvSkipped!!.text = examResult!!.skipped
        tvNegativePoints!!.text = examResult!!.negative_points
        tvPoints!!.text = examResult!!.mark_optained
        tvTotalScore!!.text = examResult!!.points
        tvYourScore!!.text = examResult!!.points
        chartinit()
    }

    fun chartinit() {
        val anyChartView = findViewById<AnyChartView>(R.id.any_chart_view)
        anyChartView.setProgressBar(findViewById(R.id.progress_bar))
        val pie = AnyChart.pie()
        pie.setOnClickListener(object : ListenersInterface.OnClickListener(arrayOf("x", "value")) {
            override fun onClick(event: Event) {
                Toast.makeText(this@ExamResultsView, event.data["x"].toString() + ":" + event.data["value"], Toast.LENGTH_SHORT).show()
            }
        })
        val data: MutableList<DataEntry> = ArrayList()
        data.add(ValueDataEntry("Right Answer", examResult!!.right_answer.toInt()))
        data.add(ValueDataEntry("Wrong Answer", examResult!!.wrong_answer.toInt()))
        data.add(ValueDataEntry("Skipped", examResult!!.skipped.toInt()))
        pie.data(data)
        // pie.title("Mcq Exam Result");
        pie.animation()
        val colors = arrayOf("#08c72e", "#e8200e", "#bad8e0", "#e6ee9c", "#ffcc80")
        pie.palette(colors)
        pie.labels().position("inside")
        pie.legend().title().enabled(true)
        pie.legend().title()
                .text("Exam Result Statistics")
                .padding(0.0, 0.0, 10.0, 0.0)
        pie.legend()
                .position("center-bottom")
                .itemsLayout(LegendLayout.HORIZONTAL)
                .align(Align.CENTER)
        anyChartView.setChart(pie)
    }

    val examResults: Unit get() {
            val post_data = HashMap<String, String>()
            post_data["hash_code"] = ConfigServer.API_HASH
            post_data["exam_id"] = EXAM_ID
            post_data["student_id"] = userSession!!.useR_SESSION_ID
            val URL = ConfigServer.API_CHECK_EXAM_ATTENDANCE
            val olf = OnlineFunctions(mContext, URL, post_data)
            val handler = Handler()
            val runnable = Runnable {
                API_RESPONSE_GET_EXAM_RESULT = olf.pOstResult
                handler.post {
                    try {
                        val jsonObject = JSONObject(API_RESPONSE_GET_EXAM_RESULT)
                        Log.d("swadseesrdsrd",jsonObject.toString())
                        val status = jsonObject.getString("status")
                        if (status == "200") {
                            val code = jsonObject.getString("code")
                            val result = jsonObject.getJSONObject("result")
                            val attend_id = result.getString("attend_id")
                            val exam_id = result.getString("exam_id")
                            val registration_id = result.getString("registration_id")
                            val attended_data = result.getString("attended_data")
                            val mark_optained = result.getString("mark_optained")
                            val total_questions = result.getString("total_questions")
                            val answered = result.getString("answered")
                            val right_answer = result.getString("right_answer")
                            val wrong_answer = result.getString("wrong_answer")
                            val skipped = result.getString("skipped")
                            val points = result.getString("points")
                            val negative_points = result.getString("negative_points")
                            val total_score = result.getString("total_score")
                            val status1 = result.getString("status")
                            val published = result.getString("published")
                            val created_at = result.getString("created_at")
                            val updated_at = result.getString("updated_at")
                            examResult = ExamResult(attend_id, exam_id, registration_id, attended_data, mark_optained, total_questions, answered,
                                    right_answer, wrong_answer, skipped, points, negative_points, total_score, status1, published, created_at, updated_at)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    setValues()
                }
            }
            Thread(runnable).start()
        }
}