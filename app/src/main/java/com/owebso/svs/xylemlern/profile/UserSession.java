package com.owebso.svs.xylemlern.profile;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.owebso.svs.xylemlern.login.LoginActivity;

public class UserSession {

    private String USER_SESSION_ID;
    private String USER_USER_NAME;
    private String USER_SESSION_TYPE;
    private String USER_SESSION_ROLE;
    private String USER_SESSION_CATEGORY;
    private boolean USER_SESSION_LOGGED_IN;
    private boolean USER_SESSION_ACCOUNT_VERIFIED;
    private String USER_SESSION_PAYMENT;

    public UserSession(String USER_SESSION_ID, String USER_USER_NAME,String USER_SESSION_TYPE, String USER_SESSION_ROLE, String USER_SESSION_CATEGORY, boolean USER_SESSION_LOGGED_IN, boolean USER_SESSION_ACCOUNT_VERIFIED,String USER_SESSION_PAYMENT) {
        this.USER_SESSION_ID = USER_SESSION_ID;
        this.USER_USER_NAME = USER_USER_NAME;
        this.USER_SESSION_TYPE = USER_SESSION_TYPE;
        this.USER_SESSION_ROLE = USER_SESSION_ROLE;
        this.USER_SESSION_CATEGORY = USER_SESSION_CATEGORY;
        this.USER_SESSION_LOGGED_IN = USER_SESSION_LOGGED_IN;
        this.USER_SESSION_ACCOUNT_VERIFIED = USER_SESSION_ACCOUNT_VERIFIED;
        this.USER_SESSION_PAYMENT = USER_SESSION_PAYMENT;
    }

    public String getUSER_SESSION_CATEGORY() {
        return USER_SESSION_CATEGORY;
    }

    public void setUSER_SESSION_CATEGORY(String USER_SESSION_CATEGORY) {
        this.USER_SESSION_CATEGORY = USER_SESSION_CATEGORY;
    }

    public String getUSER_SESSION_ID() {
        return USER_SESSION_ID;
    }

    public void setUSER_SESSION_ID(String USER_SESSION_ID) {
        this.USER_SESSION_ID = USER_SESSION_ID;
    }

    public String getUSER_USER_NAME() {
        return USER_USER_NAME;
    }

    public void setUSER_USER_NAME(String USER_USER_NAME) {
        this.USER_USER_NAME = USER_USER_NAME;
    }

    public String getUSER_SESSION_TYPE() {
        return USER_SESSION_TYPE;
    }

    public void setUSER_SESSION_TYPE(String USER_SESSION_TYPE) {
        this.USER_SESSION_TYPE = USER_SESSION_TYPE;
    }

    public String getUSER_SESSION_ROLE() {
        return USER_SESSION_ROLE;
    }

    public void setUSER_SESSION_ROLE(String USER_SESSION_ROLE) {
        this.USER_SESSION_ROLE = USER_SESSION_ROLE;
    }

    public boolean isUSER_SESSION_LOGGED_IN() {
        return USER_SESSION_LOGGED_IN;
    }

    public void setUSER_SESSION_LOGGED_IN(boolean USER_SESSION_LOGGED_IN) {
        this.USER_SESSION_LOGGED_IN = USER_SESSION_LOGGED_IN;
    }

    public boolean isUSER_SESSION_ACCOUNT_VERIFIED() {
        return USER_SESSION_ACCOUNT_VERIFIED;
    }

    public void setUSER_SESSION_ACCOUNT_VERIFIED(boolean USER_SESSION_ACCOUNT_VERIFIED) {
        this.USER_SESSION_ACCOUNT_VERIFIED = USER_SESSION_ACCOUNT_VERIFIED;
    }

    public String getUSER_SESSION_PAYMENT() {
        return USER_SESSION_PAYMENT;
    }

    public void setUSER_SESSION_PAYMENT(String USER_SESSION_PAYMENT) {
        this.USER_SESSION_PAYMENT = USER_SESSION_PAYMENT;
    }
}

