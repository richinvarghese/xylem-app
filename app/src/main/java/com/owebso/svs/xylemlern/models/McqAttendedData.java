package com.owebso.svs.xylemlern.models;

public class McqAttendedData {

    private String attend_id;
    private String mcq_id;
    private String registration_id;
    private String attended_data;
    private String scoreboard_data;
    private String mark_optained;
    private String total_questions;
    private String answered;
    private String right_answer;
    private String wrong_answer;
    private String skipped;
    private String points;
    private String sktotal_scoreipped;
    private String status;
    private String published;
    private String last_attended_question;
    private String finish;
    private String created_at;
    private String updated_at;

    public McqAttendedData() {
    }

    public String getAttend_id() {
        return attend_id;
    }

    public void setAttend_id(String attend_id) {
        this.attend_id = attend_id;
    }

    public String getMcq_id() {
        return mcq_id;
    }

    public void setMcq_id(String mcq_id) {
        this.mcq_id = mcq_id;
    }

    public String getRegistration_id() {
        return registration_id;
    }

    public void setRegistration_id(String registration_id) {
        this.registration_id = registration_id;
    }

    public String getAttended_data() {
        return attended_data;
    }

    public void setAttended_data(String attended_data) {
        this.attended_data = attended_data;
    }

    public String getScoreboard_data() {
        return scoreboard_data;
    }

    public void setScoreboard_data(String scoreboard_data) {
        this.scoreboard_data = scoreboard_data;
    }

    public String getMark_optained() {
        return mark_optained;
    }

    public void setMark_optained(String mark_optained) {
        this.mark_optained = mark_optained;
    }

    public String getTotal_questions() {
        return total_questions;
    }

    public void setTotal_questions(String total_questions) {
        this.total_questions = total_questions;
    }

    public String getAnswered() {
        return answered;
    }

    public void setAnswered(String answered) {
        this.answered = answered;
    }

    public String getRight_answer() {
        return right_answer;
    }

    public void setRight_answer(String right_answer) {
        this.right_answer = right_answer;
    }

    public String getWrong_answer() {
        return wrong_answer;
    }

    public void setWrong_answer(String wrong_answer) {
        this.wrong_answer = wrong_answer;
    }

    public String getSkipped() {
        return skipped;
    }

    public void setSkipped(String skipped) {
        this.skipped = skipped;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getSktotal_scoreipped() {
        return sktotal_scoreipped;
    }

    public void setSktotal_scoreipped(String sktotal_scoreipped) {
        this.sktotal_scoreipped = sktotal_scoreipped;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getLast_attended_question() {
        return last_attended_question;
    }

    public void setLast_attended_question(String last_attended_question) {
        this.last_attended_question = last_attended_question;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
