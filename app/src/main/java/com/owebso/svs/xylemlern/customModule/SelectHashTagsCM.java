package com.owebso.svs.xylemlern.customModule;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.ServerConfig.DataConstants;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.functionUtils.ZonStringFunctions;
import com.owebso.svs.xylemlern.models.HashTag;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;
import com.skyhope.materialtagview.TagView;
import com.skyhope.materialtagview.enums.TagSeparator;
import com.skyhope.materialtagview.interfaces.TagItemListener;
import com.skyhope.materialtagview.model.TagModel;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectHashTagsCM extends AppCompatActivity {
    PopupWindow popupWindow;
    String SELECTED_SUBCATGEORY_LIST="",SELECTED_NUM_OF_QUESTIONS;
    Context mContext=SelectHashTagsCM.this;
    String API_RESPONSE_GET_TAGS="";
    String API_RESPONSE_CREATE_CM="";
    ArrayList<HashTag> hashTagsList = new ArrayList<>();
    ArrayList<HashTag> hashTagsListSelected = new ArrayList<>();
  //  final ArrayList<Tag> tagList = new ArrayList<>();
    String[] tagListArray;
    String[] selectedTagsArray;
    ProfileUtils profileUtils;
    UserSession userSession;
    @BindView(R.id.buttonNext) Button buttonNext;
    @BindView(R.id.hashtagView) TagView tagView;

    RadioButton rbExamMode;
    RadioButton rbMcqMode;

    String CM_MODE = DataConstants.CM_MODE_EXAM;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_hash_tags_cm);
        ButterKnife.bind(this);
        changeStatusBarColor();

        SELECTED_SUBCATGEORY_LIST = getIntent().getStringExtra("SELECTED_SUBCATGEORY_LIST");
        SELECTED_NUM_OF_QUESTIONS = getIntent().getStringExtra("SELECTED_NUM_OF_QUESTIONS");

        Log.d("logtest_cm", "SELECTED_SUBCATGEORY_LIST : "+SELECTED_SUBCATGEORY_LIST);
        Log.d("logtest_cm", "SELECTED_NUM_OF_QUESTIONS : "+SELECTED_NUM_OF_QUESTIONS);


        initViews();
        initActionBar();
        getHashtags();
    }

    public void initViews(){

        profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();


        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tagView.getSelectedTags();
                List<TagModel> list = tagView.getSelectedTags();
                selectedTagsArray = new String[list.size()];
                for(int i=0;i<list.size();i++){
                    selectedTagsArray[i] = list.get(i).getTagText();
                }
                showChooseExamModeDialougue(v);

               // Toast.makeText(mContext, ""+tags_string, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void changeStatusBarColor(){
        Window window = SelectHashTagsCM.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(mContext,R.color.accentColor1));
    }

    public  void initActionBar(){
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.accentColor1);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent =(Toolbar) actionBarLayout.getParent();
        // parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
        //  parent.setContentInsetsAbsolute(0,0);
        TextView actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("Select Tags");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void showHashtags(){
        tagView.addTagSeparator(TagSeparator.AT_SEPARATOR); // @ seprator
        tagView.setTagBackgroundColor(getResources().getColor(R.color.accentColor1));
        tagView.setTagList(tagListArray);

        tagView.getSelectedTags();

        tagView.initTagListener(new TagItemListener() {
            @Override
            public void onGetAddedItem(TagModel tagModel) {
              //  hashTagsListSelected

            }

            @Override
            public void onGetRemovedItem(TagModel model) {

            }
        }); // You can implement it


    }

    public void getHashtags(){

        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        String URL = ConfigServer.API_GET_HASH_TAGS_LIST;
        final OnlineFunctions olf = new OnlineFunctions(mContext, URL, post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_TAGS = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(!API_RESPONSE_GET_TAGS.equals("null")){
                            try {
                                JSONObject response = new JSONObject(API_RESPONSE_GET_TAGS);
                                JSONArray hashtags = response.getJSONArray("result");

                                tagListArray = new String[hashtags.length()];

                                for(int i=0;i<hashtags.length();i++){
                                    JsonParser parser = new JsonParser();
                                    JsonElement mJson =  parser.parse(hashtags.getJSONObject(i).toString());
                                    Gson gson = new Gson();
                                    HashTag hashTag = gson.fromJson(mJson,HashTag.class);

                                    tagListArray[i] = hashTag.getTag_name();
                                   // tagList.add(new Tag("#"+hashTag.getTag_name()));
                                    hashTagsList.add(hashTag);
                                }
                                showHashtags();



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    public void createCustomModule(){
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        post_data.put("count", SELECTED_NUM_OF_QUESTIONS);
        post_data.put("exam_mode", CM_MODE);
        post_data.put("category_id", SELECTED_SUBCATGEORY_LIST);
        post_data.put("tags", ZonStringFunctions.getCOmmaStringFromArray(selectedTagsArray));
        String URL = ConfigServer.API_CREATE_CUSTOM_MODULE;
        final OnlineFunctions olf = new OnlineFunctions(mContext, URL, post_data);
        final Handler handler = new Handler();
        
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_CREATE_CM = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.d("skadflks",API_RESPONSE_CREATE_CM);

                        if(!API_RESPONSE_CREATE_CM.equals("null")){

                            try{
                                JSONObject response = new JSONObject(API_RESPONSE_CREATE_CM);
                                int status = response.getInt("status");
                                int code = response.getInt("code");
                                if(code==1 || code==4){
                                    Toast.makeText(mContext, "Module Created", Toast.LENGTH_SHORT).show();
                                    if(popupWindow.isShowing()){popupWindow.dismiss();}
                                    switch (CM_MODE){
                                        case DataConstants.CM_MODE_EXAM:
                                            Intent exam = new Intent(mContext,CmExam.class);
                                            startActivity(exam);
                                            finish();
                                            break;
                                        case DataConstants.CM_MODE_MCQ:
                                            Intent mcq = new Intent(mContext,CmMcq.class);
                                            startActivity(mcq);
                                            finish();
                                            break;
                                    }
                                }else {
                                    if(popupWindow.isShowing()){popupWindow.dismiss();}
                                    Toast.makeText(mContext, "Something went wrong, Try again Later", Toast.LENGTH_SHORT).show();
                                }

                            }catch (JSONException e){
                                e.printStackTrace();
                            }

                        }else {
                            Log.e("customodule","error");
                        }
                    }
                });
            }
        };
        new Thread(runnable).start();
        
    }

    public void showChooseExamModeDialougue(View view) {

        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = inflater.inflate(R.layout.pop_up_layout_select_exam_mode, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        popupWindow = new PopupWindow(popupView, width, height, focusable);
        //  popupWindow.setAnimationStyle(R.style.popup_window_animation);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        rbExamMode = (RadioButton) popupView.findViewById(R.id.rbExamMode);
        rbMcqMode = (RadioButton) popupView.findViewById(R.id.rbMcqMode);


        CardView cardExamMode = (CardView) popupView.findViewById(R.id.cardExamMode);
        CardView cardMcqMode = (CardView) popupView.findViewById(R.id.cardMcqMode);

        cardMcqMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbMcqMode.callOnClick();
            }
        });
        cardExamMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbExamMode.callOnClick();
            }
        });


        rbMcqMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbExamMode.setChecked(false);
                rbMcqMode.setChecked(true);
                CM_MODE = DataConstants.CM_MODE_MCQ;

            }
        });
        rbExamMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbMcqMode.setChecked(false);
                rbExamMode.setChecked(true);
                CM_MODE = DataConstants.CM_MODE_EXAM;
            }
        });


        Button buttonCreateCm = (Button) popupView.findViewById(R.id.buttonCreateCm);
        buttonCreateCm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCustomModule();

            }
        });

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }


    public void setCustomMode(int mode, RadioButton rb,RadioButton rb1){

    }

}
