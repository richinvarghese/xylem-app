package com.owebso.svs.xylemlern.rest;

/**
 * Created by Vishnu Saini on 2/20/2018.
 * vishnusainideveloper27@gmail.com
 */
public class ParaName {

    public static final String KEY_USER_TYPE = "user_type";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PHONE_NO = "mobile_number";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_DEVICE_ID = "device_id";
    public static final String KEY_DEVICE_TOKEN = "device_token";
    public static final String KEY_DEVICE_TYPE = "device_type";
    public static final String KEY_USER_LOGIN = "userLogin";
    public static final String KEY_LATTITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_USERID = "user_id";
    public static final String KEY_IS_VISIBLE = "is_visible";
    public static final String KEY_IMAGE = "profile_picture";
    public static final String KEY_NEW_PASSWORD = "new_password";
    public static final String KEY_RIDE_TYPE = "ride_type";

    public static final String KEY_PAYMENT_METHOD = "payment_mode";
    public static final String KEY_PICKUP_LATITUDE = "pickup_latitude";
    public static final String KEY_PICKUP_LONGITUDE = "pickup_longitude";
    public static final String KEY_DROPOFF_LATITUDE = "dropoff_latitude";
    public static final String KEY_DROPOFF_LONGITUDE = "dropoff_longitude";
    public static final String KEY_VEHICLE_TYPE_ID = "vehicle_type_id";
    public static final String KEY_BOOKING_STATUS = "booking_status";
    public static final String KEY_BOOKING_ID = "booking_id";
    public static final String KEY_PARENT_ID = "parent_id";
    public static final String KEY_RATING = "rating";
    public static final String KEY_COMMENT = "comment";
    public static final String KEY_PICKUP_DATETIME = "pickup_dateTime";
    public static final String KEY_PICKUP_ADDRESS = "pickup_address";
    public static final String KEY_DROPOFF_ADDRESS = "dropoff_address";
    public static final String KEY_POLYLINE = "polyline";
    public static final String KEY_CITY = "city";
    public static final String KEY_STATE_ID = "state_id";
    public static final String KEY_COUNTRY_ID = "country_id";
    public static final String KEY_STATE= "state";
    public static final String KEY_DISTANCE= "distance";
    public static final String KEY_TIME= "time";
    public static final String KEY_BOOKING_TIME= "booking_time";
    public static final String KEY_MOBILE_NUMBER= "mobile_number";
    public static final String KEY_TRUSTED_ID= "trusted_id";
    public static final String KEY_COUPON_CODE_ID= "coupon_code_id";
    public static final String KEY_COUPON_CODE= "coupon_code";
    public static final String KEY_LABEL= "label";
    public static final String KEY_ADDRESS= "address";
    public static final String KEY_COMPLEMENT_ID= "complement_id";
    public static final String KEY_SUBJECT_ID= "subject_id";
    public static final String KEY_AMOUNT= "amount";
    public static final String KEY_RE_PASS= "re_password";
    public static final String KEY_SHARE_TYPE= "share_type";
    public static final String KEY_REFER= "referral_code";
    public static final String KEY_FARE_AMOUNT= "fare_amount";
    public static final String KEY_FAVOURITE_ID= "favourite_id";
    public static final String KEY_COUNTRY= "country";



}
