package com.owebso.svs.xylemlern.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.VideoPlayer.XPlayer;
import com.owebso.svs.xylemlern.models.Days_Model;
import com.owebso.svs.xylemlern.models.Slider_Model;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;

public class HomeSliderAdapter extends SliderViewAdapter<HomeSliderAdapter.SliderAdapterVH> {

    private Context context;
    ArrayList<Slider_Model> slider_List;

    public HomeSliderAdapter(Context context, ArrayList<Slider_Model> slider_List) {
        this.context = context;
        this.slider_List = slider_List;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        final Slider_Model slider_data = slider_List.get(position);
        viewHolder.textViewDescription.setText("This is slider item " + position);
        Glide.with(viewHolder.itemView).load(slider_data.getImage()).into(viewHolder.imageViewBackground);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(slider_data.getVideo_id())>0&&Integer.parseInt(slider_data.getCategory_id())>0) {
                    Intent in = new Intent(context, XPlayer.class);
                    in.putExtra("PLAY_URL", slider_data.getVideo_path());
                    in.putExtra("VIDEO_ID", slider_data.getVideo_id());
                    in.putExtra("PARENT_ID", slider_data.getCategory_id());
                    in.putExtra("FAV_STS", "0");
                    context.startActivity(in);
                }
            }
        });

        /*switch (position) {
            case 0:
                Glide.with(viewHolder.itemView)
                        .load(slider_data.getImage())
                        .into(viewHolder.imageViewBackground);
                break;
            case 1:
                Glide.with(viewHolder.itemView)
                        .load("https://t4.ftcdn.net/jpg/01/07/66/03/500_F_107660388_T0odWiickF13BavIq1g5WO1x5T7SAlFT.jpg")
                        .into(viewHolder.imageViewBackground);
                break;
            case 2:
                Glide.with(viewHolder.itemView)
                        .load("https://t4.ftcdn.net/jpg/01/07/22/95/500_F_107229579_2Er8wS4QO0J45S9pqyEUB50PrvGT8101.jpg")
                        .into(viewHolder.imageViewBackground);
                break;
            default:
                Glide.with(viewHolder.itemView)
                        .load("https://silversoftworks.com/wp-content/uploads/2017/05/istock-518310332.jpg")
                        .into(viewHolder.imageViewBackground);
                break;

        }*/

    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return slider_List.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.imageViewBackground);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            this.itemView = itemView;
        }
    }
}