package com.owebso.svs.xylemlern.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.owebso.svs.xylemlern.VideoPlayer.XPlayer;
import com.owebso.svs.xylemlern.functionUtils.ImageUtils;
import com.owebso.svs.xylemlern.models.VidTimeLine;

import java.util.List;
import com.owebso.svs.xylemlern.R;
public class VideoTimeLineAdapter extends RecyclerView.Adapter<VideoTimeLineAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<VidTimeLine> data;
    VidTimeLine current;
    int currentPos=0;
    ProgressDialog progressDialog;

    public VideoTimeLineAdapter(Context context, List<VidTimeLine> data) {
        this.context = context;
        inflater= LayoutInflater.from(context);
        this.data = data;
    }

    @NonNull
    @Override
    public VideoTimeLineAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.custom_view_layout_2, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoTimeLineAdapter.ViewHolder viewHolder, int i) {
        ViewHolder myHolder= (ViewHolder) viewHolder;
        VidTimeLine timeLine=data.get(i);
        myHolder.textViewTitle.setText(timeLine.getTitle());
        myHolder.textViewTime.setText(timeLine.getSeconds()+" Min");
        ImageUtils imageUtils = new ImageUtils(context);
        String IMAGE_THUMB = ((XPlayer)context).getCUrrentVideoThumb();
        imageUtils.showImage(IMAGE_THUMB,myHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle,textViewTime;
        ImageView imageView;
        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            textViewTime = (TextView)itemView.findViewById(R.id.textViewTime);
            textViewTitle = (TextView)itemView.findViewById(R.id.textViewTitle);
            imageView = (ImageView)itemView.findViewById(R.id.imageView19);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String seekTime = ((TextView) itemView.findViewById(R.id.textViewTime)).getText().toString();
                    seekTime = seekTime.replace(" Min","");
                    ((XPlayer)context).seektoSec(seekTime);
                }
            });
        }
    }
}
