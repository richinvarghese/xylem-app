package com.owebso.svs.xylemlern.XyFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.owebso.svs.xylemlern.ActivitMcq.McqBookMarkList;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.adapters.ModuleAdapter;
import com.owebso.svs.xylemlern.adapters.VideosCategoryAdapter;
import com.owebso.svs.xylemlern.adapters.VideosListAdapter;
import com.owebso.svs.xylemlern.customModule.MyCustomModuleList;
import com.owebso.svs.xylemlern.customModule.SubjectListsCM;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Category;
import com.owebso.svs.xylemlern.models.VideoX;
import com.owebso.svs.xylemlern.ActivitMcq.ListViewOfMcqs;
import com.owebso.svs.xylemlern.ActivitMcq.McqExams;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;

public class McqListViewFragment extends Fragment {
    private Context mContext = getContext();
    String API_RESPONSE_GET_CATEGORY="",API_RESPONSE_GET_VIDEOS;
    ArrayList<Category> categoryList;
    ArrayList<Category> moduleList;
    VideosCategoryAdapter adapter;
    RecyclerView recyclerview;
    String CATEGORY_ID="45";
    LinearLayout loadingLayout,mainView;
    //ArrayList<String> catParent;
    ArrayList<VideoX> videoXArrayList;
    RecyclerView recyclerView123;
    VideosListAdapter videoAdapter;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager videoLayoutmanager;
    GridLayoutManager moduleSelectorLayoutManager;
    BottomNavigationView bottomNavigationView;
    LinearLayout noMcqLayout;
    ImageView backActionB;
    RecyclerView recyclerViewModuleSelector;
    boolean isVideoExist=false;
    boolean isMainCategory=false;
   // String CURRENT_PARENT_ID="";
    LinearLayout moduleLayout,videosLayout,frameLayout;
     public String CURRENT_HOME_PARENT_ID="";
     String PARENT_PARENT="";
     
     ArrayList<String> pageList = new ArrayList<>();
     String MODULE_PARENT="";

    String moduleListOfIds="";
    boolean isModuleAvailable=false;

    UserSession userSession;
    ProfileUtils profileUtils;
    CardView cardCustomModuleButton;

    TextView tvBookMarkCount;

    CardView cardBookMarkButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mcq_list_category_view, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
     }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void initViews(){

        profileUtils = new ProfileUtils(getActivity());
        userSession = profileUtils.getUserSession();

        CATEGORY_ID = profileUtils.getCategorytype().getCAT_MCQ();


        cardCustomModuleButton  = (CardView)getView().findViewById(R.id.cardCustomModuleButton);
        noMcqLayout = (LinearLayout)getView().findViewById(R.id.noMcqLayout);
        tvBookMarkCount = (TextView) getView().findViewById(R.id.tvBookMarkCount);
        cardBookMarkButton = (CardView) getView().findViewById(R.id.cardBookMarkButton);
        noMcqLayout.setVisibility(View.GONE);

        cardCustomModuleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MyCustomModuleList.class);
                startActivity(in);
            }
        });


        cardBookMarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), McqBookMarkList.class);
                startActivity(in);
            }
        });

        if(!isTablet()){
            gridLayoutManager = new GridLayoutManager(getContext(),1);
            videoLayoutmanager = new GridLayoutManager(getContext(),1);
          //  moduleSelectorLayoutManager = new GridLayoutManager(getContext(),1);
        }else{
            gridLayoutManager = new GridLayoutManager(getContext(),2);
            videoLayoutmanager = new GridLayoutManager(getContext(),2);
           // moduleSelectorLayoutManager = new GridLayoutManager(getContext(),1);
        }

        //    LinearLayout moduleLayout,videosLayout,frameLayout;

        moduleLayout = (LinearLayout)getView().findViewById(R.id.moduleLayout);
        moduleLayout.setVisibility(View.GONE);
        frameLayout = (LinearLayout)getView().findViewById(R.id.frameLayout);


        recyclerview = (RecyclerView)getView().findViewById(R.id.recyclerview);
        recyclerView123 = (RecyclerView)getView().findViewById(R.id.recyclerView123);
        recyclerViewModuleSelector = (RecyclerView)getView().findViewById(R.id.recyclerViewModuleSelector);
        //recyclerViewModuleSelector.setLayoutManager(moduleSelectorLayoutManager);
        recyclerViewModuleSelector.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerView123.setLayoutManager(videoLayoutmanager);

        loadingLayout = (LinearLayout)getView().findViewById(R.id.loadingLayout);
        videosLayout = (LinearLayout)getView().findViewById(R.id.videosLayout);
        loadingLayout.setVisibility(View.GONE);
        videosLayout.setVisibility(View.GONE);

        mainView = (LinearLayout)getView().findViewById(R.id.mainView);

        //getModule(CATEGORY_ID);

        isMainCategory =true;
        getCategories(CATEGORY_ID);

    }



    public boolean isTablet(){
        boolean isTablet;
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float yInches= metrics.heightPixels/metrics.ydpi;
        float xInches= metrics.widthPixels/metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches*xInches + yInches*yInches);
        if (diagonalInches>=8){
            // 6.5inch device or bigger
            isTablet=true;
        }else{
            // smaller device
            isTablet=false;
        }

        return isTablet;
    }




    public void getCategories(final String CATEGORY_ID){
        moduleListOfIds="";
        videosLayout.setVisibility(View.GONE);
        showLoading(true);
        categoryList = new ArrayList<>();
        categoryList.clear();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("parent_id",CATEGORY_ID);
        post_data.put("section","mcq");
        post_data.put("login_id",userSession.getUSER_SESSION_ID());

        Log.d("1231", "TYPE: "+profileUtils.getCategorytype().getCAT_MCQ());
        Log.d("1231", "userid: "+userSession.getUSER_SESSION_ID());
        Log.d("1231", "URL : "+ConfigServer.API_GET_CATEGORY);

        String URL = ConfigServer.API_GET_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CATEGORY = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        if(isMainCategory){
                            showLoading(false);
                        }

                      //  showLoading(false);
                        noMcqLayout.setVisibility(View.GONE);
                        Log.d("API_RESPONSEd3", ""+API_RESPONSE_GET_CATEGORY);
                        Log.d("API_RESPONSE", "CAT_ID="+CATEGORY_ID);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
                            String status = jsonObject1.getString("status");
                            String video_exist = jsonObject1.getString("video_exist");

                            if(video_exist.equals("0")){
                                isVideoExist=false;
                                Log.d("API_RESPONSE1", "NO_VIDEO");
                            }else if(video_exist.equals("1")){
                                Log.d("API_RESPONSE1", "VIDEO, CATID = "+CATEGORY_ID);
                                isVideoExist=true;
                               // getVideos(CATEGORY_ID);
                                Intent in = new Intent(getContext(), McqExams.class);
                                in.putExtra("CATEGORY_ID",CATEGORY_ID);
                                startActivity(in);
                            }

                            if(status.equals("404")){
                              //  noVidLayoutAlert.setVisibility(View.VISIBLE);
                              //  showLoading(false);
                                noDataAlert(true);
                                isModuleAvailable=false;
                            }else {
                               // showLoading(false);
                                noDataAlert(false);
                            //    noVidLayoutAlert.setVisibility(View.GONE);
                            }

                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                            isModuleAvailable=false;
                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject catObj = jsonArray.getJSONObject(i);
                                String category_id = catObj.getString("category_id");
                                String category_name = catObj.getString("category_name");
                                String description = catObj.getString("description");
                                String parent_id = catObj.getString("parent_id");
                                String category_section = catObj.getString("category_section");
                                String category_status = catObj.getString("category_status");
                                String category_image = catObj.getString("category_image");
                                String created_at = catObj.getString("created_at");
                                String is_module = catObj.getString("is_module");

                                if(is_module.equals("1")){
                                    if(moduleListOfIds.equals("")){
                                        moduleListOfIds=category_id;
                                    }else {
                                        moduleListOfIds=moduleListOfIds+","+category_id;
                                    }
                                    isModuleAvailable=true;
                                }else {
                                    isModuleAvailable=false;
                                }

                                category_image = ConfigServer.PATH_CATEGORY_THUMB+category_image;

                                categoryList.add(new Category(i,category_id,category_name,description,parent_id,category_section,category_status,category_image,created_at,1));



                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                       // showParentAtTop(CATEGORY_ID);

                        if(isModuleAvailable){
                            Intent intent = new Intent(getContext(), ListViewOfMcqs.class);
                            Log.d("API_RESPONSEd3", "run: "+moduleListOfIds);
                            intent.putExtra("MODULE_LIST",moduleListOfIds);
                            //showLoading(false);
                            startActivity(intent);
                           // showLoading(false);
                        }else {
                            showLoading(false);
                            adapter = new VideosCategoryAdapter(getContext(), categoryList, new VideosCategoryAdapter.EventListener() {
                                @Override
                                public void onCategoryClicked(String categoryId, String parentId) {

                                    if(isMainCategory){
                                        CURRENT_HOME_PARENT_ID=CATEGORY_ID;
                                    }
                                    // getModule(parentId);
                                    getCategories(categoryId);
                                    isMainCategory=false;
                                    Log.d("CLICKTESTST", "ESLSEEEE: ");
                                }
                            });
                            recyclerview.setAdapter(adapter);

                            adapter.notifyDataSetChanged();
                            frameLayout.setVisibility(View.VISIBLE);

                            getBookmarksofUser();

                        }

                    }
                });
            }
        };

        new Thread(runnable).start();
    }


    public void getModule(final String CATEGORY_ID){
       // showLoading(true);

        moduleList = new ArrayList<>();
        moduleList.clear();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("parent_id",CATEGORY_ID);
        post_data.put("section","video");
        post_data.put("login_id",userSession.getUSER_SESSION_ID());

        Log.d("CATEGORY_ID", "ID: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CATEGORY = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLoading(false);
                       // noMcqLayout.setVisibility(View.GONE);
                        Log.d("API_RESPONSE", ""+API_RESPONSE_GET_CATEGORY);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
                            String status = jsonObject1.getString("status");

                            if(status.equals("404")){
                              //  noVidLayoutAlert.setVisibility(View.VISIBLE);
                                noDataAlert(true);
                                showLoading(false);
                            }else {
                                showLoading(false);
                                noDataAlert(false);
                               // noVidLayoutAlert.setVisibility(View.GONE);
                            }
                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject catObj = jsonArray.getJSONObject(i);
                                String category_id = catObj.getString("category_id");
                                String category_name = catObj.getString("category_name");
                                String description = catObj.getString("description");
                                String parent_id = catObj.getString("parent_id");
                                String category_section = catObj.getString("category_section");
                                String category_status = catObj.getString("category_status");
                                String category_image = catObj.getString("category_image");
                                String created_at = catObj.getString("created_at");

                                category_image = ConfigServer.PATH_CATEGORY_THUMB+category_image;
                              //  CURRENT_PARENT_ID=CATEGORY_ID;

                                moduleList.add(new Category(i,category_id,category_name,description,parent_id,category_section,category_status,category_image,created_at,1));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        if(moduleList.size()<=0){
                            moduleLayout.setVisibility(View.GONE);

                        }else {
                            moduleLayout.setVisibility(View.VISIBLE);
                        }

                        ModuleAdapter adapter2 = new ModuleAdapter(getContext(), moduleList, new ModuleAdapter.EventListener() {
                            @Override
                            public void onEvent(int data) {
                               // setParentArray(data);
                                getCategories(String.valueOf(data));
                            }
                        });

                        recyclerViewModuleSelector.setAdapter(adapter2);
                      //  adapter.notifyDataSetChanged();
                      //  videosLayout.setVisibility(View.GONE);

                        PARENT_PARENT = CATEGORY_ID;

                        setPageList(getCURRENT_PARENT_ID());

                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    public void showLoading(boolean stat){
        if(stat){
            mainView.setVisibility(View.GONE);
            loadingLayout.setVisibility(View.VISIBLE);
        }else {
            mainView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        }
    }

    public void noDataAlert(boolean stat){
        if(stat){
            noMcqLayout.setVisibility(View.VISIBLE);
        }else {
            noMcqLayout.setVisibility(View.GONE);
        }
    }

    public String getCURRENT_PARENT_ID(){
        return PARENT_PARENT;
    }

    public String getHomeParentId(){
        return CURRENT_HOME_PARENT_ID;
    }

    public void setParentArray(int i){
        Log.d("CATEGORY_ID", "CAT_ID "+i);

        if(!isVideoExist){
            this.CATEGORY_ID=String.valueOf(i);
            getCategories(CATEGORY_ID);
            Log.d("BACKBUT_TEST", "GOING BACK TO ID =="+CATEGORY_ID);

        }else {
            //getModule(CATEGORY_ID);
            getVideos(CATEGORY_ID);
            Log.d("BACKBUT_TEST", "GOING BACK TO ELSE =="+CATEGORY_ID);
        }


    }



    public void getVideos(String CATEGORY_ID){
        Log.d("API_RESPONSE1", "getVideos CALLING : "+CATEGORY_ID);
        showLoading(true);
        videoXArrayList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("category_id",CATEGORY_ID);
        Log.d("API_RESPONSE", "current_id: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_VIDEOS_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_VIDEOS = olf.getPOstResult();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            showLoading(false);
                            Log.d("API_RESPONSEfdsfsd", "run: "+API_RESPONSE_GET_VIDEOS);
                            try {
                                videosLayout.setVisibility(View.VISIBLE);
                                JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_VIDEOS);
                                String status  = jsonObject1.getString("status");
                                if(!status.equals("404")){
                                    noMcqLayout.setVisibility(View.GONE);
                                    JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                                    for(int i =0;i<jsonArray.length();i++){
                                        JSONObject videoObj = jsonArray.getJSONObject(i);
                                        String video_id = videoObj.getString("video_id");
                                        String video_title = videoObj.getString("video_title");
                                        String category_id = videoObj.getString("category_id");
                                        String description = videoObj.getString("description");
                                        String thumb_image = videoObj.getString("thumb_image");
                                        String video_path = videoObj.getString("video_path");
                                        String str_paid = videoObj.getString("paid");
                                        String str_unpaid = videoObj.getString("unpaid");
                                        String str_favourite = videoObj.getString("favourite");
                                        String video_status = videoObj.getString("video_status");
                                        String str_menu_order = videoObj.getString("menu_order");
                                        String created_at = videoObj.getString("created_at");
                                        String str_watch_status = videoObj.getString("watch_status");
                                        String str_time = videoObj.getString("time");
                                        String IMAGE_URL_THUMB = ConfigServer.PATH_VIDEO_THUMB+thumb_image;
                                        video_path = ConfigServer.PATH_VIDEO+video_path;

                                        videoXArrayList.add(new VideoX(video_id,video_title,category_id,description,IMAGE_URL_THUMB,video_path,str_paid,str_unpaid,str_favourite,video_status,
                                                str_menu_order,created_at,str_watch_status,str_time,1));
                                    }
                                }else {
                                    Log.d("API_RESPONSE", "NO VIDEOS");
                                   // noVidLayoutAlert.setVisibility(View.VISIBLE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.d("API_RESPONSE", "exception");

                            }

                            videoAdapter = new VideosListAdapter(getContext(), videoXArrayList, new VideosListAdapter.EventListener() {
                                @Override
                                public void onVideoClicked(VideoX video, String pay_sts) {

                                }
                            });
                            recyclerView123.setAdapter(videoAdapter);
                            mainView.setVisibility(View.VISIBLE);
                            videosLayout.setVisibility(View.VISIBLE);
                            frameLayout.setVisibility(View.GONE);


                        }
                    });
            }
        };

        new Thread(runnable).start();
    }

    public void setPageList(String ID){
        try{
            if(!pageList.get(pageList.size()-1).equals(ID)){
                pageList.add(ID);
            }
        }catch (ArrayIndexOutOfBoundsException e){
            pageList.add(ID);
        }

    }

    public void getBookmarksofUser(){
        final String[] POST_RESPONSE_GET_BOOKMARK = {""};
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_GET_BOOKMARKS;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                POST_RESPONSE_GET_BOOKMARK[0] = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            if(!POST_RESPONSE_GET_BOOKMARK[0].equals("")){
                                JSONObject response = new JSONObject(POST_RESPONSE_GET_BOOKMARK[0]);
                                JSONArray result = response.getJSONArray("result");

                                tvBookMarkCount.setText(result.length()+" Bookmarks");

                                String[] bookmarksArray = new String[result.length()];
                                for(int i=0;i<result.length();i++){
                                    JSONObject bm = result.getJSONObject(i);
                                    bookmarksArray[i] = bm.getString("question_id");
                                }

                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        new Thread(runnable).start();
    }


}
