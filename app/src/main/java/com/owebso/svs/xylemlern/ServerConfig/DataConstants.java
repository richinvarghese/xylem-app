package com.owebso.svs.xylemlern.ServerConfig;

public class DataConstants {

    public static final String EXAM_TYPE_ALL="1";
    public static final String EXAM_TYPE_WEEKLY="2";
    public static final String EXAM_TYPE_MONTHLY="3";
    public static final String EXAM_TYPE_GRAND_TEST="4";
    public static final String EXAM_TYPE_SUBJECTWISE="5";

    //Exam Answer Status
    public static final String ANSWER_RIGHT="1";
    public static final String ANSWER_WRONG="2";
    public static final String ANSWER_SKIPPED="0";

    //Custom module exam type
    public static final String CM_MODE_EXAM="exam";
    public static final String CM_MODE_MCQ = "mcq";

}
