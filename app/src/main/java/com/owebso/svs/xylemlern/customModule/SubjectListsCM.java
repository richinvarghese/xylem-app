package com.owebso.svs.xylemlern.customModule;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.owebso.svs.xylemlern.ActivitMcq.McqExams;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.adapters.VideosListAdapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.functionUtils.ZonStringFunctions;
import com.owebso.svs.xylemlern.models.Category;
import com.owebso.svs.xylemlern.models.VideoX;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubjectListsCM extends AppCompatActivity {
    private Context mContext = SubjectListsCM.this;
    String API_RESPONSE_GET_CATEGORY="",API_RESPONSE_GET_VIDEOS;
    ArrayList<Category> categoryList;
    ArrayList<Category> moduleList;
    CmMainCategoryAdapter adapter;
    RecyclerView recyclerview;
    String CATEGORY_ID="45";
    LinearLayout loadingLayout,mainView;
    //ArrayList<String> catParent;
    ArrayList<VideoX> videoXArrayList;
   // RecyclerView recyclerView123;
    VideosListAdapter videoAdapter;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager videoLayoutmanager;
    GridLayoutManager moduleSelectorLayoutManager;
    BottomNavigationView bottomNavigationView;
    LinearLayout noMcqLayout;
    ImageView backActionB;
   // RecyclerView recyclerViewModuleSelector;
    boolean isVideoExist=false;
    boolean isMainCategory=false;
   // String CURRENT_PARENT_ID="";
  //  LinearLayout moduleLayout,videosLayout,frameLayout;
     public String CURRENT_HOME_PARENT_ID="";
     String PARENT_PARENT="";
     
     ArrayList<String> pageList = new ArrayList<>();
     String MODULE_PARENT="";

    String moduleListOfIds="";
    boolean isModuleAvailable=false;

    UserSession userSession;
    ProfileUtils profileUtils;
    String[] number_of_question;

    String CURRENT_SELECTED_MAIN_CATEGORY="0";
    String MAIN_SELECTED_CATEGORY="";
    String SELECTED_NUM_OF_QUESTIONS="";
    @BindView(R.id.spinnerOfQuestions)Spinner spinnerOfQuestions;
    @BindView(R.id.buttonNext) Button buttonNext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_main_subjects_list_cm);
        ButterKnife.bind(this);

        initActionBar();
        changeStatusBarColor();

        initViews();

    }


    public  void initActionBar(){
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.accentColor1);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent =(Toolbar) actionBarLayout.getParent();
        // parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
        //  parent.setContentInsetsAbsolute(0,0);

        TextView actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("Create Custom Module");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void changeStatusBarColor() {
        Window window = SubjectListsCM.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(SubjectListsCM.this, R.color.accentColor1));
    }

    public void initViews(){

        profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();

        CATEGORY_ID = profileUtils.getCategorytype().getCAT_MCQ();

        noMcqLayout = (LinearLayout)findViewById(R.id.noMcqLayout);
        noMcqLayout.setVisibility(View.GONE);

        initListeners();

        if(!isTablet()){
            gridLayoutManager = new GridLayoutManager(mContext,1);
            videoLayoutmanager = new GridLayoutManager(mContext,1);
          //  moduleSelectorLayoutManager = new GridLayoutManager(mContext,1);
        }else{
            gridLayoutManager = new GridLayoutManager(mContext,2);
            videoLayoutmanager = new GridLayoutManager(mContext,2);

        }
        recyclerview = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(gridLayoutManager);
        loadingLayout = (LinearLayout)findViewById(R.id.loadingLayout);
        loadingLayout.setVisibility(View.GONE);
        mainView = (LinearLayout)findViewById(R.id.mainView);
        isMainCategory =true;
     //   getCategories(CATEGORY_ID); check onReumseFunction

    }

    @Override
    protected void onResume() {
        super.onResume();
        getCategories(CATEGORY_ID);

    }

    public void initListeners(){
        number_of_question = getResources().getStringArray(R.array.custom_module_number_of_questions);
        spinnerOfQuestions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(mContext, ""+number_of_question[position], Toast.LENGTH_SHORT).show();
                SELECTED_NUM_OF_QUESTIONS = number_of_question[position];
                Log.d("logtest_cm", "11SELECTED_NUM_OF_QUESTIONS : "+SELECTED_NUM_OF_QUESTIONS);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading(true);
             //   Toast.makeText(mContext, ""+MAIN_SELECTED_CATEGORY, Toast.LENGTH_SHORT).show();
                final String[] array  = MAIN_SELECTED_CATEGORY.split(",");

                moduleListOfIds = "";
                final Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        for(int i=0;i<array.length;i++){
                            generateModuleListString(array[i]);
                        }
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    showLoading(false);
                                    Log.d("uysgdijugqwd4wq3", "run1234: "+moduleListOfIds);
                                    Intent in = new Intent(mContext,CreateCMPart2SubLists.class);
                                    in.putExtra("MODULE_LIST", ZonStringFunctions.stripCommasFromString(moduleListOfIds));
                                    in.putExtra("SELECTED_NUM_OF_QUESTIONS",SELECTED_NUM_OF_QUESTIONS);
                                    startActivity(in);
                                    finish();
                                }
                            });
                    }
                };

                new Thread(runnable).start();
            }
        });

    }

    public boolean isTablet(){
        boolean isTablet;
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float yInches= metrics.heightPixels/metrics.ydpi;
        float xInches= metrics.widthPixels/metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches*xInches + yInches*yInches);
        if (diagonalInches>=8){
            // 6.5inch device or bigger
            isTablet=true;
        }else{
            // smaller device
            isTablet=false;
        }

        return isTablet;
    }

    public void getCategories(final String CATEGORY_ID){
        //videosLayout.setVisibility(View.GONE);
        showLoading(true);
        categoryList = new ArrayList<>();
        categoryList.clear();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("parent_id",CATEGORY_ID);
        post_data.put("section","mcq");
        post_data.put("login_id",userSession.getUSER_SESSION_ID());

        String URL = ConfigServer.API_GET_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CATEGORY = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        if(isMainCategory){
                            showLoading(false);
                        }

                      //  showLoading(false);
                        noMcqLayout.setVisibility(View.GONE);
                        Log.d("API_RESPONSEd3", ""+API_RESPONSE_GET_CATEGORY);
                        Log.d("API_RESPONSE", "CAT_ID="+CATEGORY_ID);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
                            String status = jsonObject1.getString("status");
                            String video_exist = jsonObject1.getString("video_exist");

                            if(status.equals("404")){

                                noDataAlert(true);
                                isModuleAvailable=false;
                            }else {
                                noDataAlert(false);
                            }

                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                            isModuleAvailable=false;
                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject catObj = jsonArray.getJSONObject(i);
                                String category_id = catObj.getString("category_id");
                                String category_name = catObj.getString("category_name");
                                String description = catObj.getString("description");
                                String parent_id = catObj.getString("parent_id");
                                String category_section = catObj.getString("category_section");
                                String category_status = catObj.getString("category_status");
                                String category_image = catObj.getString("category_image");
                                String created_at = catObj.getString("created_at");
                                String is_module = catObj.getString("is_module");

                                category_image = ConfigServer.PATH_CATEGORY_THUMB+category_image;

                                categoryList.add(new Category(i,category_id,category_name,description,parent_id,category_section,category_status,category_image,created_at,1));


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                       // showParentAtTop(CATEGORY_ID);

                        if(isModuleAvailable){
//                            Intent intent = new Intent(mContext, CreateCMPart2SubLists.class);
//                            intent.putExtra("MODULE_LIST",moduleListOfIds);
//                            intent.putExtra("SELECTED_NUM_OF_QUESTIONS",SELECTED_NUM_OF_QUESTIONS);
//                            //showLoading(false);
//                            startActivity(intent);
//                           // showLoading(false);
                        }else {
                            showLoading(false);
                            adapter = new CmMainCategoryAdapter(mContext, categoryList, new CmMainCategoryAdapter.EventListener() {
                                @Override
                                public void onCategoryClicked(Category category,String CATEGORY_LIST) {
                                    //Toast.makeText(mContext, "LIST = "+CATEGORY_LIST, Toast.LENGTH_SHORT).show();
                                    MAIN_SELECTED_CATEGORY = CATEGORY_LIST;
                                }

                                @Override
                                public void onCheckBoxSelected(Category category,int pos,String CATEGORY_LIST) {
                                    //Toast.makeText(mContext, "LIST = "+CATEGORY_LIST, Toast.LENGTH_SHORT).show();
                                    MAIN_SELECTED_CATEGORY = CATEGORY_LIST;
                                }
                            });
                            recyclerview.setAdapter(adapter);

                            adapter.notifyDataSetChanged();
                         //   frameLayout.setVisibility(View.VISIBLE);
                        }

                    }
                });
            }
        };

        new Thread(runnable).start();
    }


    public void generateModuleListString(final String CATEGORY_ID){

        Log.d("sdjgwqkjdg", "generateModuleListString: "+CATEGORY_ID);

        //videosLayout.setVisibility(View.GONE);
       // showLoading(true);

        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("parent_id",CATEGORY_ID);
        post_data.put("section","mcq");
        post_data.put("login_id",userSession.getUSER_SESSION_ID());

        String URL = ConfigServer.API_GET_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);

        API_RESPONSE_GET_CATEGORY = olf.getPOstResult();

        Log.d("API_RESPONSEd4", ""+API_RESPONSE_GET_CATEGORY);
        Log.d("API_RESPONSE", "CAT_ID="+CATEGORY_ID);

        try {
            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
            String status = jsonObject1.getString("status");
            String video_exist = jsonObject1.getString("video_exist");


            JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
            isModuleAvailable=false;
            for(int i=0;i<jsonArray.length();i++){
                JSONObject catObj = jsonArray.getJSONObject(i);
                String category_id = catObj.getString("category_id");
                String category_name = catObj.getString("category_name");
                String description = catObj.getString("description");
                String parent_id = catObj.getString("parent_id");
                String category_section = catObj.getString("category_section");
                String category_status = catObj.getString("category_status");
                String category_image = catObj.getString("category_image");
                String created_at = catObj.getString("created_at");
                String is_module = catObj.getString("is_module");

                Log.d("uysgdijugqwd", "is_module: "+is_module);

//                                if(moduleListOfIds.equals("")){
//                                    moduleListOfIds=category_id;
//                                }else {
//                                    moduleListOfIds=moduleListOfIds+","+category_id;
//                                }

                if(is_module.equals("1")){
                    if(moduleListOfIds.equals("")){
                        moduleListOfIds=category_id;
                    }else {
                        if(!moduleListOfIds.contains(category_id)){
                        moduleListOfIds=moduleListOfIds+","+category_id;}
                    }
                    isModuleAvailable=true;
                }else {
                    isModuleAvailable=false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void showLoading(boolean stat){
        if(stat){
            mainView.setVisibility(View.GONE);
            loadingLayout.setVisibility(View.VISIBLE);
        }else {
            mainView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        }
    }
    public void noDataAlert(boolean stat){
        if(stat){
            noMcqLayout.setVisibility(View.VISIBLE);
        }else {
            noMcqLayout.setVisibility(View.GONE);
        }
    }



}
