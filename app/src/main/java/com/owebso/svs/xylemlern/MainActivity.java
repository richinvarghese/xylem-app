package com.owebso.svs.xylemlern;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.PersistableBundle;
import android.net.Uri;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.XyFragments.DaysListViewFragment;
import com.owebso.svs.xylemlern.XyFragments.ExamDaysListViewFragment;
import com.owebso.svs.xylemlern.XyFragments.ExamFragment2;
import com.owebso.svs.xylemlern.XyFragments.MainFragment;
import com.owebso.svs.xylemlern.XyFragments.McqListViewFragment;
import com.owebso.svs.xylemlern.XyFragments.StudyMaterialFragment;
import com.owebso.svs.xylemlern.XyFragments.VideosListViewFragment;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.login.LoginActivity;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.ProfileViewActivity;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity implements Exam_Day_Click{
    int CURRENT_FRAGMENT_ID = 1;
    FragmentManager fragmentManager;
    BottomNavigationView bottomNavigationView;
    int FRAGMENT_ID;
    Context mContext = MainActivity.this;
    int CURRENT_SELECTED_ITEM = 0;
    VideosListViewFragment videoFragment;
    DaysListViewFragment daysListViewFragment;
    ExamDaysListViewFragment examdaysListViewFragment;
    McqListViewFragment mcqFragment;
    ArrayList<String> pageList = new ArrayList<>();
    String VIDEO_FRAGMENT_TAG = "";
    String API_RESPONSE_LOGOUT = "";
    DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    ImageView toggleButton;
    NavigationView navigationView;
    DrawerLayout drawer;
    ImageView actionBarLogoViewImage;
    TextView textViewActionBarTitle,textView9;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    UserSession userSession;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // hideActionBar();
        FirebaseMessaging.getInstance().subscribeToTopic("xylem.message");


        initActionBar();
        changeStatusBarColor();
        initViews();
        initNavDrawer();
        try {
            FRAGMENT_ID = getIntent().getIntExtra("FRAGMENT_ID", 1);
            Log.d("FRAGMENT_ID", "" + FRAGMENT_ID);
            if (FRAGMENT_ID != 1) {
                showFragment(FRAGMENT_ID);
                Log.d("FRAGMENT_ID", "IFFFF" + FRAGMENT_ID);
            } else {
                showFragment(FRAGMENT_ID);
                Log.d("FRAGMENT_ID", "ELSEEEE" + FRAGMENT_ID);
            }
        } catch (Exception e) {
            Log.d("FRAGMENT_ID", "EXCEPTIOM : " + CURRENT_FRAGMENT_ID);
            e.printStackTrace();
        }
        getmenuId(FRAGMENT_ID); //CURRENT_SELECTED_ITEM = getmenuId(FRAGMENT_ID)
        initBottomNavigation();
        daysListViewFragment = (DaysListViewFragment) getSupportFragmentManager().findFragmentById(R.id.fullLayout);


    }

    public void test() {

    }

    public void initViews() {
        pref = getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        actionBarLogoViewImage = (ImageView) findViewById(R.id.imageView3);
        textViewActionBarTitle = (TextView) findViewById(R.id.textViewActionBarTitle);
        ProfileUtils profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();
        //Log.d("fbhdbfh",userSession.getUSER_USER_NAME()+"sbcbnhsbhncbs");
        initNavMenu();
    }

    public void initNavMenu() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
                switch (menuItem.getItemId()) {
                }
                return true;
            }
        });
    }


    public void changeStatusBarColor() {
        Window window = MainActivity.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.accentColor1));
    }

    public void initActionBar() {
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_01, null);
        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.accentColor1);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));


        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar_01, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.CENTER
                ));

        //    Toolbar parent =(Toolbar) actionBarLayout.getParent();
//        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);

        toggleButton = (ImageView) findViewById(R.id.toggleButton);
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerVisible(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });

        //ll_cat_house = (LinearLayout)findViewById(R.id.ll_cat_house);

    }

    public void hideActionBar() {
        try {
            getSupportActionBar().hide();
        } catch (Exception e) {
            try {
                getActionBar().hide();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }
    }


    public int getmenuId(int fragId) {
        switch (fragId) {
            case 1:
                CURRENT_SELECTED_ITEM = R.id.action_home;
                break;
            case 2:
                CURRENT_SELECTED_ITEM = R.id.action_videos;
                break;
            case 3:
                CURRENT_SELECTED_ITEM = R.id.action_mcq_mcq;
                break;
            case 4:
                CURRENT_SELECTED_ITEM = R.id.action_exams;
                break;
            default:
                CURRENT_SELECTED_ITEM = R.id.action_home;
        }
        return CURRENT_SELECTED_ITEM;
    }

    public void initBottomNavigation() {
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(CURRENT_SELECTED_ITEM);
        //bottomNavigationView.getMenu().findItem(CURRENT_SELECTED_ITEM).setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_home:
                        showFragment(1);
                        break;
                    case R.id.action_videos:
                        showFragment(2);
//                        Intent in = new Intent(MainActivity.this, VideosListView.class);
//                        in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                        startActivity(in);
//                        finish();
                        break;
                    case R.id.action_mcq_mcq:
                        showFragment(5);
                        break;
                    case R.id.action_exams:
                        showFragment(4);
                        break;
                }
                return true;
            }
        });
    }

    public void showFragment(int id) {

        fragmentManager = getSupportFragmentManager();
        CURRENT_FRAGMENT_ID = R.id.frameLayout;

        switch (id) {
            case 1:
                CURRENT_FRAGMENT_ID = 1;
                showTitle("", false);
                // fabIconAdd.setVisibility(View.VISIBLE);
                MainFragment mainFragment = new MainFragment();
                Bundle args = new Bundle();
                args.putString("KEY", "VALUE");
                mainFragment.setArguments(args);
                fragmentManager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.frameLayout, mainFragment, mainFragment.getTag()).commit();
                break;
            case 2:
                CURRENT_FRAGMENT_ID = 2;
                showTitle("VIDEOS", true);
                // fabIconAdd.setVisibility(View.VISIBLE);
                DaysListViewFragment daysListViewFragment = new DaysListViewFragment();
                Bundle argsd2 = new Bundle();
                argsd2.putString("KEY", "VALUE");
                daysListViewFragment.setArguments(argsd2);
                VIDEO_FRAGMENT_TAG = daysListViewFragment.getTag();
                fragmentManager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.frameLayout, daysListViewFragment, daysListViewFragment.getTag()).commit();
                break;
            case 3:
                CURRENT_FRAGMENT_ID = 3;
                // fabIconAdd.setVisibility(View.VISIBLE);
                StudyMaterialFragment videoFragment2 = new StudyMaterialFragment();
                Bundle args22 = new Bundle();
                args22.putString("KEY", "VALUE");
                videoFragment2.setArguments(args22);
                fragmentManager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.frameLayout, videoFragment2, videoFragment2.getTag()).commit();
                break;
            case 4:
                //   Intent in = new Intent(mContext, ListViewOfExams.class);
                //   startActivity(in);
                CURRENT_FRAGMENT_ID = 4;
                // fabIconAdd.setVisibility(View.VISIBLE);
                showTitle("EXAMS", true);
                //ExamFragment2 videoFragment = new ExamFragment2();
                ExamDaysListViewFragment examdaysListViewFragment = new ExamDaysListViewFragment();
                Bundle args2 = new Bundle();
                args2.putString("KEY", "VALUE");
                examdaysListViewFragment.setArguments(args2);
                fragmentManager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.frameLayout, examdaysListViewFragment, examdaysListViewFragment.getTag()).commit();
                break;
            case 5:
                CURRENT_FRAGMENT_ID = 5;
                showTitle("MCQs", true);
                // fabIconAdd.setVisibility(View.VISIBLE);
                McqListViewFragment mcqListViewFragment = new McqListViewFragment();
                Bundle args33 = new Bundle();
                args33.putString("KEY", "VALUE");
                mcqListViewFragment.setArguments(args33);
                fragmentManager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.frameLayout, mcqListViewFragment, mcqListViewFragment.getTag()).commit();
                break;

        }


    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    public void changeActionBarTitle(String title) {
        TextView textViewActionBarTitle = (TextView) findViewById(R.id.textViewActionBarTitle);
        textViewActionBarTitle.setText(title);
    }

    @Override
    public void onBackPressed() {
        if (CURRENT_FRAGMENT_ID == 2) {
            showFragment(1);
            //  bottomNavigationView.setSelectedItemId(R.id.action_home);
            View view = bottomNavigationView.findViewById(R.id.action_home);
            view.performClick();
        } else if (CURRENT_FRAGMENT_ID == 5) {
            View view = bottomNavigationView.findViewById(R.id.action_home);
            view.performClick();
        } else if (CURRENT_FRAGMENT_ID == 4) {
            View view = bottomNavigationView.findViewById(R.id.action_home);
            view.performClick();
        } else if (CURRENT_FRAGMENT_ID == 1) {
            super.onBackPressed();

        }

    }

    public void setParentArray(int parent) {
        daysListViewFragment = (DaysListViewFragment) getSupportFragmentManager().findFragmentById(R.id.fullLayout);
        videoFragment.setParentArray(parent);
    }

    public void initNavDrawer() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        View navHeaderView = navigationView.inflateHeaderView(R.layout.layout_header_01);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        mDrawerToggle.setDrawerIndicatorEnabled(false);

        CircleImageView profile = (CircleImageView) navHeaderView.findViewById(R.id.profile_image);
        textView9 = (TextView)navHeaderView.findViewById(R.id.textView9);
        textView9.setText(userSession.getUSER_USER_NAME());

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
                Intent in = new Intent(mContext, ProfileViewActivity.class);
                startActivity(in);
                //  showFragment(3);
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.my_profile:
                        mDrawerLayout.closeDrawers();
                        Intent in = new Intent(mContext, ProfileViewActivity.class);
                        startActivity(in);
                        break;
                    case R.id.action_share:
                        shareApp();
                        mDrawerLayout.closeDrawers();
                        break;
                    case R.id.action_videos:
                        mDrawerLayout.closeDrawers();
                        View view = bottomNavigationView.findViewById(R.id.action_videos);
                        view.performClick();
                        break;
                    case R.id.support:
                        mDrawerLayout.closeDrawers();
                        if(isPermissionGranted()){
                            call_action();
                        }
                        break;
                    case R.id.privacy:
                        mDrawerLayout.closeDrawers();
                        Intent intent=new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("http://www.xylemlearning.com/terms.html"));
                        startActivity(intent);
                        break;
                    case R.id.action_logout:
                        mDrawerLayout.closeDrawers();
                        Logout_user();
                        break;
                }

                return false;

            }
        });


    }

    @Override
    protected void onResume() {
        hideMcqLoading();
       /* if (CURRENT_FRAGMENT_ID == 4) {
            View view = bottomNavigationView.findViewById(R.id.action_home);
            view.performClick();
        }*/
        super.onResume();
    }
    public void hideMcqLoading() {
        try {
            Fragment mcqfrag = (McqListViewFragment) getSupportFragmentManager().findFragmentById(R.id.frameLayout);
            ((McqListViewFragment) mcqfrag).showLoading(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void showTitle(String title, boolean stat) {
        if (stat) {
            actionBarLogoViewImage.setVisibility(View.INVISIBLE);
            textViewActionBarTitle.setText(title);
            textViewActionBarTitle.setVisibility(View.VISIBLE);
            // toggleButton.setVisibility(View.GONE);
        } else {
            textViewActionBarTitle.setVisibility(View.GONE);
            actionBarLogoViewImage.setVisibility(View.VISIBLE);
            // toggleButton.setVisibility(View.VISIBLE);
        }
    }
    public void shareApp() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Xylem - The Learning App");
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) { //e.toString(); }
        }
    }

    public void Logout_user(){
        showProgress(true,"Authenticating User..");
        //Toast.makeText(mContext, "CALLING REGISTER FUNCTION", Toast.LENGTH_SHORT).show();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("login_id", userSession.getUSER_SESSION_ID());
        final OnlineFunctions olf = new OnlineFunctions(mContext,ConfigServer.API_LOG_OUT,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_LOGOUT = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("KJWHGDKWY", "RESPONSE: "+API_RESPONSE_LOGOUT);
                        try {
                            showProgress(false,"");
                            JSONObject jsonObject = new JSONObject(API_RESPONSE_LOGOUT);
                            String status = jsonObject.getString("status");
                            if(status.equals("200")){
                                setUserSession();
                                Intent ind = new Intent(mContext, LoginActivity.class);
                                ind.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                // ind.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(ind);
                                JSONObject response = jsonObject.getJSONObject("response");
                                String login_id = response.getString("login_id");
                               //     Intent in = new Intent(mContext,MainActivity.class);
//                                    startActivity(in);
//                                    finish();
                            }else {
                                Toast.makeText(mContext, "Login Error..", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };

        new Thread(runnable).start();

    }







    public void setUserSession(){
        editor.putString("USER_SESSION_ID","0");
        editor.putString("USER_SESSION_TYPE","0");
        editor.putString("USER_SESSION_ROLE","0");
        editor.putString("USER_USER_NAME","Guest");
        editor.putBoolean("USER_SESSION_LOGGED_IN",false);
        editor.putString("USER_PAYMENT_STS","0");
        editor.commit();
    }
    private void showProgress(boolean status, String msg) {

        if(progressDialog==null){
            progressDialog= new ProgressDialog(mContext);
        }

        progressDialog.setMessage(msg);

        if(status){
            if(!progressDialog.isShowing()){
                progressDialog.show();
            }
        }else {
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }
    }
    public  boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG","Permission is granted");
                return true;
            } else {

                Log.v("TAG","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG","Permission is granted");
            return true;
        }
    }

    public void call_action(){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:9072220123"));
        startActivity(callIntent);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    call_action();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void Clicked(String cat_id) {
       /* CURRENT_FRAGMENT_ID = 6;
        // fabIconAdd.setVisibility(View.VISIBLE);
        showTitle("EXAMS", true);
        ExamFragment2 videoFragment = new ExamFragment2();
        //ExamDaysListViewFragment examdaysListViewFragment = new ExamDaysListViewFragment();
        Bundle args2 = new Bundle();
        //args2.putString("KEY", "VALUE");
        args2.putString("cat_id", cat_id);
        videoFragment.setArguments(args2);
        fragmentManager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.frameLayout, videoFragment, videoFragment.getTag()).commit();
        Log.d("fjdafkjnanfkjna",cat_id);*/

    }
}
