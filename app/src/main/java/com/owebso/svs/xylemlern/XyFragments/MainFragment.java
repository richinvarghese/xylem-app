package com.owebso.svs.xylemlern.XyFragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.VideoPlayer.XPlayer;
import com.owebso.svs.xylemlern.XylemActivities.VideosLists;
import com.owebso.svs.xylemlern.adapters.HomeSliderAdapter;
import com.owebso.svs.xylemlern.adapters.Package_ListAdapter;
import com.owebso.svs.xylemlern.adapters.RelatedVideosListAdapter;
import com.owebso.svs.xylemlern.adapters.VideosListAdapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.functionUtils.WebServiceApi;
import com.owebso.svs.xylemlern.models.Package_Model;
import com.owebso.svs.xylemlern.models.Slider_Model;
import com.owebso.svs.xylemlern.models.VideoX;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserProfile;
import com.owebso.svs.xylemlern.profile.UserSession;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MainFragment extends Fragment implements PaymentResultListener {
    @BindView(R.id.rcViewRecentlyWatched)
    RecyclerView rcViewRecentlyWatched;
    WebServiceApi webServiceApi;
    UserSession userSession;
    ArrayList<VideoX> videoXArrayList;
    VideosListAdapter videosListAdapter;
    String API_RESPONSE_GET_RECENT_VIDEOS;
    String API_RESPONSE_IMAGE_SLIDERS;
    String RESPONSE_STATUS_VIDEO = "";
    String RESPONSE_STATUS_SLIDER = "";
    Dialog dialog;
    RecyclerView recyclerpackage;
    TextView actionBarTitle,tv_cancel;
    ArrayList<Package_Model> packageList;
    ArrayList<Slider_Model> slider_List;
    String API_RESPONSE_GET_CAT_SINGLE;
    Package_ListAdapter package_listAdapter;
    String str_pack_id;
    SliderView sliderView;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    public void initViews() {
        ProfileUtils profileUtils = new ProfileUtils(getContext());
        userSession = profileUtils.getUserSession();
        videoXArrayList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rcViewRecentlyWatched.setLayoutManager(layoutManager);
        setSlider();
        getRecently_Watched_Videos();
        // getRecentlyWatchedVideos();
    }


    public void setSlider() {
        sliderView = getView().findViewById(R.id.imageSlider);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        // sliderView.setSliderTransformAnimation(SliderAnimations.CUBEOUTROTATIONTRANSFORMATION);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.setCircularHandlerEnabled(true);
        sliderView.startAutoCycle();
        getImage_Slider();
    }

    public void getImage_Slider(){
        //   showLoading1(true);
        slider_List = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        post_data.put("hash_code",ConfigServer.API_HASH);
        Log.d("RESPONSEfdsfsddcssdsdss", "current_id: "+userSession.getUSER_SESSION_ID()+"  "+ConfigServer.API_HASH);
        String URL = ConfigServer.API_IMAGE_SLIDERS;
        final OnlineFunctions olf = new OnlineFunctions(getActivity(),URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_IMAGE_SLIDERS = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.d("RESPONSEfdsfsddcssdsdss", "run: "+API_RESPONSE_IMAGE_SLIDERS);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_IMAGE_SLIDERS);
                            RESPONSE_STATUS_SLIDER="";
                            RESPONSE_STATUS_SLIDER  = jsonObject1.getString("status");
                            Log.d("RESPONSE_STATUS_SLIDER", "V-status : "+RESPONSE_STATUS_SLIDER);
                            if(RESPONSE_STATUS_SLIDER.equals("200")){
                                JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                                for(int i =0;i<jsonArray.length();i++){
                                    JSONObject videoObj = jsonArray.getJSONObject(i);
                                    String slider_id = videoObj.getString("sid");
                                    String video_title = videoObj.getString("title");
                                    String category_id = videoObj.getString("category_id");
                                    String video_id = videoObj.getString("video_id");
                                    String description = videoObj.getString("description");
                                    String slidr_image = videoObj.getString("image");
                                    String video_path = videoObj.getString("video_path");
                                    video_path = ConfigServer.PATH_VIDEO+video_path;
                                    Log.d("djbfjsdgjvjdsf",video_path);
                                    slider_List.add(new Slider_Model(slider_id,video_title,category_id,video_id,description,
                                            slidr_image,video_path));
                                }
                            }else {
                                Log.d("API_RESPONSE", "NO VIDEOS");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("API_RESPONSE", "exception");
                        }

                        HomeSliderAdapter adapter = new HomeSliderAdapter(getContext(),slider_List);

                        sliderView.setSliderAdapter(adapter);

                    }
                });

            }
        };

        new Thread(runnable).start();
    }


    public void getRecently_Watched_Videos(){
     //   showLoading1(true);
        videoXArrayList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        Log.d("API_RESPONSE", "current_id: "+userSession.getUSER_SESSION_ID()+"  "+ConfigServer.API_HASH);
        String URL = ConfigServer.API_GET_RECENTLY_WATCHED_VIDEOS;
        final OnlineFunctions olf = new OnlineFunctions(getActivity(),URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_RECENT_VIDEOS = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.d("RESPONSEfdsfsddcssdsdss", "run: "+API_RESPONSE_GET_RECENT_VIDEOS);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_RECENT_VIDEOS);
                            RESPONSE_STATUS_VIDEO="";
                            RESPONSE_STATUS_VIDEO  = jsonObject1.getString("status");
                            Log.d("API_RESPONSE1", "V-status : "+RESPONSE_STATUS_VIDEO);
                            if(RESPONSE_STATUS_VIDEO.equals("200")){
                                JSONArray jsonArray = new JSONArray(jsonObject1.getString("response"));
                                for(int i =0;i<jsonArray.length();i++){
                                    JSONObject videoObj = jsonArray.getJSONObject(i);
                                    String video_id = videoObj.getString("video_id");
                                    String video_title = videoObj.getString("video_title");
                                    String category_id = videoObj.getString("category_id");
                                    String description = videoObj.getString("description");
                                    String thumb_image = videoObj.getString("thumb_image");
                                    String video_path = videoObj.getString("video_path");
                                    String str_paid = videoObj.getString("paid");
                                    String str_unpaid = videoObj.getString("unpaid");
                                    String str_favourite = "0";
                                    String video_status = videoObj.getString("video_status");
                                    String str_menu_order = videoObj.getString("menu_order");
                                    String created_at = videoObj.getString("created_at");
                                    String str_watch_status = videoObj.getString("watch_status");
                                    String str_time = videoObj.getString("time");
                                    String IMAGE_URL_THUMB = ConfigServer.PATH_VIDEO_THUMB+thumb_image;
                                    video_path = ConfigServer.PATH_VIDEO+video_path;
                                    Log.d("djbfjsdgjvjdsf",video_path);
                                    videoXArrayList.add(new VideoX(video_id,video_title,category_id,description,IMAGE_URL_THUMB,video_path,str_paid,str_unpaid,str_favourite,video_status,
                                            str_menu_order,created_at,str_watch_status,str_time,1));
                                }
                            }else {
                                Log.d("API_RESPONSE", "NO VIDEOS");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("API_RESPONSE", "exception");
                        }
                        videosListAdapter = new VideosListAdapter(getContext(), videoXArrayList, new VideosListAdapter.EventListener() {
                            @Override
                            public void onVideoClicked(VideoX video, String pay_sts) {
                                if (video.getPaid().equalsIgnoreCase("0")){
                                    Intent in = new Intent(getContext(), XPlayer.class);
                                    in.putExtra("PLAY_URL",video.getVideo_path());
                                    in.putExtra("VIDEO_ID",video.getVideo_id());
                                    in.putExtra("PARENT_ID",video.getCategory_id());
                                    in.putExtra("FAV_STS",video.getFavorite());
                                    getActivity().startActivity(in);
                                }else if (video.getPaid().equalsIgnoreCase("1")&& pay_sts.equalsIgnoreCase("1")){
                                    Intent in = new Intent(getContext(), XPlayer.class);
                                    in.putExtra("PLAY_URL",video.getVideo_path());
                                    in.putExtra("VIDEO_ID",video.getVideo_id());
                                    in.putExtra("PARENT_ID",video.getCategory_id());
                                    in.putExtra("FAV_STS",video.getFavorite());
                                    getContext().startActivity(in);
                                }else {
                                    Dialogue_package_list();
                                    Get_Package_List();
                                }
                            }

                        });
                        rcViewRecentlyWatched.setAdapter(videosListAdapter);
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

    public void Get_Package_List(){
        packageList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_GET_PACKAGE_LIST;
        final OnlineFunctions olf = new OnlineFunctions(getActivity(),URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CAT_SINGLE);
                            RESPONSE_STATUS_VIDEO="";
                            RESPONSE_STATUS_VIDEO  = jsonObject1.getString("status");
                            Log.d("API_RESPONSE_PACKAGE", "V-status : "+API_RESPONSE_GET_CAT_SINGLE);
                            if(RESPONSE_STATUS_VIDEO.equals("200")){
                                JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                                for(int i =0;i<jsonArray.length();i++){
                                    JSONObject packg_obj = jsonArray.getJSONObject(i);
                                    String pack_id = packg_obj.getString("pack_id");
                                    String pack_name = packg_obj.getString("name");
                                    String pack_days = packg_obj.getString("days");
                                    String pack_description = packg_obj.getString("description");
                                    String pack_amount = packg_obj.getString("amount");
                                    String pack_status = packg_obj.getString("status");
                                    packageList.add(new Package_Model(pack_id,pack_name,pack_days,pack_description,pack_amount,pack_status));
                                }
                            }else {
                                Log.d("API_RESPONSE", "NO VIDEOS");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("API_RESPONSE", "exception");
                        }
                        package_listAdapter = new Package_ListAdapter(getActivity(), packageList, new Package_ListAdapter.EventListener() {
                            @Override
                            public void onPackageClicked(String pack_Id, String amout) {
                                dialog.dismiss();
                                startPayment(pack_Id,amout);
                            }
                        } );
                        recyclerpackage.setAdapter(package_listAdapter);
                        //showLoading1(false);
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

    private void Dialogue_package_list() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_package_details);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        recyclerpackage = (RecyclerView) dialog.findViewById(R.id.recyclerpackage);
        tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        LinearLayoutManager vertLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerpackage.setLayoutManager(vertLayoutManager);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void startPayment(String pack_id,String amout) {
        /*
         You need to pass current activity in order to let Razorpay create CheckoutActivity
        */
        str_pack_id = pack_id;
        Activity activity = getActivity();
        double am = Double.parseDouble(amout)*100;
        final Checkout co = new Checkout();
        try {
            JSONObject options = new JSONObject();
            options.put("name", "package: "+str_pack_id+ Calendar.getInstance().getTime());
            options.put("description", "");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", am+"");
            JSONObject preFill = new JSONObject();
            preFill.put("email", "");
            preFill.put("contact", "");
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }
    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            send_payment_response(str_pack_id,razorpayPaymentID,"1");
            Toast.makeText(getActivity(), "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            Log.e("dfsdgfdsg", "Payment Successful: " + razorpayPaymentID);
        } catch (Exception e) {
            //Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }
    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(getActivity(), "Payment failed: ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            // Log.e(TAG, "Exception in onPaymentError", e);
        }
    }
    public void send_payment_response(String st_pack_id,String pay_id,String status){
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("pack_id",st_pack_id);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        post_data.put("status",status);
        post_data.put("payment_id",pay_id);
        Log.d("CATEGORY_IDhulk", "ID: "+st_pack_id);
        String URL = ConfigServer.API_REGOR_PAY_RESPONSE;
        final OnlineFunctions olf = new OnlineFunctions(getActivity(),URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("APIREGORPAYRESPONSE567", ""+API_RESPONSE_GET_CAT_SINGLE);
                       /* try{
                            JSONObject jsonObject =  new JSONObject(API_RESPONSE_GET_CAT_SINGLE);
                            String status = jsonObject.getString("status");
                            if(status.equals("200")){
                             //onBackPressed();
                                JSONArray jsonArray = jsonObject.getJSONArray("result");
                                JSONObject category = jsonArray.getJSONObject(0);
                                String category_name = category.getString("category_name");
                                // MODULE_TITLE = category_name;
                                actionBarTitle.setText(category_name);
                            }

                        }catch (JSONException e){

                        }*/
                    }
                });
            }
        };
        new Thread(runnable).start();
    }
}