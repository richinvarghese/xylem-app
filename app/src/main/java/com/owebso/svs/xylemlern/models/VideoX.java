package com.owebso.svs.xylemlern.models;

public class VideoX {
    private String video_id;
    private String video_title;
    private String category_id;
    private String description;
    private String thumb_image;
    private String video_path;
    private String paid;
    private String unpaid;
    private String favorite;
    private String video_status;
    private String menu_order;
    private String created_at;
    private String watch_status;
    private String time;
    private int TYPE;


    public VideoX(String video_id, String video_title, String category_id, String description, String thumb_image, String video_path,
                  String paid, String unpaid,String favorite, String video_status,String menu_order,  String created_at,String watch_status,String time, int TYPE) {
        this.video_id = video_id;
        this.video_title = video_title;
        this.category_id = category_id;
        this.description = description;
        this.thumb_image = thumb_image;
        this.video_path = video_path;
        this.paid = paid;
        this.unpaid = unpaid;
        this.favorite = favorite;
        this.video_status = video_status;
        this.menu_order = menu_order;
        this.created_at = created_at;
        this.watch_status = watch_status;
        this.time = time;
        this.TYPE = TYPE;
    }

    public int getTYPE() {
        return TYPE;
    }

    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public String getVideo_path() {
        return video_path;
    }

    public void setVideo_path(String video_path) {
        this.video_path = video_path;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_title() {
        return video_title;
    }

    public void setVideo_title(String video_title) {
        this.video_title = video_title;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getVideo_status() {
        return video_status;
    }

    public void setVideo_status(String video_status) {
        this.video_status = video_status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getUnpaid() {
        return unpaid;
    }

    public void setUnpaid(String unpaid) {
        this.unpaid = unpaid;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getMenu_order() {
        return menu_order;
    }

    public void setMenu_order(String menu_order) {
        this.menu_order = menu_order;
    }

    public String getWatch_status() {
        return watch_status;
    }

    public void setWatch_status(String watch_status) {
        this.watch_status = watch_status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
