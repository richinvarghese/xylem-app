package com.owebso.svs.xylemlern.ActivitMcq;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.adapters.McqCategoryAdapter;
import com.owebso.svs.xylemlern.adapters.VideosListAdapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Category;
import com.owebso.svs.xylemlern.models.VideoX;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class LastSubCategoryMcqLists extends AppCompatActivity {
    private Context mContext = LastSubCategoryMcqLists.this;
    String API_RESPONSE_GET_CATEGORY="",API_RESPONSE_GET_VIDEOS;
    ArrayList<Category> categoryList;
    ArrayList<Category> moduleList;
    McqCategoryAdapter adapter;
    RecyclerView recyclerview;
    String CATEGORY_ID="";
    LinearLayout loadingLayout,mainView;
    //ArrayList<String> catParent;
    ArrayList<VideoX> videoXArrayList;
    RecyclerView recyclerView123;
    VideosListAdapter videoAdapter;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager videoLayoutmanager;
    GridLayoutManager moduleSelectorLayoutManager;
    BottomNavigationView bottomNavigationView;
    LinearLayout noVidLayoutAlert;
    ImageView backActionB;
    RecyclerView recyclerViewModuleSelector;
    boolean isVideoExist=false;
    boolean isMainCategory=false;
    // String CURRENT_PARENT_ID="";
    LinearLayout moduleLayout,videosLayout,frameLayout;
    public String CURRENT_HOME_PARENT_ID="";
    String PARENT_PARENT="";

    ArrayList<String> pageList = new ArrayList<>();
    String MODULE_PARENT="";

    ArrayList<String> moduleIdList = new ArrayList<>();
    boolean isModuleAvailable=false;
    String moduleListOfIds="";
    
    TextView actionBarTitle;
    RelativeLayout layoutContent;

    String RESPONSE_STATUS_CATEGORY="";
    String RESPONSE_STATUS_VIDEO="";

    UserSession userSession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_sub_category_lists_mcq);
        // hideActionBar();
        initActionBar();
        changeStatusBarColor();
        initViews();

        CATEGORY_ID = getIntent().getStringExtra("CATEGORY_ID");

        Log.d("DJFHEKH", " ID "+CATEGORY_ID);

        getCategories(CATEGORY_ID);
    }
    

    public void initViews(){

        ProfileUtils profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();

        layoutContent = (RelativeLayout)findViewById(R.id.layoutContent);

        noVidLayoutAlert = (LinearLayout)findViewById(R.id.noVidLayoutAlert);
        noVidLayoutAlert.setVisibility(View.GONE);

        if(!isTablet()){
            gridLayoutManager = new GridLayoutManager(mContext,1);
            videoLayoutmanager = new GridLayoutManager(mContext,1);
            //  moduleSelectorLayoutManager = new GridLayoutManager(mContext,1);
        }else{
            gridLayoutManager = new GridLayoutManager(mContext,2);
            videoLayoutmanager = new GridLayoutManager(mContext,2);
            // moduleSelectorLayoutManager = new GridLayoutManager(mContext,1);
        }

        //    LinearLayout moduleLayout,videosLayout,frameLayout;

        moduleLayout = (LinearLayout)findViewById(R.id.moduleLayout);
        moduleLayout.setVisibility(View.GONE);
        frameLayout = (LinearLayout)findViewById(R.id.frameLayout);


        recyclerview = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView123 = (RecyclerView)findViewById(R.id.recyclerView123);
        recyclerViewModuleSelector = (RecyclerView)findViewById(R.id.recyclerViewModuleSelector);
        //recyclerViewModuleSelector.setLayoutManager(moduleSelectorLayoutManager);
        recyclerViewModuleSelector.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerView123.setLayoutManager(videoLayoutmanager);

        loadingLayout = (LinearLayout)findViewById(R.id.loadingLayout);
        videosLayout = (LinearLayout)findViewById(R.id.videosLayout);
        loadingLayout.setVisibility(View.GONE);
        videosLayout.setVisibility(View.GONE);

        mainView = (LinearLayout)findViewById(R.id.mainView);

        //getModule(CATEGORY_ID);

        isMainCategory =true;
       // showNoDataAlert(3,false);

     //   getCategories(CATEGORY_ID);

    }

    public void changeStatusBarColor(){
        Window window = LastSubCategoryMcqLists.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(LastSubCategoryMcqLists.this,R.color.accentColor1));
    }

    public  void initActionBar(){
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.accentColor1);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent =(Toolbar) actionBarLayout.getParent();
       // parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
      //  parent.setContentInsetsAbsolute(0,0);

        actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("MCQ");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public boolean isTablet(){
        boolean isTablet;
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float yInches= metrics.heightPixels/metrics.ydpi;
        float xInches= metrics.widthPixels/metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches*xInches + yInches*yInches);
        if (diagonalInches>=8){
            // 6.5inch device or bigger
            isTablet=true;
        }else{
            // smaller device
            isTablet=false;
        }

        return isTablet;
    }
    public void getCategories(final String CATEGORY_ID){
        showLoading1(true);
        videosLayout.setVisibility(View.GONE);
        categoryList = new ArrayList<>();
        categoryList.clear();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("parent_id",CATEGORY_ID);
        post_data.put("section","mcq");
        post_data.put("login_id",userSession.getUSER_SESSION_ID());

        Log.d("CATEGORY_ID", "ID: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CATEGORY = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        noVidLayoutAlert.setVisibility(View.GONE);
                        Log.d("API_RESPONSE", ""+API_RESPONSE_GET_CATEGORY);
                        Log.d("API_RESPONSE", "CAT_ID="+CATEGORY_ID);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
                            RESPONSE_STATUS_CATEGORY="";
                            RESPONSE_STATUS_CATEGORY = jsonObject1.getString("status");
                            String video_exist = jsonObject1.getString("video_exist");

//                            if(video_exist.equals("0")){
//
//                                isVideoExist=false;
//                                Log.d("API_RESPONSE1", "NO_VIDEO");
//                                Log.d("API_RESPONSE1", "status : "+RESPONSE_STATUS_CATEGORY);
//
//                            }else if(video_exist.equals("1")){
//                                Log.d("API_RESPONSE1", "VIDEO, CATID = "+CATEGORY_ID);
//                                isVideoExist=true;
//                               // getVideos(CATEGORY_ID);
//                            }

                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                            moduleListOfIds="";
                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject catObj = jsonArray.getJSONObject(i);
                                String category_id = catObj.getString("category_id");
                                String category_name = catObj.getString("category_name");
                                String description = catObj.getString("description");
                                String parent_id = catObj.getString("parent_id");
                                String category_section = catObj.getString("category_section");
                                String category_status = catObj.getString("category_status");
                                String category_image = catObj.getString("category_image");
                                String created_at = catObj.getString("created_at");
                                String is_module = catObj.getString("is_module");

//                                if(is_module.equals("1")){
//                                    if(moduleListOfIds.equals("")){
//                                        moduleListOfIds=category_id;
//                                    }else {
//                                        moduleListOfIds=moduleListOfIds+","+category_id;
//                                    }
//                                    isModuleAvailable=true;
//                                }

                                category_image = ConfigServer.PATH_CATEGORY_THUMB+category_image;
                                categoryList.add(new Category(i,category_id,category_name,description,parent_id,category_section,category_status,category_image,created_at,1));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                        adapter = new McqCategoryAdapter(mContext, categoryList, new McqCategoryAdapter.EventListener() {
                            @Override
                            public void onCategoryClicked(String categoryId, String parentId,String title) {

                                gotoMcq(categoryId,title);
                            }
                        });
                        recyclerview.setAdapter(null);
                        recyclerview.setAdapter(adapter);

                        adapter.notifyDataSetChanged();
                        frameLayout.setVisibility(View.VISIBLE);

                        showLoading1(false);

                        if(!isVideoExist){
                            showLoading1(false);
                        }

                    }


                });
            }
        };

        new Thread(runnable).start();
    }

    private void gotoMcq(String categoryId,String title) {
//        Intent in = new Intent(mContext,McqExams.class);
//        in.putExtra("CATEGORY_ID",categoryId);
//        startActivity(in);
//
        Intent in = new Intent(mContext,McqExamIntro.class);
        in.putExtra("CATEGORY_ID",categoryId);
        in.putExtra("TITLE",title);
        startActivity(in);
    }

    @Override
    public void onResume() {
        super.onResume();
       // showLoading(false);
    }



    public void showLoading1(boolean stat){
        if(stat){
            layoutContent.setVisibility(View.GONE);
            loadingLayout.setVisibility(View.VISIBLE);
        }else {
            layoutContent.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);


        }

        Log.d("RESP--TATUS", "CAT: "+RESPONSE_STATUS_CATEGORY);
        Log.d("RESP--TATUS", "VID: "+RESPONSE_STATUS_VIDEO);
    }


    public ArrayList<String> getPageList(){
        return pageList;
    }

    public void setPageList(String ID){
        try{
            if(!pageList.get(pageList.size()-1).equals(ID)){
                pageList.add(ID);
            }
        }catch (ArrayIndexOutOfBoundsException e){
            pageList.add(ID);
        }

    }

    public void goBackTo(String ID){

        try{
            pageList.remove(pageList.size()-1);
            getCategories(ID);
        }catch (ArrayIndexOutOfBoundsException e){
            getCategories(CURRENT_HOME_PARENT_ID);
        }

    }

    public void showNoDataAlert(int type,boolean stat){

        Log.d("SDKJGSKJD", "type : "+type);

        if(stat){
            Log.d("SDKJGSKJD", "showNoDataAlert: HIDDEN");
            noVidLayoutAlert.setVisibility(View.VISIBLE);
            layoutContent.setVisibility(View.GONE);
        }else {
            Log.d("SDKJGSKJD", "showNoDataAlert: SHOWN");
            noVidLayoutAlert.setVisibility(View.GONE);
            layoutContent.setVisibility(View.VISIBLE);
        }
    }




}
