package com.owebso.svs.xylemlern.test;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.owebso.svs.xylemlern.R;
public class LayoutTester extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_tester);
    }
}
