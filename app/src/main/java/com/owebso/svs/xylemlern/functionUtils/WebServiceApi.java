package com.owebso.svs.xylemlern.functionUtils;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface WebServiceApi {

    @FormUrlEncoded
    @POST(".")
    Call<String> checkMcqAttended(
            @Field("hash_code") String hashcode,
            @Field("mcq_id") String mcq_id,
            @Field("student_id") String student_id
    );

    @FormUrlEncoded
    @POST(".")
    Call<String> getAttendMcqData(
            @Field("hash_code") String hashcode,
            @Field("mcq_id") String mcq_id,
            @Field("student_id") String student_id
    );

    @FormUrlEncoded
    @POST(".")
    Call<String> get_NewExam(
            @Field("hash_code") String hashcode,
            @Field("category_id") String exam_id
    );

    @FormUrlEncoded
    @POST(".")
    Call<String> checkExamAttended(
            @Field("hash_code") String hashcode,
            @Field("exam_id") String exam_id,
            @Field("student_id") String student_id
    );


    @FormUrlEncoded
    @POST(".")
    Call<String> setExamTime(
            @Field("hash_code") String hashcode,
            @Field("exam_id") String exam_id,
            @Field("student_id") String student_id,
            @Field("start_time") String start_date,
            @Field("end_time") String end_date,
            @Field("status") String status,
            @Field("last_index") String last_index,
            @Field("attended_data") String attended_data,
            @Field("scoreboard_data") String scoreboard_data
    );

    @FormUrlEncoded
    @POST(".")
    Call<String> updateExamOnClick(
            @Field("hash_code") String hashcode,
            @Field("exam_id") String exam_id,
            @Field("student_id") String student_id,
            @Field("status") String status,
            @Field("last_index") String last_index,
            @Field("attended_data") String attended_data,
            @Field("scoreboard_data") String scoreboard_data,
            @Field("marked_question_data") String marked_question_data
    );

    @FormUrlEncoded
    @POST(".")
    Call<String> getlastWatchedVideos(
            @Field("hash_code") String hashcode,
            @Field("login_id") String login_id
    );

    @FormUrlEncoded
    @POST(".")
    Call<String> getProfileDetauls(
            @Field("hash_code") String hashcode,
            @Field("login_id") String login_id
    );

    @FormUrlEncoded
    @POST(".")
    Call<String> getAttendedExamData(
            @Field("hash_code") String hashcode,
            @Field("student_id") String student_id,
            @Field("exam_id") String exam_id
    );

}
