package com.owebso.svs.xylemlern.models;

public class Package_Model {

    private String pack_id;
    private String name;
    private String days;
    private String description;
    private String amount;
    private String status;
    public Package_Model(String pack_id, String name, String days, String description, String amount, String status) {

        this.pack_id = pack_id;
        this.name = name;
        this.days = days;
        this.description = description;
        this.amount = amount;
        this.status = status;
    }




    public String getPack_id() {
        return pack_id;
    }

    public void setPack_id(String pack_id) {
        this.pack_id = pack_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
