package com.owebso.svs.xylemlern.customModule

import java.io.Serializable

data class CmData(
        var noOfQuestions:String?=null,
        var examMode:String?=null,
        var categoryLists:String?=null,
        var examDuration:String?=null,
        var examNegMark:String?=null,
        var examTotalMArk:String?=null

):Serializable