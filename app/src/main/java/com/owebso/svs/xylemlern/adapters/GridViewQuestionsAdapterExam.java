package com.owebso.svs.xylemlern.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.models.ExamQuestion;

import org.apache.commons.lang3.ObjectUtils;

import java.util.ArrayList;

/**
 * Created by Sarath on 07/06/19.
 */

public class GridViewQuestionsAdapterExam extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<ExamQuestion> indexList;
    EventListener listener;
    int SORT_TYPE=0;
    RelativeLayout cardOldSelection;
    RecyclerView.ViewHolder myHolder1;
    String[] ATTENDED_QUESTION_DATA;
    String[] MARKED_QUESTION_DATA;
   // int[] scoreBoardArray;

    public GridViewQuestionsAdapterExam(Context mContext, ArrayList<ExamQuestion> indexList,String[] ATTENDED_QUESTION_DATA,String[] MARKED_QUESTION_DATA
            , EventListener listener, int SORT_TYPE) {
        this.mContext = mContext;
        this.indexList = indexList;
        this.listener =listener;
        this.SORT_TYPE =SORT_TYPE;
        this.ATTENDED_QUESTION_DATA=ATTENDED_QUESTION_DATA;
        this.MARKED_QUESTION_DATA=MARKED_QUESTION_DATA;
    }

    public interface EventListener {
        void onCategoryClicked(ExamQuestion question, int index);
    }
    public GridViewQuestionsAdapterExam(EventListener listener){
        this.listener =listener;
    }

    public  class MainView extends RecyclerView.ViewHolder {
        TextView textViewIndex;
        RelativeLayout fullView;
        ImageView imageMarked;
 
        public MainView(View v) {
            super(v);
            this.textViewIndex = (TextView) v.findViewById(R.id.textViewIndex);
            this.fullView = (RelativeLayout)v.findViewById(R.id.fullView);
            this.imageMarked = (ImageView)v.findViewById(R.id.imageMarked);
            
        }
    }

//    public class TitleView extends  RecyclerView.ViewHolder {
//        TextView tvModuleTitle;
//        public TitleView(View v) {
//            super(v);
//            
//        }
//    }

    public  class AdsView extends  RecyclerView.ViewHolder {
        public AdsView(View v) {
            super(v);
        }
    }


    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
        if(viewType==0)
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_view_grid_view_01, parent, false);
            return new MainView(itemView);
        }
      
        else
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_layout_video_category, parent, false);
            return new AdsView(itemView);
        }



    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ExamQuestion question = indexList.get(position);
      if(holder.getItemViewType()==0)
        {
            changeStyle(((MainView)holder).fullView,Integer.parseInt(ATTENDED_QUESTION_DATA[position]));

            try{
                if(MARKED_QUESTION_DATA[position].equals("1")){
                    ((MainView)holder).imageMarked.setVisibility(View.VISIBLE);
                }else {
                    ((MainView)holder).imageMarked.setVisibility(View.GONE);
                }
            }catch (NullPointerException e){
                ((MainView)holder).imageMarked.setVisibility(View.GONE);
            }

            if(position==0){
                setSelection(((MainView)holder).fullView);
            }
            int index = Integer.parseInt(question.getIndex());
            index++;

            ((MainView)holder).textViewIndex.setText(""+index);
            ((MainView)holder).fullView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCategoryClicked(question,position)  ;
                   // ((MainView)holder).fullView.setBackgroundColor(mContext.getResources().getColor(R.color.right_answer));
                    setSelection(((MainView)holder).fullView);
                }
            });
            
        }
        else {

            final AdsView third_holder = (AdsView)holder;
            //third_holder.third_indexList_tv.setText(catObj.getName());
        }

    }


    public void setSelection(RelativeLayout view){

        if(cardOldSelection!=null){
          //  cardOldSelection.setBackgroundColor(mContext.getResources().getColor(R.color.white));
         //   cardOldSelection.setBackground(mContext.getResources().getDrawable(R.drawable.numbering_layout));

        }
      //  view.setBackgroundColor(mContext.getResources().getColor(R.color.right_answer));
      //  view.setBackground(mContext.getResources().getDrawable(R.drawable.numbering_layout_current_selection));
      //  cardOldSelection = view;
    }

    public void changeStyle(RelativeLayout view,int style){
        // 88 SKIPPED
        // 99 UNATTENDED
        // 1 -- 4 ATTENDED

        switch (style){
            case 88:
                view.setBackground(mContext.getResources().getDrawable(R.drawable.exam_skipped_rounded_corner_01));
                break;
            case 99:
                view.setBackground(mContext.getResources().getDrawable(R.drawable.exam_un_attended_rounded_corner_01));
                break;
            default:
                view.setBackground(mContext.getResources().getDrawable(R.drawable.exam_attended_rounded_corner_01));

        }

    }


    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return indexList.size();
    }


}
