package com.owebso.svs.xylemlern.ActivitExam

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.owebso.svs.xylemlern.R

class ExamSubmittedView : AppCompatActivity() {
    var buttonGoBack: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam_submitted_view)
        initActionBar()
        initViews()
        changeStatusBarColor()
    }

    fun initViews() {
        buttonGoBack = findViewById<View>(R.id.buttonGoBack) as Button
        buttonGoBack!!.setOnClickListener { finish() }
    }

    fun changeStatusBarColor() {
       // val window = this@ExamSubmittedView.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@ExamSubmittedView, R.color.accentColor1)
    }

    fun initActionBar() {
        val actionBarLayout = layoutInflater.inflate(
                R.layout.action_bar_04,
                null) as ViewGroup
        // Set up your ActionBar
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(false)
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.customView = actionBarLayout
        // You customization
        val actionBarColor = resources.getColor(R.color.accentColor1)
        actionBar.setBackgroundDrawable(ColorDrawable(actionBarColor))
        val parent = actionBarLayout.parent as Toolbar
        //        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);
        val actionBarTitle = findViewById<View>(R.id.actionBarTitle) as TextView
        actionBarTitle.text = "Exam"
        val togBackButton = findViewById<View>(R.id.togBackButton) as ImageView
        togBackButton.setOnClickListener { finish() }
    }

}