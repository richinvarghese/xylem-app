package com.owebso.svs.xylemlern.login;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;

import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

     String API_RESPONSE_REGISTER_USER = "";
    LinearLayout layoutSignIn;
    EditText userName,userEmail,userMobile,userSchool,password1,password2;
    String stUserName,stUserEmail,stUserMobile,stUserSchool,stPassword1,stPassword2;
    Button buttonRegister;
    boolean FLAG_CHECK;
    TextView tvErrorName,tvErrorEmail,tvErrorPhone,tvErrorSchool,tvErrorPassword;
    Context mContext = RegisterActivity.this;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    RadioGroup rgCategory,rgYear;
    String userExamCategory="0",userSchoolyear="0";
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        hideActionBar();
        initViews();
        pref = getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
    }

    public void initViews(){

        //Layouts
        layoutSignIn = (LinearLayout)findViewById(R.id.layoutSignIn);
        layoutSignIn.setOnClickListener(this);

        //RadioGroup
        rgYear = (RadioGroup)findViewById(R.id.rgYear);
        rgCategory = (RadioGroup)findViewById(R.id.rgCategory);
        rgYear.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (group.getCheckedRadioButtonId()){
                    case R.id.rb1stYear:
                        userSchoolyear = "1";
                        break;
                    case R.id.rb2ndYear:
                        userSchoolyear = "2";
                        break;
                }
            }
        });  rgCategory.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (group.getCheckedRadioButtonId()){
                    case R.id.radioNeet:
                        userExamCategory = "1";
                        break;
                    case R.id.radioJeet:
                        userExamCategory = "2";
                        break;
                }
            }
        });

        //EditTexts
        userName  = (EditText)findViewById(R.id.userName);
        userEmail  = (EditText)findViewById(R.id.userEmail);
        userMobile  = (EditText)findViewById(R.id.userMobile);
        userSchool  = (EditText)findViewById(R.id.userSchool);
        password1  = (EditText)findViewById(R.id.password1);
        password2  = (EditText)findViewById(R.id.password2);

        userEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvErrorEmail.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        userMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvErrorPhone.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvErrorName.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        userSchool.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvErrorSchool.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        password1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvErrorPassword.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        password2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvErrorPassword.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        //TextView
        tvErrorName =  (TextView)findViewById(R.id.tvErrorName);
        tvErrorEmail = (TextView)findViewById(R.id.tvErrorEmail);
        tvErrorPhone = (TextView)findViewById(R.id.tvErrorPhone);
        tvErrorSchool = (TextView)findViewById(R.id.tvErrorSchool);
        tvErrorPassword = (TextView)findViewById(R.id.tvErrorPassword);

        tvErrorName.setVisibility(View.GONE);
        tvErrorEmail.setVisibility(View.GONE);
        tvErrorPhone.setVisibility(View.GONE);
        tvErrorSchool.setVisibility(View.GONE);
        tvErrorPassword.setVisibility(View.GONE);



        //Button
        buttonRegister = (Button)findViewById(R.id.buttonRegister);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkData();
            }
        });

    }

    public void checkData(){
        stUserName = userName.getText().toString();
        stUserEmail = userEmail.getText().toString();
        stUserMobile = userMobile.getText().toString();
        stUserSchool = userSchool.getText().toString();

        stPassword1 =   password1.getText().toString();
        stPassword2 =   password2.getText().toString();

        FLAG_CHECK= true;

        if(userExamCategory.equals("")){
          //  tvErrorName.setText("Enter Your Name");
            Toast.makeText(mContext, "Select Exam Type", Toast.LENGTH_SHORT).show();
            FLAG_CHECK=false;
        }if(stUserName.equals("")){
          //  tvErrorName.setText("Enter Your Name");
            tvErrorName.setVisibility(View.VISIBLE);
            FLAG_CHECK=false;
        }if(stUserEmail.equals("")){
         //   tvErrorEmail.setText("Enter Your Email Address");
            tvErrorEmail.setVisibility(View.VISIBLE);
            FLAG_CHECK=false;
        }if(stUserMobile.equals("")){
           // tvErrorPhone.setText("Enter Phone Number");
            tvErrorPhone.setVisibility(View.VISIBLE);
            FLAG_CHECK=false;
        }if(stUserSchool.equals("")){
           // tvErrorSchool.setText("Enter SCHOOL");
            tvErrorSchool.setVisibility(View.VISIBLE);
            FLAG_CHECK=false;
        }if(userSchoolyear.equals("")){
            Toast.makeText(mContext, "Select School Year", Toast.LENGTH_SHORT).show();
            FLAG_CHECK=false;
        }
        if(stPassword1.equals("") || stPassword2.equals("")){
         //   tvErrorPassword.setText("Enter Password");
            tvErrorPassword.setVisibility(View.VISIBLE);
            FLAG_CHECK=false;
        }if(!stPassword1.equals(stPassword2)){
           // tvErrorPassword.setText("Password Mismatch");
            tvErrorPassword.setVisibility(View.VISIBLE);
            FLAG_CHECK=false;
        }

        if(FLAG_CHECK){
            registerUser();
        }

    }

    public void registerUser(){
        showProgress(true,"Creating Account..");
        //Toast.makeText(mContext, "CALLING REGISTER FUNCTION", Toast.LENGTH_SHORT).show();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("name",stUserName);
        post_data.put("email",stUserEmail);
        post_data.put("mobile",stUserMobile);
        post_data.put("password",stPassword1);
        post_data.put("school_name",stUserSchool);
        post_data.put("category",userExamCategory);
        post_data.put("year",userSchoolyear);
        post_data.put("hash_code", ConfigServer.API_HASH);
        final OnlineFunctions olf = new OnlineFunctions(mContext,ConfigServer.SERVER_REGISTER_NEW_USER,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_REGISTER_USER = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.d("API_RESPONSE_USER", "" + API_RESPONSE_REGISTER_USER);
                            JSONObject responseFromServer = new JSONObject(API_RESPONSE_REGISTER_USER);
                          //  String message = responseFromServer.getString("message");
                            String status = responseFromServer.getString("status");

                            switch (status){
                                case "200":
                                    JSONObject response = responseFromServer.getJSONObject("response");
                                    String loginId = response.getString("login_id");
                                    String type = response.getString("type");
                                    String role = response.getString("role");
                                    String str_name = response.getString("name");
                                    String logged_in = response.getString("logged_in");
                                    Log.d("API_RESPONSE_USER", "LOGIN ID=" + loginId);
                                    Toast.makeText(mContext, "Login Success", Toast.LENGTH_SHORT).show();
                                    showLoading(false);
                                    Intent home = new Intent(mContext, VerifyAccount.class);
                                    home.putExtra("CURRENT_LOGIN_ID",loginId);
                                    home.putExtra("CURRENT_PHONE_NUMBER",stUserMobile);
                                    setUserSession(loginId,type,role,str_name,logged_in,"0");
                                    showProgress(false,"Creating Account..");
                                    startActivity(home);
                                    finish();
                                    break;
                                case "404":
                                    showProgress(false,"Creating Account..");
                                    String code = responseFromServer.getString("code");
                                    if(code.equals("11")){
                                        Toast.makeText(mContext, "Email Already Exists", Toast.LENGTH_SHORT).show();
                                        tvErrorEmail.setText("An Account with This Email Already Exists");
                                        tvErrorEmail.setVisibility(View.VISIBLE);
                                    }else {
                                        tvErrorEmail.setVisibility(View.GONE);
                                    }
                                    if(code.equals("12")){
                                        tvErrorPhone.setText("An Account with This Mobile Number Already Exists");
                                        tvErrorPhone.setVisibility(View.VISIBLE);
                                        Toast.makeText(mContext, "Mobile Number Already Exists", Toast.LENGTH_SHORT).show();
                                    }else {
                                        tvErrorPhone.setVisibility(View.GONE);
                                    }

                                    break;
                                default:
                                    Toast.makeText(mContext, "Something Went Wrong. Try Again Later", Toast.LENGTH_SHORT).show();
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        new Thread(runnable).start();


    }

    private void showLoading(boolean b) {

    }


    @Override
    public void onClick(View v) {
        if(v==layoutSignIn){
            finish();
        }

    }

    public void setUserSession(String userId,String type,String role,String str_name,String logged_in,String paysts){
        editor.putString("USER_SESSION_ID",userId);
        editor.putString("USER_SESSION_TYPE",type);
        editor.putString("USER_SESSION_ROLE",role);
        editor.putString("USER_USER_NAME",str_name);
        editor.putBoolean("USER_SESSION_LOGGED_IN",Boolean.valueOf(logged_in));
        editor.putString("USER_PAYMENT_STS",paysts);
        editor.commit();
    }

    public void showProgress(boolean stat,String msg){

        if(progressDialog==null){
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(msg);
        }

        if(stat){
            if(!progressDialog.isShowing()){
                progressDialog.show();
            }
        }else {
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }
    }

    public void hideActionBar(){
        try{
            getSupportActionBar().hide();
        }catch (Exception e){
            try{
                getActionBar().hide();
            }catch (Exception e1){
                e1.printStackTrace();
            }

        }
    }
}