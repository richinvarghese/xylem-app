package com.owebso.svs.xylemlern.models;

public class Days_Model {
    private int index;
    private String category_id;
    private String name;
    private String status;
    private String image;
    private int CAT_TYPE = 0;

    public Days_Model(int index, String category_id, String name, String status, String image, int CAT_TYPE) {
        this.index = index;
        this.category_id = category_id;
        this.name = name;
        this.status = status;
        this.image = image;
        this.CAT_TYPE = CAT_TYPE;
    }



    public int getCAT_TYPE() {
        return CAT_TYPE;
    }
    public void setCAT_TYPE(int CAT_TYPE) {
        this.CAT_TYPE = CAT_TYPE;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
