package com.owebso.svs.xylemlern.functionUtils;

import android.util.Log;

import org.apache.commons.lang3.StringUtils;

public class ZonStringFunctions {

    public static String stripCommasFromString(String data){
        String stringExp = data;
        stringExp = stringExp.replace(",,",",");
        stringExp = StringUtils.stripEnd(stringExp,",");
        stringExp = StringUtils.stripStart(stringExp,",");
        return stringExp;
    }

    public static String getImagefromQuestionHtml(String htmlImage){

        String op="";

        op = StringUtils.substringBetween(htmlImage, "src=", " sty");

        try{
            op = op.replaceAll("^\"|\"$", "");
        }catch (Exception e){

        }


        Log.d("jsfgjdf", "htmlImage: "+htmlImage);
        Log.d("jsfgjdf", "op: "+op);
        return op;
    }

    public static String getCOmmaStringFromArray(String[] array){
        return  StringUtils.join(array,",");
    }
}
