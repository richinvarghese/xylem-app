package com.owebso.svs.xylemlern.intro;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.model.SliderPage;
import com.owebso.svs.xylemlern.MainActivity;
import com.owebso.svs.xylemlern.login.LoginActivity;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import java.util.ArrayList;
import java.util.List;

public class IntroActivity extends AppIntro {

    Context mContext = IntroActivity.this;
    List<Fragment> fragments;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    UserSession userSession;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Note here that we DO NOT use setContentView();
        setFlowAnimation();
        hideActionBar();
        ProfileUtils profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();
        // Add your slide fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
        //  addSlide(getFragments()[0]);
        //   addSlide(getFragments()[1]);


        // Instead of fragments, you can also use our default slide.
        // Just create a `SliderPage` and provide title, description, background and image.
        // AppIntro will do the rest.
        SliderPage sliderPage = new SliderPage();
        sliderPage.setTitle("Welcome to Xylem");
        sliderPage.setDescription("From Root to Apex");
        sliderPage.setImageDrawable(R.drawable.man);

        SliderPage sliderPage2 = new SliderPage();
        sliderPage2.setTitle("Welcome to Xylem 2");
        sliderPage2.setDescription("From Root to Apex 22");
        sliderPage2.setImageDrawable(R.drawable.man);
        sliderPage2.setBgColor(getResources().getColor(R.color.colorPrimaryDark));


        // sliderPage.setBgColor(getResources().getColor(R.color.action_bar));
//        addSlide(AppIntroFragment.newInstance(sliderPage));
//        addSlide(AppIntroFragment.newInstance(sliderPage2));
//
        addSlide(getFragments().get(0));
        addSlide(getFragments().get(1));

        // OPTIONAL METHODS
        // Override bar/separator color.
        //  setBarColor(Color.parseColor("#3F51B5"));
        //  setSeparatorColor(Color.parseColor("#2196F3"));

        // Hide Skip/Done button.
        showSkipButton(true);
        setNextArrowColor(getResources().getColor(R.color.accentColor1));
        setColorSkipButton(getResources().getColor(R.color.accentColor1));
        setColorDoneText(getResources().getColor(R.color.accentColor1));
        setProgressButtonEnabled(true);
        setImageNextButton(getDrawable(R.drawable.ic_navigate_next_white));
        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        setVibrate(false);

        checkintroStatus();
        //   setVibrateIntensity(30);
    }


    public void hideActionBar(){
        try{
            getSupportActionBar().hide();
        }catch (Exception e){
            try{
                getActionBar().hide();
            }catch (Exception e1){
                e1.printStackTrace();
            }

        }
    }

    public void checkintroStatus(){
        pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        String intro_status="0";
        intro_status = pref.getString("intro_status","0");
        Log.d("dfvjdjddfs",intro_status+"   "+userSession.getUSER_SESSION_ID());
        if (userSession.getUSER_SESSION_ID().equalsIgnoreCase("")||userSession.getUSER_SESSION_ID()==null){
            if(intro_status.equals("1")){
                Intent in = new Intent(mContext, LoginActivity.class);
                startActivity(in);
                finish();
            }
        }else if (Integer.parseInt(userSession.getUSER_SESSION_ID())>0){
            Intent in = new Intent(mContext, MainActivity.class);
            startActivity(in);
            finish();
        } else if(intro_status.equals("1")){
            Intent in = new Intent(mContext, LoginActivity.class);
            startActivity(in);
            finish();
        }
    }

    public List<Fragment> getFragments(){
        fragments = new ArrayList<>();

        IntroFirst introFirst = new IntroFirst();
        IntroSecond introSecond = new IntroSecond();

        fragments.add(introFirst);
        fragments.add(introSecond);

        return fragments;
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        Intent in = new Intent(mContext, LoginActivity.class);
        startActivity(in);
        editor.putString("intro_status","1");
        editor.commit();
        finish();
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent in = new Intent(mContext, LoginActivity.class);
        startActivity(in);
        editor.putString("intro_status","1");
        editor.commit();
        finish();
        // Do something when users tap on Done button.
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
}
