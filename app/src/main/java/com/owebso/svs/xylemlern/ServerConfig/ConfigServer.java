package com.owebso.svs.xylemlern.ServerConfig;

public class ConfigServer {
    // public static final String SERVER_URL_ROOT = "http://xylemlearning.com/api";
    // public static final String SERVER_URL_ROOT = "http://192.168.1.73:8083/xylem/api";
  //  public static final String SERVER_URL_ROOTLOCAL = "http://vlcare.com/xylem_vl/api/webservice/";
    public static final String SERVER_URL_ROOTLOCAL = "http://xylemlearning.com/api/webservice/";


    public static final String SERVER_URL_ROOT = "http://xylemlearning.com/api/";
    public static final String SERVER_WEBSITE = "http://xylemlearning.com/";
    public static final String SERVER_WEBSITE_LOCAL = "http://vlcare.com/xylem_vl/";
    public static final String API_HASH = "api_hashcode_2";
    public static final String API_GET_CATEGORY = SERVER_URL_ROOT+"/webservice/get_category2";
    public static final String API_GET_CATEGORY_BASIC = SERVER_URL_ROOT+"/webservice/get_category";
    public static final String API_GET_MCQ_CATEGORY = SERVER_URL_ROOT+"/webservice/get_mcq_by_category";
    public static final String API_GET_MCQ_SINGLE_QUESTION = SERVER_URL_ROOT+"/webservice/get_mcq";
    public static final String API_GET_VIDEOS_CATEGORY = SERVER_URL_ROOT+"/webservice/get_videos_by_category";
    public static final String API_GET_EXAMS_SINGLE = SERVER_URL_ROOTLOCAL+"get_exam_questions";

    public static final String API_GET_EXAMS_LIST = SERVER_URL_ROOT+"/webservice/get_exams";
    public static final String API_GET_VIDEO_DETAILS = SERVER_URL_ROOT+"/webservice/get_video";
    public static final String API_GET_USER_DETAILS = SERVER_URL_ROOT+"/webservice/get_profile/";
    public static final String API_GET_CATEGORY_IN = SERVER_URL_ROOT+"/webservice/get_category_in";
    public static final String API_GET_CATEGORY_SINGLE = SERVER_URL_ROOT+"/webservice/get_category_single";
    public static final String API_SET_VIDEO_WATCH_TIME = SERVER_URL_ROOT+"/webservice/add_watch_list";
    public static final String API_SET_NEW_VIDEO_WATCH_TIME = SERVER_URL_ROOT+"/webservice/add_new_watch_list";
    public static final String API_GET_PARENT_CATEGORY = SERVER_URL_ROOT+"/webservice/get_parent_category/";
    public static final String API_GET_ATTEND_MCQ_DATA = SERVER_URL_ROOT+"/webservice/get_attend_mcq/";
    public static final String API_GET_ATTEND_EXAM_DATA = SERVER_URL_ROOTLOCAL+"get_attend_exam/";
    public static final String API_GET_RECENTLY_WATCHED_VIDEOS = SERVER_URL_ROOT+"/webservice/get_new_latest_watched/";
    public static final String API_GET_HASH_TAGS_LIST = SERVER_URL_ROOT+"/webservice/get_tags_list";
    public static final String API_CREATE_CUSTOM_MODULE = SERVER_URL_ROOT+"/webservice/get_custom_modules";
    public static final String API_GET_CUSTOM_MODULE = SERVER_URL_ROOT+"/webservice/get_custom_module_by_login_id";
    public static final String API_GET_DAY_CATEGORY = SERVER_URL_ROOTLOCAL+"get_categories";
    public static final String API_GET_VIDEO_BY_CATEGORY = SERVER_URL_ROOTLOCAL+"get_video_by_category";
    public static final String API_GET_RELETED_VIDEO = SERVER_URL_ROOTLOCAL+"related_video";
    public static final String API_GET_PACKAGE_LIST = SERVER_URL_ROOT+"/webservice/packages_list";
    public static final String API_MAKE_FAVORITE = SERVER_URL_ROOT+"/webservice/favourite_videos";
    public static final String API_REGOR_PAY_RESPONSE = SERVER_URL_ROOT+"/webservice/response_razorpay";
    public static final String API_FAV_VIDEO_LIST = SERVER_URL_ROOTLOCAL+"favourite_videos_list";
    public static final String API_IMAGE_SLIDERS = SERVER_URL_ROOT+"/webservice/sliders";

    public static final String API_SET_BOOKMARK_QUESTION = SERVER_URL_ROOT+"/webservice/bookmark";
    public static final String API_GET_BOOKMARKS = SERVER_URL_ROOT+"/webservice/getBookmarks";
    public static final String API_LOG_OUT = SERVER_URL_ROOT+"/webservice/user_logout";

    //USER ACCOUNT
    public static final String API_GET_USER_PROFILE_DATA = SERVER_URL_ROOT+"/webservice/get_user_profile/";
    public static final String API_GET_USER_LOGIN = SERVER_URL_ROOTLOCAL+"user_login/";
    public static final String SERVER_REGISTER_NEW_USER = SERVER_URL_ROOT+"/webservice/user_registration";

    //POSTDATA
    public static final String API_POST_EXAM_RESULT = SERVER_URL_ROOTLOCAL+"post_exam_result/";
    public static final String API_POST_MCQ_RESULT = SERVER_URL_ROOTLOCAL+"post_mcq_result/";
    public static final String API_POST_EXAM_START_END_TIME = SERVER_URL_ROOTLOCAL+"post_exam_start_date/";
    public static final String API_POST_EXAM_UPDATE_ONCLICK = SERVER_URL_ROOTLOCAL+"post_exam_update_onclick/";

    //CHECK DATA
    public static final String API_CHECK_EXAM_ATTENDANCE = SERVER_URL_ROOTLOCAL+"check_exam_attended/";
    public static final String API_CHECK_EXAM_PUBLISH_DATE = SERVER_URL_ROOTLOCAL+"get_exam_publish_date_pass/";
    public static final String API_CHECK_MCQ_ATTENDANCE = SERVER_URL_ROOTLOCAL+"check_mcq_attended/";
    public static final String API_GET_EXAM_CATEGARIES = SERVER_URL_ROOTLOCAL+"get_exam_categories";
    public static final String API_CHECK_NEW_EXAM_ATTENDANCE = SERVER_URL_ROOTLOCAL+"check_new_exam_attended/";
    public static final String API_GET_NEW_EXAM = SERVER_URL_ROOTLOCAL+"get_new_exams/";
    public static final String API_GET_NEW_EXAM_QUESTIONS = SERVER_URL_ROOTLOCAL+"get_new_exam_questions/";
    public static final String API_GET_NEW_RANK = SERVER_URL_ROOTLOCAL+"student_rank/";


    //PATHS
    public static final String UPLOADS_PATH_ROOT = SERVER_WEBSITE_LOCAL+"/uploads/";
    public static final String PATH_VIDEO = UPLOADS_PATH_ROOT+"videos/";
    public static final String PATH_VIDEO_THUMB = UPLOADS_PATH_ROOT+"videos_thumb/";
    public static final String PATH_CATEGORY_THUMB = UPLOADS_PATH_ROOT+"categories/";
    public static final String PATH_QUESTION_IMAGE = UPLOADS_PATH_ROOT+"question/";
    public static final String PATH_PDF_NOTES = UPLOADS_PATH_ROOT+"metrials/";
    public static final String PATH_SLIDE_TIMELINE_IMAGE = UPLOADS_PATH_ROOT+"time_lines/";

    public static final String USER_ID_DEFAULT="22";

    //STATIC
    public static final String CAT_MCQ_NEET="45";
    public static final String CAT_MCQ_JEE="46";

    public static final String CAT_EXAM_NEET="1";
    public static final String CAT_EXAM_JEE="2";

    public static final String CAT_VIDEO_NEET="55";
    public static final String CAT_VIDEO_JEE="56";

    //MCQ EXAM RESUME STATUS
    public static final String MCQ_STATUS_FRESH="1";
    public static final String MCQ_STATUS_PAUSED="2";
    public static final String MCQ_STATUS_FINISHED="3";

}
