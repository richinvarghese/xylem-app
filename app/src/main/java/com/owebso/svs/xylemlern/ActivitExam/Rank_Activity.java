package com.owebso.svs.xylemlern.ActivitExam;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.XylemActivities.VideosLists;
import com.owebso.svs.xylemlern.adapters.ExamListAdapter;
import com.owebso.svs.xylemlern.adapters.Rank_Adapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Exam;
import com.owebso.svs.xylemlern.models.Rank_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Rank_Activity extends AppCompatActivity implements View.OnClickListener {
    String API_RESPONSE_GET_RANK="";
    ImageView togBackButton;
    RecyclerView recycle_rank;
    LinearLayout loadingLayout;
    Rank_Adapter rank_adapter;
    ArrayList<Rank_Model> rank_List;
    String EXAM_ID="";
    TextView actionBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_rank);
        Intent intent = getIntent();
        EXAM_ID = intent.getStringExtra("EXAM_ID");
        initActionBar();
        changeStatusBarColor();
        loadingLayout = (LinearLayout)findViewById(R.id.loadingLayout);
        togBackButton = findViewById(R.id.togBackButton);
        recycle_rank = findViewById(R.id.recycle_rank);
        loadingLayout.setVisibility(View.GONE);

        togBackButton.setOnClickListener(this);
        recycle_rank.setLayoutManager(new LinearLayoutManager(this));
        getRank();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.togBackButton:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    public void getRank(){
        showLoading(true);
        rank_List = new ArrayList<>();
        rank_List.clear();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("exam_id",EXAM_ID);
        String URL = ConfigServer.API_GET_NEW_RANK;
        final OnlineFunctions olf = new OnlineFunctions(this,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_RANK = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("API_RESPONSEd3", ""+API_RESPONSE_GET_RANK);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_RANK);
                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("students"));

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String attend_id = jsonObject.getString("attend_id");
                                String exam_id = jsonObject.getString("exam_id");
                                String registration_id = jsonObject.getString("registration_id");
                                String attended_data = jsonObject.getString("attended_data");
                                String scoreboard_data = jsonObject.getString("scoreboard_data");
                                String marked_question_data = jsonObject.getString("marked_question_data");
                                String mark_optained = jsonObject.getString("mark_optained");
                                String total_questions = jsonObject.getString("total_questions");
                                String answered = jsonObject.getString("answered");
                                String right_answer = jsonObject.getString("right_answer");
                                String wrong_answer = jsonObject.getString("wrong_answer");
                                String skipped = jsonObject.getString("skipped");
                                String points = jsonObject.getString("points");
                                String negative_points = jsonObject.getString("negative_points");
                                String total_score = jsonObject.getString("total_score");
                                String last_index = jsonObject.getString("last_index");
                                String status = jsonObject.getString("status");
                                String published = jsonObject.getString("published");
                                String start_time = jsonObject.getString("start_time");
                                String end_time = jsonObject.getString("end_time");
                                String created_at = jsonObject.getString("created_at");
                                String updated_at = jsonObject.getString("updated_at");
                                String stname = jsonObject.getString("stname");
                                rank_List.add(new Rank_Model(attend_id,exam_id,registration_id,attended_data,scoreboard_data,marked_question_data,mark_optained,
                                        total_questions,answered,right_answer,wrong_answer,skipped,points,negative_points,total_score,
                                        last_index,status,published,start_time,end_time,created_at,updated_at,stname));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        rank_adapter = new Rank_Adapter(Rank_Activity.this, rank_List);
                        recycle_rank.setAdapter(rank_adapter);
                        showLoading(false);
                        // showParentAtTop(CATEGORY_ID);
                    }
                });
            }
        };

        new Thread(runnable).start();
    }


    public void showLoading(boolean stat){
        if(stat){
            loadingLayout.setVisibility(View.VISIBLE);
        }else {
            loadingLayout.setVisibility(View.GONE);
        }
    }







    public void changeStatusBarColor(){
        Window window = Rank_Activity.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(Rank_Activity.this,R.color.accentColor1));
    }
    public  void initActionBar(){
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.accentColor1);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent =(Toolbar) actionBarLayout.getParent();
        // parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
        //  parent.setContentInsetsAbsolute(0,0);

        actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("Ranking");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }








}
