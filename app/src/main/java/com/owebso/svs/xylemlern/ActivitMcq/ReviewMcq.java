package com.owebso.svs.xylemlern.ActivitMcq;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.adapters.GridViewQuestionsAdapterMcq;
import com.owebso.svs.xylemlern.adapters.IndexNumberAdapterMcq2;
import com.owebso.svs.xylemlern.functionUtils.ImageUtils;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.functionUtils.WebServiceApi;
import com.owebso.svs.xylemlern.functionUtils.ZonStringFunctions;
import com.owebso.svs.xylemlern.models.ChoiceOption;
import com.owebso.svs.xylemlern.models.McqQuestion;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.sufficientlysecure.htmltextview.HtmlResImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ReviewMcq extends AppCompatActivity {

    private static String API_RESPONSE_POST_MCQ_RESULT ="";
    ArrayList<McqQuestion> questionArrayList;
    String CATEGORY_ID="54";
    Context mContext = ReviewMcq.this;
    String API_RESPONSE="",API_RESPONSE_SINGLE_QUESTION="";
    LinearLayout loadingLayout,fullLayout;
    McqQuestion CURRENT_QUESTION;
    LinearLayout layoutRadioGroup;
    TextView tvQuestionIndex,tvQuestion,tvAnswer;
    final RadioButton[] rb = new RadioButton[4];
    LinearLayout layoutHint,layoutAnswer,layoutHintButton;
    TextView tvHint,tvAnswerExplanation;
    ImageView imageViewQuestion;
    ImageUtils imageUtils;
    int CURRENT_INDEX=0;
    Button buttonPrev,buttonNext;
    ImageView toggleButton;
    CardView answer1,answer2,answer3,answer4;
    TextView textViewA,textViewB,textViewC,textViewD;
    HtmlTextView html_text_explanation;
    RecyclerView rcViewIndex;

    int TOTAL_NUM_QUESTIONS=0;
    int TOTAL_NUM_RIGHT_ANSWERS=0;
    int TOTAL_NUM_WRONG_ANSWERS=0;
    int TOTAL_NUM_SKIPPED_ANSWERS=0;
    int TOTAL_SCORE_FROM_CURRENT_EXAM=0;
    String[] scoreBoardList;
    //int[] skippedQuestionsArray;
    // int[] userSelectedOption;
    int TOTAL_QBANK_SCORE=0;
    RecyclerView.OnScrollListener onScrollListener;
    String[] ATTENDED_QUESTION_DATA;
    TextView textViewCurrentCount;
    LinearLayoutManager indexLayoutManager;
    boolean isNextPossible=false;
    ImageView imageViewQuestion2;
    String ATTENDED_DATA_STRING = "";
    String ATTENDED_SCOREBOARD_DATA_STRING = "";
    ProgressDialog progressDialog;
    boolean isResumed = false;
    int PRIME_INDEX_NUMBER=0;
    IndexNumberAdapterMcq2 indexAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_mcq);

        CATEGORY_ID = getIntent().getStringExtra("CATEGORY_ID");

        //initActionBar();
            changeStatusBarColor();
        //  hideActionBar();

        initViews();
    }


    public void hideActionBar(){
        try{
            getSupportActionBar().hide();
        }catch (Exception e){
            try{
                getActionBar().hide();
            }catch (Exception e1){
                e1.printStackTrace();
            }

        }
    }

    public void initViews(){
        imageUtils = new ImageUtils(mContext);

        //layouts
        loadingLayout = (LinearLayout)findViewById(R.id.loadingLayout);
        fullLayout = (LinearLayout)findViewById(R.id.fullLayout);
        layoutHint = (LinearLayout)findViewById(R.id.layoutHint);
        layoutAnswer = (LinearLayout)findViewById(R.id.layoutAnswer);
        layoutRadioGroup = (LinearLayout)findViewById(R.id.layoutRadioGroup);
        layoutHintButton = (LinearLayout)findViewById(R.id.layoutHintButton);

        textViewCurrentCount = (TextView)findViewById(R.id.textViewCurrentCount);

        //RecyclerView
        indexLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);

        rcViewIndex = (RecyclerView)findViewById(R.id.rcViewIndex);
        rcViewIndex.setLayoutManager(indexLayoutManager);


        //CardView
        answer1 = (CardView) findViewById(R.id.answer1);
        answer2 = (CardView) findViewById(R.id.answer2);
        answer3 = (CardView) findViewById(R.id.answer3);
        answer4 = (CardView) findViewById(R.id.answer4);

        textViewA = (TextView)findViewById(R.id.textViewA);
        textViewB = (TextView)findViewById(R.id.textViewB);
        textViewC = (TextView)findViewById(R.id.textViewC);
        textViewD = (TextView)findViewById(R.id.textViewD);

        //Button
        buttonPrev = (Button)findViewById(R.id.buttonPrev);
        buttonNext = (Button)findViewById(R.id.buttonNext);

        //TextView
        tvQuestionIndex = (TextView) findViewById(R.id.tvQuestionIndex);
        tvAnswer = (TextView) findViewById(R.id.tvAnswer);
        tvHint = (TextView) findViewById(R.id.tvHint);
        tvQuestion = (TextView) findViewById(R.id.tvQuestion);
        tvAnswerExplanation = (TextView) findViewById(R.id.tvAnswerExplanation);

        html_text_explanation = (HtmlTextView)findViewById(R.id.html_text_explanation);


        //ImageView
        imageViewQuestion = (ImageView)findViewById(R.id.imageView4);
        imageViewQuestion2 = (ImageView)findViewById(R.id.imageViewQuestion2);

        //CHANGING VISIBILITY
        imageViewQuestion.setVisibility(View.GONE);
        layoutAnswer.setVisibility(View.GONE);
        layoutHint.setVisibility(View.GONE);


        getQuestionList();

        layoutHintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHint(true);
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNextPossible){
                    //RIGHT DIRECTION
                    gotoQuestion(1);
                }else {
                    if(ATTENDED_QUESTION_DATA[CURRENT_INDEX].equals("99")){
                        setAnswerSkippedSelection();
                        isNextPossible=true;
                        buttonNext.setText("Next");

                        ATTENDED_QUESTION_DATA[CURRENT_INDEX]="88";
                        scoreBoardList[CURRENT_INDEX]="0";

                    }else {
                        gotoQuestion(1);
                    }
                }

            }
        });

        buttonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //LEFT DIRECTION
                gotoQuestion(0);
            }
        });



        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        //we reached the target position
                        // rcViewIndex.findViewHolderForAdapterPosition(CURRENT_INDEX).itemView.performClick();
                        recyclerView.removeOnScrollListener(this);
                        break;
                }
            }
        };

    }


    private void scrollToPosition(int position){
        rcViewIndex.removeOnScrollListener(onScrollListener);
        rcViewIndex.addOnScrollListener(onScrollListener);
        rcViewIndex.smoothScrollToPosition(position);

    }

    public void gotoQuestion(int direction){
        if(direction==0){
            //prev question
            if(CURRENT_INDEX!=0){
                CURRENT_INDEX--;
                // getSingleQuestionData(questionArrayList.get(CURRENT_INDEX));
                //rcViewIndex.findViewHolderForAdapterPosition(CURRENT_INDEX).itemView.performClick();

                try{
                    //rcViewIndex.findViewHolderForAdapterPosition(CURRENT_INDEX).itemView.performClick();
                    getSingleQuestionData(questionArrayList.get(CURRENT_INDEX));

                }catch (NullPointerException e){
                    scrollToPosition(CURRENT_INDEX);
                    e.printStackTrace();
                }


            }else {
                //  Toast.makeText(mContext, "First Question", Toast.LENGTH_SHORT).show();
            }
        }else {
            //next question

            indexLayoutManager.scrollToPositionWithOffset(CURRENT_INDEX,0);

            if(CURRENT_INDEX<questionArrayList.size()-1){
                CURRENT_INDEX++;
                // getSingleQuestionData(questionArrayList.get(CURRENT_INDEX));
                try{
                    //  rcViewIndex.findViewHolderForAdapterPosition(CURRENT_INDEX).itemView.performClick();
                    getSingleQuestionData(questionArrayList.get(CURRENT_INDEX));

                }catch (NullPointerException e){
                    scrollToPosition(CURRENT_INDEX);
                    e.printStackTrace();
                }
            }else {
                 Toast.makeText(mContext, "Last Question", Toast.LENGTH_SHORT).show();
              //  showMcqFinishDialogue(fullLayout);
            }


        }
    }

    public void changeStatusBarColor(){
        Window window = ReviewMcq.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(ReviewMcq.this,R.color.white));
    }

    public  void initActionBar(){
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04_white_mcq,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.white);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent =(Toolbar) actionBarLayout.getParent();
//        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);

        TextView actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("MCQ");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        ImageView gridViewbutton = (ImageView) findViewById(R.id.gridViewbutton);

        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        gridViewbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGridPopupQuestion(v);
            }
        });


    }

    public void getQuestionList(){
        showLoading(true);
        questionArrayList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("category_id",CATEGORY_ID);
        String URL = ConfigServer.API_GET_MCQ_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLog("API_RESPONSE",API_RESPONSE);
                        try{
                            //showLoading(false);
                            JSONObject jsonObject = new JSONObject(API_RESPONSE);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");

                            TOTAL_NUM_QUESTIONS = jsonArray.length();
                            scoreBoardList = new String[TOTAL_NUM_QUESTIONS];
                            ATTENDED_QUESTION_DATA = new String[TOTAL_NUM_QUESTIONS];
                            Arrays.fill(ATTENDED_QUESTION_DATA,"99");
                            Arrays.fill(scoreBoardList,"9");

                            Log.d("attended_datwea", "onResponse11: "+"11111");

                            //  Arrays.fill(userSelectedOption,10);

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject question = jsonArray.getJSONObject(i);
                                String questionbanks_id = question.getString("questionbanks_id");
                                String questiongroups_id = question.getString("questiongroups_id");
                                String questionlevels_id = question.getString("questionlevels_id");
                                String category_id = question.getString("category_id");
                                String questionQ = question.getString("question");
                                String question_image = question.getString("question_image");
                                String explanation = question.getString("explanation");
                                String hints = question.getString("hints");
                                String mark = question.getString("mark");
                                String questiontype = question.getString("questiontype");
                                String year = question.getString("year");
                                String usertype = question.getString("usertype");
                                String tags = question.getString("tags");
                                String questionbanks_status = question.getString("questionbanks_status");
                                String created_at = question.getString("created_at");
                                String updated_at = question.getString("updated_at");
                                String no_of_options = question.getString("no_of_options");
                                String options = question.getString("options");
                                String answer = question.getString("answer");

                                TOTAL_QBANK_SCORE = TOTAL_QBANK_SCORE+(Integer.parseInt(mark));

                                questionArrayList.add(new McqQuestion(""+i,questionbanks_id,
                                        questiongroups_id,questionlevels_id,category_id
                                        ,questionQ,question_image,explanation,hints,mark,questiontype,
                                        year,usertype,tags,questionbanks_status,created_at,updated_at,
                                        no_of_options,options,answer));


                            }

                        }catch (JSONException e){

                        }

                        try{
                          //  getSingleQuestionData(questionArrayList.get(CURRENT_INDEX));
                        }catch (IndexOutOfBoundsException e){
                            Toast.makeText(mContext, "No Questions", Toast.LENGTH_SHORT).show();
                            finish();
                        }


                        indexAdapter = new IndexNumberAdapterMcq2(mContext, questionArrayList,scoreBoardList,CURRENT_INDEX, new IndexNumberAdapterMcq2.EventListener() {
                            @Override
                            public void onCategoryClicked(McqQuestion question, int index, RelativeLayout view) {
                                getSingleQuestionData(question);

                            }

                        },0);

                        indexAdapter.notifyDataSetChanged();

                        rcViewIndex.setAdapter(indexAdapter);

                           getAttendedExamData();

                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    public void showLog(String name,String value){
        Log.d(name,value);
    }

    public void getSingleQuestionData(final McqQuestion currentQuestion){

        CURRENT_INDEX = Integer.parseInt(currentQuestion.getIndex());

        String question_id = currentQuestion.getQuestionbanks_id();

        showLoading(true);
        showHint(false);
        showAnswer(false);
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("question_id",question_id);
        String URL = ConfigServer.API_GET_MCQ_SINGLE_QUESTION;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_SINGLE_QUESTION = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            JSONObject jsonObject = new JSONObject(API_RESPONSE_SINGLE_QUESTION);

                            String status = jsonObject.getString("status");

                            if(status.equals("200")){
                                JSONObject question = jsonObject.getJSONObject("result");

                                String questionbanks_id = question.getString("questionbanks_id");
                                String questiongroups_id = question.getString("questiongroups_id");
                                String questionlevels_id = question.getString("questionlevels_id");
                                String category_id = question.getString("category_id");
                                String questionQ = question.getString("question");
                                String question_image = question.getString("question_image");
                                String explanation = question.getString("explanation");
                                String hints = question.getString("hints");
                                String mark = question.getString("mark");
                                String questiontype = question.getString("questiontype");
                                String year = question.getString("year");
                                String usertype = question.getString("usertype");
                                String tags = question.getString("tags");
                                String questionbanks_status = question.getString("questionbanks_status");
                                String created_at = question.getString("created_at");
                                String updated_at = question.getString("updated_at");
                                String no_of_options = question.getString("no_of_options");
                                String options = question.getString("options");
                                String answer = question.getString("answer");

                                String IMAGE_URL="";
                                Log.d("dhgutdiue", "run: "+question_image);
                                if(!question_image.equals("") && !question_image.equals("null")){
                                    IMAGE_URL = ConfigServer.PATH_QUESTION_IMAGE+question_image;
                                    imageUtils.showImage(IMAGE_URL,imageViewQuestion);
                                    imageViewQuestion.setVisibility(View.VISIBLE);
                                }else {
                                    IMAGE_URL="";
                                    imageViewQuestion.setVisibility(View.GONE);
                                }

                                if(hints.equals("")){
                                    layoutHintButton.setVisibility(View.GONE);
                                    showLog("jhEE123e","IFF"+hints);
                                }else {
                                    layoutHintButton.setVisibility(View.VISIBLE);
                                    showLog("jhEE123e","ELSE"+hints);
                                }


                                CURRENT_QUESTION = new McqQuestion(currentQuestion.getIndex(),questionbanks_id,
                                        questiongroups_id,questionlevels_id,category_id
                                        ,questionQ,IMAGE_URL,explanation,hints,mark,questiontype,
                                        year,usertype,tags,questionbanks_status,created_at,updated_at,
                                        no_of_options,options,answer);

                                isNextPossible=false;

                            }

                        }catch (JSONException e){

                        }
                        String indexxx  = String.valueOf(((Integer.parseInt(currentQuestion.getIndex()))+1));
                        showQuestionView(CURRENT_QUESTION,indexxx);
                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    public void showQuestionView(McqQuestion question,String index){

        hideButtons();

        refreshRcViewIndex(Integer.parseInt(question.getIndex()));
        indexLayoutManager.scrollToPositionWithOffset(Integer.parseInt(question.getIndex()),0);

        setPrimeIndexNumber(Integer.parseInt(question.getIndex()));

        showProgessD(false,"");        //stopping resume loading if showing
        int count = Integer.parseInt(question.getIndex());
        count++;
        textViewCurrentCount.setText(count+" / "+TOTAL_NUM_QUESTIONS);

        tvQuestionIndex.setText(index);
        tvQuestion.setText(html2text(index+". "+question.getQuestion()));



        try{
            String questionExtraURL = ZonStringFunctions.getImagefromQuestionHtml(question.getQuestion());

            if(!questionExtraURL.equals("")){
                imageUtils.showImage(questionExtraURL,imageViewQuestion2);
                imageViewQuestion2.setVisibility(View.VISIBLE);
            }else {
                //  Toast.makeText(mContext, ""+questionExtraURL, Toast.LENGTH_SHORT).show();
            }
        }catch (NullPointerException e){
            imageViewQuestion2.setVisibility(View.GONE);
        }

        //createOptionViewButtons();
        createOptionsCards();

        selectIfAlreadySelected();
        showAnswer(false);

        if(ATTENDED_QUESTION_DATA[CURRENT_INDEX].equals("88")){
            setAnswerSkippedSelection();
            isNextPossible=true;
            buttonNext.setText("Next");
        }


        if(!isResumed){
            showLoading(false);
            getAttendedExamData();
        }else {
            showLoading(false);
        }

        showAnswer(true);

    }

    private void hideButtons() {
        if(CURRENT_INDEX==0){
            buttonPrev.setVisibility(View.GONE);
            buttonNext.setVisibility(View.VISIBLE);
        }else {
            if(CURRENT_INDEX==questionArrayList.size()-1){
                buttonNext.setVisibility(View.GONE);
                buttonPrev.setVisibility(View.VISIBLE);
            }else {
                buttonNext.setVisibility(View.VISIBLE);
                buttonPrev.setVisibility(View.VISIBLE);
            }
        }
    }


    public ArrayList<CardView> getOptionsButtonCardView(){
        ArrayList<CardView> arrayList = new ArrayList<>();
        arrayList.add(answer1);
        arrayList.add(answer2);
        arrayList.add(answer3);
        arrayList.add(answer4);
        return  arrayList;
    }

    public ArrayList<TextView> getAnswersList(){
        ArrayList<TextView> arrayList = new ArrayList<>();
        arrayList.add(textViewA);
        arrayList.add(textViewB);
        arrayList.add(textViewC);
        arrayList.add(textViewD);
        return  arrayList;
    }

    public void createOptionsCards(){
        clearSelections();
        textViewA.setText(getOptions().get(0).getOption());
        textViewB.setText(getOptions().get(1).getOption());
        textViewC.setText(getOptions().get(2).getOption());
        textViewD.setText(getOptions().get(3).getOption());

        answer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkAnswer2(0);

            }
        });
        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer2(1);
            }
        });
        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer2(2);
            }
        });
        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer2(3);
            }
        });

    }




    public ArrayList<ChoiceOption> getOptions(){
        ArrayList<ChoiceOption> choiceOptions = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(CURRENT_QUESTION.getOptions());

            for(int i =0;i<jsonArray.length();i++){

                choiceOptions.add(new ChoiceOption(String.valueOf(i),html2text(jsonArray.getString(i))));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return choiceOptions;
    }

    public void showLoading(boolean stat){
        if(stat){
            loadingLayout.setVisibility(View.VISIBLE);
            fullLayout.setVisibility(View.GONE);
        }else {
            loadingLayout.setVisibility(View.GONE);
            fullLayout.setVisibility(View.VISIBLE);
        }

    }


    public String html2text(String html) {
        String data =Jsoup.parse(html).text();

        Log.d("htmltagtest2", "HTML  : "+html);
        Log.d("htmltagtest2", "DATA  : "+data);

//        String data =html;
//        data = data.replace("<p>","");
//        data = data.replace("</p>","");
        return data ;
    }

    public void showHint(boolean show){
        if(show){
            tvHint.setText(CURRENT_QUESTION.getHints());
            layoutHint.setVisibility(View.VISIBLE);
        }else {
            layoutHint.setVisibility(View.GONE);
        }
    }

    public void showAnswer(boolean show){

        if(show){
            try{
                JSONArray array = new JSONArray(CURRENT_QUESTION.getAnswer());
                int answerIndex = array.getInt(0);
                answerIndex--;
                String answer = getOptions().get(answerIndex).getOption();
                tvAnswer.setText(answer);
                tvAnswerExplanation.setText(html2text(CURRENT_QUESTION.getExplanation()));
                tvAnswerExplanation.setVisibility(View.GONE);

                html_text_explanation.setHtml(CURRENT_QUESTION.getExplanation(),
                        new HtmlResImageGetter(html_text_explanation));

                layoutAnswer.setVisibility(View.VISIBLE);

            }catch (JSONException e){

            }
        }else {
            layoutAnswer.setVisibility(View.GONE);
        }


    }




    public void checkAnswer2(int answer) {

        selectIfAlreadySelected();

        if(ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())].equals("99")){

            buttonNext.setText("Next");

            ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())]=""+answer;
            try {
                JSONArray ansArray = new JSONArray(CURRENT_QUESTION.getAnswer());
                int right_answer = Integer.parseInt(ansArray.getString(0));
                answer++;
                Log.d("ANSWER_CHECK", "CLICKED "+answer);
                Log.d("ANSWER_CHECK", "RIGHT_ANSWER "+right_answer);

                int cur_index = Integer.parseInt(CURRENT_QUESTION.getIndex());

                if(answer==right_answer){
                    setAsTheRightAnswer2(getOptionsButtonCardView().get(answer-1),getAnswersList().get(answer-1),true);
                    scoreBoardArray(Integer.parseInt(CURRENT_QUESTION.getIndex()),true);


                }else {
                    setAsTheRightAnswer2(getOptionsButtonCardView().get(answer-1),getAnswersList().get(answer-1),false);
                    setAsTheRightAnswer3(getOptionsButtonCardView().get(right_answer-1),getAnswersList().get(right_answer-1),true);
                    scoreBoardArray(Integer.parseInt(CURRENT_QUESTION.getIndex()),false);

                }
            }catch (JSONException j){
            }
        }

       // submitAndSaveMcqintheBackground(); //SAVING MCQ DATA FOR RESUMING LATER

    }


    public void setAsTheRightAnswer2(CardView cardView,TextView tv,boolean stat){
        if(stat){
            cardView.setCardBackgroundColor(getResources().getColor(R.color.right_answer));
            tv.setTextColor(getResources().getColor(R.color.white));
            showAnswer(true);
        }else {
            showAnswer(true);
            cardView.setCardBackgroundColor(getResources().getColor(R.color.wrong_answer));
            tv.setTextColor(getResources().getColor(R.color.white));
        }
    }

    public void setAsTheRightAnswer3(CardView cardView,TextView tv,boolean stat){
        if(stat){
            cardView.setCardBackgroundColor(getResources().getColor(R.color.right_answer));
            tv.setTextColor(getResources().getColor(R.color.white));
        }else {
            cardView.setCardBackgroundColor(getResources().getColor(R.color.wrong_answer));
            tv.setTextColor(getResources().getColor(R.color.white));
        }
    }

    public void clearSelections(){
        for(int i=0;i<getOptionsButtonCardView().size();i++){
            getOptionsButtonCardView().get(i).setCardBackgroundColor(getResources().getColor(R.color.action_bar));
            getAnswersList().get(i).setTextColor(getResources().getColor(R.color.accentColor1));
        }
    }


    public void scoreBoardArray(int questionNo,boolean stat){

        if(stat){
            scoreBoardList[questionNo]="1";
        }else {
            scoreBoardList[questionNo]="2";
        }

    }

    public void calculateScore(){
        TOTAL_SCORE_FROM_CURRENT_EXAM=0;
        TOTAL_NUM_RIGHT_ANSWERS=0;
        TOTAL_NUM_WRONG_ANSWERS=0;
        TOTAL_NUM_SKIPPED_ANSWERS=0;
        int mark = Integer.parseInt(CURRENT_QUESTION.getMark());
        //  int neg_mark = Integer.parseInt(EXAM_NEG_MARK);

        ATTENDED_DATA_STRING = TextUtils.join(",",ATTENDED_QUESTION_DATA);
        ATTENDED_SCOREBOARD_DATA_STRING = TextUtils.join(",",scoreBoardList);


        //scoreBoardList 0 - skipped questions
        //scoreBoardList 1 - right answers
        //scoreBoardList 2 - wrong answers
        //scoreBoardList 9 - un attended questions

        for(int i=0;i<scoreBoardList.length;i++){

            switch (scoreBoardList[i]){
                case "0":
                    TOTAL_NUM_SKIPPED_ANSWERS++;
                    break;
                case "1":
                    TOTAL_SCORE_FROM_CURRENT_EXAM = TOTAL_SCORE_FROM_CURRENT_EXAM+mark;
                    TOTAL_NUM_RIGHT_ANSWERS++;
                    break;
                case "2":
                    // TOTAL_SCORE_FROM_CURRENT_EXAM =TOTAL_SCORE_FROM_CURRENT_EXAM-neg_mark;
                    TOTAL_NUM_WRONG_ANSWERS++;
                    break;
            }


        }
//       Toast.makeText(mContext, "TOTAL SCORE : "+TOTAL_SCORE_FROM_CURRENT_EXAM, Toast.LENGTH_SHORT).show();
        Log.d("SCOREBOARD", "TOTAL_NUM_SKIPPED_ANSWERS: "+TOTAL_NUM_SKIPPED_ANSWERS);
        Log.d("SCOREBOARD", "TOTAL_NUM_RIGHT_ANSWERS: "+TOTAL_NUM_RIGHT_ANSWERS);
        Log.d("SCOREBOARD", "TOTAL_NUM_WRONG_ANSWERS: "+TOTAL_NUM_WRONG_ANSWERS);
        Log.d("SCOREBOARD", "TOTAL_SCORE_FROM_CURRENT_EXAM TOTAL: "+TOTAL_SCORE_FROM_CURRENT_EXAM);

    }


    public void showMcqFinishDialogue(View view) {
        TOTAL_SCORE_FROM_CURRENT_EXAM=0;
        calculateScore();

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window_show_mcq_exam_finish, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        //  popupWindow.setAnimationStyle(R.style.popup_window_animation);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        Button buttonSubmitExam = (Button)popupView.findViewById(R.id.buttonSubmitExam);
        buttonSubmitExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //   submitAndSaveMcq();
             //   submitAndSaveMcq();

//                //Toast.makeText(mContext, "clicked", Toast.LENGTH_SHORT).show();
//                ATTENDED_DATA_STRING = TextUtils.join(",",ATTENDED_QUESTION_DATA);
//
//                Log.d("ATTENDED_DATA_STRING", ": "+ATTENDED_DATA_STRING);
//
//                Intent in = new Intent(mContext,McqExamFinishedView.class);
//                in.putExtra("TOTAL_SCORE_FROM_CURRENT_EXAM",""+TOTAL_SCORE_FROM_CURRENT_EXAM);
//                in.putExtra("TOTAL_NUM_RIGHT_ANSWERS",""+TOTAL_NUM_RIGHT_ANSWERS);
//                in.putExtra("TOTAL_NUM_WRONG_ANSWERS",""+TOTAL_NUM_WRONG_ANSWERS);
//                in.putExtra("TOTAL_NUM_SKIPPED_ANSWERS",""+TOTAL_NUM_SKIPPED_ANSWERS);
//                in.putExtra("TOTAL_SCORE_FROM_CURRENT_EXAM",""+TOTAL_SCORE_FROM_CURRENT_EXAM);
//                in.putExtra("TOTAL_NUM_QUESTIONS",""+TOTAL_NUM_QUESTIONS);
//                in.putExtra("EXAM_TOTAL_MARK",""+TOTAL_QBANK_SCORE);
//
//                startActivity(in);
//                finish();
            }
        });

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void selectIfAlreadySelected(){
        int answer = Integer.parseInt(ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())]);

        // Toast.makeText(mContext, ""+answer, Toast.LENGTH_SHORT).show();

        if(!ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())].equals("99") ||
                !ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())].equals("88")){
            // buttonNext.setText("Next");
            // clearSelections();
            switch (answer){
                case 0:
                    //answer1.callOnClick();
                    selectAlreadyAttenedeQ(0);
                    break;
                case 1:
                    //answer2.callOnClick();
                    selectAlreadyAttenedeQ(1);
                    break;
                case 2:
                    //answer3.callOnClick();
                    selectAlreadyAttenedeQ(2);
                    break;
                case 3:
                    //answer4.callOnClick();
                    selectAlreadyAttenedeQ(3);
                    break;

            }

        }

    }

    public void selectAlreadyAttenedeQ(int answer) {

        ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())]=""+answer;
        try {
            JSONArray ansArray = new JSONArray(CURRENT_QUESTION.getAnswer());
            int right_answer = Integer.parseInt(ansArray.getString(0));
            answer++;
            Log.d("ANSWER_CHECK", "CLICKED "+answer);
            Log.d("ANSWER_CHECK", "RIGHT_ANSWER "+right_answer);

            int cur_index = Integer.parseInt(CURRENT_QUESTION.getIndex());

            //  userSelectedOption[cur_index]=answer;

            if(answer==right_answer){
                setAsTheRightAnswer2(getOptionsButtonCardView().get(answer-1),getAnswersList().get(answer-1),true);
                scoreBoardArray(Integer.parseInt(CURRENT_QUESTION.getIndex()),true);
            }else {
                setAsTheRightAnswer2(getOptionsButtonCardView().get(answer-1),getAnswersList().get(answer-1),false);
                setAsTheRightAnswer3(getOptionsButtonCardView().get(right_answer-1),getAnswersList().get(right_answer-1),true);
                scoreBoardArray(Integer.parseInt(CURRENT_QUESTION.getIndex()),false);
            }
        }catch (JSONException j){

        }

    }


    public void showGridPopupQuestion(View view) {
        TOTAL_SCORE_FROM_CURRENT_EXAM=0;
        calculateScore();

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.poupup_layout_question_mcq_grid_view, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
           popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);


        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext,5);
        RecyclerView gridViewRcView = (RecyclerView)popupView.findViewById(R.id.gridViewRcView);
        gridViewRcView.setLayoutManager(gridLayoutManager);

        GridViewQuestionsAdapterMcq adapterMcq = new GridViewQuestionsAdapterMcq(mContext, questionArrayList, scoreBoardList, new GridViewQuestionsAdapterMcq.EventListener() {
            @Override
            public void onCategoryClicked(McqQuestion question, int index) {
                CURRENT_INDEX = Integer.parseInt(question.getIndex());
                getSingleQuestionData(questionArrayList.get(index));
                popupWindow.dismiss();

            }
        },0);

        gridViewRcView.setAdapter(adapterMcq);


        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    private void setAnswerSkippedSelection(){

        Log.d("skippedQuestionsArray1", "VALUE CHANGE OF "+CURRENT_INDEX +"to 1");

        JSONArray ansArray = null;
        try {
            ansArray = new JSONArray(CURRENT_QUESTION.getAnswer());
            int right_answer = Integer.parseInt(ansArray.getString(0));
            right_answer--;
            setAsTheRightAnswer2(getOptionsButtonCardView().get(right_answer),getAnswersList().get(right_answer),true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void getAttendedExamData(){
      //  showProgessD(true,"Loading Questions..");
        ProfileUtils profileUtils = new ProfileUtils(mContext);
        UserSession userSession = profileUtils.getUserSession();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigServer.API_GET_ATTEND_MCQ_DATA)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        WebServiceApi webServiceApi = retrofit.create(WebServiceApi.class);
        Call<String> call = webServiceApi.getAttendMcqData(ConfigServer.API_HASH,
                CATEGORY_ID, userSession.getUSER_SESSION_ID());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                showLoading(false);

                try{
                    JSONObject jsonObject = new JSONObject(response.body());

                    String responseStatus = jsonObject.getString("status");


                    if(responseStatus.equals("200")){

                        showProgessD(true,"Resuming your last session, Please wait..");

                        JSONObject jsonResponse = jsonObject.getJSONObject("response");

                        String attend_id = jsonResponse.getString("attend_id");
                        String mcq_id = jsonResponse.getString("mcq_id");
                        String registration_id = jsonResponse.getString("registration_id");
                        String attended_data = jsonResponse.getString("attended_data");
                        String scoreboard_data = jsonResponse.getString("scoreboard_data");
                        String mark_optained = jsonResponse.getString("mark_optained");
                        String total_questions = jsonResponse.getString("total_questions");
                        String answered = jsonResponse.getString("answered");
                        String right_answer = jsonResponse.getString("right_answer");
                        String wrong_answer = jsonResponse.getString("wrong_answer");
                        String skipped = jsonResponse.getString("skipped");
                        String points = jsonResponse.getString("points");
                        String total_score = jsonResponse.getString("total_score");
                        String status = jsonResponse.getString("status");
                        String published = jsonResponse.getString("published");
                        String last_attended_question = jsonResponse.getString("last_attended_question");
                        String finish = jsonResponse.getString("finish");
                        String created_at = jsonResponse.getString("created_at");
                        String updated_at = jsonResponse.getString("updated_at");

                        ATTENDED_QUESTION_DATA = attended_data.split(",");
                        TOTAL_NUM_RIGHT_ANSWERS = Integer.parseInt(right_answer);
                        TOTAL_NUM_WRONG_ANSWERS = Integer.parseInt(wrong_answer);
                        TOTAL_NUM_SKIPPED_ANSWERS = Integer.parseInt(skipped);
                        TOTAL_SCORE_FROM_CURRENT_EXAM = Integer.parseInt(total_score);

                        scoreBoardList = scoreboard_data.split(",");

                        Log.d("scoreboard_data", "22: "+scoreboard_data);

                        gotoQuestionByIndex(0);  //RESUME LAST QUESTION

                        // setColoursForBubble();
                        indexAdapter.notifyDataSetChanged();
                        rcViewIndex.setAdapter(indexAdapter);
                        indexAdapter.notifyDataSetChanged();

                    }else {
                        isResumed=true;
                        showProgessD(false,"");
                        gotoQuestionByIndex(0);  //RESUME LAST QUESTION

                    }



                }catch (JSONException e){
                    showProgessD(false,"");
                }


                //  refreshRcViewIndex();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Toast.makeText(ReviewMcq.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("ejfdhek", "onFailure: "+t.getMessage());

            }
        });
    }


    public void refreshRcViewIndex(int CURRENT_INDEX){
        indexAdapter = new IndexNumberAdapterMcq2(mContext, questionArrayList,scoreBoardList,CURRENT_INDEX, new IndexNumberAdapterMcq2.EventListener() {
            @Override
            public void onCategoryClicked(McqQuestion question, int index, RelativeLayout view) {

                getSingleQuestionData(question);


            }

        },0);

        rcViewIndex.swapAdapter(indexAdapter,false);
        indexAdapter.notifyDataSetChanged();

    }

    public void gotoQuestionByIndex(final int index){

        //indexLayoutManager.scrollToPositionWithOffset(index,0);
        showProgessD(false, "");
        if (!isResumed) {
            getSingleQuestionData(questionArrayList.get(index));
            isResumed=true;
        }
    }



    public void showProgessD(boolean stat,String msg){
        if(progressDialog==null){
            progressDialog = new ProgressDialog(mContext);
        }

        progressDialog.setMessage(msg);

        if(stat){
            if(!progressDialog.isShowing()){
                progressDialog.show();
            }
        }else {
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }

    }

    public void setPrimeIndexNumber(int number){
        //PRIME INDEX NUMBER IS THE HIGHEST INDEX NUMBER FROM  THE QUESTIONS THAT YOU ATTENDED.
        if(number!=0){
            if(number>PRIME_INDEX_NUMBER){
                PRIME_INDEX_NUMBER=number;
            }
        }

        //   Toast.makeText(mContext, "PRIME : "+PRIME_INDEX_NUMBER, Toast.LENGTH_SHORT).show();
    }

}
