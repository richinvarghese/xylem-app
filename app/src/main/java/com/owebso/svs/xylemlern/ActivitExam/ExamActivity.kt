package com.owebso.svs.xylemlern.ActivitExam

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import com.owebso.svs.xylemlern.ActivitExam.ExamActivity
import com.owebso.svs.xylemlern.R
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer
import com.owebso.svs.xylemlern.adapters.GridViewQuestionsAdapterExam
import com.owebso.svs.xylemlern.adapters.IndexNumberAdapterExam
import com.owebso.svs.xylemlern.functionUtils.ImageUtils
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions
import com.owebso.svs.xylemlern.functionUtils.WebServiceApi
import com.owebso.svs.xylemlern.functionUtils.ZonStringFunctions
import com.owebso.svs.xylemlern.models.ChoiceOption
import com.owebso.svs.xylemlern.models.ExamQuestion
import com.owebso.svs.xylemlern.profile.ProfileUtils
import com.owebso.svs.xylemlern.profile.UserSession
import kotlinx.android.synthetic.main.activity_exam.*
import kotlinx.android.synthetic.main.custom_layout_mcq_single_question.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.jsoup.Jsoup
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class ExamActivity : AppCompatActivity() {
    var questionArrayList: ArrayList<ExamQuestion>? = null
    var mContext: Context = this@ExamActivity
    var API_RESPONSE = ""
    var CURRENT_QUESTION: ExamQuestion? = null
    var imageUtils: ImageUtils? = null
    var CURRENT_INDEX = 0
    var EXAM_ID = ""
    var TOTAL_SCORE_FROM_CURRENT_EXAM = 0
    lateinit var scoreBoardList: Array<String?>
    var EXAM_NEG_MARK = ""
    var EXAM_TOTAL_MARK = ""
    var TOTAL_NUM_QUESTIONS = 0
    var TOTAL_NUM_RIGHT_ANSWERS = 0
    var TOTAL_NUM_WRONG_ANSWERS = 0
    var TOTAL_NUM_SKIPPED_ANSWERS = 0
    var selectionsArrayList: ArrayList<Int>? = null
    var EXAM_PASS_MARK = 0
    var API_RESPONSE_POST_EXAM_RESULT = ""
    var TOTAL_NEGATIVE_POINTS = 0
    var TOTAL_POSITIVE_POINTS = 0
    lateinit var ATTENDED_QUESTION_DATA: Array<String?>
    var ATTENDED_DATA_STRING = ""
    var ATTENDED_SCOREBOARD_STRING = ""
    var API_RESPONSE_CHECK_EXAM_ATT = ""
    var onScrollListener: RecyclerView.OnScrollListener? = null
    var userSession: UserSession? = null
    var textViewCurrentCount: TextView? = null
    var tv_timer: TextView? = null
    var indexLayoutManager: LinearLayoutManager? = null
    //timer
    var diff: Long = 0
    var oldLong: Long = 0
    var NewLong: Long = 0
    var webServiceApi: WebServiceApi? = null
    var adapterExam: IndexNumberAdapterExam? = null
    var EXAM_DURATION = 0
    var LAST_VIEWED_QUESTION_INDEX = "0"
    var isTimesUp = false
    var EXAM_END_DATE_TIME = ""
    var temp = 5
    lateinit var MARKED_QUESTIONS_ARRAY: Array<String?>
    var isPanelShown = false
    var hiddenPanel: ViewGroup? = null
    var imageViewMoreOptions: ImageView? = null
    var question_img=""
    var option_img1=""
    var option_img2=""
    var option_img3=""
    var option_img4=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam)
        ButterKnife.bind(this)
        //  CATEGORY_ID = getIntent().getStringExtra("CATEGORY_ID");
        EXAM_ID = intent.getStringExtra("EXAM_ID")
        EXAM_NEG_MARK = intent.getStringExtra("EXAM_NEG_MARK")
        EXAM_TOTAL_MARK = intent.getStringExtra("EXAM_TOTAL_MARK")
        EXAM_DURATION = intent.getStringExtra("EXAM_DURATION").toInt()
        EXAM_PASS_MARK = intent.getStringExtra("EXAM_PASS_MARK").toInt()
        hideActionBar()
        initViews()
        initializeClicks()
        questionList
    }
    fun hideActionBar() {
        try {
            supportActionBar!!.hide()
        } catch (e: Exception) {
            try {
                actionBar.hide()
            } catch (e1: Exception) {
                e1.printStackTrace()
            }
        }
    }
    fun initViews() {
        isPanelShown = false
        imageUtils = ImageUtils(mContext)
        selectionsArrayList = ArrayList()
        val profileUtils = ProfileUtils(mContext)
        userSession = profileUtils.userSession

        layoutHintButton!!.visibility = View.GONE
        indexLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        rcViewIndex!!.layoutManager = indexLayoutManager

        textViewCurrentCount = findViewById<View>(R.id.textViewCurrentCount) as TextView
        tv_timer = findViewById<View>(R.id.tv_timer) as TextView
        imageViewMoreOptions = findViewById<View>(R.id.imageViewMoreOptions) as ImageView
        layoutAnswer!!.visibility = View.GONE
        layoutHint!!.visibility = View.GONE
        layoutMarkQuestion!!.setOnClickListener {
            if (MARKED_QUESTIONS_ARRAY[CURRENT_INDEX] == "1") {
                MARKED_QUESTIONS_ARRAY[CURRENT_INDEX] = "0"
            } else {
                MARKED_QUESTIONS_ARRAY[CURRENT_INDEX] = "1"
            }
            markQuestion()
        }
        buttonNext!!.setOnClickListener { v ->
            saveExamDataOnClick()
           /* if (ATTENDED_QUESTION_DATA[CURRENT_INDEX] == "99") {
                ATTENDED_QUESTION_DATA[CURRENT_INDEX] = "88"
            }*/
            gotoQuestion(1, v)
        }
        imageViewMoreOptions!!.setOnClickListener { v -> showExamMoreOptions(v) }
        onScrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                    }
                }
            }
        }
    }
    inner class ExamTimer internal constructor(millisInFuture: Long, countDownInterval: Long) : CountDownTimer(millisInFuture, countDownInterval) {
        override fun onFinish() {
            val msg = "TIMES UP!"
            tv_timer!!.text = msg
           // showAlert(fullLayout, msg, "Exit")
        }

        override fun onTick(millisUntilFinished: Long) {

            val hms: String =
                    (TimeUnit.MILLISECONDS.toHours(
                    millisUntilFinished)- TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished))).toString() + ":" +
                    ((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished))).toString() + ":"
                            + (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))))
            tv_timer!!.text = hms
        }
    }
    private fun initializeClicks(){
        clickFinishExam.setOnClickListener {
            saveExamDataOnClick()
            showExitExamPopup(it)
        }
    }
    fun gotoQuestion(direction: Int, view: View?) {
        if (direction == 0) { //prev question
            if (CURRENT_INDEX != 0) {
                CURRENT_INDEX--
                CURRENT_QUESTION = questionArrayList!![CURRENT_INDEX]
                showQuestionView(questionArrayList!![CURRENT_INDEX])
            } else { //  Toast.makeText(mContext, "First Question", Toast.LENGTH_SHORT).show();
            }
        } else { //next question
            if (CURRENT_INDEX < questionArrayList!!.size - 1) {
                CURRENT_INDEX++
                CURRENT_QUESTION = questionArrayList!![CURRENT_INDEX]
                showQuestionView(questionArrayList!![CURRENT_INDEX])
            } else {
                showExamfinishDialogue(view)
            }
        }
    }
    fun showExamfinishDialogue(view: View?) {
        TOTAL_SCORE_FROM_CURRENT_EXAM = 0
        calculateScore()
        // inflate the layout of the popup window
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView = inflater.inflate(R.layout.popup_window_show_exam_finish, null)
        // create the popup window
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT
        val focusable = true // lets taps outside the popup also dismiss it
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        //  popupWindow.setAnimationStyle(R.style.popup_window_animation);
// show the popup window
// which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        val buttonSubmitExam = popupView.findViewById<View>(R.id.buttonSubmitExam) as Button
        buttonSubmitExam.setOnClickListener { submitExam() }
        // dismiss the popup window when touched
        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
    }
    fun changeStatusBarColor() {
        val window = this@ExamActivity.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@ExamActivity, R.color.accentColor1)
    }
    fun initActionBar() {
        val actionBarLayout = layoutInflater.inflate(R.layout.action_bar_04_white_mcq, null) as ViewGroup
        //Set up your ActionBar
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(false)
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.customView = actionBarLayout
        // You customization
        val actionBarColor = resources.getColor(R.color.white)
        actionBar.setBackgroundDrawable(ColorDrawable(actionBarColor))
        val parent = actionBarLayout.parent as Toolbar
        //        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);
        val actionBarTitle = findViewById<View>(R.id.actionBarTitle) as TextView
        actionBarTitle.text = "Exam"
        val togBackButton = findViewById<View>(R.id.togBackButton) as ImageView
        togBackButton.setOnClickListener { finish() }
        //
        val gridViewbutton = findViewById<View>(R.id.gridViewbutton) as ImageView
        gridViewbutton.setOnClickListener { v -> showGridPopupQuestion(v) }
    }
    fun showLog(name: String?, value: String?) {
        Log.d(name, value)
    }
    fun showQuestionView(question: ExamQuestion) { //    Toast.makeText(mContext, "what the !", Toast.LENGTH_SHORT).show();
        LAST_VIEWED_QUESTION_INDEX = question.index
        CURRENT_INDEX = LAST_VIEWED_QUESTION_INDEX.toInt()
        val offset = question.index.toInt()
        indexLayoutManager!!.isSmoothScrollbarEnabled = true
        indexLayoutManager!!.scrollToPositionWithOffset(question.index.toInt() - 3, 1)
        refreshRcViewIndex(question.index.toInt())
        var count = question.index.toInt()
        count++
        textViewCurrentCount!!.text = "$count/$TOTAL_NUM_QUESTIONS"
        CURRENT_QUESTION = question
        val index = question.indexInInteger + 1
        tvQuestionIndex!!.text = "" + index
        tvQuestion!!.text = ""+html2text(index.toString() + ". " + question.question)
        //createOptionViewButtons();
        val IMAGE_URL = ConfigServer.PATH_QUESTION_IMAGE + question.question_image
        Log.d("hgvhgvhghv",IMAGE_URL)
        createOptionsCards()
        selectIfAlreadySelected()

        if(question.question.contains("src=\"", ignoreCase = true)) {
            question_img = (question.question.subSequence(matchDetails(question.question, "src=\"", 0) + 5, matchDetails(question.question, "\" />", 0)) as String?).toString()
        }else{
            question_img=""
            imageViewQuestion2!!.visibility = View.GONE
        }

        Log.d("imagetestwjg", "image 2 : " + question_img+"\n"+question.question)


        /* if (ZonStringFunctions.getImagefromQuestionHtml(question.question) == null) {
             imageViewQuestion2!!.visibility = View.GONE
         } else {
             val IMAGE_URL = ConfigServer.PATH_QUESTION_IMAGE + question.question_image
             imageUtils!!.showImage(IMAGE_URL, imageViewQuestion2)
             imageViewQuestion2.setVisibility(View.VISIBLE)
         }*/
        if (question_img == null) {
            imageViewQuestion2!!.visibility = View.GONE
        } else {
            imageUtils!!.showImage(question_img, imageViewQuestion2)
            Log.d("imagetestwjg", "image 2 : " + ZonStringFunctions.getImagefromQuestionHtml(question.question))
        }
      /*  if (question.question_image!= null) {
            val IMAGE_URL = ConfigServer.PATH_QUESTION_IMAGE + question.question_image
            imageUtils!!.showImage(IMAGE_URL, imageViewQuestion2)
            imageViewQuestion2.setVisibility(View.VISIBLE)
        }*/
        showLoading(false)
        fullLayout!!.visibility = View.VISIBLE
        markQuestion()
    }
    val optionsButtonCardView: ArrayList<CardView?> get() {
            val arrayList = ArrayList<CardView?>()
            arrayList.add(answer1)
            arrayList.add(answer2)
            arrayList.add(answer3)
            arrayList.add(answer4)
            return arrayList
        }
    val answersList: ArrayList<TextView?> get() {
            val arrayList = ArrayList<TextView?>()
            arrayList.add(textViewA)
            arrayList.add(textViewB)
            arrayList.add(textViewC)
            arrayList.add(textViewD)
            return arrayList
        }
    fun createOptionsCards() {
        clearSelections()

        var option1 = options[0].option
        var option2 = options[1].option
        var option3 = options[2].option
        var option4 = options[3].option

        if(option1.contains("src=\"", ignoreCase = true)) {
            option_img1 = (option1.subSequence(matchDetails(option1, "src=\"", 0) + 5, matchDetails(option1, "\" />", 0)) as String?).toString()
        }else{
            option_img1=""
        }
        if(option2.contains("src=\"", ignoreCase = true)) {
            option_img2 = (option2.subSequence(matchDetails(option2, "src=\"", 0) + 5, matchDetails(option2, "\" />", 0)) as String?).toString()
        }else{
            option_img2=""
        }
        if(option3.contains("src=\"", ignoreCase = true)) {
            option_img3 = (option3.subSequence(matchDetails(option3, "src=\"", 0) + 5, matchDetails(option3, "\" />", 0)) as String?).toString()
        }else{
            option_img3=""
        }
        if(option4.contains("src=\"", ignoreCase = true)) {
            option_img4 = (option4.subSequence(matchDetails(option4, "src=\"", 0) + 5, matchDetails(option4, "\" />", 0)) as String?).toString()
        }else{
            option_img4=""
        }
       // Log.d("jvfnjdnjfvdv", option_img1)

        //Log.d("option_image", "image 1 : " + ZonStringFunctions.getImagefromQuestionHtml(options[0].option)+"\n"+options[0].option)

        if (option_img1 == null||option_img1=="") {
            imageoption1!!.visibility = View.GONE
            textViewA!!.visibility = View.VISIBLE
            textViewA!!.text = html2text(options[0].option)
        }else {
            if(options[0].option.isNullOrEmpty()){
                textViewA!!.visibility = View.GONE
                imageoption1!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img1,imageoption1)
            }else{
                textViewA!!.visibility = View.VISIBLE
                textViewA!!.text = html2text(options[0].option)
                imageoption1!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img1, imageoption1)
            }
            Log.d("imagetestwjg", "image 2 : " + ZonStringFunctions.getImagefromQuestionHtml(options[0].option))
        }
        if (option_img2 == null||option_img2=="") {
            imageoption2!!.visibility = View.GONE
            textViewB!!.visibility = View.VISIBLE
            textViewB!!.text = html2text(options[1].option)
        } else {
            if(options[1].option.isNullOrEmpty()){
                textViewB!!.visibility = View.GONE
                imageoption2!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img2, imageoption2)
            }else {
                textViewB!!.visibility = View.VISIBLE
                textViewB!!.text = html2text(options[1].option)
                imageoption2!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img2, imageoption2)
            }
        }
        if (option_img3== null||option_img3=="") {
            imageoption3!!.visibility = View.GONE
            textViewC!!.visibility = View.VISIBLE
            textViewC!!.text = html2text(options[2].option)
        } else {
        /*    textViewC!!.visibility = View.GONE
            imageoption3!!.visibility = View.VISIBLE
            imageUtils!!.showImage(ZonStringFunctions.getImagefromQuestionHtml(options[2].option), imageoption3)*/
            if(options[2].option.isNullOrEmpty()){
                //Log.d("jasfhhfhkahfia3333333",ZonStringFunctions.getImagefromQuestionHtml(options[2].option))
                textViewC!!.visibility = View.GONE
                imageoption3!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img3, imageoption3)
            }else{
                textViewC!!.visibility = View.VISIBLE
                textViewC!!.text = html2text(options[2].option)
                imageoption3!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img3, imageoption3)
            }


        }
        if (option_img4== null||option_img4=="") {
            imageoption4!!.visibility = View.GONE
            textViewD!!.visibility = View.VISIBLE
            textViewD!!.text = html2text(options[3].option)
        } else {
         /*   textViewD!!.visibility = View.GONE
            imageoption4!!.visibility = View.VISIBLE
            imageUtils!!.showImage(ZonStringFunctions.getImagefromQuestionHtml(options[3].option), imageoption4)
            Log.d("imagetestwjg", "image 2 : " + ZonStringFunctions.getImagefromQuestionHtml(options[3].option))*/
            if(options[3].option.isNullOrEmpty()){
               textViewD!!.visibility = View.GONE
                imageoption4!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img4, imageoption4)
            }else{
                textViewD!!.visibility = View.VISIBLE
                textViewD!!.text = html2text(options[3].option)
                imageoption4!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img4, imageoption4)
            }

        }
       // textViewA!!.text = options[0].option
       // textViewB!!.text = options[1].option
       // textViewC!!.text = options[2].option
        //textViewD!!.text = options[3].option
        answer1!!.setOnClickListener { setMySelectedAnswer(0) }
        answer2!!.setOnClickListener { setMySelectedAnswer(1) }
        answer3!!.setOnClickListener { setMySelectedAnswer(2) }
        answer4!!.setOnClickListener { setMySelectedAnswer(3) }
    }
    val options: ArrayList<ChoiceOption> get() {
            val choiceOptions = ArrayList<ChoiceOption>()
            try {
                val jsonArray = JSONArray(CURRENT_QUESTION!!.options)
                for (i in 0 until jsonArray.length()) {
                   // choiceOptions.add(ChoiceOption(i.toString(), html2text(jsonArray.getString(i))))
                    choiceOptions.add(ChoiceOption(i.toString(), jsonArray.getString(i)))
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return choiceOptions
        }
    fun showLoading(stat: Boolean) {
        if (stat) {
            loadingLayout!!.visibility = View.VISIBLE
            fullLayout!!.visibility = View.GONE
        } else {
            loadingLayout!!.visibility = View.GONE
            fullLayout!!.visibility = View.VISIBLE
        }
    }
    fun html2text(html: String?): String {
        return Jsoup.parse(html).text()
    }
    fun showHint(show: Boolean) {
        if (show) {
            tvHint!!.text = CURRENT_QUESTION!!.hints
            layoutHint!!.visibility = View.VISIBLE
        } else {
            layoutHint!!.visibility = View.GONE
        }
    }
    fun showAnswer(show: Boolean) {
        if (show) {
            try {
                val array = JSONArray(CURRENT_QUESTION!!.answer)
                var answerIndex = array.getInt(0)
                answerIndex--
                val answer = options[answerIndex].option
                tvAnswer!!.text = answer
                tvAnswerExplanation!!.text = html2text(CURRENT_QUESTION!!.explanation)
                layoutAnswer!!.visibility = View.VISIBLE
            } catch (e: JSONException) {
            }
        } else {
            layoutAnswer!!.visibility = View.GONE
        }
    }


    fun setMySelectedAnswer(answercheck: Int) {
        Log.d("fjcnsfncnsncfkkcnss",""+answercheck+"      "+temp)
        var answer = answercheck
        ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()] = "" + answer
        buttonNext!!.text = "Next" //setting selected answer so changing the button from "skip" to "next"
        try {
            selectionsArrayList!!.add(CURRENT_QUESTION!!.index.toInt(), answer)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
        try {
            val ansArray = JSONArray(CURRENT_QUESTION!!.answer)
            val right_answer = ansArray.getString(0).toInt()
            answer++
            Log.d("ANSWER_CHECK", "CLICKED $answer")
            Log.d("ANSWER_CHECK", "RIGHT_ANSWER $right_answer")
            if(temp==answercheck){
                clearSelections()
                temp =5
            }else{
                temp = answercheck
                clearSelections()
                setSelectedAnswer(optionsButtonCardView[answer - 1], answersList[answer - 1], true)
            }

            if (answer == right_answer) { //Toast.makeText(mContext, "RIGHT ANSWER", Toast.LENGTH_SHORT).show();
//TOTAL_SCORE_FROM_CURRENT_EXAM = TOTAL_SCORE_FROM_CURRENT_EXAM+Integer.parseInt(CURRENT_QUESTION.getMark());
                scoreBoardArray(CURRENT_QUESTION!!.index.toInt(), true)
                //Toast.makeText(mContext, ""+CURRENT_QUESTION.getIndex(), Toast.LENGTH_SHORT).show();
            } else {
                scoreBoardArray(CURRENT_QUESTION!!.index.toInt(), false)
                //Toast.makeText(mContext, "WRONG ANSWER", Toast.LENGTH_SHORT).show();
            }
        } catch (j: JSONException) {
        }
        saveExamDataOnClick()
    }

    fun setSelectedAnswer(cardView: CardView?, tv: TextView?, stat: Boolean) {
        if (stat) {
            cardView!!.setCardBackgroundColor(resources.getColor(R.color.selected_answer))
            tv!!.setTextColor(resources.getColor(R.color.white))
            showAnswer(false)
        } else {
            showAnswer(true)
            cardView!!.setCardBackgroundColor(resources.getColor(R.color.white))
            tv!!.setTextColor(resources.getColor(R.color.accentColor1))
        }
    }

    fun clearSelections() {
        for (i in optionsButtonCardView.indices) {
            optionsButtonCardView[i]!!.setCardBackgroundColor(resources.getColor(R.color.action_bar))
            answersList[i]!!.setTextColor(resources.getColor(R.color.accentColor1))
        }
    }// showLoading(false);// String tags = question.getString("tags");
    // String created_at = question.getString("created_at");
// String updated_at = question.getString("updated_at");
    //   EXAM_DURATION = Integer.parseInt(question.getString("duration"));
    // showQuestionView(questionArrayList.get(0));
// showLoading(false);
    //  Toast.makeText(mContext, "SIZE : "+jsonArray.length(), Toast.LENGTH_SHORT).show();

    // Toast.makeText(mContext, "hai how are u", Toast.LENGTH_SHORT).show();
    val questionList: Unit get() { // Toast.makeText(mContext, "hai how are u", Toast.LENGTH_SHORT).show();
            showLoading(true)
            questionArrayList = ArrayList()
            val post_data = HashMap<String, String>()
            post_data["hash_code"] = ConfigServer.API_HASH
            post_data["exam_id"] = EXAM_ID
            val URL = ConfigServer.API_GET_NEW_EXAM_QUESTIONS
            val olf = OnlineFunctions(mContext, URL, post_data)
            val handler = Handler()
            val runnable = Runnable {
                API_RESPONSE = olf.pOstResult
                handler.post {
                    showLog("API_RESADSPONSE", API_RESPONSE)
                    try {
                        val jsonObject = JSONObject(API_RESPONSE)
                        val status = jsonObject.getString("status").toInt()
                        if (status == 200) { // showLoading(false);
                            val jsonArray = jsonObject.getJSONArray("response")
                            //  Toast.makeText(mContext, "SIZE : "+jsonArray.length(), Toast.LENGTH_SHORT).show();
                            TOTAL_NUM_QUESTIONS = jsonArray.length()
                            ATTENDED_QUESTION_DATA = arrayOfNulls(TOTAL_NUM_QUESTIONS)
                            MARKED_QUESTIONS_ARRAY = arrayOfNulls(TOTAL_NUM_QUESTIONS)
                            scoreBoardList = arrayOfNulls(TOTAL_NUM_QUESTIONS)
                            Arrays.fill(scoreBoardList, "0")
                            Arrays.fill(ATTENDED_QUESTION_DATA, "99")
                            Arrays.fill(MARKED_QUESTIONS_ARRAY, "0")
                            for (i in 0 until jsonArray.length()) {
                                val objquestion = jsonArray.getJSONObject(i)
                                val questionbanks_id = objquestion.getString("questionbanks_id")
                                val category_id = objquestion.getString("category_id")
                                val questionQ = objquestion.getString("question")
                                val question_image = objquestion.getString("question_image")
                                val explanation = objquestion.getString("explanation")
                                val hints = objquestion.getString("hints")
                                val mark = objquestion.getString("mark")
                                val questiontype = objquestion.getString("questiontype")
                                val year = objquestion.getString("year")
                                val usertype = objquestion.getString("usertype")
                                // String tags = question.getString("tags");
                                val questionbanks_status = objquestion.getString("questionbanks_status")
                                // String created_at = question.getString("created_at");
                                // String updated_at = question.getString("updated_at");
                                val no_of_options = objquestion.getString("no_of_options")
                                val options = objquestion.getString("options")

                                Log.d("jdnjfvndjnvjdn",options)
                                val answer = objquestion.getString("answer")
                                // EXAM_DURATION = Integer.parseInt(question.getString("duration"));
                                val index = i + 1
                                questionArrayList!!.add(ExamQuestion("" + i, questionbanks_id, category_id, questionQ, question_image
                                        , explanation, hints, mark, questiontype, year, usertype, questionbanks_status, no_of_options, options, answer))
                            }
                            // showQuestionView(questionArrayList.get(0));
                            checkExamStatus()
                        } else { // showLoading(false);
                            Toast.makeText(mContext, "Exam Not Ready Yet.", Toast.LENGTH_SHORT).show()
                            finish()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
            Thread(runnable).start()
        }

    fun refreshRcViewIndex(CURRENT_INDEX: Int) {
        adapterExam = IndexNumberAdapterExam(mContext, questionArrayList, CURRENT_INDEX,
                IndexNumberAdapterExam.EventListener { question, pos -> showQuestionView(questionArrayList!![pos]) }, 0)
        rcViewIndex!!.swapAdapter(adapterExam, false)
        adapterExam!!.notifyDataSetChanged()
    }

    fun scoreBoardArray(questionNo: Int, stat: Boolean) {
        if (stat) {
            scoreBoardList[questionNo] = "1"
        } else {
            scoreBoardList[questionNo] = "2"
        }
    }

    fun calculateScore() {
        TOTAL_SCORE_FROM_CURRENT_EXAM = 0
        TOTAL_NUM_RIGHT_ANSWERS = 0
        TOTAL_NUM_WRONG_ANSWERS = 0
        TOTAL_NUM_SKIPPED_ANSWERS = 0
        ATTENDED_DATA_STRING = TextUtils.join(",", ATTENDED_QUESTION_DATA)
        ATTENDED_SCOREBOARD_STRING = TextUtils.join(",", scoreBoardList)
        val mark = CURRENT_QUESTION!!.mark.toInt()
        val neg_mark = EXAM_NEG_MARK.toInt()
        for (i in scoreBoardList.indices) {
            when (scoreBoardList[i]!!.toInt()) {
                0 -> TOTAL_NUM_SKIPPED_ANSWERS++
                1 -> {
                    TOTAL_SCORE_FROM_CURRENT_EXAM = TOTAL_SCORE_FROM_CURRENT_EXAM + mark
                    TOTAL_NUM_RIGHT_ANSWERS++
                    TOTAL_POSITIVE_POINTS = TOTAL_POSITIVE_POINTS + mark
                }
                2 -> {
                    TOTAL_SCORE_FROM_CURRENT_EXAM = TOTAL_SCORE_FROM_CURRENT_EXAM - neg_mark
                    TOTAL_NUM_WRONG_ANSWERS++
                    TOTAL_NEGATIVE_POINTS = TOTAL_NEGATIVE_POINTS + neg_mark
                }
            }
        }
        //       Toast.makeText(mContext, "TOTAL SCORE : "+TOTAL_SCORE_FROM_CURRENT_EXAM, Toast.LENGTH_SHORT).show();
        Log.d("SCOREBOARD", "TOTAL_NUM_SKIPPED_ANSWERS: $TOTAL_NUM_SKIPPED_ANSWERS")
        Log.d("SCOREBOARD", "TOTAL_NUM_RIGHT_ANSWERS: $TOTAL_NUM_RIGHT_ANSWERS")
        Log.d("SCOREBOARD", "TOTAL_NUM_WRONG_ANSWERS: $TOTAL_NUM_WRONG_ANSWERS")
        Log.d("SCOREBOARD", "TOTAL_SCORE_FROM_CURRENT_EXAM TOTAL: $TOTAL_SCORE_FROM_CURRENT_EXAM")
    }

    fun getExamStatus(passmark: Int, your_score: Int): String {
        var status = ""
        status = if (your_score >= passmark) {
            "P"
        } else {
            "F"
        }
        return status
    }
    fun submitExam() { //   Toast.makeText(mContext, "called submitexam", Toast.LENGTH_SHORT).show();

   /*     Log.d("kfmkeejhfiueeurwiudoi",EXAM_ID+"\nanswered" + (TOTAL_NUM_RIGHT_ANSWERS + TOTAL_NUM_WRONG_ANSWERS)+
                "\nright_answer"+ TOTAL_NUM_RIGHT_ANSWERS+"\nwrong_answer" + TOTAL_NUM_WRONG_ANSWERS+"\nskipped" + TOTAL_NUM_SKIPPED_ANSWERS
        +"\npoints" + TOTAL_SCORE_FROM_CURRENT_EXAM+" \nmark_optained" + TOTAL_POSITIVE_POINTS+
                "\ntotal_questions"+ (TOTAL_NUM_WRONG_ANSWERS + TOTAL_NUM_RIGHT_ANSWERS + TOTAL_NUM_SKIPPED_ANSWERS)
        +"\nnegative_points"+ "" + TOTAL_NUM_WRONG_ANSWERS * EXAM_NEG_MARK.toInt()+"\nstatus"+ "1"+
                "\nattended_data"+ ATTENDED_DATA_STRING+"\nscoreboard_data"+ ATTENDED_SCOREBOARD_STRING+
                "\nmarked_question_data"+TextUtils.join(",", MARKED_QUESTIONS_ARRAY))*/

        showLoading(true)
        val post_data = HashMap<String, String>()
        post_data["hash_code"] = ConfigServer.API_HASH
        post_data["exam_id"] = EXAM_ID
        post_data["student_id"] = userSession!!.useR_SESSION_ID
        post_data["answered"] = "" + (TOTAL_NUM_RIGHT_ANSWERS + TOTAL_NUM_WRONG_ANSWERS)
        post_data["right_answer"] = "" + TOTAL_NUM_RIGHT_ANSWERS
        post_data["wrong_answer"] = "" + TOTAL_NUM_WRONG_ANSWERS
        post_data["skipped"] = "" + TOTAL_NUM_SKIPPED_ANSWERS
        post_data["points"] = "" + TOTAL_SCORE_FROM_CURRENT_EXAM
        post_data["mark_optained"] = "" + TOTAL_POSITIVE_POINTS
        post_data["total_questions"] = "" + (TOTAL_NUM_WRONG_ANSWERS + TOTAL_NUM_RIGHT_ANSWERS + TOTAL_NUM_SKIPPED_ANSWERS)
        var NEG = "" + TOTAL_NUM_WRONG_ANSWERS * EXAM_NEG_MARK.toInt()
        NEG = NEG.replace("-", "")
        post_data["negative_points"] = NEG
        post_data["status"] = "1"
        post_data["attended_data"] = ATTENDED_DATA_STRING
        post_data["scoreboard_data"] = ATTENDED_SCOREBOARD_STRING
        post_data["marked_question_data"] = TextUtils.join(",", MARKED_QUESTIONS_ARRAY)
        //getExamStatus(EXAM_PASS_MARK, TOTAL_SCORE_FROM_CURRENT_EXAM) get F or P
        val URL = ConfigServer.API_POST_EXAM_RESULT
        val olf = OnlineFunctions(mContext, URL, post_data)
        val handler = Handler()
        val runnable = Runnable {
            API_RESPONSE_POST_EXAM_RESULT = olf.pOstResult
            handler.post {
                showLoading(false)
                showLog("API_RESPONSE_PO", API_RESPONSE_POST_EXAM_RESULT)
                try {
                    val jsonObject = JSONObject(API_RESPONSE_POST_EXAM_RESULT)
                    val status = jsonObject.getString("status")
                    if (status == "200") {
                        val `in` = Intent(mContext, ExamSubmittedView::class.java)
                        startActivity(`in`)
                        finish()
                    } else {
                        Toast.makeText(mContext, "Something went wrong. Try Resubmitting again", Toast.LENGTH_SHORT).show()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
        Thread(runnable).start()
    }

    fun checkExamStatus() {
        showLoading(true)
        val post_data = HashMap<String, String>()
        post_data["hash_code"] = ConfigServer.API_HASH
        post_data["exam_id"] = EXAM_ID
        post_data["student_id"] = userSession!!.useR_SESSION_ID
        val URL = ConfigServer.API_CHECK_EXAM_ATTENDANCE
        val olf = OnlineFunctions(mContext, URL, post_data)
        val handler = Handler()
        val runnable = Runnable {
            API_RESPONSE_CHECK_EXAM_ATT = olf.pOstResult
            handler.post {
                try {
                    showLog("skdadadadadghwqkd",EXAM_ID+"   "+userSession!!.useR_SESSION_ID +"\n"+ API_RESPONSE_CHECK_EXAM_ATT)
                    val jsonObject = JSONObject(API_RESPONSE_CHECK_EXAM_ATT)
                    val status = jsonObject.getString("status")
                    if (status == "200") {
                        val code = jsonObject.getString("code")
                        if (code == "2") { //exam not attended
//  getQuestionList();
                            startTimerExam(true) //START FRESH TIMER
                            showQuestionView(questionArrayList!![0])
                        } else {
                            val result = jsonObject.getJSONObject("result")
                            val attend_data = result.getString("attended_data")
                            val exam_status = result.getString("status").toInt()
                            val startTime = result.getString("start_time")
                            val endTime = result.getString("end_time")
                            val last_index = result.getString("last_index").toInt()
                            val attended_data = result.getString("attended_data")
                            val scoreboard_data = result.getString("scoreboard_data")
                            val marked_question_data = result.getString("marked_question_data")
                            EXAM_END_DATE_TIME = endTime
                            startTimerExam(false)
                            when (exam_status) {
                                0 ->  //  Toast.makeText(mContext, "exam status : 0000", Toast.LENGTH_SHORT).show();
                                    showQuestionView(questionArrayList!![0])
                                1 -> {
                                    finish()
                                   // showQuestionView(questionArrayList!![0])
                                }
                                2 -> if (isTimesUp(endTime)) { //EXAM TIMES UP
//  Toast.makeText(mContext, "TIMES UP", Toast.LENGTH_SHORT).show();
                                    Log.d("timekwgd", "startTime: $startTime")
                                    Log.d("timekwgd", "endTime: $endTime")
                                    finish()
                                } else {
                                    Toast.makeText(mContext, "Resuming Exam..", Toast.LENGTH_SHORT).show()
                                    //you have some times left /RESUMING EXAM
//goto question for resume
                                    ATTENDED_QUESTION_DATA = attended_data.split(",").toTypedArray()
                                    scoreBoardList = scoreboard_data.split(",").toTypedArray()
                                    MARKED_QUESTIONS_ARRAY = marked_question_data.split(",").toTypedArray()
                                    showQuestionView(questionArrayList!![last_index])
                                    Log.d("dqjhdjkw", "lastindex : $last_index")
                                    Log.d("dqjhdjkw", "size : " + questionArrayList!!.size)
                                }
                            }
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
        Thread(runnable).start()
    }

    fun selectIfAlreadySelected() {
        try {
            if (ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()] != "99" &&
                    ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()] != "88") {
                buttonNext!!.text = "Next"
                val answer = ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()]!!.toInt()

                clearSelections()
                setSelectedAnswer(optionsButtonCardView[answer], answersList[answer], true)
            } else {
                buttonNext!!.text = "Skip"
            }
        } catch (e: Exception) {
         //   Log.d("sfijehre", "selectIfAlreadySelected: " + ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()])
            //    Toast.makeText(mContext, "Exception", Toast.LENGTH_SHORT).show();
        }
    }

    fun showGridPopupQuestion(view: View?) {
        TOTAL_SCORE_FROM_CURRENT_EXAM = 0
        calculateScore()
        // inflate the layout of the popup window
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView = inflater.inflate(R.layout.poupup_layout_question_exam_grid_view, null)
        // create the popup window
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT
        val focusable = true // lets taps outside the popup also dismiss it
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        //  popupWindow.setAnimationStyle(R.style.popup_window_animation);
// show the popup window
// which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        val gridLayoutManager = GridLayoutManager(mContext, 5)
        val gridViewRcView = popupView.findViewById<View>(R.id.gridViewRcView) as RecyclerView
        gridViewRcView.layoutManager = gridLayoutManager
        val adapterExam = GridViewQuestionsAdapterExam(mContext,
                questionArrayList, ATTENDED_QUESTION_DATA, MARKED_QUESTIONS_ARRAY,
                GridViewQuestionsAdapterExam.EventListener { question, index ->
                    CURRENT_INDEX = question.index.toInt()
                    showQuestionView(question)
                    popupWindow.dismiss()
                }, 0)
        gridViewRcView.adapter = adapterExam
        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
    }

    fun startTimerExam(fresh: Boolean) {
        val currentTime = Calendar.getInstance().time
        val spf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val calendar = Calendar.getInstance()
        calendar.time = currentTime
        calendar.add(Calendar.MINUTE, EXAM_DURATION)
        val startTime = spf.format(currentTime)
        val endTime = spf.format(calendar.time)
        if (fresh) {
            setExamTime(startTime, endTime)
            startTimer(startTime, endTime)
        } else {
            startTimer(startTime, EXAM_END_DATE_TIME)
        }
    }

    fun isTimesUp(endT: String?): Boolean {
        val currentTime = Calendar.getInstance().time
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try { //Date sTime = format.parse(timeS);
            val eTime = format.parse(endT)
            isTimesUp = if (currentTime.after(eTime)) {
                true
            } else {
                false
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return isTimesUp
    }

    fun setExamTime(startTime: String?, endTime: String?) {
        ATTENDED_DATA_STRING = TextUtils.join(",", ATTENDED_QUESTION_DATA)
        ATTENDED_SCOREBOARD_STRING = TextUtils.join(",", scoreBoardList)
        Log.d("sdjehdewd", "ATTENDED_DATA_STRING : $ATTENDED_DATA_STRING")
        Log.d("sdjehdewd", "ATTENDED_SCOREBOARD_STRING : $ATTENDED_SCOREBOARD_STRING")
        val retrofit = Retrofit.Builder()
                .baseUrl(ConfigServer.API_POST_EXAM_START_END_TIME)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
        webServiceApi = retrofit.create(WebServiceApi::class.java)
        val call = webServiceApi!!.setExamTime(ConfigServer.API_HASH,
                EXAM_ID, userSession!!.useR_SESSION_ID, startTime, endTime, "2",
                LAST_VIEWED_QUESTION_INDEX, ATTENDED_DATA_STRING, ATTENDED_SCOREBOARD_STRING)
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) { //showLoading(false);
                try {
                    val jsonObject = JSONObject(response.body())
                    Log.d("sdjehdewd", "onResponse: " + response.body())
                } catch (je: Exception) {
                    je.printStackTrace()
                }
            }
            override fun onFailure(call: Call<String>, t: Throwable) {}
        })
    }
    private fun saveExamDataOnClick() { //Save Exam attended question and data ,when everytime user click on 1/4 option
        ATTENDED_DATA_STRING = TextUtils.join(",", ATTENDED_QUESTION_DATA)
        ATTENDED_SCOREBOARD_STRING = TextUtils.join(",", scoreBoardList)
        val retrofit = Retrofit.Builder()
                .baseUrl(ConfigServer.API_POST_EXAM_UPDATE_ONCLICK)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
        webServiceApi = retrofit.create(WebServiceApi::class.java)

        val call = webServiceApi!!.updateExamOnClick(ConfigServer.API_HASH, EXAM_ID, userSession!!.useR_SESSION_ID, "2",
                LAST_VIEWED_QUESTION_INDEX, ATTENDED_DATA_STRING, ATTENDED_SCOREBOARD_STRING, TextUtils.join(",", MARKED_QUESTIONS_ARRAY))
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) { //showLoading(false);
                try {
                    val jsonObject = JSONObject(response.body())
                    Log.d("sdjehdewd", "onResponse: " + response.body())
                } catch (je: Exception) {
                    je.printStackTrace()
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {}
        })
    }

    fun startTimer(startTime: String, endTime: String) {
        Log.d("kjdfghdf", "startTime: $startTime")
        Log.d("kjdfghdf", "endTime: $endTime")
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val oldDate: Date
        val newDate: Date
        try {
            oldDate = formatter.parse(startTime)
            newDate = formatter.parse(endTime)
            oldLong = oldDate.time
            NewLong = newDate.time
            diff = NewLong - oldLong
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val counter = ExamTimer(diff, 1000)
        Log.d("kjdfghdf", "differnce: $diff")
        counter.start()
    }

    fun slideUpDown(view: View?) {
        if (!isPanelShown) { // Show the panel
            val bottomUp = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_up)
            hiddenPanel!!.startAnimation(bottomUp)
            hiddenPanel!!.visibility = View.VISIBLE
            isPanelShown = true
        } else { // Hide the Panel
            val bottomDown = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_up)
            hiddenPanel!!.startAnimation(bottomDown)
            hiddenPanel!!.visibility = View.INVISIBLE
            isPanelShown = false
        }
    }

    fun showExamMoreOptions(view: View?) { // inflate the layout of the popup window
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView = inflater.inflate(R.layout.poupup_layout_exam_options, null)
        // create the popup window
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = true // lets taps outside the popup also dismiss it
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupView.animation = AnimationUtils.loadAnimation(this, R.anim.bottom_up)
        popupWindow.showAtLocation(view, Gravity.END, Gravity.START, Gravity.END)
        val layoutSubmitExam1 = popupView.findViewById<View>(R.id.layoutSubmitExam1) as RelativeLayout
        val layoutExitExam = popupView.findViewById<View>(R.id.layoutExitExam) as RelativeLayout
        val layoutGridView = popupView.findViewById<View>(R.id.layoutGridView) as RelativeLayout
        val layoutErrorReport = popupView.findViewById<View>(R.id.layoutErrorReport) as RelativeLayout
        layoutSubmitExam1.setOnClickListener {
            popupWindow.dismiss()
            showExamfinishDialogue(fullLayout)
        }
        layoutGridView.setOnClickListener {
            popupWindow.dismiss()
            showGridPopupQuestion(fullLayout)
        }
        layoutExitExam.setOnClickListener {
            popupWindow.dismiss()
            saveExamDataOnClick()
            showExitExamPopup(fullLayout)
        }
        // showExamfinishDialogue
// dismiss the popup window when touched
        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
    }

    fun showExitExamPopup(view: View?) { // inflate the layout of the popup window
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView = inflater.inflate(R.layout.popup_layout_exit_exam_prompt, null)
        // create the popup window
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT
        val focusable = true // lets taps outside the popup also dismiss it
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupView.animation = AnimationUtils.loadAnimation(this, R.anim.bottom_up)
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        //  RelativeLayout layoutSubmitExam1 = (RelativeLayout) popupView.findViewById(R.id.layoutSubmitExam1);
        val buttonExitExam = popupView.findViewById<View>(R.id.buttonExitExam) as Button
        val buttonCancelPopup1 = popupView.findViewById<View>(R.id.buttonCancelPopup1) as Button
        buttonExitExam.setOnClickListener {
            popupWindow.dismiss()
            finish()
        }
        buttonCancelPopup1.setOnClickListener {
            Toast.makeText(mContext, "Resuming Exam.", Toast.LENGTH_SHORT).show()
            popupWindow.dismiss()
        }
        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
    }

    fun showAlert(view: View?, msg: String?, exitButtonMsg: String?) { // inflate the layout of the popup window
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView = inflater.inflate(R.layout.poupup_layout_alert_with_button, null)
        // create the popup window
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT
        val focusable = true // lets taps outside the popup also dismiss it
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupView.animation = AnimationUtils.loadAnimation(this, R.anim.bottom_up)
        popupWindow.isOutsideTouchable = false
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        //  RelativeLayout layoutSubmitExam1 = (RelativeLayout) popupView.findViewById(R.id.layoutSubmitExam1);
        val tvAlertMessage = popupView.findViewById<View>(R.id.tvAlertMessage) as TextView
        val tvButton = popupView.findViewById<View>(R.id.tvButton) as TextView
        tvAlertMessage.text = msg
        tvButton.text = exitButtonMsg
        tvButton.setOnClickListener { finish() }
        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
    }

    fun markQuestion() {
        Log.d("hfshfsjfjsjf"," "+CURRENT_INDEX+"   "+MARKED_QUESTIONS_ARRAY.size)
        val markview = findViewById(R.id.viewMark) as View
        val tvMarkTitle = findViewById<View>(R.id.tvMarkTitle) as TextView


        if(MARKED_QUESTIONS_ARRAY.size>CURRENT_INDEX) {
            if (MARKED_QUESTIONS_ARRAY[CURRENT_INDEX] == "1") {
                tvMarkTitle.text = "UnMark"
                markview.background = ResourcesCompat.getDrawable(mContext.resources, R.drawable.marked_question_01, null)
            } else {
                tvMarkTitle.text = "Mark"
                markview.background = ResourcesCompat.getDrawable(mContext.resources, R.drawable.un_marked_question_01, null)
            }
        }else {
            tvMarkTitle.text = "Mark"
            markview.background = ResourcesCompat.getDrawable(mContext.resources, R.drawable.un_marked_question_01, null)
        }
    }


    fun matchDetails(inputString: String, whatToFind: String, startIndex: Int = 0): Int {
        val matchIndex = inputString.indexOf(whatToFind, startIndex)
        return  matchIndex
    }

}
