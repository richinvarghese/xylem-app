package com.owebso.svs.xylemlern.ActivitExam

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.owebso.svs.xylemlern.ActivitExam.ExamIntro
import com.owebso.svs.xylemlern.R
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer
import com.owebso.svs.xylemlern.functionUtils.DateTimeConverter
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions
import com.owebso.svs.xylemlern.functionUtils.WebServiceApi
import com.owebso.svs.xylemlern.models.Exam
import com.owebso.svs.xylemlern.models.McqQuestion
import com.owebso.svs.xylemlern.profile.ProfileUtils
import com.owebso.svs.xylemlern.profile.UserSession
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ExamIntro : AppCompatActivity() {
    var EXAM_ID = ""
    var API_RESPONSE = ""
    var TITLE = ""
    var questionArrayList: ArrayList<McqQuestion>? = null
    var loadingLayout: LinearLayout? = null
    var fullLayout: LinearLayout? = null
    var mContext: Context = this@ExamIntro
    var textViewCount: TextView? = null
    var buttonProceed: Button? = null
    var textViewTitle: TextView? = null
    var textView40: TextView? = null
    var tv_max_mark: TextView? = null
    var tv_pass_mark: TextView? = null
    var webServiceApi: WebServiceApi? = null
    var profileUtils: ProfileUtils? = null
    var userSession: UserSession? = null
    var GOTO_ACTIITY_NO = 1
    var EXAM_TYPE_STRING = ""
    var textViewExamDescription: TextView? = null
    var actionBarTitle: TextView? = null
    var EXAM_NEG_MARK = ""
    var EXAM_TOTAL_MARK = ""
    var EXAM_PASS_MARK = ""
    var EXAM_DURATION = ""
    var CATEGORY_ID = ""
    var isTimesUp = false
    var layoutNoResult: LinearLayout? = null
    var layoutResultPublished: LinearLayout? = null
    var API_RESPONSE_GET_CATEGORY = ""
    var API_RESPONSE_GET_EXAM_PUBLISH_CHECK = ""
    var textViewResultMsg: TextView? = null
    var tvExamTitle: TextView? = null
    var tvDateExam: TextView? = null
    var tvTimeExam: TextView? = null
    var API_RESPONSE_CHECK_EXAM_ATT = ""
    var TOTAL_SCORE_FROM_CURRENT_EXAM = ""
    var buttonViewResult: Button? = null
    var buttonViewrank: Button? = null
    var layoutExamAttendMsg: LinearLayout? = null
    var buttonGoBack: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam_intro)
        CATEGORY_ID = intent.getStringExtra("CATEGORY_ID")
        //EXAM_TYPE_STRING = intent.getStringExtra("EXAM_TYPE")
        //EXAM_NEG_MARK = intent.getStringExtra("EXAM_NEG_MARK")
        //EXAM_TOTAL_MARK = intent.getStringExtra("EXAM_TOTAL_MARK")
       // EXAM_DURATION = intent.getStringExtra("EXAM_DURATION")
        //EXAM_PASS_MARK = intent.getStringExtra("EXAM_PASS_MARK").toInt()
        //    Toast.makeText(mContext, "PASS MARK : "+EXAM_PASS_MARK, Toast.LENGTH_SHORT).show();
        Log.d("dshfbshfjsjfjssf", EXAM_ID + "   " + TITLE + "    " + EXAM_TYPE_STRING + "    " +
                EXAM_NEG_MARK + "   " + EXAM_TOTAL_MARK + "    " + EXAM_DURATION + "    " +
                EXAM_PASS_MARK)
        Log.d("DJFHEKH", " ID IDIDID $EXAM_ID")
        initActionBar()
        changeStatusBarColor()
        initViews()

        textViewExamDescription!!.text = EXAM_TYPE_STRING
        actionBarTitle!!.text = EXAM_TYPE_STRING
        getExamsList();

    }





























    private fun initViews() {
        profileUtils = ProfileUtils(mContext)
        userSession = profileUtils!!.userSession
        layoutNoResult = findViewById<View>(R.id.layoutNoResult) as LinearLayout
        layoutResultPublished = findViewById<View>(R.id.layoutResultPublished) as LinearLayout
        layoutExamAttendMsg = findViewById<View>(R.id.layoutExamAttendMsg) as LinearLayout
        layoutExamAttendMsg!!.visibility = View.GONE
        textViewResultMsg = findViewById<View>(R.id.textView32) as TextView
        tvExamTitle = findViewById<View>(R.id.tvExamTitle) as TextView
        tvTimeExam = findViewById<View>(R.id.tvTimeExam) as TextView
        tvDateExam = findViewById<View>(R.id.tvDateExam) as TextView
        textView40 = findViewById<View>(R.id.textView40) as TextView
        tv_max_mark = findViewById<View>(R.id.tv_max_mark) as TextView
        tv_pass_mark = findViewById<View>(R.id.tv_pass_mark) as TextView

        tv_pass_mark!!.visibility = View.GONE
        buttonViewResult = findViewById<View>(R.id.buttonViewResult) as Button
        buttonViewrank = findViewById<View>(R.id.buttonViewrank) as Button
        buttonViewResult!!.setOnClickListener {
            val `in` = Intent(mContext, ExamResultsView::class.java)
            `in`.putExtra("EXAM_ID", EXAM_ID)
            `in`.putExtra("TOTAL_SCORE", TOTAL_SCORE_FROM_CURRENT_EXAM)
            startActivity(`in`)
        }
        buttonProceed = findViewById<View>(R.id.buttonProceed) as Button
        buttonGoBack = findViewById<View>(R.id.buttonGoBack) as Button
        loadingLayout = findViewById<View>(R.id.loadingLayout) as LinearLayout
        fullLayout = findViewById<View>(R.id.fullLayout) as LinearLayout
        textViewCount = findViewById<View>(R.id.textViewCount) as TextView
        textViewTitle = findViewById<View>(R.id.textViewTitle) as TextView
        textViewExamDescription = findViewById<View>(R.id.textViewExamDescription) as TextView
        examQuestionListCount
        buttonProceed!!.setOnClickListener {
            //Toast.makeText(mContext, ""+GOTO_ACTIITY_NO, Toast.LENGTH_SHORT).show();
            if (GOTO_ACTIITY_NO == 1 || GOTO_ACTIITY_NO == 2) {
                val `in` = Intent(mContext, ExamActivity::class.java)
                `in`.putExtra("EXAM_ID", EXAM_ID)
                `in`.putExtra("EXAM_NEG_MARK", EXAM_NEG_MARK)
                `in`.putExtra("EXAM_TOTAL_MARK", EXAM_TOTAL_MARK)
                `in`.putExtra("EXAM_PASS_MARK", "" + EXAM_PASS_MARK)
                `in`.putExtra("EXAM_DURATION", "" + EXAM_DURATION)
                startActivity(`in`)
                finish()
            } else if (GOTO_ACTIITY_NO == 3) { //CHANGE THIS TO REVIEW CLASS
                Toast.makeText(mContext, "Times Up, Wait for the Result to be published", Toast.LENGTH_SHORT).show()
            } else if (GOTO_ACTIITY_NO == 4) {
                Toast.makeText(mContext, "Exam Finished", Toast.LENGTH_SHORT).show()
            } else if (GOTO_ACTIITY_NO == 5) {
                val `in` = Intent(mContext, ReviewExamActivity::class.java)
                `in`.putExtra("EXAM_ID", EXAM_ID)
                `in`.putExtra("EXAM_NEG_MARK", EXAM_NEG_MARK)
                `in`.putExtra("EXAM_TOTAL_MARK", EXAM_TOTAL_MARK)
                `in`.putExtra("EXAM_PASS_MARK", "" + EXAM_PASS_MARK)
                `in`.putExtra("EXAM_DURATION", ""+EXAM_DURATION)
                 startActivity(`in`)
            }
        }
        buttonViewrank!!.setOnClickListener{
            val `inrank` = Intent(mContext, Rank_Activity::class.java)
            `inrank`.putExtra("EXAM_ID", EXAM_ID)
            startActivity(`inrank`)
        }
        buttonGoBack!!.setOnClickListener { finish() }
    }
    fun initActionBar() {
        val actionBarLayout = layoutInflater.inflate(R.layout.action_bar_04, null) as ViewGroup
        //Set up your ActionBar
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(false)
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.customView = actionBarLayout
        //You customization
        val actionBarColor = resources.getColor(R.color.accentColor1)
        actionBar.setBackgroundDrawable(ColorDrawable(actionBarColor))
        val parent = actionBarLayout.parent as Toolbar
        //parent.setPadding(0,0,0,0);
        //for tab otherwise give space in tab;
        //parent.setContentInsetsAbsolute(0,0);
        actionBarTitle = findViewById<View>(R.id.actionBarTitle) as TextView
        actionBarTitle!!.text = "Exam"
        val togBackButton = findViewById<View>(R.id.togBackButton) as ImageView
        togBackButton.setOnClickListener { finish() }
    }

    fun changeStatusBarColor() {
        val window = this@ExamIntro.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@ExamIntro, R.color.accentColor1)
    }

    fun showLoading(stat: Boolean) {
        if (stat) {
            loadingLayout!!.visibility = View.VISIBLE
            fullLayout!!.visibility = View.GONE
        } else {
            loadingLayout!!.visibility = View.GONE
            fullLayout!!.visibility = View.VISIBLE
        }
    }

    //showLoading(false);
    val examQuestionListCount: Unit get() {
            showLoading(true)
            questionArrayList = ArrayList()
            val post_data = HashMap<String, String>()
            post_data["hash_code"] = ConfigServer.API_HASH
            post_data["exam_id"] = EXAM_ID
            val URL = ConfigServer.API_GET_EXAMS_SINGLE
            val olf = OnlineFunctions(mContext, URL, post_data)
            val handler = Handler()
            val runnable = Runnable {
                API_RESPONSE = olf.pOstResult
                handler.post {
                    showLog("API_RESPONSE", API_RESPONSE)
                    try { //showLoading(false);
                        val jsonObject = JSONObject(API_RESPONSE)
                        val jsonArray = jsonObject.getJSONArray("response")
                        textViewCount!!.text = "" + jsonArray.length()
                    } catch (e: JSONException) {
                    }
                    // showLoading(false);
                }
            }
            Thread(runnable).start()
        }

    private fun showLog(tag: String, api_response1: String) {
        Log.d(tag, api_response1)
    }
    fun getExamsList() {
        val retrofit = Retrofit.Builder().baseUrl(ConfigServer.API_GET_NEW_EXAM).addConverterFactory(ScalarsConverterFactory.create()).build()
        webServiceApi = retrofit.create(WebServiceApi::class.java)
        val call = webServiceApi!!.get_NewExam(ConfigServer.API_HASH, CATEGORY_ID)
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("djfhkeyuyuyytttf", "onResponse: CAT = " + EXAM_ID + " USERID :" + userSession!!.useR_SESSION_ID + "   body =" + response.body())
                showLoading(false)
                try {
                    val jsonObject = JSONObject(response.body())
                    val status = jsonObject.getString("status")
                    if (status == "200") {
                        // String video_exist = jsonObject1.getString("video_exist");
                        val jsonArray = JSONArray(jsonObject.getString("response"))
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject = jsonArray.getJSONObject(i)
                            EXAM_ID = jsonObject.getString("exam_id")
                            TITLE = jsonObject.getString("exam_title")
                            val description = jsonObject.getString("description")
                            val category_id = jsonObject.getString("category_id")
                            val university_id = jsonObject.getString("university_id")
                            val coursetype_id = jsonObject.getString("coursetype_id")
                            val course_id = jsonObject.getString("course_id")
                            val subject_id = jsonObject.getString("subject_id")
                            val instruction_id = jsonObject.getString("instruction_id")
                            EXAM_TYPE_STRING = jsonObject.getString("examtype")
                            val exam_type_cat = jsonObject.getString("exam_type_cat")
                            val start_date = jsonObject.getString("start_date")
                            val end_date = jsonObject.getString("end_date")
                            EXAM_DURATION = jsonObject.getString("duration")
                            EXAM_TOTAL_MARK = jsonObject.getString("total_mark")
                            EXAM_PASS_MARK = jsonObject.getString("pass_mark")
                            EXAM_NEG_MARK = jsonObject.getString("negative_mark")
                            val published = jsonObject.getString("published")
                            val exam_status = jsonObject.getString("exam_status")
                            val questions = jsonObject.getString("questions")
                            val total_qst = jsonObject.getString("total_qst")
                            val publish_date = jsonObject.getString("publish_date")
                            val created_at = jsonObject.getString("created_at")
                            val updated_at = jsonObject.getString("updated_at")
                            val paid_type = jsonObject.getString("paid_type")
                            val correctmark = jsonObject.getString("correctmark")

                            textViewTitle!!.text = TITLE
                            textViewExamDescription!!.text = description
                            textViewCount!!.text = total_qst + " Questions"
                            textView40!!.text = "Duration: "+ EXAM_DURATION+" Minutes"
                            tv_pass_mark!!.text = "Passing Marks: "+ EXAM_PASS_MARK+" Minutes"
                            tv_max_mark!!.text = "Max. Marks: "+ EXAM_TOTAL_MARK
                            checkIfExamAlreadyAttended()
                                    Log.d("fhdsufvusdugfvuugvusdd",TITLE+"   "+total_qst+"   "+EXAM_DURATION)
                        }
                    }else{
                        Toast.makeText(this@ExamIntro, "Exam not available.", Toast.LENGTH_SHORT).show()
                        finish()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<String>, t: Throwable) {
                Toast.makeText(this@ExamIntro, "" + t.message, Toast.LENGTH_SHORT).show()
                Log.d("ejfdhek", "onFailure: " + t.message)
            }
        })
    }


    fun checkIfExamAlreadyAttended() {
        val retrofit = Retrofit.Builder().baseUrl(ConfigServer.API_CHECK_EXAM_ATTENDANCE).addConverterFactory(ScalarsConverterFactory.create()).build()
        webServiceApi = retrofit.create(WebServiceApi::class.java)
        val call = webServiceApi!!.checkExamAttended(ConfigServer.API_HASH, EXAM_ID, userSession!!.useR_SESSION_ID)
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d("djfhkeyuyuyytttf", "onResponse: CAT = " + EXAM_ID + " USERID :" + userSession!!.useR_SESSION_ID + "   body =" + response.body())
                showLoading(false)
                try {
                    val jsonObject = JSONObject(response.body())
                    val status = jsonObject.getString("status")
                    if (status == "200") {
                        val code = jsonObject.getString("code")
                        when (code.toInt()) {
                            1 -> {
                                //EXAM ATTENDED
                                val resultObj = jsonObject.getJSONObject("result")
                                val start_time = resultObj.getString("start_time")
                                val dtc = DateTimeConverter()
                                val exam_date = dtc.formateDateFromstring("yyyy-MM-dd HH:mm:ss", "dd MMM", start_time)
                                val exam_time = dtc.formateDateFromstring("yyyy-MM-dd HH:mm:ss", "hh:mm a", start_time)
                                val exam_end_time = resultObj.getString("end_time")
                                val exam_status = resultObj.getString("status").toInt()
                                when (exam_status) {
                                    1 -> {
                                        //exam finished
//  Toast.makeText(ExamIntro.this, "Exam Finished", Toast.LENGTH_SHORT).show();
                                        tvDateExam!!.text = exam_date
                                        tvTimeExam!!.text = exam_time
                                        buttonProceed!!.text = "Finished"
                                        GOTO_ACTIITY_NO = 4
                                        checkIfExamResultAvailable()
                                        layoutExamAttendMsg!!.visibility = View.VISIBLE
                                    }
                                    2 -> if (isTimesUp(exam_end_time)) {
                                        buttonProceed!!.text = "Times Up!"
                                        GOTO_ACTIITY_NO = 3
                                        checkIfExamResultAvailable()
                                    } else {
                                        buttonProceed!!.text = "Resume"
                                        GOTO_ACTIITY_NO = 2
                                    }
                                }
                            }
                            2 -> {
                                //EXAM NOT ATTENDED
                                GOTO_ACTIITY_NO = 1
                                buttonProceed!!.text = "Start Test"
                            }
                        }
                        //tvDateExam.setText(exam_date);
//tvTimeExam.setText(exam_time);
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Toast.makeText(this@ExamIntro, "" + t.message, Toast.LENGTH_SHORT).show()
                Log.d("ejfdhek", "onFailure: " + t.message)
            }
        })
    }

    fun isTimesUp(endT: String?): Boolean {
        val currentTime = Calendar.getInstance().time
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try { //Date sTime = format.parse(timeS);
            val eTime = format.parse(endT)
            isTimesUp = if (currentTime.after(eTime)) {
                true
            } else {
                false
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return isTimesUp
    }

    fun showResultsPublished(stat: Boolean) {
        if(stat){
            layoutNoResult!!.visibility = View.GONE
            layoutResultPublished!!.visibility = View.VISIBLE
        }else{
            layoutNoResult!!.visibility = View.GONE
            layoutResultPublished!!.visibility = View.VISIBLE
        }
    }

    fun checkIfExamResultAvailable() {
        val post_data = HashMap<String, String>()
        post_data["hash_code"] = ConfigServer.API_HASH
        post_data["exam_id"] = EXAM_ID
        post_data["student_id"] =  userSession!!.useR_SESSION_ID
        val URL = ConfigServer.API_CHECK_EXAM_PUBLISH_DATE
        val olf = OnlineFunctions(mContext, URL, post_data)
        val handler = Handler()
        val runnable = Runnable {
            API_RESPONSE_GET_EXAM_PUBLISH_CHECK = olf.pOstResult
            handler.post {
                Log.d("sfjhsdnknkkkj", "run: $API_RESPONSE_GET_EXAM_PUBLISH_CHECK")
                try {
                    val jsonObject1 = JSONObject(API_RESPONSE_GET_EXAM_PUBLISH_CHECK)
                    val if_result = jsonObject1.getString("if_result")
                    val status = jsonObject1.getString("status")
                    val code = jsonObject1.getString("code")

                    if (code=="1") {
                        GOTO_ACTIITY_NO = 5
                        buttonProceed!!.text = "Review Exam"
                        showResultsPublished(true)
                    } else if (code == "2") {
                        showResultsPublished(false)
                    }
                    if (if_result=="0"){
                        layoutResultPublished!!.visibility = View.VISIBLE
                    }else{
                        layoutResultPublished!!.visibility = View.VISIBLE
                    }
                    val array1 = jsonObject1.getJSONArray("response")
                    val jsonObject = array1.getJSONObject(0)
                    val exam_id = jsonObject.getString("exam_id")
                    val exam_title = jsonObject.getString("exam_title")
                    val description = jsonObject.getString("description")
                    val category_id = jsonObject.getString("category_id")
                    val university_id = jsonObject.getString("university_id")
                    val coursetype_id = jsonObject.getString("coursetype_id")
                    val course_id = jsonObject.getString("course_id")
                    val subject_id = jsonObject.getString("subject_id")
                    val instruction_id = jsonObject.getString("instruction_id")
                    val examtype = jsonObject.getString("examtype")
                    val start_date = jsonObject.getString("start_date")
                    val end_date = jsonObject.getString("end_date")
                    val duration = jsonObject.getString("duration")
                    val total_mark = jsonObject.getString("total_mark")
                    val pass_mark = jsonObject.getString("pass_mark")
                    val negative_mark = jsonObject.getString("negative_mark")
                    val published = jsonObject.getString("published")
                    val exam_status = jsonObject.getString("exam_status")
                    val questions = jsonObject.getString("questions")
                    val created_at = jsonObject.getString("created_at")
                    val updated_at = jsonObject.getString("updated_at")
                    var publish_date = jsonObject.getString("publish_date")
                    TOTAL_SCORE_FROM_CURRENT_EXAM = total_mark
                    val dtc = DateTimeConverter()
                    publish_date = dtc.formateDateFromstring("yyyy-MM-dd", "dd MMM YYYY", publish_date)
                    textViewResultMsg!!.text = "Exam result will be published on $publish_date"
                    tvExamTitle!!.text = exam_title

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
        Thread(runnable).start()
    }
}