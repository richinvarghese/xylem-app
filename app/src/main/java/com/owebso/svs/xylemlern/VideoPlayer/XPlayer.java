package com.owebso.svs.xylemlern.VideoPlayer;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.material.tabs.TabLayout;

import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.interfaces.ItemClickListener;
import com.denzcoskun.imageslider.models.SlideModel;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.TrackNameProvider;
import com.google.android.exoplayer2.ui.TrackSelectionView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import com.owebso.svs.xylemlern.PreventScreenshot;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.XylemActivities.VideosLists;
import com.owebso.svs.xylemlern.adapters.Package_ListAdapter;
import com.owebso.svs.xylemlern.adapters.VideoTimeLineAdapter;
import com.owebso.svs.xylemlern.adapters.RelatedVideosListAdapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Package_Model;
import com.owebso.svs.xylemlern.models.VidTimeLine;
import com.owebso.svs.xylemlern.models.VideoX;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

public class XPlayer extends AppCompatActivity implements ExoPlayer.EventListener, View.OnClickListener, PaymentResultListener {
    final int HI_BITRATE = 2097152;
    final int MI_BITRATE = 1048576;
    final int LO_BITRATE = 524288;
    String API_RESPONSE_UPDATE_WATCH_TIME = "";
    Button btn720, btn480, btn360, btnAuto;
    String NOTES_URL = "";
    DefaultTrackSelector trackSelector;
    boolean mExoPlayerFullscreen;
    Dialog mFullScreenDialog;
    String CURRENT_PLAYLIST_VIDEO_URL = "";
    ImageButton mFullScreenButton, exo_change_quality;
    String CURRENT_VIDEO_ID = "";
    String CURRENT_PARENT_ID = "";
    String API_RESPONSE_VIDEO_DETAILS = "";
    ArrayList<VidTimeLine> timeLineArrayList;
    VideoTimeLineAdapter adapterTimeLine;
    RecyclerView recyclerViewRelatedVideos;
    ProgressBar progressBar2;
    String thumb_image;
    RelativeLayout player_layout;
    ImageButton exo_expand;
    boolean ZOOM_VIEW = false;
    TextView textViewVideo, textViewDescription, tv_cancel;
    RelativeLayout layoutViewNotes;
    String API_RESPONSE_GET_VIDEOS = "";
    ArrayList<VideoX> videoXArrayList;
    String RESPONSE_STATUS_VIDEO = "";
    RelatedVideosListAdapter videoAdapter;
    GridLayoutManager gridLayoutManager;
    String API_RESPONSE_GET_CAT_SINGLE = "", str_pack_id;
    TextView actionBarTitle;
    ArrayList<SlideModel> imageList;
    ImageSlider imageSlider;
    TabLayout tabLayout12;
    NestedScrollView nestedScrollView;
    Dialog dialog;
    UserSession userSession;
    RecyclerView recyclerpackage;
    Package_ListAdapter package_listAdapter;
    ArrayList<Package_Model> packageList;
    ImageView img_like;
    String Fav_Sts;
    private PlayerView simpleExoPlayerView;
    private String CURRENT_PLAYLIST_VIDEO_URL1 = "http://playertest.longtailvideo.com/adaptive/bbbfull/bbbfull.m3u8";
    private String hlsVideoUri = "https://owebso.in/test/VIDEO/003/MASTER_LAYLIST.m3u8";
    //private String T3 = "http://192.168.1.14/compre_1/uploads/videos/5Ba2RLdO3QF89NITcfbqGtoZ/MASTER_LAYLIST.m3u8";
    //private String CURRENT_PLAYLIST_VIDEO_URL = "http://xylemlearning.com/uploads/videos/xDPAUbNjhXst8diCLzoOk5a4/MASTER_LAYLIST.m3u8";
    private SimpleExoPlayer player;
    private ProgressBar progressBar;
    private Context mContext = XPlayer.this;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xplayer);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        pref = getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        CURRENT_PLAYLIST_VIDEO_URL = getIntent().getStringExtra("PLAY_URL");
        CURRENT_VIDEO_ID = getIntent().getStringExtra("VIDEO_ID");
        CURRENT_PARENT_ID = getIntent().getStringExtra("PARENT_ID");
        Fav_Sts = getIntent().getStringExtra("FAV_STS");
        Log.d("PLAY_URL", "URL : " + CURRENT_PLAYLIST_VIDEO_URL + " " + CURRENT_VIDEO_ID);
        initViews();
        initActionBar();
        changeStatusBarColor();
        getVideos();
        setModuleTitle(CURRENT_PARENT_ID);
        getRelatedVideos(CURRENT_PARENT_ID);
        Handler mainHandler = new Handler();
        trackSelector = new DefaultTrackSelector();
        DefaultTrackSelector.Parameters defaultTrackParam = trackSelector.buildUponParameters().build();
        trackSelector.setParameters(defaultTrackParam);
        LoadControl loadControl = new DefaultLoadControl();
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        simpleExoPlayerView = (PlayerView) findViewById(R.id.player_view);
        simpleExoPlayerView.setPlayer(player);
        simpleExoPlayerView.setPlayer(player);
        DefaultDataSourceFactory fac = new DefaultDataSourceFactory(this, Util.getUserAgent(this, getString(R.string.app_name)));
        HlsMediaSource videoSource = new HlsMediaSource.Factory(fac).createMediaSource(Uri.parse(CURRENT_PLAYLIST_VIDEO_URL));
        player.prepare(videoSource);
        simpleExoPlayerView.setKeepScreenOn(true);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        player.addListener(new Player.EventListener() {
            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            }
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == ExoPlayer.STATE_BUFFERING) {
                    progressBar2.setVisibility(View.VISIBLE);
                } else {
                    progressBar2.setVisibility(View.GONE);
                }
            }


            @Override
            public void onSeekProcessed() {
                progressBar2.setVisibility(View.VISIBLE);
            }


        });
        player.setPlayWhenReady(true);

        exo_expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ZOOM_VIEW) {
                    simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                    ZOOM_VIEW = false;
                } else {
                    simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
                    ZOOM_VIEW = true;
                }
            }
        });
    }

    public void hideActionBar() {
        try {
            getSupportActionBar().hide();
        } catch (Exception e) {
            try {
                getActionBar().hide();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }
    }

    public void changeStatusBarColor() {
        Window window = XPlayer.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(XPlayer.this, R.color.black));
    }

    public void initViews() {
        ProfileUtils profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();
        btn360 = (Button) findViewById(R.id.btn360);
        btn480 = (Button) findViewById(R.id.btn480);
        btn720 = (Button) findViewById(R.id.btn720);
        btnAuto = (Button) findViewById(R.id.btnAuto);
        img_like = (ImageView) findViewById(R.id.img_like);
        btn360.setOnClickListener(this);
        btn480.setOnClickListener(this);
        btn720.setOnClickListener(this);
        btnAuto.setOnClickListener(this);
        img_like.setOnClickListener(this);


        if (Fav_Sts.equalsIgnoreCase("1")) {
            img_like.setImageResource(R.drawable.like);
        } else {
            img_like.setImageResource(R.drawable.dislike);
        }
        exo_change_quality = (ImageButton) findViewById(R.id.exo_change_quality);
        exo_change_quality.setOnClickListener(this);
        initFullscreenDialog();

        layoutViewNotes = (RelativeLayout) findViewById(R.id.layoutViewNotes);
        layoutViewNotes.setOnClickListener(this);

        mFullScreenButton = (ImageButton) findViewById(R.id.exo_fullscreen);
        exo_expand = (ImageButton) findViewById(R.id.exo_expand);
        mFullScreenButton.setOnClickListener(this);
        exo_expand.setOnClickListener(this);

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        if (!isTablet()) {
            gridLayoutManager = new GridLayoutManager(mContext, 1);
        } else {
            gridLayoutManager = new GridLayoutManager(mContext, 2);
        }

        recyclerViewRelatedVideos = (RecyclerView) findViewById(R.id.recyclerViewRelatedVideos);
        recyclerViewRelatedVideos.setLayoutManager(gridLayoutManager);

        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
        progressBar2.setVisibility(View.GONE);

        player_layout = (RelativeLayout) findViewById(R.id.player_layout);
        textViewVideo = (TextView) findViewById(R.id.textViewVideoTitle);
        textViewDescription = (TextView) findViewById(R.id.textViewDescription);

        imageSlider = (ImageSlider) findViewById(R.id.image_slider);

        ImageButton imageButton = (ImageButton) findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVideoOptions(v);
            }
        });

        tabLayout12 = (TabLayout) findViewById(R.id.tabLayout12);
        tabLayout12.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                View view = findViewById(R.id.fullLayout);
                if (position == 1) {
                    if (imageList.size() > 0) {
                        showSlideMenu(view);
//                        new ImageViewer.Builder<VidTimeLine>(mContext,timeLineArrayList)
//                                .setFormatter(new ImageViewer.Formatter<VidTimeLine>() {
//                                    @Override
//                                    public String format(VidTimeLine customImage) {
//                                        return customImage.getImagePath();
//                                    }
//
//                                })
//                                .show();

                    } else {
                        Toast.makeText(mContext, "This Video has no slides to show.", Toast.LENGTH_SHORT).show();
                        TabLayout.Tab tab1 = tabLayout12.getTabAt(0);
                        tab1.select();
                    }

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    public void initActionBar() {
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_05_black,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.black);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent = (Toolbar) actionBarLayout.getParent();
        //  parent.setPadding(0,0,0,0);
        // for tab otherwise give space in tab
        //  parent.setContentInsetsAbsolute(0,0);
        actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("Video");
        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initFullscreenDialog() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        mFullScreenDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("CURRENT_POSI", "TIME : " + player.getCurrentPosition());
        updateWatchTime();
        if (mExoPlayerFullscreen) {
            closeFullscreenDialog();
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private void closeFullscreenDialog() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        ((ViewGroup) simpleExoPlayerView.getParent()).removeView(simpleExoPlayerView);
        ((RelativeLayout) findViewById(R.id.player_layout)).addView(simpleExoPlayerView);
        PreventScreenshot.on(mFullScreenDialog);
        mExoPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
        mFullScreenButton.setImageDrawable(ContextCompat.getDrawable(XPlayer.this, R.drawable.exo_controls_fullscreen_enter));
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private void openFullscreenDialog() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ((ViewGroup) simpleExoPlayerView.getParent()).removeView(simpleExoPlayerView);
        mFullScreenDialog.addContentView(simpleExoPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenButton.setImageDrawable(ContextCompat.getDrawable(XPlayer.this, R.drawable.exo_controls_fullscreen_exit));
        PreventScreenshot.on(mFullScreenDialog);
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        // Toast.makeText(this, "changed", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case ExoPlayer.STATE_BUFFERING:
                //You can use progress dialog to show user that video is preparing or buffering so please wait
                progressBar.setVisibility(View.VISIBLE);
                break;
            case ExoPlayer.STATE_IDLE:
                //idle state
                break;
            case ExoPlayer.STATE_READY:
                // dismiss your dialog here because our video is ready to play now
                progressBar.setVisibility(View.GONE);
                break;
            case ExoPlayer.STATE_ENDED:
                // do your processing after ending of video
                break;
        }
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

        AlertDialog.Builder adb = new AlertDialog.Builder(XPlayer.this);
        adb.setTitle("Could not able to stream video");
        adb.setMessage("It seems that something is going wrong.\nPlease try again.");
        adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish(); // take out user from this activity. you can skip this
            }
        });
        AlertDialog ad = adb.create();
        ad.show();
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (player != null) {
            player.setPlayWhenReady(false); //to pause a video because now our video player is not in focus
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.release();
    }

    public void getVideoDetails() {

    }


    @Override
    public void onClick(View v) {
        if (v == btn360) {
            simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);

//            DefaultTrackSelector.Parameters parameters = trackSelector.buildUponParameters()
//                    .setMaxVideoBitrate(LO_BITRATE)
//                    .setForceHighestSupportedBitrate(true)
//                    .build();
//            trackSelector.setParameters(parameters);
        }
        if (v == btn480) {
            simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);

//            openFullscreenDialog();
//            DefaultTrackSelector.Parameters parameters = trackSelector.buildUponParameters()
//                    .setMaxVideoBitrate(MI_BITRATE)
//                    .setForceHighestSupportedBitrate(true)
//                    .build();
//            trackSelector.setParameters(parameters);
        }
        if (v == btn720) {
            simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);

//            DefaultTrackSelector.Parameters parameters = trackSelector.buildUponParameters()
//                    .setMaxVideoBitrate(HI_BITRATE)
//                    .setForceHighestSupportedBitrate(true)
//                    .build();
//            trackSelector.setParameters(parameters);
        }

        if (v == mFullScreenButton) {
            if (!mExoPlayerFullscreen) {
                openFullscreenDialog();

            } else {
                closeFullscreenDialog();

            }
        }

        if (v == btnAuto) {
            simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
        }

        if (v == layoutViewNotes) {
            if (NOTES_URL.contains(".pdf")) {
                Intent in = new Intent(mContext, NotesViewer.class);
                in.putExtra("PDF_URL", NOTES_URL);
                startActivity(in);
            } else {
                Toast.makeText(mContext, "Notes not found", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == exo_change_quality) {
            MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
            if (mappedTrackInfo != null) {
                int rendererIndex = 0;
                int rendererType = mappedTrackInfo.getRendererType(rendererIndex);
                boolean allowAdaptiveSelections =
                        rendererType == C.TRACK_TYPE_VIDEO
                                || (rendererType == C.TRACK_TYPE_AUDIO
                                && mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                                == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_NO_TRACKS);

                Pair<AlertDialog, TrackSelectionView> dialogPair =
                        TrackSelectionView.getDialog(XPlayer.this, "Quality", trackSelector, rendererIndex);

                //  Toast.makeText(this, "INDEX : "+rendererIndex, Toast.LENGTH_SHORT).show();
                dialogPair.second.setShowDisableOption(false);
                dialogPair.second.setAllowAdaptiveSelections(false);


                TrackNameProvider trackNameProvider = new TrackNameProvider() {
                    @Override
                    public String getTrackName(Format format) {
                        String trackTitle = "";
                        switch (format.bitrate) {
                            case HI_BITRATE:
                                trackTitle = "720p";

                                break;
                            case MI_BITRATE:
                                trackTitle = "480p";

                                break;
                            case LO_BITRATE:
                                trackTitle = "360p";

                                break;
                            default:
                                trackTitle = "Auto";

                        }

                        return trackTitle;
                    }
                };
                dialogPair.second.setTrackNameProvider(trackNameProvider);
                dialogPair.first.show();


            }
        }
        if (v == img_like) {
            if (Fav_Sts.equalsIgnoreCase("0")) {
                img_like.setImageResource(R.drawable.like);
                Make_Favourite(CURRENT_VIDEO_ID, "1");
                Fav_Sts = "1";
            } else {
                img_like.setImageResource(R.drawable.dislike);
                Make_Favourite(CURRENT_VIDEO_ID, "0");
                Fav_Sts = "0";
            }

        }
    }

    public void getVideos() {
        timeLineArrayList = new ArrayList<>();
        imageList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("video_id", CURRENT_VIDEO_ID);
        post_data.put("login_id", userSession.getUSER_SESSION_ID());
        Log.d("fhdbfddqqq", userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_GET_VIDEO_DETAILS;
        final OnlineFunctions olf = new OnlineFunctions(mContext, URL, post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_VIDEO_DETAILS = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("API_RESPONSE_01_Details", "run: " + API_RESPONSE_VIDEO_DETAILS);

                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_VIDEO_DETAILS);
                            String status = jsonObject1.getString("status");
                            if (status.equals("200")) {
                                JSONObject result = jsonObject1.getJSONObject("result");
                                String video_title = result.getString("video_title");
                                String description = result.getString("description");
                                thumb_image = result.getString("thumb_image");
                                //String type = result.getString("type");

                                //NOTES_URL = result.getString("matirial_path");
                                String watch_time = result.getString("time");
                                //long seekTime = Long.parseLong(watch_time);
                                //  player.seekTo(seekTime);
                                //  NOTES_URL = ConfigServer.PATH_PDF_NOTES + NOTES_URL;
                                textViewVideo.setText(video_title);
                                textViewDescription.setText(description);
                                thumb_image = ConfigServer.PATH_VIDEO_THUMB + thumb_image;

                                Log.d("sfbdsjfj", video_title + "  " + description);

                               /* JSONArray sliderArray = result.getJSONArray("timelines");

                                for (int i = 0; i < sliderArray.length(); i++) {
                                    JSONObject jsonObject = sliderArray.getJSONObject(i);
                                    String titel = jsonObject.getString("title");
                                    String time = jsonObject.getString("second");
                                    String image_path = jsonObject.getString("image_path");

                                    image_path = ConfigServer.PATH_SLIDE_TIMELINE_IMAGE + image_path;

                                    // long timeInMin = TimeUnit.SECONDS.toMinutes(Long.parseLong(time));
                                    timeLineArrayList.add(new VidTimeLine("0", titel, time, image_path));
                                    imageList.add(new SlideModel(image_path, titel));
                                }*/

                               /* Iterator<String> keys = timelines.keys();
                              while (keys.hasNext()){
                                 JSONObject jsonObject = timelines.getJSONObject(keys.next());

                                }*/


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                       /* adapterTimeLine = new VideoTimeLineAdapter(mContext, timeLineArrayList);
                        recyclerViewRelatedVideos.setAdapter(adapterTimeLine);
                        adapterTimeLine.notifyDataSetChanged();*/

                    }
                });
            }
        };
        new Thread(runnable).start();
    }


    public void getRelatedVideos(String CATEGORY_ID) {
        videoXArrayList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("category_id", CATEGORY_ID);
        post_data.put("video_id", CURRENT_VIDEO_ID);
        post_data.put("login_id", userSession.getUSER_SESSION_ID());
        Log.d("API_RESPONSE", "current_id: " + CATEGORY_ID);
        String URL = ConfigServer.API_GET_RELETED_VIDEO;
        final OnlineFunctions olf = new OnlineFunctions(mContext, URL, post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_VIDEOS = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.d("API_RESPONSEreleted", "run: " + API_RESPONSE_GET_VIDEOS);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_VIDEOS);
                            RESPONSE_STATUS_VIDEO = "";
                            RESPONSE_STATUS_VIDEO = jsonObject1.getString("status");
                            Log.d("API_RESPONSE1", "V-status : " + RESPONSE_STATUS_VIDEO);

                            if (!RESPONSE_STATUS_VIDEO.equals("404")) {

                                JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject videoObj = jsonArray.getJSONObject(i);
                                    String video_id = videoObj.getString("video_id");
                                    String video_title = videoObj.getString("video_title");
                                    String category_id = videoObj.getString("category_id");
                                    String description = videoObj.getString("description");
                                    String thumb_image = videoObj.getString("thumb_image");
                                    String video_path = videoObj.getString("video_path");
                                    String str_paid = videoObj.getString("paid");
                                    String str_unpaid = videoObj.getString("unpaid");
                                    String str_favourite = videoObj.getString("favourite");
                                    String video_status = videoObj.getString("video_status");
                                    String str_menu_order = videoObj.getString("menu_order");
                                    String created_at = videoObj.getString("created_at");
                                    String str_watch_status = videoObj.getString("watch_status");
                                    String str_time = videoObj.getString("time");
                                    String IMAGE_URL_THUMB = ConfigServer.PATH_VIDEO_THUMB + thumb_image;
                                    video_path = ConfigServer.PATH_VIDEO + video_path;

                                    videoXArrayList.add(new VideoX(video_id, video_title, category_id, description, IMAGE_URL_THUMB, video_path, str_paid, str_unpaid, str_favourite, video_status,
                                            str_menu_order, created_at, str_watch_status, str_time, 1));
                                }
                            } else {
                                Log.d("API_RESPONSE", "NO VIDEOS");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("API_RESPONSE", "exception");
                        }
                        videoAdapter = new RelatedVideosListAdapter(mContext, videoXArrayList, new RelatedVideosListAdapter.EventListener() {
                            @Override
                            public void onVideoClicked(String url, String videoId, String parentId, String str_paid, String str_fav,String pay_sts) {
                                if (str_paid.equalsIgnoreCase("0")) {
                                    CURRENT_VIDEO_ID = videoId;
                                    Intent in = new Intent(mContext, XPlayer.class);
                                    in.putExtra("PLAY_URL", url);
                                    in.putExtra("VIDEO_ID", videoId);
                                    in.putExtra("PARENT_ID", parentId);
                                    in.putExtra("FAV_STS", str_fav);
                                    // in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    startActivity(in);
                                    finish();
                                } else if (str_paid.equalsIgnoreCase("1") && pay_sts.equalsIgnoreCase("1")) {
                                    CURRENT_VIDEO_ID = videoId;
                                    Intent in = new Intent(mContext, XPlayer.class);
                                    in.putExtra("PLAY_URL", url);
                                    in.putExtra("VIDEO_ID", videoId);
                                    in.putExtra("PARENT_ID", parentId);
                                    in.putExtra("FAV_STS", str_fav);
                                    // in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    startActivity(in);
                                    finish();
                                } else {
                                    Dialogue_package_list();
                                    Get_Package_List();
                                }
                            }
                        });
                        recyclerViewRelatedVideos.setAdapter(videoAdapter);


                    }
                });

            }
        };

        new Thread(runnable).start();
    }

    public void seektoSec(String secs) {
        long secondss = Long.parseLong(secs);
//        long secs = TimeUnit.MINUTES.toSeconds(Long.parseLong(mins));
        //long seektime = Integer.parseInt(secs);
        long seektimeInMilSecs = (secondss * 1000);
        player.seekTo(seektimeInMilSecs);
    }

    public String getCUrrentVideoThumb() {
        return thumb_image;
    }

    public void showVideoOptions(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window_show_video_options, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.setAnimationStyle(R.style.popup_window_animation);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void showSlideMenu(View view) {

        pausePlayer();
        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window_show_video_slides, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.setAnimationStyle(R.style.popup_window_animation);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

        imageSlider = (ImageSlider) popupView.findViewById(R.id.image_slider);

        imageSlider.setImageList(imageList, true); // centerCrop for all images

        imageSlider.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemSelected(int i) {
                String time = timeLineArrayList.get(i).getSeconds();

                Log.d("firuhtiuh", "title: " + timeLineArrayList.get(i).getTitle());
                Log.d("firuhtiuh", "secs: " + timeLineArrayList.get(i).getSeconds());

               // seektoSec(time);
                nestedScrollView.scrollTo(0, 0);
                popupWindow.dismiss();
                TabLayout.Tab tab = tabLayout12.getTabAt(0);
                tab.select();
            }
        });

        // dismiss the popup window when touched
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                TabLayout.Tab tab = tabLayout12.getTabAt(0);
                tab.select();
                startPlayer();
            }
        });

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public boolean isTablet() {
        boolean isTablet;
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float yInches = metrics.heightPixels / metrics.ydpi;
        float xInches = metrics.widthPixels / metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);
        if (diagonalInches >= 8) {
            // 6.5inch device or bigger
            isTablet = true;
        } else {
            // smaller device
            isTablet = false;
        }

        return isTablet;
    }

    public void setModuleTitle(String PARENT_CATEGORY_ID) {

        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("category_id", PARENT_CATEGORY_ID);
        //Log.d("CATEGORY_ID", "ID: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_CATEGORY_SINGLE;
        final OnlineFunctions olf = new OnlineFunctions(mContext, URL, post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("API_RESPONSE_GET16567", "" + API_RESPONSE_GET_CAT_SINGLE);

                        try {
                            JSONObject jsonObject = new JSONObject(API_RESPONSE_GET_CAT_SINGLE);
                            String status = jsonObject.getString("status");

                            if (status.equals("200")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("result");
                                JSONObject category = jsonArray.getJSONObject(0);
                                String category_name = category.getString("category_name");
                                // MODULE_TITLE = category_name;
                                actionBarTitle.setText(category_name);
                            }

                        } catch (JSONException e) {

                        }
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

    private void pausePlayer() {
        player.setPlayWhenReady(false);
        player.getPlaybackState();
    }

    private void startPlayer() {
        player.setPlayWhenReady(true);
        player.getPlaybackState();
    }

    public void updateWatchTime() {
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("video_id", CURRENT_VIDEO_ID);
        post_data.put("watch_status", "1");
        post_data.put("time", "" + player.getCurrentPosition());
        post_data.put("login_id", userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_SET_NEW_VIDEO_WATCH_TIME;
        final OnlineFunctions olf = new OnlineFunctions(mContext, URL, post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_UPDATE_WATCH_TIME = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("76DHGFJDG", "run: " + API_RESPONSE_UPDATE_WATCH_TIME);
                    }
                });

            }
        };

        new Thread(runnable).start();
    }

    private void Dialogue_package_list() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_package_details);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        recyclerpackage = (RecyclerView) dialog.findViewById(R.id.recyclerpackage);
        tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        LinearLayoutManager vertLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerpackage.setLayoutManager(vertLayoutManager);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void Get_Package_List() {
        packageList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("login_id", userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_GET_PACKAGE_LIST;
        final OnlineFunctions olf = new OnlineFunctions(this, URL, post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CAT_SINGLE);
                            RESPONSE_STATUS_VIDEO = "";
                            RESPONSE_STATUS_VIDEO = jsonObject1.getString("status");
                            Log.d("API_RESPONSE_PACKAGE", "V-status : " + API_RESPONSE_GET_CAT_SINGLE);
                            if (RESPONSE_STATUS_VIDEO.equals("200")) {
                                JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject packg_obj = jsonArray.getJSONObject(i);
                                    String pack_id = packg_obj.getString("pack_id");
                                    String pack_name = packg_obj.getString("name");
                                    String pack_days = packg_obj.getString("days");
                                    String pack_description = packg_obj.getString("description");
                                    String pack_amount = packg_obj.getString("amount");
                                    String pack_status = packg_obj.getString("status");
                                    packageList.add(new Package_Model(pack_id, pack_name, pack_days, pack_description, pack_amount, pack_status));
                                }


                            } else {
                                Log.d("API_RESPONSE", "NO VIDEOS");

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("API_RESPONSE", "exception");
                        }
                        package_listAdapter = new Package_ListAdapter(XPlayer.this, packageList, new Package_ListAdapter.EventListener() {
                            @Override
                            public void onPackageClicked(String pack_Id, String parentId) {
                                dialog.dismiss();
                                startPayment(pack_Id, parentId);
                            }
                        });
                        recyclerpackage.setAdapter(package_listAdapter);

                        //showLoading1(false);
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

    public void startPayment(String pack_id, String amout) {
        /*
         You need to pass current activity in order to let Razorpay create CheckoutActivity
        */
        str_pack_id = pack_id;
        Activity activity = this;
        double am = Double.parseDouble(amout) * 100;
        final Checkout co = new Checkout();
        try {
            JSONObject options = new JSONObject();
            options.put("name", "package: " + str_pack_id + Calendar.getInstance().getTime());
            options.put("description", "");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", am + "");
            JSONObject preFill = new JSONObject();
            preFill.put("email", "");
            preFill.put("contact", "");
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(this, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            send_payment_response(str_pack_id, razorpayPaymentID, "1");
            Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            Log.e("dfsdgfdsg", "Payment Successful: " + razorpayPaymentID);
        } catch (Exception e) {
            //Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(this, "Payment failed: ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            // Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    public void send_payment_response(String st_pack_id, String pay_id, String status) {
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("pack_id", st_pack_id);
        post_data.put("login_id", userSession.getUSER_SESSION_ID());
        post_data.put("status", status);
        post_data.put("payment_id", pay_id);
        Log.d("CATEGORY_IDhulk", "ID: " + st_pack_id);
        String URL = ConfigServer.API_REGOR_PAY_RESPONSE;
        final OnlineFunctions olf = new OnlineFunctions(this, URL, post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("APIREGORPAYRESPONSE567", "" + API_RESPONSE_GET_CAT_SINGLE);
                        setUserSession(userSession.getUSER_SESSION_ID(),userSession.getUSER_USER_NAME(),userSession.getUSER_SESSION_TYPE(),userSession.getUSER_SESSION_ROLE(),"1","1");
                        getRelatedVideos(CURRENT_PARENT_ID);
                       /* try{
                            JSONObject jsonObject =  new JSONObject(API_RESPONSE_GET_CAT_SINGLE);
                            String status = jsonObject.getString("status");
                            if(status.equals("200")){
                             //onBackPressed();
                                JSONArray jsonArray = jsonObject.getJSONArray("result");
                                JSONObject category = jsonArray.getJSONObject(0);
                                String category_name = category.getString("category_name");
                                // MODULE_TITLE = category_name;
                                actionBarTitle.setText(category_name);
                            }

                        }catch (JSONException e){

                        }*/
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

    public void Make_Favourite(String str_vid_id, String fav_sts) {
        packageList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("login_id", userSession.getUSER_SESSION_ID());
        post_data.put("video_id", str_vid_id);
        post_data.put("status", fav_sts);
        String URL = ConfigServer.API_MAKE_FAVORITE;
        final OnlineFunctions olf = new OnlineFunctions(this, URL, post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CAT_SINGLE);
                            String Fav_sts = jsonObject1.getString("status");
                            Log.d("API_RESPONSE_Fav_sts", "F-status : " + API_RESPONSE_GET_CAT_SINGLE);
                            if (Fav_sts.equals("200")) {
                            } else {
                                Log.d("API_RESPONSE", "NO VIDEOS");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("API_RESPONSE", "exception");
                        }
                        //showLoading1(false);
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

    private class OnPinchListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        float startingSpan;
        float endSpan;
        float startFocusX;
        float startFocusY;


        public boolean onScaleBegin(ScaleGestureDetector detector) {
            startingSpan = detector.getCurrentSpan();
            startFocusX = detector.getFocusX();
            startFocusY = detector.getFocusY();
            return true;
        }


        public boolean onScale(ScaleGestureDetector detector) {
            //mZoomableRelativeLayout.scale(detector.getCurrentSpan()/startingSpan, startFocusX, startFocusY);
            //player_layout.setScaleX(detector.getCurrentSpan()/startingSpan, startFocusX, startFocusY);
            Log.d("PINCH123", "onScale: ");
            return true;
        }

        public void onScaleEnd(ScaleGestureDetector detector) {
            Log.d("PINCH123", "onScaleEnd: ");
            //mZoomableRelativeLayout.restore();
        }

        public void checkIfUserisPaid() {

        }


    }
    public void setUserSession(String userId,String user_name,String type,String role,String logged_in,String payment_sts){
        editor.putString("USER_SESSION_ID",userId);
        editor.putString("USER_USER_NAME",user_name);
        editor.putString("USER_SESSION_TYPE",type);
        editor.putString("USER_SESSION_ROLE",role);
        editor.putBoolean("USER_SESSION_LOGGED_IN",Boolean.valueOf(logged_in));
        editor.putString("USER_PAYMENT_STS",payment_sts);
        editor.commit();
    }
}