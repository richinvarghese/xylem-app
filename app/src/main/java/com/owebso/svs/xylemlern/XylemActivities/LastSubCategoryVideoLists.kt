package com.owebso.svs.xylemlern.XylemActivities

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.owebso.svs.xylemlern.R
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer
import com.owebso.svs.xylemlern.adapters.VideosCategoryAdapter
import com.owebso.svs.xylemlern.adapters.VideosListAdapter
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions
import com.owebso.svs.xylemlern.models.Category
import com.owebso.svs.xylemlern.models.VideoX
import com.owebso.svs.xylemlern.profile.ProfileUtils
import com.owebso.svs.xylemlern.profile.UserSession
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.math.sqrt

class LastSubCategoryVideoLists : AppCompatActivity() {
    private val mContext: Context = this@LastSubCategoryVideoLists
    var API_RESPONSE_GET_CATEGORY = ""
    var API_RESPONSE_GET_VIDEOS: String? = null
    var categoryList: ArrayList<Category>? = null
    var moduleList: ArrayList<Category>? = null
    var adapter: VideosCategoryAdapter? = null
    var recyclerview: RecyclerView? = null
    var CATEGORY_ID = ""
    var loadingLayout: LinearLayout? = null
    var mainView: LinearLayout? = null
    //ArrayList<String> catParent;
    var videoXArrayList: ArrayList<VideoX>? = null
    var recyclerView123: RecyclerView? = null
    var videoAdapter: VideosListAdapter? = null
    var gridLayoutManager: GridLayoutManager? = null
    var videoLayoutmanager: GridLayoutManager? = null
    var moduleSelectorLayoutManager: GridLayoutManager? = null
    var bottomNavigationView: BottomNavigationView? = null
    var noVidLayoutAlert: LinearLayout? = null
    var backActionB: ImageView? = null
    var recyclerViewModuleSelector: RecyclerView? = null
    var isVideoExist = false
    var isMainCategory = false
    // String CURRENT_PARENT_ID="";
    var moduleLayout: LinearLayout? = null
    var videosLayout: LinearLayout? = null
    var frameLayout: LinearLayout? = null
    var CURRENT_HOME_PARENT_ID = ""
    var PARENT_PARENT = ""
    var pageList = ArrayList<String>()
    var MODULE_PARENT = ""
    var moduleIdList = ArrayList<String>()
    var isModuleAvailable = false
    var moduleListOfIds = ""
    var actionBarTitle: TextView? = null
    var layoutContent: RelativeLayout? = null
    var RESPONSE_STATUS_CATEGORY = ""
    var RESPONSE_STATUS_VIDEO = ""
    var API_RESPONSE_GET_CAT_SINGLE = ""
    var userSession: UserSession? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_last_sub_category_lists_video)
        // hideActionBar();
        initActionBar()
        changeStatusBarColor()
        initViews()
        CATEGORY_ID = intent.getStringExtra("CATEGORY_ID")
        getCategories(CATEGORY_ID)
    }

    fun initViews() {
        val profileUtils = ProfileUtils(mContext)
        userSession = profileUtils.userSession
        layoutContent = findViewById<View>(R.id.layoutContent) as RelativeLayout
        noVidLayoutAlert = findViewById<View>(R.id.noVidLayoutAlert) as LinearLayout
        noVidLayoutAlert!!.visibility = View.GONE
        if (!isTablet) {
            gridLayoutManager = GridLayoutManager(mContext, 1)
            videoLayoutmanager = GridLayoutManager(mContext, 1)
            //  moduleSelectorLayoutManager = new GridLayoutManager(mContext,1);
        } else {
            gridLayoutManager = GridLayoutManager(mContext, 2)
            videoLayoutmanager = GridLayoutManager(mContext, 2)
            // moduleSelectorLayoutManager = new GridLayoutManager(mContext,1);
        }
        //    LinearLayout moduleLayout,videosLayout,frameLayout;
        moduleLayout = findViewById<View>(R.id.moduleLayout) as LinearLayout
        moduleLayout!!.visibility = View.GONE
        frameLayout = findViewById<View>(R.id.frameLayout) as LinearLayout
        recyclerview = findViewById<View>(R.id.recyclerview) as RecyclerView
        recyclerView123 = findViewById<View>(R.id.recyclerView123) as RecyclerView
        recyclerViewModuleSelector = findViewById<View>(R.id.recyclerViewModuleSelector) as RecyclerView
        //recyclerViewModuleSelector.setLayoutManager(moduleSelectorLayoutManager);
        recyclerViewModuleSelector!!.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        recyclerview!!.layoutManager = gridLayoutManager
        recyclerView123!!.layoutManager = videoLayoutmanager
        loadingLayout = findViewById<View>(R.id.loadingLayout) as LinearLayout
        videosLayout = findViewById<View>(R.id.videosLayout) as LinearLayout
        loadingLayout!!.visibility = View.GONE
        videosLayout!!.visibility = View.GONE
        mainView = findViewById<View>(R.id.mainView) as LinearLayout
        //getModule(CATEGORY_ID);
        isMainCategory = true
        // showNoDataAlert(3,false);
        getCategories(CATEGORY_ID)
    }

    fun changeStatusBarColor() {
        val window = this@LastSubCategoryVideoLists.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@LastSubCategoryVideoLists, R.color.accentColor1)
    }

    fun initActionBar() {
        val actionBarLayout = layoutInflater.inflate(
                R.layout.action_bar_04,
                null) as ViewGroup
        // Set up your ActionBar
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(false)
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.customView = actionBarLayout
        // You customization
        val actionBarColor = resources.getColor(R.color.accentColor1)
        actionBar.setBackgroundDrawable(ColorDrawable(actionBarColor))
        val parent = actionBarLayout.parent as Toolbar
        // parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//  parent.setContentInsetsAbsolute(0,0);
        actionBarTitle = findViewById<View>(R.id.actionBarTitle) as TextView
        actionBarTitle!!.text = "VIDEOS"
        val togBackButton = findViewById<View>(R.id.togBackButton) as ImageView
        togBackButton.setOnClickListener { finish() }
    }// smaller device

    // 6.5inch device or bigger
    val isTablet: Boolean
        get() {
            val isTablet: Boolean
            val metrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(metrics)
            val yInches = metrics.heightPixels / metrics.ydpi
            val xInches = metrics.widthPixels / metrics.xdpi
            val diagonalInches = sqrt(xInches * xInches + yInches * yInches.toDouble())
            isTablet = diagonalInches >= 8
            return isTablet
        }

    fun getCategories(CATEGORY_ID: String) {
        showLoading1(true)
        setModuleTitle(CATEGORY_ID)
        videosLayout!!.visibility = View.GONE
        categoryList = ArrayList()
        categoryList!!.clear()
        val post_data = HashMap<String, String>()
        post_data["hash_code"] = ConfigServer.API_HASH
        post_data["parent_id"] = CATEGORY_ID
        post_data["section"] = "video"
        post_data["login_id"] = userSession!!.useR_SESSION_ID
        Log.d("CATEGORY_ID", "ID: $CATEGORY_ID")
        val URL = ConfigServer.API_GET_CATEGORY
        val olf = OnlineFunctions(mContext, URL, post_data)
        val handler = Handler()
        val runnable = Runnable {
            API_RESPONSE_GET_CATEGORY = olf.pOstResult
            handler.post {
                noVidLayoutAlert!!.visibility = View.GONE
                Log.d("API_RESPONSE", "" + API_RESPONSE_GET_CATEGORY)
                Log.d("API_RESPONSE", "CAT_ID=$CATEGORY_ID")
                try {
                    val jsonObject1 = JSONObject(API_RESPONSE_GET_CATEGORY)
                    RESPONSE_STATUS_CATEGORY = ""
                    RESPONSE_STATUS_CATEGORY = jsonObject1.getString("status")
                    val video_exist = jsonObject1.getString("video_exist")
                    //                            if(video_exist.equals("0")){
//
//                                isVideoExist=false;
//                                Log.d("API_RESPONSE1", "NO_VIDEO");
//                                Log.d("API_RESPONSE1", "status : "+RESPONSE_STATUS_CATEGORY);
//
//                            }else if(video_exist.equals("1")){
//                                Log.d("API_RESPONSE1", "VIDEO, CATID = "+CATEGORY_ID);
//                                isVideoExist=true;
//                            }
                    val jsonArray = JSONArray(jsonObject1.getString("result"))
                    moduleListOfIds = ""
                    for (i in 0 until jsonArray.length()) {
                        val catObj = jsonArray.getJSONObject(i)
                        val category_id = catObj.getString("category_id")
                        val category_name = catObj.getString("category_name")
                        val description = catObj.getString("description")
                        val parent_id = catObj.getString("parent_id")
                        val category_section = catObj.getString("category_section")
                        val category_status = catObj.getString("category_status")
                        var category_image = catObj.getString("category_image")
                        val created_at = catObj.getString("created_at")
                        val is_module = catObj.getString("is_module")
                        if (is_module == "1") {
                            moduleListOfIds = if (moduleListOfIds == "") {
                                category_id
                            } else {
                                "$moduleListOfIds,$category_id"
                            }
                            isModuleAvailable = true
                        }
                        category_image = ConfigServer.PATH_CATEGORY_THUMB + category_image
                        categoryList!!.add(Category(i, category_id, category_name, description, parent_id, category_section, category_status, category_image, created_at, 1))
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                adapter = VideosCategoryAdapter(mContext, categoryList, VideosCategoryAdapter.EventListener {
                    categoryId, parentId ->
                    val `in` = Intent(mContext, VideosLists::class.java)
                    `in`.putExtra("CATEGORY_ID", categoryId)
                    startActivity(`in`)
                })
                recyclerview!!.adapter = adapter
                adapter!!.notifyDataSetChanged()
                frameLayout!!.visibility = View.VISIBLE
                //  showLoading1(false);
                if (!isVideoExist) {
                    showLoading1(false)
                }
            }
        }
        Thread(runnable).start()
    }

    public override fun onResume() {
        super.onResume()
        // showLoading(false);
    }

    fun showLoading1(stat: Boolean) {
        if (stat) {
            layoutContent!!.visibility = View.GONE
            loadingLayout!!.visibility = View.VISIBLE
        } else {
            layoutContent!!.visibility = View.VISIBLE
            loadingLayout!!.visibility = View.GONE
            if (RESPONSE_STATUS_CATEGORY == "200") {
                showNoDataAlert(0, false)
                Log.d("RESP--TATUS", "1111")
            } else if (RESPONSE_STATUS_CATEGORY == "404") {
                if (!isVideoExist) {
                    showNoDataAlert(1, true)
                } else {
                    showNoDataAlert(1, false)
                }
                Log.d("RESP--TATUS", "2222")
                //                if(RESPONSE_STATUS_VIDEO.equals("200")){
//                    showNoDataAlert(0,false);
//                    Log.d("RESP--TATUS", "22222");
//                }else if(RESPONSE_STATUS_VIDEO.equals("404")) {
//                    Log.d("RESP--TATUS", "333333");
//                    showNoDataAlert(0,true);
//                }
            }
        }
        Log.d("RESP--TATUS", "CAT: $RESPONSE_STATUS_CATEGORY")
        Log.d("RESP--TATUS", "VID: $RESPONSE_STATUS_VIDEO")
    }

    fun setPageList(ID: String) {
        try {
            if (pageList[pageList.size - 1] != ID) {
                pageList.add(ID)
            }
        } catch (e: ArrayIndexOutOfBoundsException) {
            pageList.add(ID)
        }
    }

    fun showNoDataAlert(type: Int, stat: Boolean) {
        Log.d("SDKJGSKJD", "type : $type")
        //        if(stat){
//            Log.d("SDKJGSKJD", "showNoDataAlert: HIDDEN");
//            noVidLayoutAlert.setVisibility(View.GONE);
//            layoutContent.setVisibility(View.vi);
//        }else {
//            Log.d("SDKJGSKJD", "showNoDataAlert: SHOWN");
//            noVidLayoutAlert.setVisibility(View.GONE);
//            layoutContent.setVisibility(View.VISIBLE);
//        }
    }

    fun setModuleTitle(PARENT_CATEGORY_ID: String) {
        val post_data = HashMap<String, String>()
        post_data["hash_code"] = ConfigServer.API_HASH
        post_data["category_id"] = PARENT_CATEGORY_ID
        //Log.d("CATEGORY_ID", "ID: "+CATEGORY_ID);
        val URL = ConfigServer.API_GET_CATEGORY_SINGLE
        val olf = OnlineFunctions(mContext, URL, post_data)
        val handler = Handler()
        val runnable = Runnable {
            API_RESPONSE_GET_CAT_SINGLE = olf.pOstResult
            handler.post {
                Log.d("API_RESPONSE_GET16567", "" + API_RESPONSE_GET_CAT_SINGLE)
                try {
                    val jsonObject = JSONObject(API_RESPONSE_GET_CAT_SINGLE)
                    val status = jsonObject.getString("status")
                    if (status == "200") {
                        val jsonArray = jsonObject.getJSONArray("result")
                        val category = jsonArray.getJSONObject(0)
                        val category_name = category.getString("category_name")
                        // MODULE_TITLE = category_name;
                        actionBarTitle!!.text = category_name
                    }
                } catch (e: JSONException) {
                }
            }
        }
        Thread(runnable).start()
    }
}