package com.owebso.svs.xylemlern.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.owebso.svs.xylemlern.MainActivity;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.profile.UserProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    Context mContext = LoginActivity.this;
    String STRING_USERNAME,STRING_PASSWORD;
    EditText etUsername,etPassword;
    String API_RESPONSE_LOGIN="";
    ProgressDialog progressDialog;
    String API_RESPONSE_GET_USER_DETAIL="";
    String API_RESPONSE_GET_FULL_PROFILE="";
    String user_name;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        hideActionBar();
        changeStatusBarColor();
        initViews();
    }
    public void changeStatusBarColor(){
        Window window = LoginActivity.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(LoginActivity.this,R.color.accentColor1));
    }
    public void initViews(){
        pref = getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        etUsername = (EditText)findViewById(R.id.etUsername);
        etPassword = (EditText)findViewById(R.id.etPassword);

       // etUsername.setText("jhon1@gmail.com");
       // etPassword.setText("123!");


        Button buttonLogin = (Button)findViewById(R.id.buttonLogin);
        Button buttonRegister = (Button)findViewById(R.id.buttonRegister);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkData();
               // openMainActivity();
            }
        });
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegisterActivity();
            }
        });

    }

    public void checkData(){
        boolean FLAG = true;
        STRING_USERNAME = etUsername.getText().toString().trim();
        STRING_PASSWORD = etPassword.getText().toString();

        if(STRING_USERNAME.equals("")){
            FLAG = false;
        }
        if(STRING_PASSWORD.equals("")){
            FLAG = false;
        }

        if(FLAG){
            authenticateUser();
        }else {
            Toast.makeText(mContext, "Enter Login", Toast.LENGTH_SHORT).show();
        }
    }

    public void authenticateUser(){


        Log.d("KJWHGDKWY", "RESPONSE: "+STRING_USERNAME+"      "+STRING_PASSWORD+"     "+ConfigServer.API_HASH);
        showProgress(true,"Authenticating User..");
        //Toast.makeText(mContext, "CALLING REGISTER FUNCTION", Toast.LENGTH_SHORT).show();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("user_name",STRING_USERNAME);
        post_data.put("password",STRING_PASSWORD);
        post_data.put("hash_code", ConfigServer.API_HASH);
        final OnlineFunctions olf = new OnlineFunctions(mContext,ConfigServer.API_GET_USER_LOGIN,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_LOGIN = olf.getPOstResult();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("KJWHGDKWY", "RESPONSE: "+API_RESPONSE_LOGIN);
                            try {
                                showProgress(false,"");
                                JSONObject jsonObject = new JSONObject(API_RESPONSE_LOGIN);
                                String status = jsonObject.getString("status");
                                if(status.equals("200")){
                                    JSONObject response = jsonObject.getJSONObject("response");
                                    String login_id = response.getString("login_id");
                                    user_name = response.getString("name");
                                    String type = response.getString("type");
                                    String role = response.getString("role");
                                    String logged_in = response.getString("logged_in");
                                    String payment_sts = response.getString("payment_status");
                                    Log.d("fbhdbfh",user_name+"   Login  "+payment_sts+"   "+login_id);
                                    setUserSession(login_id,user_name,type,role,logged_in,payment_sts);
                                    getProfileDetails(login_id);
//                                    Intent in = new Intent(mContext,MainActivity.class);
//                                    startActivity(in);
//                                    finish();
                                }else if (status.equals("404")){
                                    showProgress(false,"");
                                   // Toast.makeText(mContext, "Already LoggedIn in some other device, You have to log out first from that device.", Toast.LENGTH_SHORT).show();
                                    Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                                }else {
                                    Toast.makeText(mContext, "Login Error..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            }
        };
        new Thread(runnable).start();

    }

    public void getProfileDetails(String userId){

        showProgress(true,"Authenticating User..");
        //Toast.makeText(mContext, "CALLING REGISTER FUNCTION", Toast.LENGTH_SHORT).show();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        String URL = ConfigServer.API_GET_USER_PROFILE_DATA+userId;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_FULL_PROFILE = olf.getPOstResult();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            Log.d("KJWHGDKWwdY", "RESPONSE: "+API_RESPONSE_GET_FULL_PROFILE);

                            try {
                                showProgress(false,"");
                                JSONObject jsonObject = new JSONObject(API_RESPONSE_GET_FULL_PROFILE);
                                String status = jsonObject.getString("status");
                                if(status.equals("200")){
                                    JSONObject response = jsonObject.getJSONObject("response");
                                    String login_id = response.getString("login_id");
                                    String category = response.getString("category");
                                        editor.putString("USER_SESSION_CATEGORY",category);
                                        editor.commit();
                                    Intent in = new Intent(mContext,MainActivity.class);
                                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(in);
                                    finish();
                                }else {
                                    Toast.makeText(mContext, "Login Error..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            }
        };

        new Thread(runnable).start();

    }


    private void showProgress(boolean status, String msg) {

        if(progressDialog==null){
            progressDialog= new ProgressDialog(mContext);
        }

        progressDialog.setMessage(msg);

        if(status){
            if(!progressDialog.isShowing()){
                progressDialog.show();
            }
        }else {
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }
    }


    public void hideActionBar(){
        try{
            getSupportActionBar().hide();
        }catch (Exception e){
            try{
                getActionBar().hide();
            }catch (Exception e1){
                e1.printStackTrace();
            }

        }
    }

    public void openMainActivity(){
        Intent in = new Intent(mContext, MainActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(in);
    }

    public void openRegisterActivity(){
        Intent in = new Intent(mContext, RegisterActivity.class);
        startActivity(in);
    }

    public void getUserProfile(String userId){
        //API_GET_USER_PROFILE_DATA

        showProgress(true,"Creating Account..");
        //Toast.makeText(mContext, "CALLING REGISTER FUNCTION", Toast.LENGTH_SHORT).show();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        String URL = ConfigServer.API_GET_USER_PROFILE_DATA+userId;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject jsonObject = new JSONObject(API_RESPONSE_GET_USER_DETAIL);
                                JSONObject response = jsonObject.getJSONObject("response");

                                String registration_id = response.getString("registration_id");
                                String login_id = response.getString("login_id");
                                String register_no = response.getString("register_no");
                                String name = response.getString("name");
                                String year = response.getString("year");
                                String category = response.getString("category");
                                String school_name = response.getString("school_name");
                                String address = response.getString("address");
                                String registration_status = response.getString("registration_status");
                                String paid = response.getString("paid");
                                String expiry_date = response.getString("expiry_date");
                                String created_at = response.getString("created_at");
                                String updated_at = response.getString("updated_at");
                                String user_name = response.getString("user_name");
                                String password = response.getString("password");
                                String email = response.getString("email");
                                String phone = response.getString("phone");
                                String role = response.getString("role");
                                String status = response.getString("status");
                                String permissions = response.getString("permissions");
                                String tocken = response.getString("tocken");
                                String password_reset_code = response.getString("password_reset_code");
                                String last_ip = response.getString("last_ip");
                                String is_verify = response.getString("is_verify");

                                UserProfile userSession = new UserProfile(registration_id,login_id,name,year,category,school_name,
                                        address,registration_status,paid,expiry_date,user_name,password,email,phone,role,status,permissions,tocken);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            }
        };

        new Thread(runnable).start();
    }

    public void setUserSession(String userId,String user_name,String type,String role,String logged_in,String payment_sts){
        editor.putString("USER_SESSION_ID",userId);
        editor.putString("USER_USER_NAME",user_name);
        editor.putString("USER_SESSION_TYPE",type);
        editor.putString("USER_SESSION_ROLE",role);
        editor.putBoolean("USER_SESSION_LOGGED_IN",Boolean.valueOf(logged_in));
        editor.putString("USER_PAYMENT_STS",payment_sts);
        editor.commit();
    }
}
