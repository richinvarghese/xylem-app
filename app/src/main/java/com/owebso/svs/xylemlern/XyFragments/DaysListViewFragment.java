package com.owebso.svs.xylemlern.XyFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.XylemActivities.ListViewOfVideoModules;
import com.owebso.svs.xylemlern.XylemActivities.VideosLists;
import com.owebso.svs.xylemlern.adapters.Day_ListAdapter;
import com.owebso.svs.xylemlern.adapters.ModuleAdapter;
import com.owebso.svs.xylemlern.adapters.VideosCategoryAdapter;
import com.owebso.svs.xylemlern.adapters.VideosListAdapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Category;
import com.owebso.svs.xylemlern.models.Days_Model;
import com.owebso.svs.xylemlern.models.VideoX;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DaysListViewFragment extends Fragment {
    String API_RESPONSE_GET_CATEGORY = "", API_RESPONSE_GET_SINGLE_CAT_RESULT;
    ArrayList<Days_Model> days_List;
    Day_ListAdapter adapter;
    RecyclerView recyclerview_days;
    String CATEGORY_ID = "55";
    LinearLayout loadingLayout, mainView;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager videoLayoutmanager;
    LinearLayout noVidLayoutAlert;
    boolean isVideoExist = false;
    boolean isMainCategory = false;
    LinearLayout frameLayout;
    boolean isModuleAvailable = false;
    String moduleListOfIds = "";
    RelativeLayout contentLayout;
    int ii = 0;
    UserSession userSession;
    ProfileUtils profileUtils;
    private Context mContext = getContext();
    CardView card_favorite;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.days_list_view, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void initViews() {

        profileUtils = new ProfileUtils(getActivity());
        userSession = profileUtils.getUserSession();

        CATEGORY_ID = profileUtils.getCategorytype().getCAT_VIDEO();


        noVidLayoutAlert = (LinearLayout) getView().findViewById(R.id.noVidLayoutAlert);
        contentLayout = (RelativeLayout) getView().findViewById(R.id.contentLayout);
        noVidLayoutAlert.setVisibility(View.GONE);

        if (!isTablet()) {
            gridLayoutManager = new GridLayoutManager(getContext(), 1);
            videoLayoutmanager = new GridLayoutManager(getContext(), 1);
            //  moduleSelectorLayoutManager = new GridLayoutManager(getContext(),1);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
            videoLayoutmanager = new GridLayoutManager(getContext(), 2);
            // moduleSelectorLayoutManager = new GridLayoutManager(getContext(),1);
        }

        //    LinearLayout moduleLayout,videosLayout,frameLayout;

        frameLayout = (LinearLayout) getView().findViewById(R.id.frameLayout);


        recyclerview_days = (RecyclerView) getView().findViewById(R.id.recyclerview_days);
        card_favorite = (CardView) getView().findViewById(R.id.card_favorite);
        recyclerview_days.setLayoutManager(gridLayoutManager);
        loadingLayout = (LinearLayout) getView().findViewById(R.id.loadingLayout);
        loadingLayout.setVisibility(View.GONE);
        card_favorite.setVisibility(View.VISIBLE);


        mainView = (LinearLayout) getView().findViewById(R.id.mainView);
        //getModule(CATEGORY_ID);
        isMainCategory = true;
        getDays();


        card_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), VideosLists.class);
                intent.putExtra("CATEGORY_ID","fav");
                startActivity(intent);
            }
        });
        //  showLoading(true);

    }


    public boolean isTablet() {
        boolean isTablet;
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float yInches = metrics.heightPixels / metrics.ydpi;
        float xInches = metrics.widthPixels / metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);
        if (diagonalInches >= 8) {
            // 6.5inch device or bigger
            isTablet = true;
        } else {
            // smaller device
            isTablet = false;
        }

        return isTablet;
    }

    public void getDays() {
        //videosLayout.setVisibility(View.GONE);
        showLoading(true);
        days_List = new ArrayList<>();
        days_List.clear();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        String URL = ConfigServer.API_GET_DAY_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext, URL, post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CATEGORY = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        if (isMainCategory) {
                            showLoading(false);
                        }

                        //  showLoading(false);
                        noVidLayoutAlert.setVisibility(View.GONE);
                        Log.d("NeW_API_RESPONSE", "" + API_RESPONSE_GET_CATEGORY);
                        Log.d("API_RESPONSE", "CAT_ID=" + CATEGORY_ID);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
                            String status = jsonObject1.getString("status");
                          //  String video_exist = jsonObject1.getString("video_exist");
/*
                            if (video_exist.equals("0")) {
                                isVideoExist = false;
                                Log.d("API_RESPONSE1", "NO_VIDEO");
                            } else if (video_exist.equals("1")) {
                                Log.d("API_RESPONSE1", "VIDEO, CATID = " + CATEGORY_ID);
                                isVideoExist = true;
                                // getVideos(CATEGORY_ID);
                            }*/
                            if (status.equals("404")) {

                            } else {

                            }

                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                            moduleListOfIds = "";
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject catObj = jsonArray.getJSONObject(i);
                                String category_id = catObj.getString("category_id");
                                String day_name = catObj.getString("name");
                                String day_sts = catObj.getString("status");
                                String day_img = catObj.getString("image");
                                String is_module = catObj.getString("is_module");
                                if (is_module.equals("1")) {
                                    if (moduleListOfIds.equals("")) {
                                        moduleListOfIds = category_id;
                                    } else {
                                        moduleListOfIds = moduleListOfIds + "," + category_id;
                                    }
                                    isModuleAvailable = true;
                                }
                                days_List.add(new Days_Model(i, category_id, day_name, day_sts, day_img, 1));
                                showLoading(false);
                                adapter = new Day_ListAdapter(getContext(), days_List, new Day_ListAdapter.EventListener() {
                                    @Override
                                    public void onCategoryClicked(String categoryId, String parentId) {
                                        if (isMainCategory) {

                                        }
                                        Intent intent = new Intent(getContext(), VideosLists.class);
                                        intent.putExtra("CATEGORY_ID",categoryId);
                                        startActivity(intent);
                                        // getModule(parentId);
                                      //  getDays();
                                        isMainCategory = false;
                                        Log.d("CLICKTESTST", "ESLSEEEE: ");
                                    }
                                });
                                recyclerview_days.setAdapter(adapter);

                                adapter.notifyDataSetChanged();
                                frameLayout.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                 /*       if (isModuleAvailable) {
                            //showLoading(false);
                            Intent intent = new Intent(getContext(), ListViewOfVideoModules.class);
                            intent.putExtra("MODULE_LIST", moduleListOfIds);
                            intent.putExtra("CATEGORY_ID", CATEGORY_ID);
                            startActivity(intent);
                        } else {
                          //  showLoading(false);
                            adapter = new Day_ListAdapter(getContext(), days_List, new Day_ListAdapter.EventListener() {
                                @Override
                                public void onCategoryClicked(String categoryId, String parentId) {
                                    if (isMainCategory) {

                                    }
                                    // getModule(parentId);
                                    getDays();
                                    isMainCategory = false;
                                    Log.d("CLICKTESTST", "ESLSEEEE: ");
                                }
                            });
                            recyclerview_days.setAdapter(adapter);

                            adapter.notifyDataSetChanged();
                            frameLayout.setVisibility(View.VISIBLE);
                        }*/
                        // showParentAtTop(CATEGORY_ID);
                        //setPageList(CATEGORY_ID);


                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (ii == 0) {
            ii++;
        } else {
            showLoading(false);
        }
        //  showLoading(false);

    }


    public void showLoading(boolean stat) {
        if (stat) {
            contentLayout.setVisibility(View.GONE);
            loadingLayout.setVisibility(View.VISIBLE);
        } else {
            contentLayout.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        }
    }


}
