package com.owebso.svs.xylemlern.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.login.LoginActivity;

public class ProfileUtils {

    private Context mContext;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    UserSession userSession;

    public ProfileUtils(Context mContext) {
        this.mContext = mContext;
    }

    public UserSession getUserSession(){
        pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode

        String  USER_SESSION_ID = pref.getString("USER_SESSION_ID","");
        String  USER_USER_NAME = pref.getString("USER_USER_NAME","");
        String  USER_SESSION_TYPE = pref.getString("USER_SESSION_TYPE","");
        String  USER_SESSION_ROLE = pref.getString("USER_SESSION_ROLE","");
        String  USER_SESSION_CATEGORY = pref.getString("USER_SESSION_CATEGORY","");
        boolean  USER_SESSION_LOGGED_IN = pref.getBoolean("USER_SESSION_LOGGED_IN",false);
        boolean  USER_SESSION_ACCOUNT_VERIFIED = pref.getBoolean("USER_SESSION_ACCOUNT_VERIFIED",false);
        String USER_SESSION_PAYMENT = pref.getString("USER_PAYMENT_STS","");
//        boolean LOGGEDIN = Boolean.parseBoolean(USER_SESSION_LOGGED_IN);
        userSession = new UserSession(USER_SESSION_ID,USER_USER_NAME,USER_SESSION_TYPE,USER_SESSION_ROLE,USER_SESSION_CATEGORY,USER_SESSION_LOGGED_IN,USER_SESSION_ACCOUNT_VERIFIED,USER_SESSION_PAYMENT);
        return userSession;
    }

    public void logoutUserSession(){
        pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        editor.clear();
        editor.commit();
        Intent in = new Intent(mContext, LoginActivity.class);
        mContext.startActivity(in);
        ((Activity)mContext).finish();
    }

    public CategoryType getCategorytype(){
        String  USER_SESSION_CATEGORY = pref.getString("USER_SESSION_CATEGORY","");
        CategoryType categoryType;
        if(USER_SESSION_CATEGORY.equals("1")){
            categoryType = new CategoryType(ConfigServer.CAT_VIDEO_NEET,ConfigServer.CAT_MCQ_NEET);
        }else {
            categoryType = new CategoryType(ConfigServer.CAT_VIDEO_JEE,ConfigServer.CAT_MCQ_JEE);
        }

        return categoryType;
    }

    public class CategoryType{
        private String CAT_VIDEO;
        private String CAT_MCQ;

        public CategoryType(String CAT_VIDEO, String CAT_MCQ) {
            this.CAT_VIDEO = CAT_VIDEO;
            this.CAT_MCQ = CAT_MCQ;
        }

        public String getCAT_VIDEO() {
            return CAT_VIDEO;
        }

        public void setCAT_VIDEO(String CAT_VIDEO) {
            this.CAT_VIDEO = CAT_VIDEO;
        }

        public String getCAT_MCQ() {
            return CAT_MCQ;
        }

        public void setCAT_MCQ(String CAT_MCQ) {
            this.CAT_MCQ = CAT_MCQ;
        }
    }

}
