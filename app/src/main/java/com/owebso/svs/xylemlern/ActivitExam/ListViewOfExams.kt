package com.owebso.svs.xylemlern.ActivitExam

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.owebso.svs.xylemlern.R
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer
import com.owebso.svs.xylemlern.ServerConfig.DataConstants
import com.owebso.svs.xylemlern.adapters.ExamListWithTabsAdapter
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions
import com.owebso.svs.xylemlern.models.Exam
import com.owebso.svs.xylemlern.profile.ProfileUtils
import com.owebso.svs.xylemlern.profile.UserSession
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ListViewOfExams : AppCompatActivity() {
    var recyclerView: RecyclerView? = null
    var mContext: Context = this@ListViewOfExams
    private var API_RESPONSE_GET_Exam = ""
    var allExamsList: ArrayList<Exam>? = null
    var weeklyTestList: ArrayList<Exam>? = null
    var monthlyTestList: ArrayList<Exam>? = null
    var grandTestList: ArrayList<Exam>? = null
    var subjectwiseTestList: ArrayList<Exam>? = null
    var moduleList: ArrayList<String>? = null
    var isModule = false
    var adapter22: ExamListWithTabsAdapter? = null
    var CURRENT_MONTH = ""
    var isExamTitleSet = false
    //String LIST_OF_MODULES="";
    var userSession: UserSession? = null
    var layoutNoDataAlert: LinearLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_exam_list_view_with_tabs)
        initActionBar()
        changeStatusBarColor()
        initViews()
        setupTabLayout()
        //getSupportActionBar().hide();
        examsList
    }

    fun changeStatusBarColor() {
        val window = this@ListViewOfExams.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(mContext, R.color.accentColor1)
    }

    fun initActionBar() {
        val actionBarLayout = layoutInflater.inflate(
                R.layout.action_bar_04,
                null) as ViewGroup
        // Set up your ActionBar
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(false)
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.customView = actionBarLayout
        // You customization
        val actionBarColor = resources.getColor(R.color.accentColor1)
        actionBar.setBackgroundDrawable(ColorDrawable(actionBarColor))
        val parent = actionBarLayout.parent as Toolbar
        // parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//  parent.setContentInsetsAbsolute(0,0);
        val actionBarTitle = findViewById<View>(R.id.actionBarTitle) as TextView
        actionBarTitle.text = "Exams"
        val togBackButton = findViewById<View>(R.id.togBackButton) as ImageView
        togBackButton.setOnClickListener { finish() }
    }

    fun initViews() {
        val profileUtils = ProfileUtils(mContext)
        userSession = profileUtils.userSession
        layoutNoDataAlert = findViewById<View>(R.id.layoutNoDataAlert) as LinearLayout
        layoutNoDataAlert!!.visibility = View.GONE
        recyclerView = findViewById<View>(R.id.recyclerView) as RecyclerView
        val layoutManager = LinearLayoutManager(mContext)
        recyclerView!!.layoutManager = layoutManager
        moduleList = ArrayList()
    }// String video_exist = jsonObject1.getString("video_exist");
    // getSortedList(); //CREATING SORTED LIST
//Log.d("API_RESPONSE", "CAT_ID="+Exam_ID);

    //CHANGE THIS PARAMETERS BEFORE RUN #IMP
//Log.d("Exam_ID", "ID: "+Exam_ID);
    val examsList: Unit
        get() {
            allExamsList = ArrayList()
            weeklyTestList = ArrayList()
            monthlyTestList = ArrayList()
            grandTestList = ArrayList()
            subjectwiseTestList = ArrayList()
            allExamsList!!.clear()
            weeklyTestList!!.clear()
            monthlyTestList!!.clear()
            grandTestList!!.clear()
            subjectwiseTestList!!.clear()
            val post_data = HashMap<String, String>()
            post_data["hash_code"] = ConfigServer.API_HASH
            post_data["category_id"] = ConfigServer.CAT_EXAM_NEET
            //CHANGE THIS PARAMETERS BEFORE RUN #IMP
//Log.d("Exam_ID", "ID: "+Exam_ID);
            val URL = ConfigServer.API_GET_EXAMS_LIST
            val olf = OnlineFunctions(mContext, URL, post_data)
            val handler = Handler()
            val runnable = Runnable {
                API_RESPONSE_GET_Exam = olf.pOstResult
                handler.post {
                    Log.d("API_RESPONSE", "" + API_RESPONSE_GET_Exam)
                    //Log.d("API_RESPONSE", "CAT_ID="+Exam_ID);
                    try {
                        val jsonObject1 = JSONObject(API_RESPONSE_GET_Exam)
                        val status = jsonObject1.getString("status")
                        // String video_exist = jsonObject1.getString("video_exist");
                        val jsonArray = JSONArray(jsonObject1.getString("response"))
                        CURRENT_MONTH = "NULLL"
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject = jsonArray.getJSONObject(i)
                            val exam_id = jsonObject.getString("exam_id")
                            val exam_title = jsonObject.getString("exam_title")
                            val description = jsonObject.getString("description")
                            val category_id = jsonObject.getString("category_id")
                            val university_id = jsonObject.getString("university_id")
                            val coursetype_id = jsonObject.getString("coursetype_id")
                            val course_id = jsonObject.getString("course_id")
                            val subject_id = jsonObject.getString("subject_id")
                            val instruction_id = jsonObject.getString("instruction_id")
                            val examtype = jsonObject.getString("examtype")
                            val exam_type_cat = jsonObject.getString("exam_type_cat")
                            val start_date = jsonObject.getString("start_date")
                            val end_date = jsonObject.getString("end_date")
                            val duration = jsonObject.getString("duration")
                            val total_mark = jsonObject.getString("total_mark")
                            val pass_mark = jsonObject.getString("pass_mark")
                            val negative_mark = jsonObject.getString("negative_mark")
                            val published = jsonObject.getString("published")
                            val exam_status = jsonObject.getString("exam_status")
                            val questions = jsonObject.getString("questions")
                            val created_at = jsonObject.getString("created_at")
                            val updated_at = jsonObject.getString("updated_at")
                            val examData = Exam(i, exam_id, exam_title, description, category_id, university_id, coursetype_id,
                                    course_id, subject_id, instruction_id, examtype, exam_type_cat, start_date, end_date, duration,
                                    total_mark, pass_mark, negative_mark, published, exam_status, questions,"","", created_at, updated_at
                                    ,"","", 1)
                            if (CURRENT_MONTH != getMonthFromDate(examData.start_date)) {
                                CURRENT_MONTH = getMonthFromDate(examData.start_date)
                                val exam = Exam()
                                exam.index = i
                                exam.start_date = examData.start_date
                                exam.type = 0
                                exam.exam_type_cat = examData.exam_type_cat
                                allExamsList!!.add(exam)
                                allExamsList!!.add(examData)
                                isExamTitleSet = true
                            } else {
                                isExamTitleSet = false
                                allExamsList!!.add(examData)
                            }
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    // getSortedList(); //CREATING SORTED LIST
                    generateExamTestcategories()
                    recyclerView!!.adapter = null
                    adapter22 = ExamListWithTabsAdapter(mContext, allExamsList, ExamListWithTabsAdapter.EventListener { examId, exam -> gotoExam(exam) }, 0)
                    recyclerView!!.adapter = adapter22
                }
            }
            Thread(runnable).start()
        }

    fun getMonthFromDate(dateString: String): String {
        var monthString = ""
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try {
            val date = format.parse(dateString)
            monthString = DateFormat.format("MMM yyyy", date) as String // Jun
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return monthString
    }

    fun gotoExam(examObj: Exam) {
        val `in` = Intent(mContext, ExamIntro::class.java)
        `in`.putExtra("EXAM_ID", examObj.exam_id)
        `in`.putExtra("EXAM_TITLE", examObj.exam_title)
        `in`.putExtra("EXAM_TYPE", getExamType(examObj.exam_type_cat))
        `in`.putExtra("EXAM_NEG_MARK", examObj.negative_mark)
        `in`.putExtra("EXAM_TOTAL_MARK", examObj.total_mark)
        `in`.putExtra("EXAM_PASS_MARK", examObj.pass_mark)
        `in`.putExtra("EXAM_DURATION", examObj.duration)
        startActivity(`in`)
    }

    fun getExamType(type: String?): String {
        var op = ""
        when (type) {
            DataConstants.EXAM_TYPE_ALL -> op = "Normal Test"
            DataConstants.EXAM_TYPE_WEEKLY -> op = "Weekly Test"
            DataConstants.EXAM_TYPE_MONTHLY -> op = "Monthly Test"
            DataConstants.EXAM_TYPE_GRAND_TEST -> op = "Grand Test"
            DataConstants.EXAM_TYPE_SUBJECTWISE -> op = "Subject Wise Test"
        }
        return op
    }

    fun generateExamTestcategories() {
        weeklyTestList!!.clear()
        monthlyTestList!!.clear()
        grandTestList!!.clear()
        subjectwiseTestList!!.clear()
        for (i in allExamsList!!.indices) {
            Log.d("dshkjfh", "type: " + allExamsList!![i].exam_type_cat)
            when (allExamsList!![i].exam_type_cat) {
                DataConstants.EXAM_TYPE_ALL -> {
                }
                DataConstants.EXAM_TYPE_WEEKLY -> weeklyTestList!!.add(allExamsList!![i])
                DataConstants.EXAM_TYPE_MONTHLY -> monthlyTestList!!.add(allExamsList!![i])
                DataConstants.EXAM_TYPE_GRAND_TEST -> grandTestList!!.add(allExamsList!![i])
                DataConstants.EXAM_TYPE_SUBJECTWISE -> subjectwiseTestList!!.add(allExamsList!![i])
                else -> {
                    weeklyTestList!!.add(allExamsList!![i])
                    monthlyTestList!!.add(allExamsList!![i])
                    grandTestList!!.add(allExamsList!![i])
                    subjectwiseTestList!!.add(allExamsList!![i])
                }
            }
            //            if(allExamsList.get(i).getYear().equals("1")){
//                weeklyTestList.add(allExamsList.get(i));
//            }else if(allExamsList.get(i).getYear().equals("2")){
//                monthlyTestList.add(allExamsList.get(i));
//            }else {
//
//                weeklyTestList.add(allExamsList.get(i));
//                monthlyTestList.add(allExamsList.get(i));
//            }
        }
        Log.d("TESTSUT7", "weeklyTestList SIZE: " + weeklyTestList!!.size)
        Log.d("TESTSUT7", "monthlyTestList : SIZE " + monthlyTestList!!.size)
        //   getTrimList();
    }

    //  ArrayList<Exam> cat123 = new ArrayList<>();
    val trimList: Unit
        get() { //  ArrayList<Exam> cat123 = new ArrayList<>();
            for (i in weeklyTestList!!.indices) {
                try {
                    if (getMonthFromDate(weeklyTestList!![i].start_date) == "2" && getMonthFromDate(weeklyTestList!![i + 1].start_date) == "2") {
                        weeklyTestList!!.removeAt(i)
                        weeklyTestList!!.removeAt(i + 1)
                    }
                } catch (e: IndexOutOfBoundsException) {
                    weeklyTestList!!.removeAt(i)
                }
            }
            for (i in monthlyTestList!!.indices) {
                try {
                    if (getMonthFromDate(monthlyTestList!![i].start_date) == "3" && getMonthFromDate(monthlyTestList!![i + 1].start_date) == "3") {
                        monthlyTestList!!.removeAt(i)
                        monthlyTestList!!.removeAt(i + 1)
                    }
                } catch (e: IndexOutOfBoundsException) {
                    monthlyTestList!!.removeAt(i)
                }
            }
            for (i in grandTestList!!.indices) {
                try {
                    if (getMonthFromDate(grandTestList!![i].start_date) == "4" && getMonthFromDate(grandTestList!![i + 1].start_date) == "4") {
                        grandTestList!!.removeAt(i)
                        grandTestList!!.removeAt(i + 1)
                    }
                } catch (e: IndexOutOfBoundsException) {
                    grandTestList!!.removeAt(i)
                }
            }
            for (i in subjectwiseTestList!!.indices) {
                try {
                    if (getMonthFromDate(subjectwiseTestList!![i].start_date) == "5" && getMonthFromDate(subjectwiseTestList!![i + 1].start_date) == "5") {
                        subjectwiseTestList!!.removeAt(i)
                        subjectwiseTestList!!.removeAt(i + 1)
                    }
                } catch (e: IndexOutOfBoundsException) {
                    subjectwiseTestList!!.removeAt(i)
                }
            }
        }

    fun setupTabLayout() {
        val mTabLayout = findViewById<View>(R.id.tabs) as TabLayout
        mTabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> {
                        Log.d("sizewhat", "all exam size :" + allExamsList!!.size)
                        recyclerView!!.adapter = null
                        adapter22 = ExamListWithTabsAdapter(mContext, allExamsList, ExamListWithTabsAdapter.EventListener { examId, exam -> gotoExam(exam) }, 0)
                        recyclerView!!.adapter = adapter22
                        if (allExamsList!!.size == 0) {
                            showNoDataAlert(true)
                        } else {
                            showNoDataAlert(false)
                        }
                    }
                    1 -> {
                        Log.d("sizewhat", "weekly exam size :" + weeklyTestList!!.size)
                        recyclerView!!.adapter = null
                        adapter22 = ExamListWithTabsAdapter(mContext, weeklyTestList, ExamListWithTabsAdapter.EventListener { examId, exam -> gotoExam(exam) }, 1)
                        recyclerView!!.adapter = adapter22
                        if (weeklyTestList!!.size == 0) {
                            showNoDataAlert(true)
                        } else {
                            showNoDataAlert(false)
                        }
                    }
                    2 -> {
                        Log.d("sizewhat", "monthly exam size :" + monthlyTestList!!.size)
                        recyclerView!!.adapter = null
                        adapter22 = ExamListWithTabsAdapter(mContext, monthlyTestList, ExamListWithTabsAdapter.EventListener { examId, exam -> gotoExam(exam) }, 2)
                        recyclerView!!.adapter = adapter22
                        if (monthlyTestList!!.size == 0) {
                            showNoDataAlert(true)
                        } else {
                            showNoDataAlert(false)
                        }
                    }
                    3 -> {
                        Log.d("sizewhat", "monthly exam size :" + grandTestList!!.size)
                        recyclerView!!.adapter = null
                        adapter22 = ExamListWithTabsAdapter(mContext, grandTestList, ExamListWithTabsAdapter.EventListener { examId, exam -> gotoExam(exam) }, 2)
                        recyclerView!!.adapter = adapter22
                        if (grandTestList!!.size == 0) {
                            showNoDataAlert(true)
                        } else {
                            showNoDataAlert(false)
                        }
                    }
                    4 -> {
                        Log.d("sizewhat", "monthly exam size :" + subjectwiseTestList!!.size)
                        recyclerView!!.adapter = null
                        adapter22 = ExamListWithTabsAdapter(mContext, subjectwiseTestList, ExamListWithTabsAdapter.EventListener { examId, exam -> gotoExam(exam) }, 2)
                        recyclerView!!.adapter = adapter22
                        if (subjectwiseTestList!!.size == 0) {
                            showNoDataAlert(true)
                        } else {
                            showNoDataAlert(false)
                        }
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun onTabSelected1(pos: Int) {
        when (pos) {
            0 -> Toast.makeText(this, "Tapped $pos", Toast.LENGTH_SHORT)
            else -> {
            }
        }
    }

    override fun onBackPressed() {
        finish()
        super.onBackPressed()
    }

    fun showNoDataAlert(stat: Boolean) {
        if (stat) {
            layoutNoDataAlert!!.visibility = View.VISIBLE
        } else {
            layoutNoDataAlert!!.visibility = View.GONE
        }
    }
}