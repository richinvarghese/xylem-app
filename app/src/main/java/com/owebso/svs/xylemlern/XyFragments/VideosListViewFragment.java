package com.owebso.svs.xylemlern.XyFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.adapters.ModuleAdapter;
import com.owebso.svs.xylemlern.adapters.VideosCategoryAdapter;
import com.owebso.svs.xylemlern.adapters.VideosListAdapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Category;
import com.owebso.svs.xylemlern.models.VideoX;
import com.owebso.svs.xylemlern.XylemActivities.ListViewOfVideoModules;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class VideosListViewFragment extends Fragment {
    private Context mContext = getContext();
    String API_RESPONSE_GET_CATEGORY="",API_RESPONSE_GET_SINGLE_CAT_RESULT;
    ArrayList<Category> categoryList;
    ArrayList<Category> moduleList;
    VideosCategoryAdapter adapter;
    RecyclerView recyclerview;
    String CATEGORY_ID="55";
    LinearLayout loadingLayout,mainView;
    //ArrayList<String> catParent;
    ArrayList<VideoX> videoXArrayList;
    RecyclerView recyclerView123;
    VideosListAdapter videoAdapter;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager videoLayoutmanager;
    GridLayoutManager moduleSelectorLayoutManager;
    BottomNavigationView bottomNavigationView;
    LinearLayout noVidLayoutAlert;
    ImageView backActionB;
    RecyclerView recyclerViewModuleSelector;
    boolean isVideoExist=false;
    boolean isMainCategory=false;
   // String CURRENT_PARENT_ID="";
    LinearLayout moduleLayout,videosLayout,frameLayout;
     public String CURRENT_HOME_PARENT_ID="";
     String PARENT_PARENT="";
     
     ArrayList<String> pageList = new ArrayList<>();
     String MODULE_PARENT="";

     ArrayList<String> moduleIdList = new ArrayList<>();
     boolean isModuleAvailable=false;
     String moduleListOfIds="";

     RelativeLayout contentLayout;

     int ii=0;
     String API_RESPONSE_GET_CAT_SINGLE="";
     String PARENT_CATEGORY="";
     UserSession userSession;
     ProfileUtils profileUtils;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.video_list_category_view, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
     }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void initViews(){

        profileUtils = new ProfileUtils(getActivity());
        userSession = profileUtils.getUserSession();

        CATEGORY_ID = profileUtils.getCategorytype().getCAT_VIDEO();


        noVidLayoutAlert = (LinearLayout)getView().findViewById(R.id.noVidLayoutAlert);
        contentLayout = (RelativeLayout) getView().findViewById(R.id.contentLayout);
        noVidLayoutAlert.setVisibility(View.GONE);

        if(!isTablet()){
            gridLayoutManager = new GridLayoutManager(getContext(),1);
            videoLayoutmanager = new GridLayoutManager(getContext(),1);
          //  moduleSelectorLayoutManager = new GridLayoutManager(getContext(),1);
        }else{
            gridLayoutManager = new GridLayoutManager(getContext(),2);
            videoLayoutmanager = new GridLayoutManager(getContext(),2);
           // moduleSelectorLayoutManager = new GridLayoutManager(getContext(),1);
        }

        //    LinearLayout moduleLayout,videosLayout,frameLayout;

        moduleLayout = (LinearLayout)getView().findViewById(R.id.moduleLayout);
        moduleLayout.setVisibility(View.GONE);
        frameLayout = (LinearLayout)getView().findViewById(R.id.frameLayout);


        recyclerview = (RecyclerView)getView().findViewById(R.id.recyclerview);
        recyclerView123 = (RecyclerView)getView().findViewById(R.id.recyclerView123);
        recyclerViewModuleSelector = (RecyclerView)getView().findViewById(R.id.recyclerViewModuleSelector);
        //recyclerViewModuleSelector.setLayoutManager(moduleSelectorLayoutManager);
        recyclerViewModuleSelector.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerView123.setLayoutManager(videoLayoutmanager);

        loadingLayout = (LinearLayout)getView().findViewById(R.id.loadingLayout);
        videosLayout = (LinearLayout)getView().findViewById(R.id.videosLayout);
        loadingLayout.setVisibility(View.GONE);
        videosLayout.setVisibility(View.GONE);

        mainView = (LinearLayout)getView().findViewById(R.id.mainView);

        //getModule(CATEGORY_ID);

        isMainCategory =true;
        getCategories("55");

      //  showLoading(true);

    }



    public boolean isTablet(){
        boolean isTablet;
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float yInches= metrics.heightPixels/metrics.ydpi;
        float xInches= metrics.widthPixels/metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches*xInches + yInches*yInches);
        if (diagonalInches>=8){
            // 6.5inch device or bigger
            isTablet=true;
        }else{
            // smaller device
            isTablet=false;
        }

        return isTablet;
    }
    public void getCategories(final String CATEGORY_ID){
        //videosLayout.setVisibility(View.GONE);
        showLoading(true);
        categoryList = new ArrayList<>();
        categoryList.clear();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("parent_id",CATEGORY_ID);
        post_data.put("section","video");
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        Log.d("CATEGORY_IDre", "ID: "+profileUtils.getCategorytype().getCAT_VIDEO()+"   "+userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_GET_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() { API_RESPONSE_GET_CATEGORY = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        if(isMainCategory){
                            showLoading(false);
                        }

                      //  showLoading(false);
                        noVidLayoutAlert.setVisibility(View.GONE);
                        Log.d("API_RESPONSE", ""+API_RESPONSE_GET_CATEGORY);
                        Log.d("API_RESPONSE", "CAT_ID="+CATEGORY_ID);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
                            String status = jsonObject1.getString("status");
                            String video_exist = jsonObject1.getString("video_exist");

                            if(video_exist.equals("0")){
                                isVideoExist=false;
                                Log.d("API_RESPONSE1", "NO_VIDEO");
                            }else if(video_exist.equals("1")){
                                Log.d("API_RESPONSE1", "VIDEO, CATID = "+CATEGORY_ID);
                                isVideoExist=true;
                               // getVideos(CATEGORY_ID);
                            }
                            if(status.equals("404")){

                            }else {

                            }

                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                            moduleListOfIds="";
                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject catObj = jsonArray.getJSONObject(i);
                                String category_id = catObj.getString("category_id");
                                String category_name = catObj.getString("category_name");
                                String description = catObj.getString("description");
                                String parent_id = catObj.getString("parent_id");
                                String category_section = catObj.getString("category_section");
                                String category_status = catObj.getString("category_status");
                                String category_image = catObj.getString("category_image");
                                String created_at = catObj.getString("created_at");
                                String is_module = catObj.getString("is_module");
                                if(is_module.equals("1")){
                                    if(moduleListOfIds.equals("")){
                                        moduleListOfIds=category_id;
                                    }else {
                                        moduleListOfIds=moduleListOfIds+","+category_id;
                                    }
                                    isModuleAvailable=true;
                                }
                                category_image = ConfigServer.PATH_CATEGORY_THUMB+category_image;
                                categoryList.add(new Category(i,category_id,category_name,description,parent_id,category_section,category_status,category_image,created_at,1));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(isModuleAvailable){
                            //showLoading(false);
                            Intent intent = new Intent(getContext(), ListViewOfVideoModules.class);
                            intent.putExtra("MODULE_LIST",moduleListOfIds);
                            intent.putExtra("CATEGORY_ID",CATEGORY_ID);
                            startActivity(intent);
                        }else {
                            showLoading(false);
                            adapter = new VideosCategoryAdapter(getContext(), categoryList, new VideosCategoryAdapter.EventListener() {
                                @Override
                                public void onCategoryClicked(String categoryId, String parentId) {
                                    if(isMainCategory){
                                        CURRENT_HOME_PARENT_ID=CATEGORY_ID;
                                    }
                                    // getModule(parentId);
                                    getCategories(categoryId);
                                    isMainCategory=false;
                                    Log.d("CLICKTESTST", "ESLSEEEE: ");
                                }
                            });
                            recyclerview.setAdapter(adapter);

                            adapter.notifyDataSetChanged();
                            frameLayout.setVisibility(View.VISIBLE);
                        }
                       // showParentAtTop(CATEGORY_ID);
                        //setPageList(CATEGORY_ID);



                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(ii==0){
            ii++;
        }else {
            showLoading(false);
        }
      //  showLoading(false);

    }

    public void getModule(final String CATEGORY_ID){
       // showLoading(true);

        moduleList = new ArrayList<>();
        moduleList.clear();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("parent_id",profileUtils.getCategorytype().getCAT_VIDEO());
        post_data.put("section","video");
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        Log.d("CATEGORY_ID", "ID: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CATEGORY = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLoading(false);
                        noVidLayoutAlert.setVisibility(View.GONE);
                        Log.d("API_RESPONSE", ""+API_RESPONSE_GET_CATEGORY);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
                            String status = jsonObject1.getString("status");

                            if(status.equals("404")){
                                noVidLayoutAlert.setVisibility(View.VISIBLE);
                            }else {
                                noVidLayoutAlert.setVisibility(View.GONE);
                            }
                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject catObj = jsonArray.getJSONObject(i);
                                String category_id = catObj.getString("category_id");
                                String category_name = catObj.getString("category_name");
                                String description = catObj.getString("description");
                                String parent_id = catObj.getString("parent_id");
                                String category_section = catObj.getString("category_section");
                                String category_status = catObj.getString("category_status");
                                String category_image = catObj.getString("category_image");
                                String created_at = catObj.getString("created_at");

                                category_image = ConfigServer.PATH_CATEGORY_THUMB+category_image;
                              //  CURRENT_PARENT_ID=CATEGORY_ID;

                                moduleList.add(new Category(i,category_id,category_name,description,parent_id,category_section,category_status,category_image,created_at,1));



                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        if(moduleList.size()<=0){
                            moduleLayout.setVisibility(View.GONE);
                        }else {
                            moduleLayout.setVisibility(View.VISIBLE);
                        }

                        ModuleAdapter adapter2 = new ModuleAdapter(getContext(), moduleList, new ModuleAdapter.EventListener() {
                            @Override
                            public void onEvent(int data) {
                               // setParentArray(data);
                                getCategories(String.valueOf(data));
                            }
                        });

                        recyclerViewModuleSelector.setAdapter(adapter2);
                      //  adapter.notifyDataSetChanged();
                      //  videosLayout.setVisibility(View.GONE);

                        PARENT_PARENT = CATEGORY_ID;

                        setPageList(getCURRENT_PARENT_ID());

                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    public void showLoading(boolean stat){
        if(stat){
            contentLayout.setVisibility(View.GONE);
            loadingLayout.setVisibility(View.VISIBLE);
        }else {
            contentLayout.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        }
    }

    public void callRefresh(String CAT){
            showLoading(true);
          this.CATEGORY_ID= CAT;

    }

    public String getCURRENT_PARENT_ID(){
        return PARENT_PARENT;
    }

    public String getHomeParentId(){
        return CURRENT_HOME_PARENT_ID;
    }

    public void setParentArray(int i){
        Log.d("CATEGORY_ID", "CAT_ID "+i);

        if(!isVideoExist){
            this.CATEGORY_ID=String.valueOf(i);
            getCategories(CATEGORY_ID);
            Log.d("BACKBUT_TEST", "GOING BACK TO ID =="+CATEGORY_ID);

        }else {
            //getModule(CATEGORY_ID);
           // getVideos(CATEGORY_ID);
            Log.d("BACKBUT_TEST", "GOING BACK TO ELSE =="+CATEGORY_ID);
        }


    }





    public ArrayList<String> getPageList(){
       return pageList;
    }

    public void setPageList(String ID){
        try{
            if(!pageList.get(pageList.size()-1).equals(ID)){
                pageList.add(ID);
            }
        }catch (ArrayIndexOutOfBoundsException e){
            pageList.add(ID);
        }

    }

    public void goBackTo(String ID){

        try{
            pageList.remove(pageList.size()-1);
            getCategories(ID);
        }catch (ArrayIndexOutOfBoundsException e){
            getCategories(CURRENT_HOME_PARENT_ID);
        }

    }

    public void setCurrentCategoryId(String id){
        this.CATEGORY_ID = id;
    }

    public void showParentAtTop(String parentID){
        Log.d("API_GET_PARENT_CATEGORY", ": FUNCTION CALLED");

        final String[] API_RESPONSE_GET_PARENT_CATEGORY = {""};
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("category_id",parentID);
        //Log.d("CATEGORY_ID", "ID: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_PARENT_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_PARENT_CATEGORY[0] = olf.getPOstResult();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("API_GET_PARENT_CATEGORY", ": "+API_RESPONSE_GET_PARENT_CATEGORY[0]);
                            try {
                                JSONObject jsonObject = new JSONObject(API_RESPONSE_GET_PARENT_CATEGORY[0]);
                                String status = jsonObject.getString("status");
                                if(status.equals("200")){
                                    JSONObject result = jsonObject.getJSONObject("result");
                                    String parent_id = result.getString("parent_id");

                                    if(!parent_id.equals("0")){
                                        getModule(parent_id);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            }
        };
        new Thread(runnable).start();
    }




}
