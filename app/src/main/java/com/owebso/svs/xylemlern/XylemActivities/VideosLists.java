package com.owebso.svs.xylemlern.XylemActivities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.VideoPlayer.XPlayer;
import com.owebso.svs.xylemlern.adapters.Package_ListAdapter;
import com.owebso.svs.xylemlern.adapters.VideosCategoryAdapter;
import com.owebso.svs.xylemlern.adapters.VideosListAdapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Category;
import com.owebso.svs.xylemlern.models.Package_Model;
import com.owebso.svs.xylemlern.models.VideoX;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class VideosLists extends AppCompatActivity implements PaymentResultListener {
    private Context mContext = VideosLists.this;
    String API_RESPONSE_GET_CATEGORY="",API_RESPONSE_GET_VIDEOS;
    ArrayList<Category> categoryList;
    ArrayList<Category> moduleList;
    VideosCategoryAdapter adapter;
    RecyclerView recyclerview;
    String CATEGORY_ID="";
    String msg,str_title,RESPONSE_STATUS_VIDEO;
    ArrayList<Package_Model> packageList;
    String API_RESPONSE_GET_CAT_SINGLE;
    Package_ListAdapter package_listAdapter;
    LinearLayout loadingLayout,mainView;
    //ArrayList<String> catParent;
    ArrayList<VideoX> videoXArrayList;
    RecyclerView recyclerView123;
    VideosListAdapter videoAdapter;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager videoLayoutmanager;
    GridLayoutManager moduleSelectorLayoutManager;
    BottomNavigationView bottomNavigationView;
    LinearLayout noVidLayoutAlert;
    ImageView backActionB;
    RecyclerView recyclerViewModuleSelector;
    boolean isVideoExist=false;
    boolean isMainCategory=false;
    // String CURRENT_PARENT_ID="";
    LinearLayout moduleLayout,videosLayout,frameLayout;
    public String CURRENT_HOME_PARENT_ID="";

    ArrayList<String> pageList = new ArrayList<>();
    String MODULE_PARENT="";
    String str_pack_id;
    ArrayList<String> moduleIdList = new ArrayList<>();
    boolean isModuleAvailable=false;
    String moduleListOfIds="";
    
    TextView actionBarTitle,tv_cancel;
    RelativeLayout layoutContent;

    String RESPONSE_STATUS_CATEGORY="";
    RecyclerView recyclerpackage;
    UserSession userSession;
    Dialog dialog;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_lists);
        pref = getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        // hideActionBar();
        initActionBar();
        changeStatusBarColor();
        initViews();

        CATEGORY_ID = getIntent().getStringExtra("CATEGORY_ID");

        //getCategories(CATEGORY_ID);
        if (CATEGORY_ID.equalsIgnoreCase("fav")){
            getFavouriteVideos();
        }else {
            getVideos(CATEGORY_ID);
        }

    }
    

    public void initViews(){

        ProfileUtils profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();

        layoutContent = (RelativeLayout)findViewById(R.id.layoutContent);

        noVidLayoutAlert = (LinearLayout)findViewById(R.id.noVidLayoutAlert);
        noVidLayoutAlert.setVisibility(View.GONE);

        if(!isTablet()){
            gridLayoutManager = new GridLayoutManager(mContext,1);
            videoLayoutmanager = new GridLayoutManager(mContext,1);
            //  moduleSelectorLayoutManager = new GridLayoutManager(mContext,1);
        }else{
            gridLayoutManager = new GridLayoutManager(mContext,2);
            videoLayoutmanager = new GridLayoutManager(mContext,2);
            // moduleSelectorLayoutManager = new GridLayoutManager(mContext,1);
        }

        //    LinearLayout moduleLayout,videosLayout,frameLayout;

        moduleLayout = (LinearLayout)findViewById(R.id.moduleLayout);
        moduleLayout.setVisibility(View.GONE);
        frameLayout = (LinearLayout)findViewById(R.id.frameLayout);


        recyclerview = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView123 = (RecyclerView)findViewById(R.id.recyclerView123);
        recyclerViewModuleSelector = (RecyclerView)findViewById(R.id.recyclerViewModuleSelector);
        //recyclerViewModuleSelector.setLayoutManager(moduleSelectorLayoutManager);
        recyclerViewModuleSelector.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerView123.setLayoutManager(videoLayoutmanager);

        loadingLayout = (LinearLayout)findViewById(R.id.loadingLayout);
        videosLayout = (LinearLayout)findViewById(R.id.videosLayout);
        loadingLayout.setVisibility(View.GONE);
        videosLayout.setVisibility(View.GONE);

        mainView = (LinearLayout)findViewById(R.id.mainView);

        //getModule(CATEGORY_ID);

        isMainCategory =true;
       // showNoDataAlert(3,false);



    }

    public void changeStatusBarColor(){
        Window window = VideosLists.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(VideosLists.this,R.color.accentColor1));
    }

    public  void initActionBar(){
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.accentColor1);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent =(Toolbar) actionBarLayout.getParent();
       // parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
      //  parent.setContentInsetsAbsolute(0,0);

        actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("VIDEOS");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public boolean isTablet(){
        boolean isTablet;
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float yInches= metrics.heightPixels/metrics.ydpi;
        float xInches= metrics.widthPixels/metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches*xInches + yInches*yInches);
        if (diagonalInches>=8){
            // 6.5inch device or bigger
            isTablet=true;
        }else{
            // smaller device
            isTablet=false;
        }

        return isTablet;
    }

    @Override
    public void onResume() {
        super.onResume();
       // showLoading(false);
    }



    public void showLoading1(boolean stat){
        if(stat){
            layoutContent.setVisibility(View.GONE);
            loadingLayout.setVisibility(View.VISIBLE);
        }else {
            layoutContent.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);

            if(RESPONSE_STATUS_CATEGORY.equals("200")){
                showNoDataAlert(0,false);
                Log.d("RESP--TATUS", "1111");

            }else if(RESPONSE_STATUS_CATEGORY.equals("404")) {

                if(!isVideoExist){
                    showNoDataAlert(1,true);
                }else {
                    showNoDataAlert(1,false);
                }
                Log.d("RESP--TATUS", "2222");

//                if(RESPONSE_STATUS_VIDEO.equals("200")){
//                    showNoDataAlert(0,false);
//                    Log.d("RESP--TATUS", "22222");
//                }else if(RESPONSE_STATUS_VIDEO.equals("404")) {
//                    Log.d("RESP--TATUS", "333333");
//                    showNoDataAlert(0,true);
//                }

            }

        }

        Log.d("RESP--TATUS", "CAT: "+RESPONSE_STATUS_CATEGORY);
        Log.d("RESP--TATUS", "VID: "+RESPONSE_STATUS_VIDEO);
    }





    public void getFavouriteVideos(){
        setModuleTitle(CATEGORY_ID);
        showLoading1(true);
        videoXArrayList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        Log.d("API_RESPONSE", "current_id: "+userSession.getUSER_SESSION_ID()+"  "+ConfigServer.API_HASH);
        String URL = ConfigServer.API_FAV_VIDEO_LIST;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_VIDEOS = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.d("API_RESPONSEfdsfsd", "run: "+API_RESPONSE_GET_VIDEOS);
                        try {
                            videosLayout.setVisibility(View.VISIBLE);
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_VIDEOS);
                            RESPONSE_STATUS_VIDEO="";
                            RESPONSE_STATUS_VIDEO  = jsonObject1.getString("status");
                            Log.d("API_RESPONSE1", "V-status : "+RESPONSE_STATUS_VIDEO);
                            if(!RESPONSE_STATUS_VIDEO.equals("404")){
                                JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                                for(int i =0;i<jsonArray.length();i++){
                                    JSONObject videoObj = jsonArray.getJSONObject(i);
                                    String video_id = videoObj.getString("video_id");
                                    String video_title = videoObj.getString("video_title");
                                    String category_id = videoObj.getString("category_id");
                                    String description = videoObj.getString("description");
                                    String thumb_image = videoObj.getString("thumb_image");
                                    String video_path = videoObj.getString("video_path");
                                    String str_paid = videoObj.getString("paid");
                                    String str_unpaid = videoObj.getString("unpaid");
                                    String str_favourite = videoObj.getString("favourite");
                                    String video_status = videoObj.getString("video_status");
                                    String str_menu_order = videoObj.getString("menu_order");
                                    String created_at = videoObj.getString("created_at");
                                    String str_watch_status = videoObj.getString("watch_status");
                                    String str_time = videoObj.getString("time");
                                    String IMAGE_URL_THUMB = ConfigServer.PATH_VIDEO_THUMB+thumb_image;
                                    video_path = ConfigServer.PATH_VIDEO+video_path;
                                    Log.d("djbfjsdgjvjdsf",video_path);
                                    videoXArrayList.add(new VideoX(video_id,video_title,category_id,description,IMAGE_URL_THUMB,video_path,str_paid,str_unpaid,str_favourite,video_status,
                                            str_menu_order,created_at,str_watch_status,str_time,1));
                                }


                            }else {
                                Log.d("API_RESPONSE", "NO VIDEOS");

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("API_RESPONSE", "exception");
                        }


                        videoAdapter = new VideosListAdapter(mContext, videoXArrayList, new VideosListAdapter.EventListener() {
                            @Override
                            public void onVideoClicked(VideoX video, String pay_sts) {
                                if (video.getPaid().equalsIgnoreCase("0")){
                                    Intent in = new Intent(mContext, XPlayer.class);
                                    in.putExtra("PLAY_URL",video.getVideo_path());
                                    in.putExtra("VIDEO_ID",video.getVideo_id());
                                    in.putExtra("PARENT_ID",video.getCategory_id());
                                    in.putExtra("FAV_STS",video.getFavorite());
                                    mContext.startActivity(in);
                                }else if (video.getPaid().equalsIgnoreCase("1")&& pay_sts.equalsIgnoreCase("1")){
                                    Intent in = new Intent(mContext, XPlayer.class);
                                    in.putExtra("PLAY_URL",video.getVideo_path());
                                    in.putExtra("VIDEO_ID",video.getVideo_id());
                                    in.putExtra("PARENT_ID",video.getCategory_id());
                                    in.putExtra("FAV_STS",video.getFavorite());
                                    mContext.startActivity(in);
                                }else {
                                    Dialogue_package_list();
                                    Get_Package_List();
                                }


                            }

                        });
                        recyclerView123.setAdapter(videoAdapter);
                        mainView.setVisibility(View.VISIBLE);
                        videosLayout.setVisibility(View.VISIBLE);
                        frameLayout.setVisibility(View.GONE);

                        showLoading1(false);

                    }
                });

            }
        };

        new Thread(runnable).start();
    }


    public void getVideos(String CATEGORY_ID){
        setModuleTitle(CATEGORY_ID);
        showLoading1(true);
        videoXArrayList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("category_id",CATEGORY_ID);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        Log.d("API_RESPONSE", "current_id: "+CATEGORY_ID+"    "+userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_GET_VIDEO_BY_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_VIDEOS = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.d("API_RESPONSEfdsfsd", "run: "+API_RESPONSE_GET_VIDEOS);
                        try {
                            videosLayout.setVisibility(View.VISIBLE);
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_VIDEOS);
                            RESPONSE_STATUS_VIDEO="";
                            RESPONSE_STATUS_VIDEO  = jsonObject1.getString("status");
                            Log.d("API_RESPONSE1", "V-status : "+RESPONSE_STATUS_VIDEO);
                            if(!RESPONSE_STATUS_VIDEO.equals("404")){
                                JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                                for(int i =0;i<jsonArray.length();i++){
                                    JSONObject videoObj = jsonArray.getJSONObject(i);
                                    String video_id = videoObj.getString("video_id");
                                    String video_title = videoObj.getString("video_title");
                                    String category_id = videoObj.getString("category_id");
                                    String description = videoObj.getString("description");
                                    String thumb_image = videoObj.getString("thumb_image");
                                    String video_path = videoObj.getString("video_path");
                                    String str_paid = videoObj.getString("paid");
                                    String str_unpaid = videoObj.getString("unpaid");
                                    String str_favourite = videoObj.getString("favourite");
                                    String video_status = videoObj.getString("video_status");
                                    String str_menu_order = videoObj.getString("menu_order");
                                    String created_at = videoObj.getString("created_at");
                                    String str_watch_status = videoObj.getString("watch_status");
                                    String str_time = videoObj.getString("time");
                                    String IMAGE_URL_THUMB = ConfigServer.PATH_VIDEO_THUMB+thumb_image;
                                    video_path = ConfigServer.PATH_VIDEO+video_path;
                                    Log.d("djbfjsdgjvjdsf",video_path);
                                    videoXArrayList.add(new VideoX(video_id,video_title,category_id,description,IMAGE_URL_THUMB,video_path,str_paid,str_unpaid,str_favourite,video_status,
                                            str_menu_order,created_at,str_watch_status,str_time,1));
                                }


                            }else {
                                Log.d("API_RESPONSE", "NO VIDEOS");

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("API_RESPONSE", "exception");
                        }


                        videoAdapter = new VideosListAdapter(mContext, videoXArrayList, new VideosListAdapter.EventListener() {
                            @Override
                            public void onVideoClicked(VideoX video,String pay_sts) {

                                Log.d("fdfdfdfdfdfd",userSession.getUSER_SESSION_PAYMENT());
                                if (video.getPaid().equalsIgnoreCase("0")){
                                    Intent in = new Intent(mContext, XPlayer.class);
                                    in.putExtra("PLAY_URL",video.getVideo_path());
                                    in.putExtra("VIDEO_ID",video.getVideo_id());
                                    in.putExtra("PARENT_ID",video.getCategory_id());
                                    in.putExtra("FAV_STS",video.getFavorite());
                                    mContext.startActivity(in);
                                }else if (video.getPaid().equalsIgnoreCase("1")&& pay_sts.equalsIgnoreCase("1")){
                                    Intent in = new Intent(mContext, XPlayer.class);
                                    in.putExtra("PLAY_URL",video.getVideo_path());
                                    in.putExtra("VIDEO_ID",video.getVideo_id());
                                    in.putExtra("PARENT_ID",video.getCategory_id());
                                    in.putExtra("FAV_STS",video.getFavorite());
                                    mContext.startActivity(in);
                                }else {
                                    Dialogue_package_list();
                                    Get_Package_List();
                                }
//bsbs@nxnxn.dnxn

                            }

                        });
                        recyclerView123.setAdapter(videoAdapter);
                        mainView.setVisibility(View.VISIBLE);
                        videosLayout.setVisibility(View.VISIBLE);
                        frameLayout.setVisibility(View.GONE);

                        showLoading1(false);

                    }
                });

            }
        };

        new Thread(runnable).start();
    }

    public ArrayList<String> getPageList(){
        return pageList;
    }

    public void setPageList(String ID){
        try{
            if(!pageList.get(pageList.size()-1).equals(ID)){
                pageList.add(ID);
            }
        }catch (ArrayIndexOutOfBoundsException e){
            pageList.add(ID);
        }
    }

    public void showNoDataAlert(int type,boolean stat){
        Log.d("SDKJGSKJD", "type : "+type);
        if(stat){
            Log.d("SDKJGSKJD", "showNoDataAlert: HIDDEN");
            noVidLayoutAlert.setVisibility(View.VISIBLE);
            layoutContent.setVisibility(View.GONE);
        }else {
            Log.d("SDKJGSKJD", "showNoDataAlert: SHOWN");
            noVidLayoutAlert.setVisibility(View.GONE);
            layoutContent.setVisibility(View.VISIBLE);
        }
    }

    public void setModuleTitle(String PARENT_CATEGORY_ID){

        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("category_id",PARENT_CATEGORY_ID);
        Log.d("CATEGORY_IDhulk", "ID: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_CATEGORY_SINGLE;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("API_RESPONSE_GET1656733", ""+API_RESPONSE_GET_CAT_SINGLE);

                        try{
                            JSONObject jsonObject =  new JSONObject(API_RESPONSE_GET_CAT_SINGLE);
                            String status = jsonObject.getString("status");

                            if(status.equals("200")){
                                JSONArray jsonArray = jsonObject.getJSONArray("result");
                                JSONObject category = jsonArray.getJSONObject(0);
                                String category_name = category.getString("category_name");
                                // MODULE_TITLE = category_name;
                                actionBarTitle.setText(category_name);
                            }
                        }catch (JSONException e){

                        }
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

    public void Get_Package_List(){
        packageList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_GET_PACKAGE_LIST;
        final OnlineFunctions olf = new OnlineFunctions(this,URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CAT_SINGLE);
                            RESPONSE_STATUS_VIDEO="";
                            RESPONSE_STATUS_VIDEO  = jsonObject1.getString("status");
                            Log.d("API_RESPONSE_PACKAGE", "V-status : "+API_RESPONSE_GET_CAT_SINGLE);
                            if(RESPONSE_STATUS_VIDEO.equals("200")){
                                JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                                for(int i =0;i<jsonArray.length();i++){
                                    JSONObject packg_obj = jsonArray.getJSONObject(i);
                                    String pack_id = packg_obj.getString("pack_id");
                                    String pack_name = packg_obj.getString("name");
                                    String pack_days = packg_obj.getString("days");
                                    String pack_description = packg_obj.getString("description");
                                    String pack_amount = packg_obj.getString("amount");
                                    String pack_status = packg_obj.getString("status");
                                    packageList.add(new Package_Model(pack_id,pack_name,pack_days,pack_description,pack_amount,pack_status));
                                }


                            }else {
                                Log.d("API_RESPONSE", "NO VIDEOS");

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("API_RESPONSE", "exception");
                        }


                        package_listAdapter = new Package_ListAdapter(VideosLists.this, packageList, new Package_ListAdapter.EventListener() {
                            @Override
                            public void onPackageClicked(String pack_Id, String amout) {
                                dialog.dismiss();

                                startPayment(pack_Id,amout);

                            }
                        } );
                        recyclerpackage.setAdapter(package_listAdapter);

                        //showLoading1(false);
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

    private void Dialogue_package_list() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_package_details);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        recyclerpackage = (RecyclerView) dialog.findViewById(R.id.recyclerpackage);
        tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        LinearLayoutManager vertLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerpackage.setLayoutManager(vertLayoutManager);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void startPayment(String pack_id,String amt) {
        /*
         You need to pass current activity in order to let Razorpay create CheckoutActivity
        */
        str_pack_id = pack_id;
        Activity activity = this;
        double am = Double.parseDouble(amt)*100;
        final Checkout co = new Checkout();
        try {
            JSONObject options = new JSONObject();
            options.put("name", "package: "+str_pack_id+ Calendar.getInstance().getTime());
            options.put("description", "");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", am+"");
            JSONObject preFill = new JSONObject();
            preFill.put("email", "");
            preFill.put("contact", "");
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(this, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }
    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            send_payment_response(str_pack_id,razorpayPaymentID,"1");
            Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            Log.e("dfsdgfdsg", "Payment Successful: " + razorpayPaymentID);
        } catch (Exception e) {
            //Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }
    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(this, "Payment failed: ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            // Log.e(TAG, "Exception in onPaymentError", e);
        }
    }
    public void send_payment_response(String st_pack_id,String pay_id,String status){
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("pack_id",st_pack_id);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        post_data.put("status",status);
        post_data.put("payment_id",pay_id);
        Log.d("CATEGORY_IDhulk", "ID: "+st_pack_id);
        String URL = ConfigServer.API_REGOR_PAY_RESPONSE;
        final OnlineFunctions olf = new OnlineFunctions(this,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("APIREGORPAYRESPONSE567", ""+API_RESPONSE_GET_CAT_SINGLE);
                        Log.d("dsjvfsdufeuuew",userSession.getUSER_SESSION_ID()+"  "+
                                userSession.getUSER_USER_NAME()+" "+
                                userSession.getUSER_SESSION_TYPE()+" "+
                                userSession.getUSER_SESSION_ROLE()+" "+
                                "1 "+" 1");
                        setUserSession(userSession.getUSER_SESSION_ID(),userSession.getUSER_USER_NAME(),
                                userSession.getUSER_SESSION_TYPE(),userSession.getUSER_SESSION_ROLE(),"1","1");
                        getVideos(CATEGORY_ID);
                    }
                });
            }
        };
        new Thread(runnable).start();
    }
    public void setUserSession(String userId,String user_name,String type,String role,String logged_in,String payment_sts){
        editor.putString("USER_SESSION_ID",userId);
        editor.putString("USER_USER_NAME",user_name);
        editor.putString("USER_SESSION_TYPE",type);
        editor.putString("USER_SESSION_ROLE",role);
        editor.putBoolean("USER_SESSION_LOGGED_IN",Boolean.valueOf(logged_in));
        editor.putString("USER_PAYMENT_STS",payment_sts);
        editor.commit();
    }
}
