package com.owebso.svs.xylemlern.models

import java.io.Serializable

data class ResultModelMcq(
        var CATEGORY_ID: String,
        var TOTAL_SCORE_FROM_CURRENT_EXAM: Int,
        var TOTAL_NUM_RIGHT_ANSWERS: Int,
        var TOTAL_NUM_WRONG_ANSWERS: Int,
        var TOTAL_NUM_SKIPPED_ANSWERS: Int,
        var TOTAL_NUM_QUESTIONS: Int,
        var TOTAL_QBANK_SCORE: Int

):Serializable
