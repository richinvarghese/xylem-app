package com.owebso.svs.xylemlern.functionUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.android.material.snackbar.Snackbar;
import android.view.View;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

public class NetworkFunctions {

    private Context mContext;

    public NetworkFunctions(Context mContext) {
        this.mContext = mContext;
    }

    public boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public void showOfflineAlert(final View v){
        Snackbar.make(v, "No Internet", Snackbar.LENGTH_INDEFINITE)

                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(!haveNetworkConnection()){
                            showOfflineAlert(v);
                        }else {
//                            Intent in = new Intent(mContext, MainHomeActivity.class);
//                            in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                                mContext.startActivity(in);
//                            ((Activity)mContext).finish();
                        }
                    }
                })
                .setActionTextColor(mContext.getResources().getColor(android.R.color.holo_red_light ))
                .show();
    }

    public SSLSocketFactory getNoSSLFactory(){
        SSLContext sslcontext = null;
        SSLSocketFactory sslSocketFactory = null;
        try {
            sslcontext = SSLContext.getInstance("TLSv1.2");
            sslcontext.init(null, null, null);
            sslSocketFactory = new NoSSLv3SocketFactory(sslcontext.getSocketFactory());

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return sslSocketFactory;
    }
}
