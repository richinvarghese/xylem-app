package com.owebso.svs.xylemlern.VideoPlayer;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.downloader.Progress;
//import com.github.barteksc.pdfviewer.PDFView;
import com.github.pdfviewer.PDFView;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.owebso.svs.xylemlern.R;

import java.io.File;

public class NotesViewer extends AppCompatActivity {

    String PDF_URL = "";
    ProgressBar progressBar3;
    LinearLayout loadingLayout;
    String NOTES_FILE_NAME="NOTES_TEMP_FILE.pdf";
    TextView loadingTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_viewer);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder(); StrictMode.setVmPolicy(builder.build());
        PRDownloader.initialize(getApplicationContext());
        //PDF_URL = getIntent().getStringExtra("PDF_URL");
        PDF_URL = "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
        getSupportActionBar().hide();
        initViews();
    }
    private void initViews() {
        loadingTextView = (TextView)findViewById(R.id.loadingTextView);
        loadingTextView.setText("Loading Notes...");
        progressBar3 = (ProgressBar)findViewById(R.id.progressBar3);
        loadingLayout = (LinearLayout)findViewById(R.id.loadingLayout);
      //  pdfView = (PDFView)findViewById(R.id.pdfView);
      //  pdfView.enableSwipe(true);

        initDownloader();
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        Permissions.check(this/*context*/, permissions, null/*rationale*/, null/*options*/, new PermissionHandler() {
            @Override
            public void onGranted() {
                // do your task.
                downloadAFile();
            }
        });



    }

    public void showLoading(boolean stat){
        if(stat){
            loadingLayout.setVisibility(View.VISIBLE);
           // pdfView.setVisibility(View.GONE);
        }else {
            loadingLayout.setVisibility(View.GONE);
          //  pdfView.setVisibility(View.VISIBLE);
        }
    }

    public void initDownloader(){
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);

    }

    public void openFile(File file) {
        try {
            String fileExtension = "pdf";
            Uri path = Uri.fromFile(file);
            Intent fileIntent = new Intent(Intent.ACTION_VIEW);
            fileIntent.setDataAndType(path, "application/" + fileExtension);
            startActivity(fileIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(NotesViewer.this, "Cant Find Your File", Toast.LENGTH_LONG).show();
        }

    }

    public void downloadAFile(){
        showLoading(true);
        Log.d("pausgdw", "downloadAFile: "+Environment.getExternalStorageDirectory());
        PRDownloader.download(PDF_URL, Environment.getExternalStorageDirectory()+"/Download/", NOTES_FILE_NAME)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
                        progressBar3.setProgress(0);
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                            progressBar3.setProgress((int)progress.currentBytes);
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                      //  Toast.makeText(NotesViewer.this, "Download Complete", Toast.LENGTH_SHORT).show();
                        File file = new File(Environment.getExternalStorageDirectory()+"/Download/"+NOTES_FILE_NAME);
                        Log.d("pausgdw", "path: "+file.getAbsolutePath());

                        if(file.exists()){
                            showLoading(false);

                            PDFView.with(NotesViewer.this)
                                    .setfilepath(file.getAbsolutePath()).setSwipeOrientation(0)
                                    .start();



                        //    pdfView.enableSwipe(true);
                        //    pdfView.enableDoubletap(true);
                            //pdfView.loadPages();
                         //   pdfView.fromFile(file).load();
                            progressBar3.setVisibility(View.GONE);
                        }else {
                            Toast.makeText(NotesViewer.this, "Something went wrong.", Toast.LENGTH_SHORT).show();
                            showLoading(false);
                            finish();
                        }


                    }

                    @Override
                    public void onError(Error error) {
                        Toast.makeText(NotesViewer.this, "Download Error"+error.getServerErrorMessage(), Toast.LENGTH_SHORT).show();

                    }

                });
    }
}
