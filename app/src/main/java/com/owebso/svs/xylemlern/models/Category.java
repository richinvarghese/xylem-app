package com.owebso.svs.xylemlern.models;

public class Category {
    private int index;
    private String category_id;
    private String category_name;
    private String description;
    private String parent_id;
    private String category_section;
    private String category_status;
    private String category_image;
    private String created_at;
    private String year;
    private int CAT_TYPE = 0;

    public Category(int index, String category_id, String category_name, String description, String parent_id, String category_section, String category_status, String category_image, String created_at, int CAT_TYPE) {
        this.index = index;
        this.category_id = category_id;
        this.category_name = category_name;
        this.description = description;
        this.parent_id = parent_id;
        this.category_section = category_section;
        this.category_status = category_status;
        this.category_image = category_image;
        this.created_at = created_at;
        this.CAT_TYPE = CAT_TYPE;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getCAT_TYPE() {
        return CAT_TYPE;
    }
    public void setCAT_TYPE(int CAT_TYPE) {
        this.CAT_TYPE = CAT_TYPE;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Category() {

    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getCategory_section() {
        return category_section;
    }

    public void setCategory_section(String category_section) {
        this.category_section = category_section;
    }

    public String getCategory_status() {
        return category_status;
    }

    public void setCategory_status(String category_status) {
        this.category_status = category_status;
    }

    public String getCategory_image() {
        return category_image;
    }

    public void setCategory_image(String category_image) {
        this.category_image = category_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
