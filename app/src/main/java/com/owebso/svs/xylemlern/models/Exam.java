package com.owebso.svs.xylemlern.models;

public class Exam {
    private int index;
    private String exam_id;
    private String exam_title;
    private String description;
    private String category_id;
    private String university_id;
    private String coursetype_id;
    private String course_id;
    private String subject_id;
    private String instruction_id;
    private String examtype;
    private String exam_type_cat;
    private String start_date;
    private String end_date;
    private String duration;
    private String total_mark;
    private String pass_mark;
    private String negative_mark;
    private String published;
    private String exam_status;
    private String questions;
    private String total_qst;
    private String publish_date;
    private String created_at;
    private String updated_at;
    private String paid_type;
    private String correctmark;
    private int TYPE;

    public Exam(int index, String exam_id, String exam_title, String description, String category_id, String university_id, String coursetype_id, String course_id, String subject_id, String instruction_id, String examtype, String exam_type_cat, String start_date, String end_date, String duration, String total_mark, String pass_mark, String negative_mark, String published, String exam_status, String questions, String total_qst,String publish_date,String created_at, String updated_at,String paid_type,String correctmark,int TYPE) {
        this.index = index;
        this.exam_id = exam_id;
        this.exam_title = exam_title;
        this.description = description;
        this.category_id = category_id;
        this.university_id = university_id;
        this.coursetype_id = coursetype_id;
        this.course_id = course_id;
        this.subject_id = subject_id;
        this.instruction_id = instruction_id;
        this.examtype = examtype;
        this.exam_type_cat = exam_type_cat;
        this.start_date = start_date;
        this.end_date = end_date;
        this.duration = duration;
        this.total_mark = total_mark;
        this.pass_mark = pass_mark;
        this.negative_mark = negative_mark;
        this.published = published;
        this.exam_status = exam_status;
        this.questions = questions;
        this.total_qst = total_qst;
        this.publish_date = publish_date;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.paid_type = paid_type;
        this.correctmark = correctmark;
        this.TYPE = TYPE;
    }

    public Exam() {

    }

    public int getTYPE() {
        return TYPE;
    }

    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getExam_type_cat() {
        return exam_type_cat;
    }

    public void setExam_type_cat(String exam_type_cat) {
        this.exam_type_cat = exam_type_cat;
    }

    public String getExam_id() {
        return exam_id;
    }

    public void setExam_id(String exam_id) {
        this.exam_id = exam_id;
    }

    public String getExam_title() {
        return exam_title;
    }

    public void setExam_title(String exam_title) {
        this.exam_title = exam_title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getUniversity_id() {
        return university_id;
    }

    public void setUniversity_id(String university_id) {
        this.university_id = university_id;
    }

    public String getCoursetype_id() {
        return coursetype_id;
    }

    public void setCoursetype_id(String coursetype_id) {
        this.coursetype_id = coursetype_id;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getInstruction_id() {
        return instruction_id;
    }

    public void setInstruction_id(String instruction_id) {
        this.instruction_id = instruction_id;
    }

    public String getExamtype() {
        return examtype;
    }

    public void setExamtype(String examtype) {
        this.examtype = examtype;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getTotal_mark() {
        return total_mark;
    }

    public void setTotal_mark(String total_mark) {
        this.total_mark = total_mark;
    }

    public String getPass_mark() {
        return pass_mark;
    }

    public void setPass_mark(String pass_mark) {
        this.pass_mark = pass_mark;
    }

    public String getNegative_mark() {
        return negative_mark;
    }

    public void setNegative_mark(String negative_mark) {
        this.negative_mark = negative_mark;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getExam_status() {
        return exam_status;
    }

    public void setExam_status(String exam_status) {
        this.exam_status = exam_status;
    }

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions;
    }

    public String getTotal_qst() {
        return total_qst;
    }

    public void setTotal_qst(String total_qst) {
        this.total_qst = total_qst;
    }

    public String getPublish_date() {
        return publish_date;
    }

    public void setPublish_date(String publish_date) {
        this.publish_date = publish_date;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getPaid_type() {
        return paid_type;
    }

    public void setPaid_type(String paid_type) {
        this.paid_type = paid_type;
    }

    public String getCorrectmark() {
        return correctmark;
    }

    public void setCorrectmark(String correctmark) {
        this.correctmark = correctmark;
    }
}
