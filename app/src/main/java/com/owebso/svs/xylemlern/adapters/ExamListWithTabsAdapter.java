package com.owebso.svs.xylemlern.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.DataConstants;
import com.owebso.svs.xylemlern.models.Exam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Sarath on 07/06/19.
 */

public class ExamListWithTabsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<Exam> examsList;
    EventListener listener;
    String API_RESPONSE_GET_CAT_SINGLE="";
    String MODULE_TITLE="";
    int SORT_TYPE=0;

    public ExamListWithTabsAdapter(Context mContext, ArrayList<Exam> examsList, EventListener listener, int SORT_TYPE) {
        this.mContext = mContext;
        this.examsList = examsList;
        this.listener =listener;
        this.SORT_TYPE =SORT_TYPE;
    }

    public interface EventListener {
        void onCategoryClicked(String examId, Exam exam);
    }
    public ExamListWithTabsAdapter(EventListener listener){
        this.listener =listener;
    }

    public  class MainView extends RecyclerView.ViewHolder {
        TextView textViewTitle,cat_id,textViewDescription,moduleIndex,textViewDatePublish;
        ImageView thumbnailCat;
        CardView cardClickView;
        RelativeLayout fullItemView;
        TextView textViewExamType;
        public MainView(View v) {
            super(v);
            this.cat_id = (TextView) v.findViewById(R.id.id);
            this.textViewExamType = (TextView) v.findViewById(R.id.textViewExamType);
            this.textViewTitle = (TextView) v.findViewById(R.id.textViewTitle);
            this.textViewDescription = (TextView) v.findViewById(R.id.textViewDescription);
            this.moduleIndex = (TextView) v.findViewById(R.id.moduleIndex);
            this.textViewDatePublish = (TextView) v.findViewById(R.id.textViewDatePublish);
            this.thumbnailCat = (ImageView) v.findViewById(R.id.thumbnailCat);
            this.cardClickView = (CardView) v.findViewById(R.id.cardClickView);
            this.fullItemView = (RelativeLayout) v.findViewById(R.id.fullItemView);
            this.textViewDescription.setVisibility(View.VISIBLE);

        }
    }

    public class TitleView extends  RecyclerView.ViewHolder {
        TextView tvModuleTitle;
        public TitleView(View v) {
            super(v);
            tvModuleTitle = (TextView)v.findViewById(R.id.tvModuleTitle);
        }
    }

    public  class AdsView extends  RecyclerView.ViewHolder {
        public AdsView(View v) {
            super(v);
        }
    }

    public void showImage(String IMAGE_URL, ImageView imageView){

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_cash);
        requestOptions.error(R.drawable.ic_cash);

        Glide.with(mContext)
                .applyDefaultRequestOptions(requestOptions)
                .load(IMAGE_URL)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        int i=0;
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        return false;
                    }
                }).into(imageView)
        ;

    }

    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
        if(viewType==0)
        {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_module_title, parent, false);
            return new TitleView(itemView);
        }
        else if(viewType==1)
        {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_view_without_numbering, parent, false);
            return new MainView(itemView);
        }
        else
        {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_layout_video_category, parent, false);
            return new AdsView(itemView);
        }



    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Exam examObj = examsList.get(position);
        if(holder.getItemViewType()==1)
        {
            ((MainView)holder).fullItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCategoryClicked(examObj.getExam_id(),examObj);
                }
            });
            ((MainView)holder).cat_id.setText(examObj.getCategory_id());
            ((MainView)holder).textViewTitle.setText(examObj.getExam_title());
            ((MainView)holder).textViewDescription.setText(examObj.getDescription());
            ((MainView)holder).moduleIndex.setText(""+(examObj.getIndex()+1));
            ((MainView)holder).textViewExamType.setText(getExamType(examObj.getExam_type_cat()));
            ((MainView)holder).textViewDatePublish.setText(examObj.getDuration() +" Mins");
        }
        else if(holder.getItemViewType()==0)
        {
            ((TitleView)holder).tvModuleTitle.setText(getMonthFromDate(examsList.get(position).getStart_date()));
           // setModuleTitle(examObj.getParent_id(),((TitleView)holder).tvModuleTitle);
        }
        else {
            final AdsView third_holder = (AdsView)holder;
            //third_holder.third_examsList_tv.setText(examObj.getName());
        }
    }


    public String getMonthFromDate(String dateString){
        String dtStart = dateString;
        String monthString="";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(dtStart);
            monthString  = (String) DateFormat.format("MMM yyyy",  date); // Jun

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return monthString;
    }

    public void removeAnItem(int item){
        examsList.remove(item);
        notifyItemRemoved(item);
    }

    @Override
    public int getItemViewType(int position) {
       return examsList.get(position).getTYPE();
    }

    @Override
    public int getItemCount() {
        return examsList.size();
    }

    public String getExamType(String type){
        String op="";
        switch (type){
            case DataConstants.EXAM_TYPE_ALL:
                op="Normal Test";
                break;
            case DataConstants.EXAM_TYPE_WEEKLY:
                op = "Weekly Test";
                break;
            case DataConstants.EXAM_TYPE_MONTHLY:
                op="Monthly Test";
                break;
            case DataConstants.EXAM_TYPE_GRAND_TEST:
                op="Grand Test";
                break;
            case DataConstants.EXAM_TYPE_SUBJECTWISE:
                op="Subject Wise Test";
                break;
        }
        return op;
    }

}
