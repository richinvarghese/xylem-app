package com.owebso.svs.xylemlern.profile;

public class UserProfile {
    private String registration_id;
    private String login_id;
    private String name;
    private String year;
    private String category;
    private String school_name;
    private String address;
    private String registration_status;
    private String paid;
    private String expiry_date;
    private String user_name;
    private String password;
    private String email;
    private String phone;
    private String role;
    private String status;
    private String permissions;
    private String tocken;

    public UserProfile() {

    }

    public UserProfile(String registration_id, String login_id, String name, String year, String category, String school_name, String address, String registration_status, String paid, String expiry_date, String user_name, String password, String email, String phone, String role, String status, String permissions, String tocken) {
        this.registration_id = registration_id;
        this.login_id = login_id;
        this.name = name;
        this.year = year;
        this.category = category;
        this.school_name = school_name;
        this.address = address;
        this.registration_status = registration_status;
        this.paid = paid;
        this.expiry_date = expiry_date;
        this.user_name = user_name;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.role = role;
        this.status = status;
        this.permissions = permissions;
        this.tocken = tocken;
    }

    public String getRegistration_id() {
        return registration_id;
    }

    public void setRegistration_id(String registration_id) {
        this.registration_id = registration_id;
    }

    public String getLogin_id() {
        return login_id;
    }

    public void setLogin_id(String login_id) {
        this.login_id = login_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegistration_status() {
        return registration_status;
    }

    public void setRegistration_status(String registration_status) {
        this.registration_status = registration_status;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public String getTocken() {
        return tocken;
    }

    public void setTocken(String tocken) {
        this.tocken = tocken;
    }
}
