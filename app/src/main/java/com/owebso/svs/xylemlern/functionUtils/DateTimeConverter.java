package com.owebso.svs.xylemlern.functionUtils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateTimeConverter {

    public DateTimeConverter() {

    }

    public String convertTo12h(String time){
        String newTime="0";
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(time);
            System.out.println(dateObj);

            newTime=new SimpleDateFormat("K:mm a").format(dateObj);

        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return newTime;
    }

    public String changeDateFormate(String date, String pattern1, String pattern2){

        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern1);
        //SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
//You gotta parse it to a Date before correcting it
        Date parsedDate = null;
        try {
            parsedDate = simpleDateFormat2.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //simpleDateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
        simpleDateFormat2 = new SimpleDateFormat(pattern2);
        String newFormatttedDate = simpleDateFormat2.format(parsedDate);
        return newFormatttedDate;

    }

    public String changeDateFormateWord(String date){

        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss");
       // Log.d("DATECHECK", "changeDateFormateWord: "+date);
        //SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
//You gotta parse it to a Date before correcting it
        Date parsedDate = null;
        try {
            parsedDate = simpleDateFormat2.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //simpleDateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
        simpleDateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
        String newFormatttedDate = simpleDateFormat2.format(parsedDate);



        return newFormatttedDate;

    }

    public String formatChange(String date1){
        Log.d("DATECHECK", "changeDateFormateWord: "+date1);

        String date=date1;
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);

        Date newDate= null;
        try {
            newDate = spf.parse(date1);
            Log.d("DATECHECK", "NEW DATE: "+newDate.toString());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("dd MMM");
        date = spf.format(newDate);
        System.out.println(date);

        Log.d("DATECHECK", "changeDateFormateWord 2: "+date);

        return date;
    }

    public String[] formatChangeToArray(String date1){
        Log.d("DATECHECK", "changeDateFormateWord: "+date1);
        String[] dateDate = new String[2];
        String date=date1;
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);

        Date newDate= null;
        try {
            newDate = spf.parse(date1);
            Log.d("DATECHECK", "NEW DATE: "+newDate.toString());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("dd MMM");
        date = spf.format(newDate);
        System.out.println(date);

        Log.d("DATECHECK", "changeDateFormateWord 2: "+date);

        return dateDate;
    }

    public String getCurrentTime(){
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        String time = df.format(Calendar.getInstance().getTime());
        time = convertTo12h(time);
        return time;
    }

    public String getDate() {
        String result = "";
        Date date = new Date();
        SimpleDateFormat sdf0 = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        result = sdf0.format(date);
        return result;
    }

    public String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
         //   LOGE(TAG, "ParseException - dateFormat");
        }

        return outputDate;

    }
}
