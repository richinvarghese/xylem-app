package com.owebso.svs.xylemlern.XyFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.ActivitExam.ExamActivity;
import com.owebso.svs.xylemlern.adapters.ExamListAdapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Exam;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ExamFragment extends Fragment {
    private Context mContext = getContext();
    String API_RESPONSE_GET_CATEGORY="";
    ArrayList<Exam> examList;
    ExamListAdapter adapter;
    RecyclerView recyclerview;
    String CATEGORY_ID="45";
    LinearLayout loadingLayout,mainView;
    //ArrayList<String> catParent;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager videoLayoutmanager;
    LinearLayout noMcqLayout;
    boolean isVideoExist=false;
    boolean isMainCategory=false;
    LinearLayout moduleLayout,frameLayout;
    UserSession userSession;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_exams_01_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void initViews(){

        ProfileUtils profileUtils  = new ProfileUtils(getActivity());
        userSession = profileUtils.getUserSession();

        noMcqLayout = (LinearLayout)getView().findViewById(R.id.noMcqLayout);
        noMcqLayout.setVisibility(View.GONE);

        if(!isTablet()){
            gridLayoutManager = new GridLayoutManager(getContext(),1);
            videoLayoutmanager = new GridLayoutManager(getContext(),1);
            //  moduleSelectorLayoutManager = new GridLayoutManager(getContext(),1);
        }else{
            gridLayoutManager = new GridLayoutManager(getContext(),2);
            videoLayoutmanager = new GridLayoutManager(getContext(),2);
            // moduleSelectorLayoutManager = new GridLayoutManager(getContext(),1);
        }

        //    LinearLayout moduleLayout,videosLayout,frameLayout;

//        moduleLayout = (LinearLayout)getView().findViewById(R.id.moduleLayout);
//        moduleLayout.setVisibility(View.GONE);
        frameLayout = (LinearLayout)getView().findViewById(R.id.frameLayout);


        recyclerview = (RecyclerView)getView().findViewById(R.id.recyclerview);
        //recyclerViewModuleSelector.setLayoutManager(moduleSelectorLayoutManager);

        recyclerview.setLayoutManager(gridLayoutManager);

        loadingLayout = (LinearLayout)getView().findViewById(R.id.loadingLayout);
        loadingLayout.setVisibility(View.GONE);

        mainView = (LinearLayout)getView().findViewById(R.id.mainView);

        //getModule(CATEGORY_ID);

        isMainCategory =true;
        getExams();

    }

    public boolean isTablet(){
        boolean isTablet;
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float yInches= metrics.heightPixels/metrics.ydpi;
        float xInches= metrics.widthPixels/metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches*xInches + yInches*yInches);
        if (diagonalInches>=8){
            // 6.5inch device or bigger
            isTablet=true;
        }else{
            // smaller device
            isTablet=false;
        }

        return isTablet;
    }

    public void getExams(){
        showLoading(true);
        examList = new ArrayList<>();
        examList.clear();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("category_id",ConfigServer.CAT_EXAM_NEET);
        String URL = ConfigServer.API_GET_EXAMS_LIST;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CATEGORY = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        noMcqLayout.setVisibility(View.GONE);
                        Log.d("API_RESPONSEd3", ""+API_RESPONSE_GET_CATEGORY);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
                            String status = jsonObject1.getString("status");

                            if(status.equals("404")){
                                noDataAlert(true);
                            }else {
                                noDataAlert(false);
                            }

                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("response"));

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                String exam_id = jsonObject.getString("exam_id");
                                String exam_title = jsonObject.getString("exam_title");
                                String description = jsonObject.getString("description");
                                String category_id = jsonObject.getString("category_id");
                                String university_id = jsonObject.getString("university_id");
                                String coursetype_id = jsonObject.getString("coursetype_id");
                                String course_id = jsonObject.getString("course_id");
                                String subject_id = jsonObject.getString("subject_id");
                                String instruction_id = jsonObject.getString("instruction_id");
                                String examtype = jsonObject.getString("examtype");
                                String exam_type_cat = jsonObject.getString("exam_type_cat");
                                String start_date = jsonObject.getString("start_date");
                                String end_date = jsonObject.getString("end_date");
                                String duration = jsonObject.getString("duration");
                                String total_mark = jsonObject.getString("total_mark");
                                String pass_mark = jsonObject.getString("pass_mark");
                                String negative_mark = jsonObject.getString("negative_mark");
                                String published = jsonObject.getString("published");
                                String exam_status = jsonObject.getString("exam_status");
                                String questions = jsonObject.getString("questions");
                                String created_at = jsonObject.getString("created_at");
                                String updated_at = jsonObject.getString("updated_at");

                                examList.add(new Exam(i,exam_id,exam_title,description,category_id,university_id,coursetype_id,
                                        course_id,subject_id,instruction_id,examtype,exam_type_cat,start_date,end_date,duration,
                                        total_mark,pass_mark,negative_mark,published,exam_status,questions,"","",created_at,updated_at
                                        ,"","",1));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        adapter = new ExamListAdapter(mContext, examList, new ExamListAdapter.EventListener() {
                            @Override
                            public void onExamClicked(Exam exam) {
                                gotoExamView(exam);
                            }

                        });

                        recyclerview.setAdapter(adapter);
                        showLoading(false);
                        // showParentAtTop(CATEGORY_ID);
                    }
                });
            }
        };

        new Thread(runnable).start();
    }


    public void gotoExamView(Exam exam){
        Intent in = new Intent(getContext(), ExamActivity.class);
        in.putExtra("EXAM_ID",exam.getExam_id());
        in.putExtra("EXAM_NEG_MARK",exam.getNegative_mark());
        in.putExtra("EXAM_TOTAL_MARK",exam.getTotal_mark());
        in.putExtra("EXAM_PASS_MARK",exam.getPass_mark());
        startActivity(in);
    }

    public void showLoading(boolean stat){
        if(stat){
            mainView.setVisibility(View.GONE);
            loadingLayout.setVisibility(View.VISIBLE);
        }else {
            mainView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        }
    }

    public void noDataAlert(boolean stat){
        if(stat){
            noMcqLayout.setVisibility(View.VISIBLE);
        }else {
            noMcqLayout.setVisibility(View.GONE);
        }
    }







}
