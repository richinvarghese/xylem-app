package com.owebso.svs.xylemlern.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.models.VideoX;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import java.util.ArrayList;

/**
 * Created by Sarath on 07/06/19.
 */

public class RelatedVideosListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<VideoX> videoXList;
    boolean PAYMENT_CLICKABLE=true;
    EventListener listener;
    UserSession userSession;

    public RelatedVideosListAdapter(Context mContext, ArrayList<VideoX> videoXList,EventListener listener) {
        this.mContext = mContext;
        this.videoXList = videoXList;
        this.listener=listener;
    }

    public interface EventListener {
        void onVideoClicked(String url,String videoId,String parentId,String str_paid,String str_fav,String pay_sts);
    }

    public  class MainView extends RecyclerView.ViewHolder {
        TextView videoId,video_url,textViewTitle;
        ImageView imgVideoThumb,playButtonCentre;
        TextView textViewDescription;
        CardView fullView;
        public MainView(View v) {
            super(v);

            ProfileUtils profileUtils = new ProfileUtils(mContext);
            userSession = profileUtils.getUserSession();
            this.videoId = (TextView) v.findViewById(R.id.id);
            this.video_url = (TextView) v.findViewById(R.id.video_url);
            this.textViewTitle = (TextView) v.findViewById(R.id.textViewTitle);
            this.imgVideoThumb = (ImageView) v.findViewById(R.id.imgVideoThumb);
            this.playButtonCentre = (ImageView) v.findViewById(R.id.playButtonCentre);
            this.textViewDescription = (TextView) v.findViewById(R.id.textViewDescription);
            this.fullView = (CardView) v.findViewById(R.id.fullView);


        }
    }

    public class SportAudView extends  RecyclerView.ViewHolder {
        public SportAudView(View v) {
            super(v);
        }
    }

    public  class AdsView extends  RecyclerView.ViewHolder {
        public AdsView(View v) {
            super(v);
        }
    }

    public void showImage(String IMAGE_URL,ImageView imageView){
        Log.d("image_url", "URL : "+ IMAGE_URL);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.placeholder_01);
        requestOptions.error(R.drawable.placeholder_01);

        Glide.with(mContext)
                .applyDefaultRequestOptions(requestOptions)
                .load(IMAGE_URL)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        int i=0;
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        return false;
                    }
                }).into(imageView)
        ;

    }

    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
        if(viewType==1)
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.test_layout_card_02, parent, false);
            return new MainView(itemView
            );
        }
        else if(viewType==2)
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_layout_video_view, parent, false);
            return new SportAudView(itemView);
        }
        else
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_layout_video_view, parent, false);
            return new AdsView(itemView);
        }



    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final VideoX catObj = videoXList.get(position);
        if(holder.getItemViewType()==1)
        {
            ((MainView)holder).videoId.setText(catObj.getVideo_id());
            ((MainView)holder).video_url.setText(catObj.getVideo_path());
            ((MainView)holder).textViewDescription.setText(catObj.getDescription());
            ((MainView)holder).textViewTitle.setText(catObj.getVideo_title().toUpperCase());

            showImage(catObj.getThumb_image(),((MainView)holder).imgVideoThumb);

            ((MainView)holder).fullView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onVideoClicked(catObj.getVideo_path(),catObj.getVideo_id(),catObj.getCategory_id(),catObj.getPaid(),catObj.getFavorite(), userSession.getUSER_SESSION_PAYMENT());

//                    Intent in = new Intent(mContext, XPlayer.class);
//                    in.putExtra("PLAY_URL",catObj.getVideo_path());
//                    in.putExtra("VIDEO_ID",catObj.getVideo_id());
//                    in.putExtra("PARENT_ID",catObj.getCategory_id());
//                    mContext.startActivity(in);
                }
            });
            if(catObj.getPaid().equalsIgnoreCase("0")){
                ((MainView)holder).playButtonCentre.setImageResource(R.drawable.play_button_02);
            }else  if(catObj.getPaid().equalsIgnoreCase("1")&& userSession.getUSER_SESSION_PAYMENT().equalsIgnoreCase("1")){
                ((MainView)holder).playButtonCentre.setImageResource(R.drawable.play_button_02);
            }else {
                ((MainView)holder).playButtonCentre.setImageResource(R.drawable.play_button_02_locked);
            }
                /*if(catObj.getUnpaid().equalsIgnoreCase("1")){
                    ((MainView)holder).playButtonCentre.setImageResource(R.drawable.play_button_02);
                }else {
                    ((MainView)holder).playButtonCentre.setImageResource(R.drawable.play_button_02_locked);
                }*/
           // Log.d("VIDEO_URL", "URL : "+ catObj.getVideo_path());

        }
        else if(holder.getItemViewType()==2)
        {

        }
        else {

            final AdsView third_holder = (AdsView)holder;
            //third_holder.third_videoXList_tv.setText(catObj.getName());
        }

    }




    @Override
    public int getItemViewType(int position) {

        return videoXList.get(position).getTYPE();
    }

    @Override
    public int getItemCount() {
        return videoXList.size();
    }
}
