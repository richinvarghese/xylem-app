package com.owebso.svs.xylemlern.models

import java.io.Serializable

data class HashTag(
    var tag_id:String,
    var tag_name:String,
    var tag_status:String,
    var created_at:String,
    var updated_at:String
):Serializable