package com.owebso.svs.xylemlern.ActivitExam

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.owebso.svs.xylemlern.ActivitExam.ReviewExamActivity
import com.owebso.svs.xylemlern.R
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer
import com.owebso.svs.xylemlern.ServerConfig.DataConstants
import com.owebso.svs.xylemlern.adapters.GridViewQuestionsAdapterExam2Review
import com.owebso.svs.xylemlern.adapters.IndexNumberAdapterExam2Review
import com.owebso.svs.xylemlern.functionUtils.ImageUtils
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions
import com.owebso.svs.xylemlern.functionUtils.WebServiceApi
import com.owebso.svs.xylemlern.functionUtils.ZonStringFunctions
import com.owebso.svs.xylemlern.models.ChoiceOption
import com.owebso.svs.xylemlern.models.ExamQuestion
import com.owebso.svs.xylemlern.profile.ProfileUtils
import com.owebso.svs.xylemlern.profile.UserSession
import kotlinx.android.synthetic.main.custom_layout_mcq_single_question.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.jsoup.Jsoup
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*

class ReviewExamActivity : AppCompatActivity() {
    var questionArrayList: ArrayList<ExamQuestion>? = null
    var mContext: Context = this@ReviewExamActivity
    var API_RESPONSE = ""
    var API_RESPONSE_SINGLE_QUESTION = ""
    var loadingLayout: LinearLayout? = null
    var fullLayout: LinearLayout? = null
    var CURRENT_QUESTION: ExamQuestion? = null
    var layoutRadioGroup: LinearLayout? = null
    var layoutHintButton: LinearLayout? = null
    var tvQuestionIndex: TextView? = null
    var tvQuestion: TextView? = null
    var tvAnswer: TextView? = null
    val rb = arrayOfNulls<RadioButton>(4)
    var layoutHint: LinearLayout? = null
    var layoutAnswer: LinearLayout? = null
    var tvHint: TextView? = null
    var tvAnswerExplanation: TextView? = null
    var imageViewQuestion: ImageView? = null
    var imageViewQuestion2: ImageView? = null
    var imageUtils: ImageUtils? = null
    var CURRENT_INDEX = 0
    var buttonNext: Button? = null
    var toggleButton: ImageView? = null
    var answer1: CardView? = null
    var answer2: CardView? = null
    var answer3: CardView? = null
    var answer4: CardView? = null
    var textViewA: TextView? = null
    var textViewB: TextView? = null
    var textViewC: TextView? = null
    var textViewD: TextView? = null
    var rcViewIndex: RecyclerView? = null
    var EXAM_ID = ""
    var TOTAL_SCORE_FROM_CURRENT_EXAM = 0
    lateinit var scoreBoardList: Array<String?>
    var EXAM_NEG_MARK = ""
    var EXAM_TOTAL_MARK = ""
    var TOTAL_NUM_QUESTIONS = 0
    var TOTAL_NUM_RIGHT_ANSWERS = 0
    var TOTAL_NUM_WRONG_ANSWERS = 0
    var TOTAL_NUM_SKIPPED_ANSWERS = 0
    var selectionsArrayList: ArrayList<Int>? = null
    var EXAM_PASS_MARK = 0
    var API_RESPONSE_POST_EXAM_RESULT = ""
    var TOTAL_NEGATIVE_POINTS = 0
    var TOTAL_POSITIVE_POINTS = 0
    lateinit var ATTENDED_QUESTION_DATA: Array<String?>
    var ATTENDED_DATA_STRING = ""
    var ATTENDED_SCOREBOARD_STRING = ""
    var API_RESPONSE_CHECK_EXAM_ATT = ""
    var onScrollListener: RecyclerView.OnScrollListener? = null
    var userSession: UserSession? = null
    var textViewCurrentCount: TextView? = null
    var tvQAnswerStatus: TextView? = null
    var indexLayoutManager: LinearLayoutManager? = null
    //timer
    var diff: Long = 0
    var oldLong: Long = 0
    var NewLong: Long = 0
    var webServiceApi: WebServiceApi? = null
    var adapterExam: IndexNumberAdapterExam2Review? = null
    var EXAM_DURATION = 0
    var LAST_VIEWED_QUESTION_INDEX = "0"
    var isTimesUp = false
    var EXAM_END_DATE_TIME = ""
    lateinit var MARKED_QUESTIONS_ARRAY: IntArray
    var isPanelShown = false
    var hiddenPanel: ViewGroup? = null
    var buttonPrev: Button? = null

    var question_img=""
    var option_img1=""
    var option_img2=""
    var option_img3=""
    var option_img4=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam_review)
        //  CATEGORY_ID = getIntent().getStringExtra("CATEGORY_ID");
        EXAM_ID = intent.getStringExtra("EXAM_ID")
        EXAM_NEG_MARK = intent.getStringExtra("EXAM_NEG_MARK")
        EXAM_TOTAL_MARK = intent.getStringExtra("EXAM_TOTAL_MARK")
        EXAM_DURATION = intent.getStringExtra("EXAM_DURATION").toInt()
        EXAM_PASS_MARK = intent.getStringExtra("EXAM_PASS_MARK").toInt()
        //  initActionBar();
//  changeStatusBarColor();
        initViews()
        questionList
        //  timer();
    }

    fun initViews() {
        hiddenPanel = findViewById<View>(R.id.layoutFullView) as ViewGroup
        hiddenPanel!!.visibility = View.VISIBLE
        isPanelShown = false
        imageUtils = ImageUtils(mContext)
        selectionsArrayList = ArrayList()
        val profileUtils = ProfileUtils(mContext)
        userSession = profileUtils.userSession
        //layouts
        loadingLayout = findViewById<View>(R.id.loadingLayout) as LinearLayout
        fullLayout = findViewById<View>(R.id.fullLayout) as LinearLayout
        layoutHint = findViewById<View>(R.id.layoutHint) as LinearLayout
        layoutAnswer = findViewById<View>(R.id.layoutAnswer) as LinearLayout
        layoutRadioGroup = findViewById<View>(R.id.layoutRadioGroup) as LinearLayout
        layoutHintButton = findViewById<View>(R.id.layoutHintButton) as LinearLayout
        layoutHintButton!!.visibility = View.GONE
        //RecyclerView
        indexLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        rcViewIndex = findViewById<View>(R.id.rcViewIndex) as RecyclerView
        rcViewIndex!!.layoutManager = indexLayoutManager
        //CardView
        answer1 = findViewById<View>(R.id.answer1) as CardView
        answer2 = findViewById<View>(R.id.answer2) as CardView
        answer3 = findViewById<View>(R.id.answer3) as CardView
        answer4 = findViewById<View>(R.id.answer4) as CardView
        textViewA = findViewById<View>(R.id.textViewA) as TextView
        textViewB = findViewById<View>(R.id.textViewB) as TextView
        textViewC = findViewById<View>(R.id.textViewC) as TextView
        textViewD = findViewById<View>(R.id.textViewD) as TextView
        //Button
        buttonNext = findViewById<View>(R.id.buttonNext) as Button
        buttonPrev = findViewById<View>(R.id.buttonPrev) as Button
        //TextView
        tvQuestionIndex = findViewById<View>(R.id.tvQuestionIndex) as TextView
        tvAnswer = findViewById<View>(R.id.tvAnswer) as TextView
        tvHint = findViewById<View>(R.id.tvHint) as TextView
        tvQuestion = findViewById<View>(R.id.tvQuestion) as TextView
        tvAnswerExplanation = findViewById<View>(R.id.tvAnswerExplanation) as TextView
        textViewCurrentCount = findViewById<View>(R.id.textViewCurrentCount) as TextView
        tvQAnswerStatus = findViewById<View>(R.id.tvQAnswerStatus) as TextView
        //ImageView
       // imageViewQuestion = findViewById<View>(R.id.imageView4) as ImageView
        imageViewQuestion2 = findViewById<View>(R.id.imageViewQuestion2) as ImageView
        //CHANGING VISIBILITY
       // imageViewQuestion!!.visibility = View.GONE
        layoutAnswer!!.visibility = View.GONE
        layoutHint!!.visibility = View.GONE
        //  getQuestionList();
//  checkExamStatus(); //CHECK ATTENDACE AND GOTO EXAM VIEW (IF THE USER NOT ALREADY ATTENDED)
        buttonNext!!.setOnClickListener { v ->
            //  saveExamDataOnClick();
            gotoQuestion(1, v)
        }
        buttonPrev!!.setOnClickListener { v ->
            imageViewQuestion2!!.visibility = View.VISIBLE
            gotoQuestion(0, v) }
        onScrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                    }
                }
            }
        }
    }

    fun gotoQuestion(direction: Int, view: View?) {
        if (direction == 0) { //prev question
            if (CURRENT_INDEX != 0) {
                CURRENT_INDEX--
                CURRENT_QUESTION = questionArrayList!![CURRENT_INDEX]
                showQuestionView(questionArrayList!![CURRENT_INDEX])
            } else { //  Toast.makeText(mContext, "First Question", Toast.LENGTH_SHORT).show();
            }
        } else { //next question
            if (CURRENT_INDEX < questionArrayList!!.size - 1) {
                CURRENT_INDEX++
                CURRENT_QUESTION = questionArrayList!![CURRENT_INDEX]
                showQuestionView(questionArrayList!![CURRENT_INDEX])
                //  buttonNext.setVisibility(View.VISIBLE);
            } else { //  showExamfinishDialogue(view);
// buttonNext.setVisibility(View.GONE);
            }
        }
    }

    fun showExamfinishDialogue(view: View?) {
        TOTAL_SCORE_FROM_CURRENT_EXAM = 0
        calculateScore()
        // inflate the layout of the popup window
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView = inflater.inflate(R.layout.popup_window_show_exam_finish, null)
        // create the popup window
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT
        val focusable = true // lets taps outside the popup also dismiss it
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        //  popupWindow.setAnimationStyle(R.style.popup_window_animation);
// show the popup window
// which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        val buttonSubmitExam = popupView.findViewById<View>(R.id.buttonSubmitExam) as Button
        buttonSubmitExam.setOnClickListener { submitExam() }
        // dismiss the popup window when touched
        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
    }

    fun changeStatusBarColor() {
        val window = this@ReviewExamActivity.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@ReviewExamActivity, R.color.accentColor1)
    }

    fun initActionBar() {
        val actionBarLayout = layoutInflater.inflate(
                R.layout.action_bar_04_white_mcq,
                null) as ViewGroup
        // Set up your ActionBar
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(false)
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.customView = actionBarLayout
        // You customization
        val actionBarColor = resources.getColor(R.color.white)
        actionBar.setBackgroundDrawable(ColorDrawable(actionBarColor))
        val parent = actionBarLayout.parent as Toolbar
        //        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);
        val actionBarTitle = findViewById<View>(R.id.actionBarTitle) as TextView
        actionBarTitle.text = "Exam"
        val togBackButton = findViewById<View>(R.id.togBackButton) as ImageView
        togBackButton.setOnClickListener { finish() }
        //
        val gridViewbutton = findViewById<View>(R.id.gridViewbutton) as ImageView
        gridViewbutton.setOnClickListener { v -> showGridPopupQuestion(v) }
    }

    private fun hideButtons() {
        if (CURRENT_INDEX == 0) {
            buttonPrev!!.visibility = View.GONE
            buttonNext!!.visibility = View.VISIBLE
        } else {
            if (CURRENT_INDEX == questionArrayList!!.size - 1) {
                buttonNext!!.visibility = View.GONE
                buttonPrev!!.visibility = View.VISIBLE
            } else {
                buttonNext!!.visibility = View.VISIBLE
                buttonPrev!!.visibility = View.VISIBLE
            }
        }
    }

    fun showLog(name: String?, value: String?) {
        Log.d(name, value)
    }

    fun showQuestionView(question: ExamQuestion) { //  Toast.makeText(mContext, "what the !", Toast.LENGTH_SHORT).show();
        Log.d("thiskdhsofhd", "showQuestionView")
        LAST_VIEWED_QUESTION_INDEX = question.index
        CURRENT_INDEX = LAST_VIEWED_QUESTION_INDEX.toInt()
        showLoading(false)
        hideButtons()
        //  if(scoreBoardList[CURRENT_INDEX].equals(DataConstants.ANSWER_RIGHT))
        val offset = question.index.toInt()
        indexLayoutManager!!.isSmoothScrollbarEnabled = true
        indexLayoutManager!!.scrollToPositionWithOffset(question.index.toInt() - 3, 1)
        refreshRcViewIndex(question.index.toInt())
        var count = question.index.toInt()
        count++
        textViewCurrentCount!!.text = "$count / $TOTAL_NUM_QUESTIONS"
        CURRENT_QUESTION = question
        val index = question.indexInInteger + 1
        tvQuestionIndex!!.text = "" + index
        tvQuestion!!.text = html2text(index.toString() + ". " + question.question)
        //createOptionViewButtons();
        createOptionsCards()
        selectIfAlreadySelected()

        if(question.question.contains("src=\"", ignoreCase = true)) {
            question_img = (question.question.subSequence(matchDetails(question.question, "src=\"", 0) + 5, matchDetails(question.question, "\" />", 0)) as String?).toString()
        }else{
            imageViewQuestion2!!.visibility = View.GONE
            question_img=""
        }
        Log.d("imagetestwjg", "image 2 : " + question_img+"\n"+question.question)
        if (question_img == null) {
            imageViewQuestion2!!.visibility = View.GONE
        } else {
            imageUtils!!.showImage(question_img, imageViewQuestion2)
            //Log.d("imagetestwjg", "image 2 : " + ZonStringFunctions.getImagefromQuestionHtml(question.question))
        }

     /*   if (ZonStringFunctions.getImagefromQuestionHtml(question.question_image) == null) {
            imageViewQuestion2!!.visibility = View.GONE
        } else {
            Log.d("imagetestwjg", "image 2 : " + ZonStringFunctions.getImagefromQuestionHtml(question.question_image))
        }*/
        Log.d("KSJDHSUKG", "SBOARD: " + scoreBoardList[CURRENT_INDEX])
        when (scoreBoardList[CURRENT_INDEX]) {
            DataConstants.ANSWER_RIGHT -> {
                tvQAnswerStatus!!.text = "Right Answer"
                tvQAnswerStatus!!.setTextColor(resources.getColor(R.color.right_answer))
            }
            DataConstants.ANSWER_WRONG -> {
                tvQAnswerStatus!!.text = "Wrong Answer"
                tvQAnswerStatus!!.setTextColor(resources.getColor(R.color.wrong_answer))
            }
            DataConstants.ANSWER_SKIPPED -> {
                tvQAnswerStatus!!.text = "Skipped Question"
                tvQAnswerStatus!!.setTextColor(resources.getColor(R.color.skipped))
                selectTheRightAnswerGreen()
            }
        }
    }

    val optionsButtonCardView: ArrayList<CardView?>
        get() {
            val arrayList = ArrayList<CardView?>()
            arrayList.add(answer1)
            arrayList.add(answer2)
            arrayList.add(answer3)
            arrayList.add(answer4)
            return arrayList
        }

    val answersList: ArrayList<TextView?>
        get() {
            val arrayList = ArrayList<TextView?>()
            arrayList.add(textViewA)
            arrayList.add(textViewB)
            arrayList.add(textViewC)
            arrayList.add(textViewD)
            return arrayList
        }

    fun createOptionsCards() {
        clearSelections()
        Log.d("dnwjndjsdds",options[0].option)
        var option1 = options[0].option
        var option2 = options[1].option
        var option3 = options[2].option
        var option4 = options[3].option
        Log.d("kfdksfkjsdkdjfjcs",option1)



 /*       textViewA!!.text = options[0].option
        textViewB!!.text = options[1].option
        textViewC!!.text = options[2].option
        textViewD!!.text = options[3].option*/

        if(option1.contains("src=\"", ignoreCase = true)) {
            option_img1 = (option1.subSequence(matchDetails(option1, "src=\"", 0) + 5, matchDetails(option1, "\" />", 0)) as String?).toString()
        }else{
            option_img1=""
            imageoption1!!.visibility = View.GONE
        }
        if(option2.contains("src=\"", ignoreCase = true)) {
            option_img2 = (option2.subSequence(matchDetails(option2, "src=\"", 0) + 5, matchDetails(option2, "\" />", 0)) as String?).toString()
        }else{
            option_img2=""
            imageoption2!!.visibility = View.GONE
        }
        if(option3.contains("src=\"", ignoreCase = true)) {
            option_img3 = (option3.subSequence(matchDetails(option3, "src=\"", 0) + 5, matchDetails(option3, "\" />", 0)) as String?).toString()
        }else{
            option_img3=""
            imageoption3!!.visibility = View.GONE
        }
        if(option4.contains("src=\"", ignoreCase = true)) {
            option_img4 = (option4.subSequence(matchDetails(option4, "src=\"", 0) + 5, matchDetails(option4, "\" />", 0)) as String?).toString()
        }else{
            option_img4=""
            imageoption4!!.visibility = View.GONE
        }

        Log.d("jhdsbfbsjfjsjfs",option_img1)


        if (option_img1 == null||option_img1=="") {
            imageoption1!!.visibility = View.GONE
            textViewA!!.visibility = View.VISIBLE
            textViewA!!.text = html2text(options[0].option)
        }else {
            if(options[0].option.isNullOrEmpty()){
                textViewA!!.visibility = View.GONE
                imageoption1!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img1,imageoption1)
            }else{
                textViewA!!.visibility = View.VISIBLE
                textViewA!!.text = html2text(options[0].option)
                imageoption1!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img1, imageoption1)
            }
            Log.d("imagetestwjg", "image 2 : " + ZonStringFunctions.getImagefromQuestionHtml(options[0].option))
        }
        if (option_img2 == null||option_img2=="") {
            imageoption2!!.visibility = View.GONE
            textViewB!!.visibility = View.VISIBLE
            textViewB!!.text = html2text(options[1].option)
        } else {
            if(options[1].option.isNullOrEmpty()){
                textViewB!!.visibility = View.GONE
                imageoption2!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img2, imageoption2)
            }else {
                textViewB!!.visibility = View.VISIBLE
                textViewB!!.text = html2text(options[1].option)
                imageoption2!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img2, imageoption2)
            }
        }
        if (option_img3== null||option_img3=="") {
            imageoption3!!.visibility = View.GONE
            textViewC!!.visibility = View.VISIBLE
            textViewC!!.text = html2text(options[2].option)
        } else {
            if(options[2].option.isNullOrEmpty()){
                textViewC!!.visibility = View.GONE
                imageoption3!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img3, imageoption3)
            }else{
                textViewC!!.visibility = View.VISIBLE
                textViewC!!.text = html2text(options[2].option)
                imageoption3!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img3, imageoption3)
            }


        }
        if (option_img4== null||option_img4=="") {
            imageoption4!!.visibility = View.GONE
            textViewD!!.visibility = View.VISIBLE
            textViewD!!.text = html2text(options[3].option)
        } else {
            if(options[3].option.isNullOrEmpty()){
                textViewD!!.visibility = View.GONE
                imageoption4!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img4, imageoption4)
            }else{
                textViewD!!.visibility = View.VISIBLE
                textViewD!!.text = html2text(options[3].option)
                imageoption4!!.visibility = View.VISIBLE
                imageUtils!!.showImage(option_img4, imageoption4)
            }

        }


    }

    val options: ArrayList<ChoiceOption>
        get() {
            val choiceOptions = ArrayList<ChoiceOption>()
            try {
                val jsonArray = JSONArray(CURRENT_QUESTION!!.options)
                for (i in 0 until jsonArray.length()) {
                    choiceOptions.add(ChoiceOption(i.toString(), jsonArray.getString(i)))
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return choiceOptions
        }

    fun showLoading(stat: Boolean) {
        if (stat) {
            loadingLayout!!.visibility = View.VISIBLE
            fullLayout!!.visibility = View.GONE
        } else {
            loadingLayout!!.visibility = View.GONE
            fullLayout!!.visibility = View.VISIBLE
        }
    }

    fun html2text(html: String?): String {
        return Jsoup.parse(html).text()
    }

    fun showHint(show: Boolean) {
        if (show) {
            tvHint!!.text = CURRENT_QUESTION!!.hints
            layoutHint!!.visibility = View.VISIBLE
        } else {
            layoutHint!!.visibility = View.GONE
        }
    }

    fun showAnswer(show: Boolean) {
        if (show) {
            try {
                val array = JSONArray(CURRENT_QUESTION!!.answer)
                var answerIndex = array.getInt(0)
                answerIndex--
                val answer = options[answerIndex].option
                tvAnswer!!.text = answer
                tvAnswerExplanation!!.text = html2text(CURRENT_QUESTION!!.explanation)
                layoutAnswer!!.visibility = View.VISIBLE
            } catch (e: JSONException) {
            }
        } else {
            layoutAnswer!!.visibility = View.GONE
        }
    }

    fun setMySelectedAnswer(answer: Int) {
        var answer = answer
        ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()] = "" + answer
        buttonNext!!.text = "Next" //setting selected answer so changing the button from "skip" to "next"
        try {
            selectionsArrayList!!.add(CURRENT_QUESTION!!.index.toInt(), answer)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
        try {
            val ansArray = JSONArray(CURRENT_QUESTION!!.answer)
            val right_answer = ansArray.getString(0).toInt()
            answer++
            Log.d("ANSWER_CHECK", "CLICKED $answer")
            Log.d("ANSWER_CHECK", "RIGHT_ANSWER $right_answer")
            clearSelections()
            setSelectedAnswer(optionsButtonCardView[answer - 1], answersList[answer - 1], true)
            if (answer == right_answer) { //Toast.makeText(mContext, "RIGHT ANSWER", Toast.LENGTH_SHORT).show();
//TOTAL_SCORE_FROM_CURRENT_EXAM = TOTAL_SCORE_FROM_CURRENT_EXAM+Integer.parseInt(CURRENT_QUESTION.getMark());
                scoreBoardArray(CURRENT_QUESTION!!.index.toInt(), true)
                //Toast.makeText(mContext, ""+CURRENT_QUESTION.getIndex(), Toast.LENGTH_SHORT).show();
            } else {
                scoreBoardArray(CURRENT_QUESTION!!.index.toInt(), false)
                //Toast.makeText(mContext, "WRONG ANSWER", Toast.LENGTH_SHORT).show();
            }
        } catch (j: JSONException) {
        }
    }

    fun setSelectedAnswer(cardView: CardView?, tv: TextView?, stat: Boolean) {
        if (stat) {
            cardView!!.setCardBackgroundColor(resources.getColor(R.color.selected_answer))
            tv!!.setTextColor(resources.getColor(R.color.white))
            showAnswer(false)
        } else {
            showAnswer(true)
            cardView!!.setCardBackgroundColor(resources.getColor(R.color.white))
            tv!!.setTextColor(resources.getColor(R.color.accentColor1))
        }
    }

    fun clearSelections() {
        for (i in optionsButtonCardView.indices) {
            optionsButtonCardView[i]!!.setCardBackgroundColor(resources.getColor(R.color.action_bar))
            answersList[i]!!.setTextColor(resources.getColor(R.color.accentColor1))
        }
    }// showLoading(false);// String tags = question.getString("tags");
    // String created_at = question.getString("created_at");
// String updated_at = question.getString("updated_at");
    //   EXAM_DURATION = Integer.parseInt(question.getString("duration"));
    // showQuestionView(questionArrayList.get(0));
// showLoading(false);

    // Toast.makeText(mContext, "hai how are u", Toast.LENGTH_SHORT).show();
    val questionList: Unit
        get() { // Toast.makeText(mContext, "hai how are u", Toast.LENGTH_SHORT).show();
            showLoading(true)
            questionArrayList = ArrayList()
            val post_data = HashMap<String, String>()
            post_data["hash_code"] = ConfigServer.API_HASH
            post_data["exam_id"] = EXAM_ID
            val URL = ConfigServer.API_GET_EXAMS_SINGLE
            val olf = OnlineFunctions(mContext, URL, post_data)
            val handler = Handler()
            val runnable = Runnable {
                API_RESPONSE = olf.pOstResult
                handler.post {
                    showLog("API_RESADSPONSE", API_RESPONSE)
                    try {
                        val jsonObject = JSONObject(API_RESPONSE)
                        val status = jsonObject.getString("status").toInt()
                        if (status == 200) {
                             showLoading(false);
                            val jsonArray = jsonObject.getJSONArray("response")
                            TOTAL_NUM_QUESTIONS = jsonArray.length()
                            ATTENDED_QUESTION_DATA = arrayOfNulls(TOTAL_NUM_QUESTIONS)
                            MARKED_QUESTIONS_ARRAY = IntArray(TOTAL_NUM_QUESTIONS)
                            scoreBoardList = arrayOfNulls(TOTAL_NUM_QUESTIONS)
                            Arrays.fill(scoreBoardList, "0")
                            Arrays.fill(ATTENDED_QUESTION_DATA, "99")
                            Arrays.fill(MARKED_QUESTIONS_ARRAY, 0)
                            for (i in 0 until jsonArray.length()) {
                                val question = jsonArray.getJSONObject(i)
                                val questionbanks_id = question.getString("questionbanks_id")
                                val category_id = question.getString("category_id")
                                val questionQ = question.getString("question")
                                val question_image = question.getString("question_image")
                                val explanation = question.getString("explanation")
                                val hints = question.getString("hints")
                                val mark = question.getString("mark")
                                val questiontype = question.getString("questiontype")
                                val year = question.getString("year")
                                val usertype = question.getString("usertype")
                                // String tags = question.getString("tags");
                                val questionbanks_status = question.getString("questionbanks_status")
                                // String created_at = question.getString("created_at");
                                // String updated_at = question.getString("updated_at");
                                val no_of_options = question.getString("no_of_options")
                                val options = question.getString("options")
                                val answer = question.getString("answer")
                                //   EXAM_DURATION = Integer.parseInt(question.getString("duration"));
                                val index = i + 1
                                questionArrayList!!.add(ExamQuestion("" + i, questionbanks_id, category_id, questionQ, question_image
                                        , explanation, hints, mark, questiontype, year, usertype, questionbanks_status, no_of_options, options, answer))
                            }
                            // showQuestionView(questionArrayList.get(0));
                        } else { //
                            showLoading(false);
                            Toast.makeText(mContext, "Exam Not Ready Yet.", Toast.LENGTH_SHORT).show()
                            finish()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    setExamAttendedData()
                }
            }
            Thread(runnable).start()
        }

    fun refreshRcViewIndex(CURRENT_INDEX: Int) {
        adapterExam = IndexNumberAdapterExam2Review(mContext, questionArrayList, scoreBoardList, CURRENT_INDEX,
                IndexNumberAdapterExam2Review.EventListener { question, index, view -> showQuestionView(questionArrayList!![index]) }, 0)
        rcViewIndex!!.swapAdapter(adapterExam, false)
        adapterExam!!.notifyDataSetChanged()
    }

    fun scoreBoardArray(questionNo: Int, stat: Boolean) {
        if (stat) {
            scoreBoardList[questionNo] = "1"
        } else {
            scoreBoardList[questionNo] = "2"
        }
    }

    fun calculateScore() {
        TOTAL_SCORE_FROM_CURRENT_EXAM = 0
        TOTAL_NUM_RIGHT_ANSWERS = 0
        TOTAL_NUM_WRONG_ANSWERS = 0
        TOTAL_NUM_SKIPPED_ANSWERS = 0
        ATTENDED_DATA_STRING = TextUtils.join(",", ATTENDED_QUESTION_DATA)
        ATTENDED_SCOREBOARD_STRING = TextUtils.join(",", scoreBoardList)
        val mark = CURRENT_QUESTION!!.mark.toInt()
        val neg_mark = EXAM_NEG_MARK.toInt()
        for (i in scoreBoardList.indices) {
            when (scoreBoardList[i]!!.toInt()) {
                0 -> TOTAL_NUM_SKIPPED_ANSWERS++
                1 -> {
                    TOTAL_SCORE_FROM_CURRENT_EXAM = TOTAL_SCORE_FROM_CURRENT_EXAM + mark
                    TOTAL_NUM_RIGHT_ANSWERS++
                    TOTAL_POSITIVE_POINTS = TOTAL_POSITIVE_POINTS + mark
                }
                2 -> {
                    TOTAL_SCORE_FROM_CURRENT_EXAM = TOTAL_SCORE_FROM_CURRENT_EXAM - neg_mark
                    TOTAL_NUM_WRONG_ANSWERS++
                    TOTAL_NEGATIVE_POINTS = TOTAL_NEGATIVE_POINTS + neg_mark
                }
            }
        }
        //       Toast.makeText(mContext, "TOTAL SCORE : "+TOTAL_SCORE_FROM_CURRENT_EXAM, Toast.LENGTH_SHORT).show();
        Log.d("SCOREBOARD", "TOTAL_NUM_SKIPPED_ANSWERS: $TOTAL_NUM_SKIPPED_ANSWERS")
        Log.d("SCOREBOARD", "TOTAL_NUM_RIGHT_ANSWERS: $TOTAL_NUM_RIGHT_ANSWERS")
        Log.d("SCOREBOARD", "TOTAL_NUM_WRONG_ANSWERS: $TOTAL_NUM_WRONG_ANSWERS")
        Log.d("SCOREBOARD", "TOTAL_SCORE_FROM_CURRENT_EXAM TOTAL: $TOTAL_SCORE_FROM_CURRENT_EXAM")
    }

    fun getExamStatus(passmark: Int, your_score: Int): String {
        var status = ""
        status = if (your_score >= passmark) {
            "P"
        } else {
            "F"
        }
        return status
    }

    fun submitExam() {
        Toast.makeText(mContext, "called submitexam", Toast.LENGTH_SHORT).show()
        showLoading(true)
        val post_data = HashMap<String, String>()
        post_data["hash_code"] = ConfigServer.API_HASH
        post_data["exam_id"] = EXAM_ID
        post_data["student_id"] = userSession!!.useR_SESSION_ID
        post_data["answered"] = "" + (TOTAL_NUM_RIGHT_ANSWERS + TOTAL_NUM_WRONG_ANSWERS)
        post_data["right_answer"] = "" + TOTAL_NUM_RIGHT_ANSWERS
        post_data["wrong_answer"] = "" + TOTAL_NUM_WRONG_ANSWERS
        post_data["skipped"] = "" + TOTAL_NUM_SKIPPED_ANSWERS
        post_data["points"] = "" + TOTAL_SCORE_FROM_CURRENT_EXAM
        post_data["mark_optained"] = "" + TOTAL_POSITIVE_POINTS
        post_data["total_questions"] = "" + (TOTAL_NUM_WRONG_ANSWERS + TOTAL_NUM_RIGHT_ANSWERS + TOTAL_NUM_SKIPPED_ANSWERS)
        var NEG = "" + TOTAL_NUM_WRONG_ANSWERS * EXAM_NEG_MARK.toInt()
        NEG = NEG.replace("-", "")
        post_data["negative_points"] = NEG
        post_data["status"] = "1"
        post_data["attended_data"] = ATTENDED_DATA_STRING
        post_data["scoreboard_data"] = ATTENDED_SCOREBOARD_STRING
        //getExamStatus(EXAM_PASS_MARK, TOTAL_SCORE_FROM_CURRENT_EXAM) get F or P
        val URL = ConfigServer.API_POST_EXAM_RESULT
        val olf = OnlineFunctions(mContext, URL, post_data)
        val handler = Handler()
        val runnable = Runnable {
            API_RESPONSE_POST_EXAM_RESULT = olf.pOstResult
            handler.post {
                showLoading(false)
                showLog("API_RESPONSE_PO", API_RESPONSE_POST_EXAM_RESULT)
                try {
                    val jsonObject = JSONObject(API_RESPONSE_POST_EXAM_RESULT)
                    val status = jsonObject.getString("status")
                    if (status == "200") {
                        val `in` = Intent(mContext, ExamSubmittedView::class.java)
                        startActivity(`in`)
                        finish()
                    } else {
                        Toast.makeText(mContext, "Something went wrong. Try Resubmitting again", Toast.LENGTH_SHORT).show()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
        Thread(runnable).start()
    }

    fun selectIfAlreadySelectedOld() {
        try {
            if (ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()] != "99" &&
                    ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()] != "88") {
                buttonNext!!.text = "Next"
                val answer = ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()]!!.toInt()
                clearSelections()
                setSelectedAnswer(optionsButtonCardView[answer], answersList[answer], true)
            } else {
                buttonNext!!.text = "Skip"
            }
        } catch (e: Exception) {
            Log.d("sfijehre", "selectIfAlreadySelected: " + ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()])
            Toast.makeText(mContext, "Exception", Toast.LENGTH_SHORT).show()
        }
    }

    fun selectIfAlreadySelected() {
        val answer = ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()]!!.toInt()
        // Toast.makeText(mContext, ""+answer, Toast.LENGTH_SHORT).show();
        if (ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()] != "99" ||
                ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()] != "88") { // buttonNext.setText("Next");
// clearSelections();
            when (answer) {
                0 ->  //answer1.callOnClick();
                    selectAlreadyAttenedeQ(0)
                1 ->  //answer2.callOnClick();
                    selectAlreadyAttenedeQ(1)
                2 ->  //answer3.callOnClick();
                    selectAlreadyAttenedeQ(2)
                3 ->  //answer4.callOnClick();
                    selectAlreadyAttenedeQ(3)
            }
        }
    }

    fun showGridPopupQuestion(view: View?) {
        TOTAL_SCORE_FROM_CURRENT_EXAM = 0
        calculateScore()
        // inflate the layout of the popup window
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView = inflater.inflate(R.layout.poupup_layout_question_exam_review_grid_view, null)
        // create the popup window
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT
        val focusable = true // lets taps outside the popup also dismiss it
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        //  popupWindow.setAnimationStyle(R.style.popup_window_animation);
// show the popup window
// which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        val gridLayoutManager = GridLayoutManager(mContext, 5)
        val gridViewRcView = popupView.findViewById<View>(R.id.gridViewRcView) as RecyclerView
        gridViewRcView.layoutManager = gridLayoutManager
        val adapterExam = GridViewQuestionsAdapterExam2Review(mContext,
                questionArrayList, ATTENDED_QUESTION_DATA, scoreBoardList,
                GridViewQuestionsAdapterExam2Review.EventListener { question, index ->
                    CURRENT_INDEX = question.index.toInt()
                    showQuestionView(question)
                    popupWindow.dismiss()
                }, 0)
        gridViewRcView.adapter = adapterExam
        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
    }

    fun setExamAttendedData() {
        Log.d("thiskdhsofhd", "calling setExamAttendedData: ")
        val retrofit = Retrofit.Builder()
                .baseUrl(ConfigServer.API_GET_ATTEND_EXAM_DATA)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
        webServiceApi = retrofit.create(WebServiceApi::class.java)
        val call = webServiceApi!!.getAttendedExamData(ConfigServer.API_HASH,
                userSession!!.useR_SESSION_ID, EXAM_ID)
        call.enqueue(object : Callback<String?> {
            override fun onResponse(call: Call<String?>, response: Response<String?>) { //showLoading(false);
                try {
                    val jsonObject = JSONObject(response.body())
                    Log.d("vfdsafvhhweweq",jsonObject.toString())

                    val examData = jsonObject.getJSONObject("response")
                    val attended_data = examData.getString("attended_data")
                    val scoreboard_data = examData.getString("scoreboard_data")
                    val start_time = examData.getString("start_time")
                    val end_time = examData.getString("end_time")
                    ATTENDED_DATA_STRING = attended_data
                    ATTENDED_QUESTION_DATA = attended_data.split(",").toTypedArray()
                    ATTENDED_SCOREBOARD_STRING = scoreboard_data
                    scoreBoardList = scoreboard_data.split(",").toTypedArray()
                    showQuestionView(questionArrayList!![0])
                    Log.d("thiskdhsofhd", "calling trying showQuestionview: ")
                } catch (je: Exception) {
                    je.printStackTrace()
                    Log.d("thiskdhsofhd", "Exception json")
                }
            }

            override fun onFailure(call: Call<String?>, t: Throwable) {}
        })
    }

    fun selectAlreadyAttenedeQ(answer: Int) {
        var answer = answer
        ATTENDED_QUESTION_DATA[CURRENT_QUESTION!!.index.toInt()] = "" + answer
        try {
            val ansArray = JSONArray(CURRENT_QUESTION!!.answer)
            val right_answer = ansArray.getString(0).toInt()
            answer++
            Log.d("ANSWER_CHECK", "CLICKED $answer")
            Log.d("ANSWER_CHECK", "RIGHT_ANSWER $right_answer")
            val cur_index = CURRENT_QUESTION!!.index.toInt()
            //  userSelectedOption[cur_index]=answer;
            if (answer == right_answer) {
                setAsTheRightAnswer2(optionsButtonCardView[answer - 1], answersList[answer - 1], true)
                scoreBoardArray(CURRENT_QUESTION!!.index.toInt(), true)
            } else {
                setAsTheRightAnswer2(optionsButtonCardView[answer - 1], answersList[answer - 1], false)
                setAsTheRightAnswer3(optionsButtonCardView[right_answer - 1], answersList[right_answer - 1], true)
                scoreBoardArray(CURRENT_QUESTION!!.index.toInt(), false)
            }
        } catch (j: JSONException) {
        }
    }

    fun setAsTheRightAnswer2(cardView: CardView?, tv: TextView?, stat: Boolean) {
        if (stat) {
            cardView!!.setCardBackgroundColor(resources.getColor(R.color.right_answer))
            tv!!.setTextColor(resources.getColor(R.color.white))
            showAnswer(true)
        } else {
            showAnswer(true)
            cardView!!.setCardBackgroundColor(resources.getColor(R.color.wrong_answer))
            tv!!.setTextColor(resources.getColor(R.color.white))
        }
    }

    fun setAsTheRightAnswer3(cardView: CardView?, tv: TextView?, stat: Boolean) {
        if (stat) {
            cardView!!.setCardBackgroundColor(resources.getColor(R.color.right_answer))
            tv!!.setTextColor(resources.getColor(R.color.white))
        } else {
            cardView!!.setCardBackgroundColor(resources.getColor(R.color.wrong_answer))
            tv!!.setTextColor(resources.getColor(R.color.white))
        }
    }

    fun slideUpDown(view: View?) {
        if (!isPanelShown) { // Show the panel
            val bottomUp = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_up)
            hiddenPanel!!.startAnimation(bottomUp)
            hiddenPanel!!.visibility = View.VISIBLE
            isPanelShown = true
        } else { // Hide the Panel
            val bottomDown = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_up)
            hiddenPanel!!.startAnimation(bottomDown)
            hiddenPanel!!.visibility = View.INVISIBLE
            isPanelShown = false
        }
    }

    fun selectTheRightAnswerGreen() { //Log.d("kdfhek", "selectTheRightAnswerGreen: "+CURRENT_QUESTION.getAnswer());
        Log.d("kdfhek", "INDEX: $CURRENT_INDEX")
        Log.d("kdfhek", "EXAM_ID: $EXAM_ID")
        Log.d("kdfhek", "Q BANK ID: " + questionArrayList!![CURRENT_INDEX].questionbanks_id)
        Log.d("SFDQWEJLFHNE", "answer : " + questionArrayList!![CURRENT_INDEX].answer)
        try {
            val ansArray = JSONArray(questionArrayList!![CURRENT_INDEX].answer)
            val right_answer = ansArray.getString(0).toInt()
            //right_answer = right_answer-1;
            setAsTheRightAnswer2(optionsButtonCardView[right_answer - 1], answersList[right_answer - 1], true)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
    fun matchDetails(inputString: String, whatToFind: String, startIndex: Int = 0): Int {
        val matchIndex = inputString.indexOf(whatToFind, startIndex)
        return  matchIndex
    }


}
