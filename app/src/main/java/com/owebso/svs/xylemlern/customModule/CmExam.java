package com.owebso.svs.xylemlern.customModule;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.owebso.svs.xylemlern.ActivitExam.ExamSubmittedView;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.adapters.GridViewQuestionsAdapterExam;
import com.owebso.svs.xylemlern.adapters.IndexNumberAdapterExam;
import com.owebso.svs.xylemlern.functionUtils.ImageUtils;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.functionUtils.WebServiceApi;
import com.owebso.svs.xylemlern.models.ChoiceOption;
import com.owebso.svs.xylemlern.models.ExamQuestion;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.owebso.svs.xylemlern.functionUtils.ZonStringFunctions.getImagefromQuestionHtml;

public class CmExam extends AppCompatActivity {

    ArrayList<ExamQuestion> questionArrayList;
    Context mContext = CmExam.this;
    String API_RESPONSE = "", API_RESPONSE_SINGLE_QUESTION = "";
    LinearLayout loadingLayout, fullLayout;
    ExamQuestion CURRENT_QUESTION;
    LinearLayout layoutRadioGroup, layoutHintButton;
    TextView tvQuestionIndex, tvQuestion, tvAnswer;
    final RadioButton[] rb = new RadioButton[4];
    LinearLayout layoutHint, layoutAnswer;
    TextView tvHint, tvAnswerExplanation;
    ImageView imageViewQuestion, imageViewQuestion2;
    ImageUtils imageUtils;
    int CURRENT_INDEX = 0;
    Button buttonNext;
    ImageView toggleButton;
    CardView answer1, answer2, answer3, answer4;
    TextView textViewA, textViewB, textViewC, textViewD;
    RecyclerView rcViewIndex;
    String CM_EXAM_ID = "";
    int TOTAL_SCORE_FROM_CURRENT_EXAM = 0;
    String[] scoreBoardList;
    String EXAM_NEG_MARK = "", EXAM_TOTAL_MARK = "";
    int TOTAL_NUM_QUESTIONS = 0;
    int TOTAL_NUM_RIGHT_ANSWERS = 0;
    int TOTAL_NUM_WRONG_ANSWERS = 0;
    int TOTAL_NUM_SKIPPED_ANSWERS = 0;
    ArrayList<Integer> selectionsArrayList;
    int EXAM_PASS_MARK = 0;
    String API_RESPONSE_POST_EXAM_RESULT = "";

    int TOTAL_NEGATIVE_POINTS = 0;
    int TOTAL_POSITIVE_POINTS = 0;

    String[] ATTENDED_QUESTION_DATA;
    String ATTENDED_DATA_STRING = "";
    String ATTENDED_SCOREBOARD_STRING = "";

    String API_RESPONSE_CHECK_EXAM_ATT = "";

    RecyclerView.OnScrollListener onScrollListener;

    UserSession userSession;
    TextView textViewCurrentCount, tv_timer;
    LinearLayoutManager indexLayoutManager;
    //timer
    long diff;
    long oldLong;
    long NewLong;
    WebServiceApi webServiceApi;

    IndexNumberAdapterExam adapterExam;
    int EXAM_DURATION = 0;
    String LAST_VIEWED_QUESTION_INDEX = "0";
    boolean isTimesUp = false;
    String EXAM_END_DATE_TIME = "";

    String[] MARKED_QUESTIONS_ARRAY;
    boolean isPanelShown;
    ViewGroup hiddenPanel;
    ImageView imageViewMoreOptions;
    @BindView(R.id.layoutMarkQuestion)RelativeLayout layoutMarkQuestion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam);
        ButterKnife.bind(this);

        //  CATEGORY_ID = getIntent().getStringExtra("CATEGORY_ID");
//        CM_EXAM_ID = getIntent().getStringExtra("CM_EXAM_ID");
//        EXAM_NEG_MARK = getIntent().getStringExtra("EXAM_NEG_MARK");
//        EXAM_TOTAL_MARK = getIntent().getStringExtra("EXAM_TOTAL_MARK");
//        EXAM_DURATION = Integer.parseInt(getIntent().getStringExtra("EXAM_DURATION"));
//        EXAM_PASS_MARK = Integer.parseInt(getIntent().getStringExtra("EXAM_PASS_MARK"));

       // initActionBar();
        hideActionBar();
        //  changeStatusBarColor();

        initViews();
        getCustomModuleQuestions();
        //  timer();
        CmData cmData = new CmData();

    }


    public class ExamTimer extends CountDownTimer {
        ExamTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            String msg = "TIMES UP!";
            tv_timer.setText(msg);
            showAlert(fullLayout,msg,"Exit");

        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
//            String hms =(TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)) + ":")
//                    + (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)) + ":"
//                    + (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
//
            String hms = (TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)) + ":") +
                    (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)) + ":"
                            + (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
            tv_timer.setText(/*context.getString(R.string.ends_in) + " " +*/ hms);
        }
    }

    public void initViews() {

       // hiddenPanel = (ViewGroup)findViewById(R.id.layoutFullView);
     //   hiddenPanel.setVisibility(View.INVISIBLE);
        isPanelShown = false;

        imageUtils = new ImageUtils(mContext);
        selectionsArrayList = new ArrayList<>();

        ProfileUtils profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();

        //layouts
        loadingLayout = (LinearLayout) findViewById(R.id.loadingLayout);
        fullLayout = (LinearLayout) findViewById(R.id.fullLayout);
        layoutHint = (LinearLayout) findViewById(R.id.layoutHint);
        layoutAnswer = (LinearLayout) findViewById(R.id.layoutAnswer);
        layoutRadioGroup = (LinearLayout) findViewById(R.id.layoutRadioGroup);
        layoutHintButton = (LinearLayout) findViewById(R.id.layoutHintButton);
        layoutHintButton.setVisibility(View.GONE);

        //RecyclerView
        indexLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);

        rcViewIndex = (RecyclerView) findViewById(R.id.rcViewIndex);
        rcViewIndex.setLayoutManager(indexLayoutManager);


        //CardView
        answer1 = (CardView) findViewById(R.id.answer1);
        answer2 = (CardView) findViewById(R.id.answer2);
        answer3 = (CardView) findViewById(R.id.answer3);
        answer4 = (CardView) findViewById(R.id.answer4);

        textViewA = (TextView) findViewById(R.id.textViewA);
        textViewB = (TextView) findViewById(R.id.textViewB);
        textViewC = (TextView) findViewById(R.id.textViewC);
        textViewD = (TextView) findViewById(R.id.textViewD);

        //Button
        buttonNext = (Button) findViewById(R.id.buttonNext);

        //TextView
        tvQuestionIndex = (TextView) findViewById(R.id.tvQuestionIndex);
        tvAnswer = (TextView) findViewById(R.id.tvAnswer);
        tvHint = (TextView) findViewById(R.id.tvHint);
        tvQuestion = (TextView) findViewById(R.id.tvQuestion);
        tvAnswerExplanation = (TextView) findViewById(R.id.tvAnswerExplanation);
        textViewCurrentCount = (TextView) findViewById(R.id.textViewCurrentCount);
        tv_timer = (TextView) findViewById(R.id.tv_timer);

        //ImageView
        imageViewQuestion = (ImageView) findViewById(R.id.imageView4);
        imageViewQuestion2 = (ImageView) findViewById(R.id.imageViewQuestion2);
        imageViewMoreOptions = (ImageView) findViewById(R.id.imageViewMoreOptions);

        //CHANGING VISIBILITY
        imageViewQuestion.setVisibility(View.GONE);
        layoutAnswer.setVisibility(View.GONE);
        layoutHint.setVisibility(View.GONE);


        layoutMarkQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MARKED_QUESTIONS_ARRAY[CURRENT_INDEX].equals("1")){
                    MARKED_QUESTIONS_ARRAY[CURRENT_INDEX]="0";
                }else {
                    MARKED_QUESTIONS_ARRAY[CURRENT_INDEX]="1";
                }
                markQuestion();
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveExamDataOnClick();

                if(ATTENDED_QUESTION_DATA[CURRENT_INDEX].equals("99")){
                    ATTENDED_QUESTION_DATA[CURRENT_INDEX]="88";
                }

                gotoQuestion(1, v);
            }
        });

        imageViewMoreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExamMoreOptions(v);
            }
        });


        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        //we reached the target position
                        //  rcViewIndex.findViewHolderForAdapterPosition(CURRENT_INDEX).itemView.performClick();
                        //  recyclerView.removeOnScrollListener(this);
                        break;
                }
            }
        };


    }


    public void hideActionBar(){
        try{
            getSupportActionBar().hide();
        }catch (Exception e){
            try{
                getActionBar().hide();
            }catch (Exception e1){
                e1.printStackTrace();
            }

        }
    }

    public void gotoQuestion(int direction, View view) {
        if (direction == 0) {
            //prev question
            if (CURRENT_INDEX != 0) {
                CURRENT_INDEX--;
                CURRENT_QUESTION = questionArrayList.get(CURRENT_INDEX);
                showQuestionView(questionArrayList.get(CURRENT_INDEX));

            } else {
                //  Toast.makeText(mContext, "First Question", Toast.LENGTH_SHORT).show();
            }
        } else {
            //next question

            if (CURRENT_INDEX < questionArrayList.size() - 1) {
                CURRENT_INDEX++;
                CURRENT_QUESTION = questionArrayList.get(CURRENT_INDEX);
                showQuestionView(questionArrayList.get(CURRENT_INDEX));
            } else {
                showExamfinishDialogue(view);
            }

        }
    }

    public void showExamfinishDialogue(View view) {
        TOTAL_SCORE_FROM_CURRENT_EXAM = 0;
        calculateScore();

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window_show_exam_finish, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        //  popupWindow.setAnimationStyle(R.style.popup_window_animation);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        Button buttonSubmitExam = (Button) popupView.findViewById(R.id.buttonSubmitExam);
        buttonSubmitExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitExam();
            }
        });

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void changeStatusBarColor() {
        Window window = CmExam.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(CmExam.this, R.color.accentColor1));
    }

    public void initActionBar() {
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04_white_mcq,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();


        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setCustomView(actionBarLayout);
        // You customization
        final int actionBarColor = getResources().getColor(R.color.white);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent = (Toolbar) actionBarLayout.getParent();
//        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);

        TextView actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("Exam");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//

        ImageView gridViewbutton = (ImageView) findViewById(R.id.gridViewbutton);
        gridViewbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGridPopupQuestion(v);
            }
        });

    }

    public void showLog(String name, String value) {
        Log.d(name, value);
    }

    public void showQuestionView(ExamQuestion question) {

      //    Toast.makeText(mContext, "what the !", Toast.LENGTH_SHORT).show();

        LAST_VIEWED_QUESTION_INDEX = question.getIndex();
        CURRENT_INDEX= Integer.parseInt(LAST_VIEWED_QUESTION_INDEX);

        int offset = Integer.parseInt(question.getIndex());
        indexLayoutManager.setSmoothScrollbarEnabled(true);
        indexLayoutManager.scrollToPositionWithOffset(Integer.parseInt(question.getIndex()) - 3, 1);

        refreshRcViewIndex(Integer.parseInt(question.getIndex()));

        int count = Integer.parseInt(question.getIndex());
        count++;
        textViewCurrentCount.setText(count + " / " + TOTAL_NUM_QUESTIONS);

        CURRENT_QUESTION = question;

        int index = question.getIndexInInteger() + 1;
        tvQuestionIndex.setText("" + index);
        tvQuestion.setText(html2text(index + ". " + question.getQuestion()));
        //createOptionViewButtons();
        createOptionsCards();

        selectIfAlreadySelected();

        if (getImagefromQuestionHtml(question.getQuestion_image()) == null) {
            imageViewQuestion2.setVisibility(View.GONE);
        } else {
            Log.d("imagetestwjg", "image 2 : " + getImagefromQuestionHtml(question.getQuestion_image()));
        }
        showLoading(false);

        fullLayout.setVisibility(View.VISIBLE);
        markQuestion();

    }


    public ArrayList<CardView> getOptionsButtonCardView() {
        ArrayList<CardView> arrayList = new ArrayList<>();
        arrayList.add(answer1);
        arrayList.add(answer2);
        arrayList.add(answer3);
        arrayList.add(answer4);
        return arrayList;
    }

    public ArrayList<TextView> getAnswersList() {
        ArrayList<TextView> arrayList = new ArrayList<>();
        arrayList.add(textViewA);
        arrayList.add(textViewB);
        arrayList.add(textViewC);
        arrayList.add(textViewD);
        return arrayList;
    }

    public void createOptionsCards() {
        clearSelections();
        textViewA.setText(getOptions().get(0).getOption());
        textViewB.setText(getOptions().get(1).getOption());
        textViewC.setText(getOptions().get(2).getOption());
        textViewD.setText(getOptions().get(3).getOption());

        answer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMySelectedAnswer(0);
            }
        });
        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMySelectedAnswer(1);
            }
        });
        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMySelectedAnswer(2);
            }
        });
        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMySelectedAnswer(3);
            }
        });

    }

    public ArrayList<ChoiceOption> getOptions() {


        ArrayList<ChoiceOption> choiceOptions = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(CURRENT_QUESTION.getOptions());

            for (int i = 0; i < jsonArray.length(); i++) {

                choiceOptions.add(new ChoiceOption(String.valueOf(i), html2text(jsonArray.getString(i))));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return choiceOptions;
    }

    public void showLoading(boolean stat) {
        if (stat) {
            loadingLayout.setVisibility(View.VISIBLE);
            fullLayout.setVisibility(View.GONE);
        } else {
            loadingLayout.setVisibility(View.GONE);
            fullLayout.setVisibility(View.VISIBLE);
        }

    }


    public String html2text(String html) {
        return Jsoup.parse(html).text();
    }

    public void showHint(boolean show) {
        if (show) {
            tvHint.setText(CURRENT_QUESTION.getHints());
            layoutHint.setVisibility(View.VISIBLE);
        } else {
            layoutHint.setVisibility(View.GONE);
        }
    }

    public void showAnswer(boolean show) {

        if (show) {
            try {
                JSONArray array = new JSONArray(CURRENT_QUESTION.getAnswer());
                int answerIndex = array.getInt(0);
                answerIndex--;
                String answer = getOptions().get(answerIndex).getOption();
                tvAnswer.setText(answer);
                tvAnswerExplanation.setText(html2text(CURRENT_QUESTION.getExplanation()));
                layoutAnswer.setVisibility(View.VISIBLE);

            } catch (JSONException e) {

            }
        } else {
            layoutAnswer.setVisibility(View.GONE);
        }


    }

    public void setMySelectedAnswer(int answer) {

        ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())] = "" + answer;

        buttonNext.setText("Next"); //setting selected answer so changing the button from "skip" to "next"

        try {
            selectionsArrayList.add(Integer.parseInt(CURRENT_QUESTION.getIndex()), answer);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        try {
            JSONArray ansArray = new JSONArray(CURRENT_QUESTION.getAnswer());
            int right_answer = Integer.parseInt(ansArray.getString(0));
            answer++;
            Log.d("ANSWER_CHECK", "CLICKED " + answer);
            Log.d("ANSWER_CHECK", "RIGHT_ANSWER " + right_answer);
            clearSelections();
            setSelectedAnswer(getOptionsButtonCardView().get(answer - 1), getAnswersList().get(answer - 1), true);

            if (answer == right_answer) {

                //Toast.makeText(mContext, "RIGHT ANSWER", Toast.LENGTH_SHORT).show();
                //TOTAL_SCORE_FROM_CURRENT_EXAM = TOTAL_SCORE_FROM_CURRENT_EXAM+Integer.parseInt(CURRENT_QUESTION.getMark());
                scoreBoardArray(Integer.parseInt(CURRENT_QUESTION.getIndex()), true);
                //Toast.makeText(mContext, ""+CURRENT_QUESTION.getIndex(), Toast.LENGTH_SHORT).show();
            } else {
                scoreBoardArray(Integer.parseInt(CURRENT_QUESTION.getIndex()), false);
                //Toast.makeText(mContext, "WRONG ANSWER", Toast.LENGTH_SHORT).show();

            }
        } catch (JSONException j) {

        }


        saveExamDataOnClick();

    }


    public void setSelectedAnswer(CardView cardView, TextView tv, boolean stat) {
        if (stat) {
            cardView.setCardBackgroundColor(getResources().getColor(R.color.selected_answer));
            tv.setTextColor(getResources().getColor(R.color.white));
            showAnswer(false);
        } else {
            showAnswer(true);
            cardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            tv.setTextColor(getResources().getColor(R.color.accentColor1));
        }
    }

    public void clearSelections() {
        for (int i = 0; i < getOptionsButtonCardView().size(); i++) {
            getOptionsButtonCardView().get(i).setCardBackgroundColor(getResources().getColor(R.color.action_bar));
            getAnswersList().get(i).setTextColor(getResources().getColor(R.color.accentColor1));
        }
    }

//    public void getQuestionList() {
//        // Toast.makeText(mContext, "hai how are u", Toast.LENGTH_SHORT).show();
//        showLoading(true);
//        questionArrayList = new ArrayList<>();
//        final HashMap<String, String> post_data = new HashMap<>();
//        post_data.put("hash_code", ConfigServer.API_HASH);
//        post_data.put("CM_EXAM_ID", CM_EXAM_ID);
//        String URL = ConfigServer.API_GET_EXAMS_SINGLE;
//        final OnlineFunctions olf = new OnlineFunctions(mContext, URL, post_data);
//        final Handler handler = new Handler();
//
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                API_RESPONSE = olf.getPOstResult();
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        showLog("API_RESADSPONSE", API_RESPONSE);
//                        try {
//                            JSONObject jsonObject = new JSONObject(API_RESPONSE);
//                            int status = Integer.parseInt(jsonObject.getString("status"));
//                            if (status == 200) {
//                                // shwLoading(false);
//                                JSONArray jsonArray = jsonObject.getJSONArray("response");
//                              //  Toast.makeText(mContext, "SIZE : "+jsonArray.length(), Toast.LENGTH_SHORT).show();
//                                TOTAL_NUM_QUESTIONS = jsonArray.length();
//                                ATTENDED_QUESTION_DATA = new String[TOTAL_NUM_QUESTIONS];
//                                MARKED_QUESTIONS_ARRAY = new String[TOTAL_NUM_QUESTIONS];
//                                scoreBoardList = new String[TOTAL_NUM_QUESTIONS];
//                                Arrays.fill(scoreBoardList, "0");
//                                Arrays.fill(ATTENDED_QUESTION_DATA, "99");
//                                Arrays.fill(MARKED_QUESTIONS_ARRAY, "0");
//
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    JSONObject question = jsonArray.getJSONObject(i);
//                                    String questionbanks_id = question.getString("questionbanks_id");
//                                    String category_id = question.getString("category_id");
//                                    String questionQ = question.getString("question");
//                                    String question_image = question.getString("question_image");
//                                    String explanation = question.getString("explanation");
//                                    String hints = question.getString("hints");
//                                    String mark = question.getString("mark");
//                                    String questiontype = question.getString("questiontype");
//                                    String year = question.getString("year");
//                                    String usertype = question.getString("usertype");
//                                    // String tags = question.getString("tags");
//                                    String questionbanks_status = question.getString("questionbanks_status");
//                                    // String created_at = question.getString("created_at");
//                                    // String updated_at = question.getString("updated_at");
//                                    String no_of_options = question.getString("no_of_options");
//                                    String options = question.getString("options");
//                                    String answer = question.getString("answer");
//                                    //   EXAM_DURATION = Integer.parseInt(question.getString("duration"));
//
//                                    int index = i + 1;
//
//                                    questionArrayList.add(new ExamQuestion("" + i, questionbanks_id, category_id, questionQ, question_image
//                                            , explanation, hints, mark, questiontype, year, usertype, questionbanks_status, no_of_options, options, answer));
//
//
//                                }
//                                // showQuestionView(questionArrayList.get(0));
//                                checkExamStatus();
//
//                            } else {
//                                // showLoading(false);
//                                Toast.makeText(mContext, "Exam Not Ready Yet.", Toast.LENGTH_SHORT).show();
//                                finish();
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                });
//            }
//        };
//
//        new Thread(runnable).start();
//    }

    public void getCustomModuleQuestions(){
        showLoading(true);
        questionArrayList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_GET_CUSTOM_MODULE;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLog("API_RESPONSE45345",API_RESPONSE);
                        try{
                            //showLoading(false);
                            JSONObject jsonObject = new JSONObject(API_RESPONSE);
                            int status = jsonObject.getInt("status");

                            if(status==200){
                                JSONObject resultO = jsonObject.getJSONObject("result");
                                JSONArray questions = resultO.getJSONArray("questions");

                                TOTAL_NUM_QUESTIONS = questions.length();
                                EXAM_DURATION = TOTAL_NUM_QUESTIONS;
                                ATTENDED_QUESTION_DATA = new String[TOTAL_NUM_QUESTIONS];
                                MARKED_QUESTIONS_ARRAY = new String[TOTAL_NUM_QUESTIONS];
                                scoreBoardList = new String[TOTAL_NUM_QUESTIONS];
                                Arrays.fill(scoreBoardList, "0");
                                Arrays.fill(ATTENDED_QUESTION_DATA, "99");
                                Arrays.fill(MARKED_QUESTIONS_ARRAY, "0");


                                for(int i=0;i<questions.length();i++){
                                    JSONObject question = questions.getJSONObject(i);
                                    String questionbanks_id = question.getString("questionbanks_id");
                                    String questiongroups_id = "";
                                    String questionlevels_id = "";
                                    String category_id = question.getString("category_id");
                                    String questionQ = question.getString("question");
                                    String question_image = question.getString("question_image");
                                    String explanation = question.getString("explanation");
                                    String hints = question.getString("hints");
                                    String mark = question.getString("mark");
                                    String questiontype = question.getString("questiontype");
                                    String year = question.getString("year");
                                    String usertype = question.getString("usertype");
                                    String tags = "";
                                    String questionbanks_status = question.getString("questionbanks_status");
                                    String created_at = "";
                                    String updated_at = "";
                                    String no_of_options = question.getString("no_of_options");
                                    String options = question.getString("options");
                                    String answer = question.getString("answer");

                                    questionArrayList.add(new ExamQuestion("" + i, questionbanks_id, category_id, questionQ, question_image
                                            , explanation, hints, mark, questiontype, year, usertype, questionbanks_status, no_of_options, options, answer));

                                }

                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                        showQuestionView(questionArrayList.get(0));
                        startTimerExam(true);

//                        indexAdapter = new IndexNumberAdapterMcq2(mContext, questionArrayList,scoreBoardList,CURRENT_INDEX, new IndexNumberAdapterMcq2.EventListener() {
//                            @Override
//                            public void onCategoryClicked(McqQuestion question, int index, RelativeLayout view) {
//
//                                if(index<=PRIME_INDEX_NUMBER){
//                                    getSingleQuestionData(question);
//                                    // Toast.makeText(mContext, "PRIME : "+PRIME_INDEX_NUMBER, Toast.LENGTH_SHORT).show();
//                                }
//
//                            }
//
//                        },0);
//
//                        indexAdapter.notifyDataSetChanged();
//
//                        rcViewIndex.setAdapter(indexAdapter);
//
//                        //   getAttendedExamData();
//

                    }
                });
            }
        };

        new Thread(runnable).start();
    }



    public void refreshRcViewIndex(int CURRENT_INDEX) {
        adapterExam = new IndexNumberAdapterExam(mContext, questionArrayList, CURRENT_INDEX,
                new IndexNumberAdapterExam.EventListener() {
                    @Override
                    public void onCategoryClicked(ExamQuestion question, int pos) {
                        showQuestionView(questionArrayList.get(pos));
                    }
                }, 0);

        rcViewIndex.swapAdapter(adapterExam, false);
        adapterExam.notifyDataSetChanged();
    }

    public void scoreBoardArray(int questionNo, boolean stat) {

        if (stat) {
            scoreBoardList[questionNo] = "1";
        } else {
            scoreBoardList[questionNo] = "2";
        }

    }

    public void calculateScore() {
        TOTAL_SCORE_FROM_CURRENT_EXAM = 0;
        TOTAL_NUM_RIGHT_ANSWERS = 0;
        TOTAL_NUM_WRONG_ANSWERS = 0;
        TOTAL_NUM_SKIPPED_ANSWERS = 0;

        ATTENDED_DATA_STRING = TextUtils.join(",", ATTENDED_QUESTION_DATA);
        ATTENDED_SCOREBOARD_STRING = TextUtils.join(",", scoreBoardList);

        int mark = 4;
        int neg_mark = -1;

        for (int i = 0; i < scoreBoardList.length; i++) {

            switch (Integer.parseInt(scoreBoardList[i])) {
                case 0:
                    TOTAL_NUM_SKIPPED_ANSWERS++;
                    break;
                case 1:
                    TOTAL_SCORE_FROM_CURRENT_EXAM = TOTAL_SCORE_FROM_CURRENT_EXAM + mark;
                    TOTAL_NUM_RIGHT_ANSWERS++;
                    TOTAL_POSITIVE_POINTS = TOTAL_POSITIVE_POINTS + mark;
                    break;
                case 2:
                    TOTAL_SCORE_FROM_CURRENT_EXAM = TOTAL_SCORE_FROM_CURRENT_EXAM - neg_mark;
                    TOTAL_NUM_WRONG_ANSWERS++;
                    TOTAL_NEGATIVE_POINTS = TOTAL_NEGATIVE_POINTS + neg_mark;
                    break;
            }


        }
//       Toast.makeText(mContext, "TOTAL SCORE : "+TOTAL_SCORE_FROM_CURRENT_EXAM, Toast.LENGTH_SHORT).show();
        Log.d("SCOREBOARD", "TOTAL_NUM_SKIPPED_ANSWERS: " + TOTAL_NUM_SKIPPED_ANSWERS);
        Log.d("SCOREBOARD", "TOTAL_NUM_RIGHT_ANSWERS: " + TOTAL_NUM_RIGHT_ANSWERS);
        Log.d("SCOREBOARD", "TOTAL_NUM_WRONG_ANSWERS: " + TOTAL_NUM_WRONG_ANSWERS);
        Log.d("SCOREBOARD", "TOTAL_SCORE_FROM_CURRENT_EXAM TOTAL: " + TOTAL_SCORE_FROM_CURRENT_EXAM);

    }


    public String getExamStatus(int passmark, int your_score) {
        String status = "";
        if (your_score >= passmark) {
            status = "P";
        } else {
            status = "F";
        }
        return status;
    }

    public void submitExam() {
     //   Toast.makeText(mContext, "called submitexam", Toast.LENGTH_SHORT).show();
        showLoading(true);
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("CM_EXAM_ID", CM_EXAM_ID);
        post_data.put("student_id", userSession.getUSER_SESSION_ID());
        post_data.put("answered", "" + (TOTAL_NUM_RIGHT_ANSWERS + TOTAL_NUM_WRONG_ANSWERS));
        post_data.put("right_answer", "" + TOTAL_NUM_RIGHT_ANSWERS);
        post_data.put("wrong_answer", "" + TOTAL_NUM_WRONG_ANSWERS);
        post_data.put("skipped", "" + TOTAL_NUM_SKIPPED_ANSWERS);
        post_data.put("points", "" + TOTAL_SCORE_FROM_CURRENT_EXAM);
        post_data.put("mark_optained", "" + TOTAL_POSITIVE_POINTS);
        post_data.put("total_questions", "" + (TOTAL_NUM_WRONG_ANSWERS + TOTAL_NUM_RIGHT_ANSWERS + TOTAL_NUM_SKIPPED_ANSWERS));
        String NEG = "" + (TOTAL_NUM_WRONG_ANSWERS * Integer.parseInt(EXAM_NEG_MARK));
        NEG = NEG.replace("-", "");
        post_data.put("negative_points", NEG);
        post_data.put("status", "1");
        post_data.put("attended_data", ATTENDED_DATA_STRING);
        post_data.put("scoreboard_data", ATTENDED_SCOREBOARD_STRING);
        post_data.put("marked_question_data",TextUtils.join(",",MARKED_QUESTIONS_ARRAY));

        //getExamStatus(EXAM_PASS_MARK, TOTAL_SCORE_FROM_CURRENT_EXAM) get F or P

        String URL = ConfigServer.API_POST_EXAM_RESULT;
        final OnlineFunctions olf = new OnlineFunctions(mContext, URL, post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_POST_EXAM_RESULT = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLoading(false);

                        showLog("API_RESPONSE_PO", API_RESPONSE_POST_EXAM_RESULT);

                        try {
                            JSONObject jsonObject = new JSONObject(API_RESPONSE_POST_EXAM_RESULT);
                            String status = jsonObject.getString("status");

                            if (status.equals("200")) {

                                Intent in = new Intent(mContext, ExamSubmittedView.class);
                                startActivity(in);
                                finish();
                            } else {
                                Toast.makeText(mContext, "Something went wrong. Try Resubmitting again", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        };
        new Thread(runnable).start();

    }

    public void checkExamStatus() {
        showLoading(true);
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("CM_EXAM_ID", CM_EXAM_ID);
        post_data.put("student_id", userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_CHECK_EXAM_ATTENDANCE;
        final OnlineFunctions olf = new OnlineFunctions(mContext, URL, post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_CHECK_EXAM_ATT = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            showLog("skdghwqkd", API_RESPONSE_CHECK_EXAM_ATT);

                            JSONObject jsonObject = new JSONObject(API_RESPONSE_CHECK_EXAM_ATT);
                            String status = jsonObject.getString("status");

                            if (status.equals("200")) {
                                String code = jsonObject.getString("code");

                                if (code.equals("2")) {
                                    //exam not attended
                                    //  getQuestionList();
                                    startTimerExam(true); //START FRESH TIMER
                                    showQuestionView(questionArrayList.get(0));
                                } else {
                                    JSONObject result = jsonObject.getJSONObject("result");

                                    String attend_data = result.getString("attended_data");
                                    int exam_status = Integer.parseInt(result.getString("status"));
                                    String startTime = result.getString("start_time");
                                    String endTime = result.getString("end_time");
                                    int last_index = Integer.parseInt(result.getString("last_index"));
                                    String attended_data = result.getString("attended_data");
                                    String scoreboard_data = result.getString("scoreboard_data");
                                    String marked_question_data = result.getString("marked_question_data");

                                    EXAM_END_DATE_TIME = endTime;

                                    startTimerExam(false);
                                    //   Toast.makeText(mContext, "exam status : "+exam_status, Toast.LENGTH_SHORT).show();

                                    switch (exam_status) {
                                        case 0:
                                            //  Toast.makeText(mContext, "exam status : 0000", Toast.LENGTH_SHORT).show();
                                            showQuestionView(questionArrayList.get(0));

                                            break;
                                        case 1:
                                            //EXAM FINISHED WAITING FOR RESULT
                                          //  Toast.makeText(mContext, "REVIEW", Toast.LENGTH_SHORT).show();
                                            break;
                                        case 2:
                                            if (isTimesUp(endTime)) {
                                                //EXAM TIMES UP
                                              //  Toast.makeText(mContext, "TIMES UP", Toast.LENGTH_SHORT).show();

                                                Log.d("timekwgd", "startTime: " + startTime);
                                                Log.d("timekwgd", "endTime: " + endTime);

                                                finish();
                                            } else {
                                                Toast.makeText(mContext, "Resuming Exam..", Toast.LENGTH_SHORT).show();

                                                //you have some times left /RESUMING EXAM
                                                //goto question for resume
                                                ATTENDED_QUESTION_DATA = attended_data.split(",");
                                                scoreBoardList = scoreboard_data.split(",");
                                                MARKED_QUESTIONS_ARRAY = marked_question_data.split(",");

                                                showQuestionView(questionArrayList.get(last_index));

                                                Log.d("dqjhdjkw", "lastindex : "+last_index);
                                                Log.d("dqjhdjkw", "size : "+questionArrayList.size());

                                            }
                                            break;


                                    }

                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    public void selectIfAlreadySelected() {


        try {
            if (!ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())].equals("99") &&
                    !ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())].equals("88")) {

                buttonNext.setText("Next");


                int answer = Integer.parseInt(ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())]);
                clearSelections();
                setSelectedAnswer(getOptionsButtonCardView().get(answer), getAnswersList().get(answer), true);
            } else {
                buttonNext.setText("Skip");
            }
        } catch (Exception e) {
            Log.d("sfijehre", "selectIfAlreadySelected: "+ATTENDED_QUESTION_DATA[Integer.parseInt(CURRENT_QUESTION.getIndex())]);

        //    Toast.makeText(mContext, "Exception", Toast.LENGTH_SHORT).show();

        }


    }

    public void showGridPopupQuestion(View view) {
        TOTAL_SCORE_FROM_CURRENT_EXAM = 0;
        calculateScore();

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.poupup_layout_question_exam_grid_view, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        //  popupWindow.setAnimationStyle(R.style.popup_window_animation);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);


        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 5);
        RecyclerView gridViewRcView = (RecyclerView) popupView.findViewById(R.id.gridViewRcView);
        gridViewRcView.setLayoutManager(gridLayoutManager);

        GridViewQuestionsAdapterExam adapterExam = new GridViewQuestionsAdapterExam(mContext,
                questionArrayList,ATTENDED_QUESTION_DATA,MARKED_QUESTIONS_ARRAY,
                new GridViewQuestionsAdapterExam.EventListener() {
                    @Override
                    public void onCategoryClicked(ExamQuestion question, int index) {
                        CURRENT_INDEX = Integer.parseInt(question.getIndex());
                        showQuestionView(question);
                        popupWindow.dismiss();

                    }
                }, 0);

        gridViewRcView.setAdapter(adapterExam);


        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }


    public void startTimerExam(boolean fresh) {
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.MINUTE, EXAM_DURATION);

        String startTime = spf.format(currentTime);
        String endTime = spf.format(calendar.getTime());

        if (fresh) {
            setExamTime(startTime, endTime);
            startTimer(startTime, endTime);
        } else {
            startTimer(startTime, EXAM_END_DATE_TIME);
        }


    }


    public boolean isTimesUp(String endT) {

        Date currentTime = Calendar.getInstance().getTime();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            //Date sTime = format.parse(timeS);
            Date eTime = format.parse(endT);

            if (currentTime.after(eTime)) {
                isTimesUp = true;
            } else {
                isTimesUp = false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return isTimesUp;
    }

    public void setExamTime(String startTime, String endTime) {

        ATTENDED_DATA_STRING = TextUtils.join(",", ATTENDED_QUESTION_DATA);
        ATTENDED_SCOREBOARD_STRING = TextUtils.join(",", scoreBoardList);

        Log.d("sdjehdewd", "ATTENDED_DATA_STRING : " + ATTENDED_DATA_STRING);
        Log.d("sdjehdewd", "ATTENDED_SCOREBOARD_STRING : " + ATTENDED_SCOREBOARD_STRING);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigServer.API_POST_EXAM_START_END_TIME)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        webServiceApi = retrofit.create(WebServiceApi.class);


        Call<String> call = webServiceApi.setExamTime(ConfigServer.API_HASH,
                CM_EXAM_ID, userSession.getUSER_SESSION_ID(), startTime, endTime, "2",
                LAST_VIEWED_QUESTION_INDEX, ATTENDED_DATA_STRING, ATTENDED_SCOREBOARD_STRING);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //showLoading(false);
                try {

                    JSONObject jsonObject = new JSONObject(response.body());

                    Log.d("sdjehdewd", "onResponse: " + response.body());


                } catch (Exception je) {
                    je.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });

    }

    private void saveExamDataOnClick() {
        //Save Exam attended question and data ,when everytime user click on 1/4 option

        ATTENDED_DATA_STRING = TextUtils.join(",", ATTENDED_QUESTION_DATA);
        ATTENDED_SCOREBOARD_STRING = TextUtils.join(",", scoreBoardList);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigServer.API_POST_EXAM_UPDATE_ONCLICK)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        webServiceApi = retrofit.create(WebServiceApi.class);


        Call<String> call = webServiceApi.updateExamOnClick(ConfigServer.API_HASH,
                CM_EXAM_ID, userSession.getUSER_SESSION_ID(), "2",
                LAST_VIEWED_QUESTION_INDEX,
                ATTENDED_DATA_STRING, ATTENDED_SCOREBOARD_STRING,TextUtils.join(",",MARKED_QUESTIONS_ARRAY));

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //showLoading(false);
                try {

                    JSONObject jsonObject = new JSONObject(response.body());

                    Log.d("sdjehdewd", "onResponse: " + response.body());


                } catch (Exception je) {
                    je.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });

    }


    public void startTimer(String startTime, String endTime) {

        Log.d("kjdfghdf", "startTime: " + startTime);
        Log.d("kjdfghdf", "endTime: " + endTime);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date oldDate, newDate;
        try {
            oldDate = formatter.parse(startTime);
            newDate = formatter.parse(endTime);
            oldLong = oldDate.getTime();
            NewLong = newDate.getTime();
            diff = NewLong - oldLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ExamTimer counter = new ExamTimer(diff, 1000);

        Log.d("kjdfghdf", "differnce: " + diff);

        counter.start();
    }


    public void slideUpDown(final View view) {
        if(!isPanelShown) {
            // Show the panel
            Animation bottomUp = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_up);

            hiddenPanel.startAnimation(bottomUp);
            hiddenPanel.setVisibility(View.VISIBLE);
            isPanelShown = true;
        }
        else {
            // Hide the Panel
            Animation bottomDown = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_up);

            hiddenPanel.startAnimation(bottomDown);
            hiddenPanel.setVisibility(View.INVISIBLE);
            isPanelShown = false;
        }
    }

    public void showExamMoreOptions(View view){
        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.poupup_layout_exam_options, null);


        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupView.setAnimation(AnimationUtils.loadAnimation(this,R.anim.bottom_up));

        popupWindow.showAtLocation(view, Gravity.END, Gravity.START, Gravity.END);

        RelativeLayout layoutSubmitExam1 = (RelativeLayout) popupView.findViewById(R.id.layoutSubmitExam1);
        RelativeLayout layoutExitExam = (RelativeLayout) popupView.findViewById(R.id.layoutExitExam);
        RelativeLayout layoutGridView = (RelativeLayout) popupView.findViewById(R.id.layoutGridView);
        RelativeLayout layoutErrorReport = (RelativeLayout) popupView.findViewById(R.id.layoutErrorReport);
        
        layoutSubmitExam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                showExamfinishDialogue(fullLayout);
            }
        });

        layoutGridView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                showGridPopupQuestion(fullLayout);
            }
        });
        layoutExitExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                saveExamDataOnClick();
                showExitExamPopup(fullLayout);
            }
        });
   
        // showExamfinishDialogue

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void showExitExamPopup(View view){
        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_layout_exit_exam_prompt, null);


        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupView.setAnimation(AnimationUtils.loadAnimation(this,R.anim.bottom_up));

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

      //  RelativeLayout layoutSubmitExam1 = (RelativeLayout) popupView.findViewById(R.id.layoutSubmitExam1);
      Button buttonExitExam = (Button) popupView.findViewById(R.id.buttonExitExam);
      Button buttonCancelPopup1 = (Button) popupView.findViewById(R.id.buttonCancelPopup1);

      buttonExitExam.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              popupWindow.dismiss();
              finish();
          }
      });     buttonCancelPopup1.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Toast.makeText(mContext, "Resuming Exam.", Toast.LENGTH_SHORT).show();
              popupWindow.dismiss();
          }
      });

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void showAlert(View view,String msg,String exitButtonMsg){
        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.poupup_layout_alert_with_button, null);


        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupView.setAnimation(AnimationUtils.loadAnimation(this,R.anim.bottom_up));
        popupWindow.setOutsideTouchable(false);

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //  RelativeLayout layoutSubmitExam1 = (RelativeLayout) popupView.findViewById(R.id.layoutSubmitExam1);
        TextView tvAlertMessage = (TextView) popupView.findViewById(R.id.tvAlertMessage);
        TextView tvButton = (TextView) popupView.findViewById(R.id.tvButton);

        tvAlertMessage.setText(msg);
        tvButton.setText(exitButtonMsg);

        tvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }


    public void markQuestion(){
        View markview = (View)findViewById(R.id.viewMark);
        TextView tvMarkTitle = (TextView)findViewById(R.id.tvMarkTitle);

        if(MARKED_QUESTIONS_ARRAY[CURRENT_INDEX].equals("1")){
            tvMarkTitle.setText("UnMark");
            markview.setBackground(ResourcesCompat.getDrawable(mContext.getResources(),R.drawable.marked_question_01,null));
        }else {
            tvMarkTitle.setText("Mark");
            markview.setBackground(ResourcesCompat.getDrawable(mContext.getResources(),R.drawable.un_marked_question_01,null));
        }
    }


}
