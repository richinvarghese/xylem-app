package com.owebso.svs.xylemlern.rest;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.util.Log;


import androidx.annotation.NonNull;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;

/**
 * Created by Vishnu Saini on 2/20/2018.
 * vishnusainideveloper27@gmail.com
 */

public class Rest {
    public static final String MULTIPARt_FORM_DATA = "multipart/form-data";
    private static final String MULTIPART_FORM_DATA = "multipart/form-data";
    public Context ctx;
    public ProgressDialog dialog;
    Callback callback;
    RestService restService;
    private String pDialogMessage = "Loading...";

    public Rest(Context ctx, Callback callback) {
        this.callback = callback;
        this.ctx = ctx;
        init();
    }

    public static void printLog(String msg) {
        Log.e("hommzi", msg);
    }

    public static String getExtension(String filePath) {
        File f = new File(filePath);
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf(".");

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    private void init() {
        dialog = new ProgressDialog(ctx);
        dialog.setMessage(pDialogMessage);
        dialog.setCancelable(false);
        restService = RestAdapter.getAdapter();

    }

    public boolean isInterentAvaliable() {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected();
    }

    public void AlertForInternet() {
        AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
        alert.setMessage("Internet Not avalible");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismissProgressdialog();
            }
        });
        alert.show();
    }

    public void ShowDialogue(String message) {
        dialog.setMessage(message);
        dialog.show();
    }

    public void dismissProgressdialog() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            Rest.printLog("" + e);
        }
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
    }








}
