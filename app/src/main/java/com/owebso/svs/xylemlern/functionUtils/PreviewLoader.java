package com.owebso.svs.xylemlern.functionUtils;

public interface PreviewLoader {
    void loadPreview(long currentPosition, long max);
}
