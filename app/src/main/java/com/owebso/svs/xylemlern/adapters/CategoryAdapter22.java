package com.owebso.svs.xylemlern.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sarath on 07/06/19.
 */

public class CategoryAdapter22 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<Category> categoryList;
    EventListener listener;
    String API_RESPONSE_GET_CAT_SINGLE="";
    String MODULE_TITLE="";
    int SORT_TYPE=0;

    public CategoryAdapter22(Context mContext, ArrayList<Category> categoryList, EventListener listener,int SORT_TYPE) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.listener =listener;
        this.SORT_TYPE =SORT_TYPE;
    }

    public interface EventListener {
        void onCategoryClicked(String categoryId, String parentId);
    }
    public CategoryAdapter22(EventListener listener){
        this.listener =listener;
    }

    public  class MainView extends RecyclerView.ViewHolder {
        TextView textViewTitle,cat_id,textViewDescription,moduleIndex,textViewYear;
        ImageView thumbnailCat;
        CardView cardClickView;
        RelativeLayout fullItemView;
        public MainView(View v) {
            super(v);
            this.cat_id = (TextView) v.findViewById(R.id.id);
            this.textViewTitle = (TextView) v.findViewById(R.id.textViewTitle);
            this.textViewDescription = (TextView) v.findViewById(R.id.textViewDescription);
            this.moduleIndex = (TextView) v.findViewById(R.id.moduleIndex);
            this.textViewYear = (TextView) v.findViewById(R.id.textViewYear);
            this.thumbnailCat = (ImageView) v.findViewById(R.id.thumbnailCat);
            this.cardClickView = (CardView) v.findViewById(R.id.cardClickView);
            this.fullItemView = (RelativeLayout) v.findViewById(R.id.fullItemView);

        }
    }

    public class TitleView extends  RecyclerView.ViewHolder {
        TextView tvModuleTitle;
        public TitleView(View v) {
            super(v);
            tvModuleTitle = (TextView)v.findViewById(R.id.tvModuleTitle);
        }
    }

    public  class AdsView extends  RecyclerView.ViewHolder {
        public AdsView(View v) {
            super(v);
        }
    }

    public void showImage(String IMAGE_URL, ImageView imageView){

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_cash);
        requestOptions.error(R.drawable.ic_cash);

        Glide.with(mContext)
                .applyDefaultRequestOptions(requestOptions)
                .load(IMAGE_URL)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        int i=0;
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        return false;
                    }
                }).into(imageView)
        ;

    }

    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
        if(viewType==0)
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_module_title, parent, false);
            return new TitleView(itemView);
        }
        else if(viewType==1)
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.category_view_with_numbering, parent, false);
            return new MainView(itemView);
        }
        else
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_layout_video_category, parent, false);
            return new AdsView(itemView);
        }



    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Category catObj = categoryList.get(position);
        if(holder.getItemViewType()==1)
        {

            ((MainView)holder).fullItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCategoryClicked(catObj.getCategory_id(),catObj.getParent_id());
                }
            });

            ((MainView)holder).cat_id.setText(catObj.getCategory_id());
            ((MainView)holder).textViewTitle.setText(catObj.getCategory_name());
            ((MainView)holder).textViewDescription.setText(catObj.getDescription());
            ((MainView)holder).moduleIndex.setText(""+(catObj.getIndex()+1));

                if(catObj.getYear().equals("1")){
                    ((MainView)holder).textViewYear.setText(catObj.getYear()+"st Year");
                }else {
                    ((MainView)holder).textViewYear.setText(catObj.getYear()+"nd Year");
                }

              Log.d("IMAGE_URL23", "URL : "+catObj.getCategory_image());

            showImage(catObj.getCategory_image(),((MainView)holder).thumbnailCat);


        }
        else if(holder.getItemViewType()==0)
        {
            ((TitleView)holder).tvModuleTitle.setText("Loading..");
            setModuleTitle(catObj.getParent_id(),((TitleView)holder).tvModuleTitle);
        }
        else {

            final AdsView third_holder = (AdsView)holder;
            //third_holder.third_categoryList_tv.setText(catObj.getName());
        }

    }


    public void removeAnItem(int item){
        categoryList.remove(item);
        notifyItemRemoved(item);
    }

    @Override
    public int getItemViewType(int position) {
        return categoryList.get(position).getCAT_TYPE();
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void setModuleTitle(String catId, final TextView tvTitle1){

        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("category_id",catId);
        //Log.d("CATEGORY_ID", "ID: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_CATEGORY_SINGLE;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("API_RESPONSE_GET", ""+API_RESPONSE_GET_CAT_SINGLE);

                            try{
                                JSONObject jsonObject =  new JSONObject(API_RESPONSE_GET_CAT_SINGLE);
                                String status = jsonObject.getString("status");

                                if(status.equals("200")){
                                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                                    JSONObject category = jsonArray.getJSONObject(0);
                                    String category_name = category.getString("category_name");
                                    MODULE_TITLE = category_name;
                                    tvTitle1.setText(category_name);
                                }

                            }catch (JSONException e){

                            }
                        }
                    });
            }
        };
        new Thread(runnable).start();
    }

}
