package com.owebso.svs.xylemlern.XyFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.owebso.svs.xylemlern.ActivitExam.ExamIntro;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.ServerConfig.DataConstants;
import com.owebso.svs.xylemlern.adapters.ExamListWithTabsAdapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Exam;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ExamFragment2 extends Fragment {
    private Context mContext = getContext();

    RecyclerView recyclerView;
    private String API_RESPONSE_GET_Exam="";

    ArrayList<Exam> allExamsList;
    ArrayList<Exam> weeklyTestList;
    ArrayList<Exam> monthlyTestList;
    ArrayList<Exam> grandTestList;
    ArrayList<Exam> subjectwiseTestList;

    ArrayList<String> moduleList;
    boolean isModule=false;
    ExamListWithTabsAdapter adapter22;

    String CATEGORY_ID="";
    String CURRENT_MONTH="";
    boolean isExamTitleSet=false;
    //String LIST_OF_MODULES="";
    UserSession userSession;
    LinearLayout layoutNoDataAlert;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        CATEGORY_ID = getArguments().getString("cat_id");
        return inflater.inflate(R.layout.activity_exams_02_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews();
        setupTabLayout();
        //getSupportActionBar().hide();
        getExamsList();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void initViews(){

        ProfileUtils profileUtils = new ProfileUtils(getActivity());
        userSession = profileUtils.getUserSession();

        layoutNoDataAlert = (LinearLayout)getView().findViewById(R.id.layoutNoDataAlert);
        layoutNoDataAlert.setVisibility(View.GONE);

        recyclerView = (RecyclerView)getView().findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);
        moduleList = new ArrayList<>();

    }

    public void getExamsList(){
        allExamsList = new ArrayList<>();
        weeklyTestList = new ArrayList<>();
        monthlyTestList = new ArrayList<>();
        grandTestList = new ArrayList<>();
        subjectwiseTestList= new ArrayList<>();
        allExamsList.clear();
        weeklyTestList.clear();
        monthlyTestList.clear();
        grandTestList.clear();
        subjectwiseTestList.clear();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("category_id", CATEGORY_ID);
        //CHANGE THIS PARAMETERS BEFORE RUN #IMP
        //Log.d("Exam_ID", "ID: "+Exam_ID);
        String URL = ConfigServer.API_GET_NEW_EXAM;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_Exam = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.d("API_RESPONSE", ""+API_RESPONSE_GET_Exam);
                        //Log.d("API_RESPONSE", "CAT_ID="+Exam_ID);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_Exam);
                            String status = jsonObject1.getString("status");
                            // String video_exist = jsonObject1.getString("video_exist");

                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("response"));
                            CURRENT_MONTH="NULLL";

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String exam_id = jsonObject.getString("exam_id");
                                String exam_title = jsonObject.getString("exam_title");
                                String description = jsonObject.getString("description");
                                String category_id = jsonObject.getString("category_id");
                                String university_id = jsonObject.getString("university_id");
                                String coursetype_id = jsonObject.getString("coursetype_id");
                                String course_id = jsonObject.getString("course_id");
                                String subject_id = jsonObject.getString("subject_id");
                                String instruction_id = jsonObject.getString("instruction_id");
                                String examtype = jsonObject.getString("examtype");
                                String exam_type_cat = jsonObject.getString("exam_type_cat");
                                String start_date = jsonObject.getString("start_date");
                                String end_date = jsonObject.getString("end_date");
                                String duration = jsonObject.getString("duration");
                                String total_mark = jsonObject.getString("total_mark");
                                String pass_mark = jsonObject.getString("pass_mark");
                                String negative_mark = jsonObject.getString("negative_mark");
                                String published = jsonObject.getString("published");
                                String exam_status = jsonObject.getString("exam_status");
                                String questions = jsonObject.getString("questions");
                                String total_qst = jsonObject.getString("total_qst");
                                String publish_date = jsonObject.getString("publish_date");
                                String created_at = jsonObject.getString("created_at");
                                String updated_at = jsonObject.getString("updated_at");
                                String paid_type = jsonObject.getString("paid_type");
                                String correctmark = jsonObject.getString("correctmark");

                                Intent in = new Intent(getActivity(), ExamIntro.class);
                                in.putExtra("EXAM_ID",exam_id);
                                in.putExtra("EXAM_TITLE",exam_title);
                                in.putExtra("EXAM_TYPE",exam_type_cat);
                                in.putExtra("EXAM_NEG_MARK",negative_mark);
                                in.putExtra("EXAM_TOTAL_MARK",total_mark);
                                in.putExtra("EXAM_PASS_MARK",pass_mark);
                                in.putExtra("EXAM_DURATION",duration);
                                startActivity(in);
                                Exam examData = new Exam(i,exam_id,exam_title,description,category_id,university_id,coursetype_id,
                                        course_id,subject_id,instruction_id,examtype,exam_type_cat,start_date,end_date,duration,
                                        total_mark,pass_mark,negative_mark,published,exam_status,questions,total_qst,publish_date,created_at,updated_at
                                        ,paid_type,correctmark,1);
                                if(!CURRENT_MONTH.equals(getMonthFromDate(examData.getStart_date()))){
                                    CURRENT_MONTH =getMonthFromDate(examData.getStart_date());
                                    Exam exam = new Exam();
                                    exam.setIndex(i);
                                    exam.setStart_date(examData.getStart_date());
                                    exam.setTYPE(0);
                                    exam.setExam_type_cat(examData.getExam_type_cat());
                                    allExamsList.add(exam);
                                    allExamsList.add(examData);
                                    isExamTitleSet=true;

                                }else {
                                    isExamTitleSet=false;
                                    allExamsList.add(examData);

                                }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // getSortedList(); //CREATING SORTED LIST
                        generateExamTestcategories();
                        recyclerView.setAdapter(null);
                        adapter22 = new ExamListWithTabsAdapter(mContext, allExamsList, new ExamListWithTabsAdapter.EventListener() {
                            @Override
                            public void onCategoryClicked(String examId, Exam exam) {
                                gotoExam(exam);
                            }
                        },0);
                      //  recyclerView.setAdapter(adapter22);
                    }
                });
            }
        };

        new Thread(runnable).start();
    }


    public String getMonthFromDate(String dateString){
        String dtStart = dateString;
        String monthString="";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(dtStart);
            monthString  = (String) DateFormat.format("MMM yyyy",  date); // Jun

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return monthString;
    }

    public void gotoExam(Exam examObj){
        Intent in = new Intent(getActivity(), ExamIntro.class);
        in.putExtra("EXAM_ID",examObj.getExam_id());
        in.putExtra("EXAM_TITLE",examObj.getExam_title());
        in.putExtra("EXAM_TYPE",getExamType(examObj.getExam_type_cat()));
        in.putExtra("EXAM_NEG_MARK",examObj.getNegative_mark());
        in.putExtra("EXAM_TOTAL_MARK",examObj.getTotal_mark());
        in.putExtra("EXAM_PASS_MARK",examObj.getPass_mark());
        in.putExtra("EXAM_DURATION",examObj.getDuration());
        startActivity(in);

    }

    public String getExamType(String type){
        String op="";
        switch (type){
            case DataConstants.EXAM_TYPE_ALL:
                op="Normal Test";
                break;
            case DataConstants.EXAM_TYPE_WEEKLY:
                op = "Weekly Test";
                break;
            case DataConstants.EXAM_TYPE_MONTHLY:
                op="Monthly Test";
                break;
            case DataConstants.EXAM_TYPE_GRAND_TEST:
                op="Grand Test";
                break;
            case DataConstants.EXAM_TYPE_SUBJECTWISE:
                op="Subject Wise Test";
                break;
        }
        return op;
    }

    public void generateExamTestcategories(){

        weeklyTestList.clear();
        monthlyTestList.clear();
        grandTestList.clear();
        subjectwiseTestList.clear();

        for(int i=0;i<allExamsList.size();i++){

            Log.d("dshkjfh", "type: "+allExamsList.get(i).getExam_type_cat());

            switch (allExamsList.get(i).getExam_type_cat()){
                case DataConstants
                        .EXAM_TYPE_ALL:
                    //do nothing
                    break;
                case DataConstants.EXAM_TYPE_WEEKLY:
                    weeklyTestList.add(allExamsList.get(i));
                    break;
                case DataConstants.EXAM_TYPE_MONTHLY:
                    monthlyTestList.add(allExamsList.get(i));
                    break;
                case DataConstants.EXAM_TYPE_GRAND_TEST:
                    grandTestList.add(allExamsList.get(i));
                    break;
                case DataConstants.EXAM_TYPE_SUBJECTWISE:
                    subjectwiseTestList.add(allExamsList.get(i));
                    break;
                default:
                    weeklyTestList.add(allExamsList.get(i));
                    monthlyTestList.add(allExamsList.get(i));
                    grandTestList.add(allExamsList.get(i));
                    subjectwiseTestList.add(allExamsList.get(i));
            }


//            if(allExamsList.get(i).getYear().equals("1")){
//                weeklyTestList.add(allExamsList.get(i));
//            }else if(allExamsList.get(i).getYear().equals("2")){
//                monthlyTestList.add(allExamsList.get(i));
//            }else {
//
//                weeklyTestList.add(allExamsList.get(i));
//                monthlyTestList.add(allExamsList.get(i));
//            }
        }

        Log.d("TESTSUT7", "weeklyTestList SIZE: "+weeklyTestList.size());
        Log.d("TESTSUT7", "monthlyTestList : SIZE "+monthlyTestList.size());



        //   getTrimList();
    }

    public void setupTabLayout() {
        TabLayout mTabLayout = (TabLayout) getView().findViewById(R.id.tabs);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()){
                    case 0:
                        Log.d("sizewhat", "all exam size :"+allExamsList.size());
                        recyclerView.setAdapter(null);
                        adapter22 = new ExamListWithTabsAdapter(mContext, allExamsList, new ExamListWithTabsAdapter.EventListener() {
                            @Override
                            public void onCategoryClicked(String examId, Exam exam) {
                                gotoExam(exam);
                            }

                        },0);
                        recyclerView.setAdapter(adapter22);
                        if(allExamsList.size()==0){
                            showNoDataAlert(true);
                        }else {
                            showNoDataAlert(false);
                        }
                        break;

                    case 1:
                        Log.d("sizewhat", "weekly exam size :"+weeklyTestList.size());
                        recyclerView.setAdapter(null);
                        adapter22 = new ExamListWithTabsAdapter(mContext, weeklyTestList, new ExamListWithTabsAdapter.EventListener() {
                            @Override
                            public void onCategoryClicked(String examId, Exam exam) {
                                gotoExam(exam);
                            }
                        },1);
                        recyclerView.setAdapter(adapter22);
                        if(weeklyTestList.size()==0){
                            showNoDataAlert(true);
                        }else {
                            showNoDataAlert(false);
                        }
                        break;

                    case 2:
                        Log.d("sizewhat", "monthly exam size :"+monthlyTestList.size());
                        recyclerView.setAdapter(null);
                        adapter22 = new ExamListWithTabsAdapter(mContext, monthlyTestList, new ExamListWithTabsAdapter.EventListener() {
                            @Override
                            public void onCategoryClicked(String examId, Exam exam) {
                                gotoExam(exam);
                            }

                        },2);
                        recyclerView.setAdapter(adapter22);

                        if(monthlyTestList.size()==0){
                            showNoDataAlert(true);
                        }else {
                            showNoDataAlert(false);
                        }
                        break;

                    case 3:
                        Log.d("sizewhat", "monthly exam size :"+grandTestList.size());
                        recyclerView.setAdapter(null);
                        adapter22 = new ExamListWithTabsAdapter(mContext, grandTestList, new ExamListWithTabsAdapter.EventListener() {
                            @Override
                            public void onCategoryClicked(String examId, Exam exam) {
                                gotoExam(exam);
                            }

                        },2);
                        recyclerView.setAdapter(adapter22);

                        if(grandTestList.size()==0){
                            showNoDataAlert(true);
                        }else {
                            showNoDataAlert(false);
                        }
                        break;
                    case 4:
                        Log.d("sizewhat", "monthly exam size :"+subjectwiseTestList.size());
                        recyclerView.setAdapter(null);
                        adapter22 = new ExamListWithTabsAdapter(mContext, subjectwiseTestList, new ExamListWithTabsAdapter.EventListener() {
                            @Override
                            public void onCategoryClicked(String examId,Exam exam) {
                                gotoExam(exam);
                            }

                        },2);
                        recyclerView.setAdapter(adapter22);

                        if(subjectwiseTestList.size()==0){
                            showNoDataAlert(true);
                        }else {
                            showNoDataAlert(false);
                        }
                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    public void showNoDataAlert(boolean stat){
        if(stat){
            layoutNoDataAlert.setVisibility(View.VISIBLE);
        }else {
            layoutNoDataAlert.setVisibility(View.GONE);
        }
    }


}
