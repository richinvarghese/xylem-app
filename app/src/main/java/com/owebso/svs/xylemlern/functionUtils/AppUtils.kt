package com.owebso.svs.xylemlern.functionUtils

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson




class AppUtils(context: Context) {
    var mContext=context

    fun toast(message: String){
     Toast.makeText(mContext,message,Toast.LENGTH_SHORT).show()
    }
}