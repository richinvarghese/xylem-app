package com.owebso.svs.xylemlern.models;

public class ChoiceOption {
    private String optionId;
    private String option;

    public ChoiceOption(String optionId, String option) {
        this.optionId = optionId;
        this.option = option;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
}
