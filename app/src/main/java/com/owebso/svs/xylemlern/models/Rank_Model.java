package com.owebso.svs.xylemlern.models;

public class Rank_Model {
    String attend_id;
    String exam_id;
    String registration_id;
    String attended_data;
    String scoreboard_data;
    String marked_question_data;
    String mark_optained;
    String total_questions;
    String answered;
    String right_answer;
    String wrong_answer;
    String skipped;
    String points;
    String negative_points;
    String total_score;
    String last_index;
    String status;
    String published;
    String start_time;
    String end_time;
    String created_at;
    String updated_at;
    String stname;
    public Rank_Model(String attend_id, String exam_id, String registration_id, String attended_data,
                      String scoreboard_data, String marked_question_data,String mark_optained,String total_questions,
                      String answered,String right_answer,String wrong_answer,String skipped,String points,
                      String negative_points,String total_score,String last_index,String status ,String published,
                      String start_time,String end_time,String created_at,String updated_at,String stname) {
        this.attend_id = attend_id;
        this.exam_id = exam_id;
        this.registration_id = registration_id;
        this.attended_data = attended_data;
        this.scoreboard_data = scoreboard_data;
        this.marked_question_data = marked_question_data;
        this.mark_optained = mark_optained;
        this.total_questions = total_questions;
        this.answered = answered;
        this.right_answer = right_answer;
        this.wrong_answer = wrong_answer;
        this.skipped = skipped;
        this.points = points;
        this.negative_points = negative_points;
        this.total_score = total_score;
        this.last_index = last_index;
        this.status = status;
        this.published = published;
        this.start_time = start_time;
        this.end_time = end_time;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.stname = stname;
    }

    public String getAttend_id() {
        return attend_id;
    }

    public void setAttend_id(String attend_id) {
        this.attend_id = attend_id;
    }

    public String getExam_id() {
        return exam_id;
    }

    public void setExam_id(String exam_id) {
        this.exam_id = exam_id;
    }

    public String getRegistration_id() {
        return registration_id;
    }

    public void setRegistration_id(String registration_id) {
        this.registration_id = registration_id;
    }

    public String getAttended_data() {
        return attended_data;
    }

    public void setAttended_data(String attended_data) {
        this.attended_data = attended_data;
    }

    public String getScoreboard_data() {
        return scoreboard_data;
    }

    public void setScoreboard_data(String scoreboard_data) {
        this.scoreboard_data = scoreboard_data;
    }

    public String getMarked_question_data() {
        return marked_question_data;
    }

    public void setMarked_question_data(String marked_question_data) {
        this.marked_question_data = marked_question_data;
    }

    public String getMark_optained() {
        return mark_optained;
    }

    public void setMark_optained(String mark_optained) {
        this.mark_optained = mark_optained;
    }

    public String getTotal_questions() {
        return total_questions;
    }

    public void setTotal_questions(String total_questions) {
        this.total_questions = total_questions;
    }

    public String getAnswered() {
        return answered;
    }

    public void setAnswered(String answered) {
        this.answered = answered;
    }

    public String getRight_answer() {
        return right_answer;
    }

    public void setRight_answer(String right_answer) {
        this.right_answer = right_answer;
    }

    public String getWrong_answer() {
        return wrong_answer;
    }

    public void setWrong_answer(String wrong_answer) {
        this.wrong_answer = wrong_answer;
    }

    public String getSkipped() {
        return skipped;
    }

    public void setSkipped(String skipped) {
        this.skipped = skipped;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getNegative_points() {
        return negative_points;
    }

    public void setNegative_points(String negative_points) {
        this.negative_points = negative_points;
    }

    public String getTotal_score() {
        return total_score;
    }

    public void setTotal_score(String total_score) {
        this.total_score = total_score;
    }

    public String getLast_index() {
        return last_index;
    }

    public void setLast_index(String last_index) {
        this.last_index = last_index;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getStname() {
        return stname;
    }

    public void setStname(String stname) {
        this.stname = stname;
    }
}

