package com.owebso.svs.xylemlern.profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.functionUtils.WebServiceApi;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ProfileViewActivity extends AppCompatActivity {

    UserSession userSession;
    ProfileUtils profileUtils;
    Context mContext=ProfileViewActivity.this;
    @BindView(R.id.tvEmail) TextView tvEmail;
    @BindView(R.id.tvMobileNumber) TextView tvMobileNumber;
    @BindView(R.id.tvSchoolName) TextView tvSchoolName;
    @BindView(R.id.tvScheme) TextView tvScheme;
    @BindView(R.id.tvCurrentPlan) TextView tvCurrentPlan;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvPlace) TextView tvPlace;
    @BindView(R.id.tvUpgradeAlertLink) TextView tvUpgradeAlertLink;
    @BindView(R.id.profile_image) CircleImageView profile_image;
    @BindView(R.id.nestedScrollView) NestedScrollView nestedScrollView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_view);
        ButterKnife.bind(this);
        hideActionBar();
        showLoading(false);
        initViews();
        getProfileDetails();
    }
    private void initViews() {
        profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();
    }
    public void showLoading(boolean stat){
        LinearLayout loadingLayout = (LinearLayout)findViewById(R.id.loadingLayout);
        if(stat){
            loadingLayout.setVisibility(View.VISIBLE);
            nestedScrollView.setVisibility(View.INVISIBLE);
        }else {
            loadingLayout.setVisibility(View.GONE);
            nestedScrollView.setVisibility(View.VISIBLE);
        }
    }


    public void hideActionBar(){
        try{
            getSupportActionBar().hide();
        }catch (Exception e){
            try{
                getActionBar().hide();
            }catch (Exception e1){
                e1.printStackTrace();
            }

        }
    }


    public void getProfileDetails(){
        showLoading(true);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ConfigServer.API_GET_USER_DETAILS)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
            WebServiceApi webServiceApi = retrofit.create(WebServiceApi.class);
            Call<String> call = webServiceApi.getProfileDetauls(ConfigServer.API_HASH,
                    userSession.getUSER_SESSION_ID());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    showLoading(false);
                    try{
                        JSONObject SERVER_RESPONSE = new JSONObject(response.body());
                        JSONObject profile = SERVER_RESPONSE.getJSONObject("response");
                        UserProfile profile1 = new UserProfile();
                        String registration_id = profile.getString("registration_id");
                        String login_id = profile.getString("login_id");
                        String register_no = profile.getString("register_no");
                        String name = profile.getString("name");
                        String year = profile.getString("year");
                        String category = profile.getString("category");
                        String school_name = profile.getString("school_name");
                        String address = profile.getString("address");
                        String registration_status = profile.getString("registration_status");
                        String paid = profile.getString("paid");
                        String expiry_date = profile.getString("expiry_date");
                        String created_at = profile.getString("created_at");
                        String updated_at = profile.getString("updated_at");
                        String user_name = profile.getString("user_name");
                        String email = profile.getString("email");
                        String phone = profile.getString("phone");
                        String role = profile.getString("role");
                        String status = profile.getString("status");
                        String permissions = profile.getString("permissions");
                        String tocken = profile.getString("tocken");
                        String last_ip = profile.getString("last_ip");
                        String is_verify = profile.getString("is_verify");
                        tvName.setText(name);
                        tvEmail.setText(email);
                        tvMobileNumber.setText(phone);
                        tvSchoolName.setText(school_name);
                        tvPlace.setText(address);
                        if(paid.equals("1")){
                            tvCurrentPlan.setText("Paid");
                            tvUpgradeAlertLink.setVisibility(View.GONE);
                        }else if(paid.equals("2")){
                            tvCurrentPlan.setText("Free");
                            tvUpgradeAlertLink.setVisibility(View.VISIBLE);
                        }
                    }catch (JSONException e){
                    }
                }
                @Override
                public void onFailure(Call<String> call, Throwable t) {}
            });
        }
}
