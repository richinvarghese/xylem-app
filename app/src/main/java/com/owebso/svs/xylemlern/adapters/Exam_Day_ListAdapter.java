package com.owebso.svs.xylemlern.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.models.Days_Model;

import java.util.ArrayList;

/**
 * Created by Sarath on 07/06/19.
 */

public class Exam_Day_ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<Days_Model> daysList;
    EventListener listener;

    public Exam_Day_ListAdapter(Context mContext, ArrayList<Days_Model> daysList, EventListener listener) {
        this.mContext = mContext;
        this.daysList = daysList;
        this.listener =listener;
        setHasStableIds(true);
    }

    public interface EventListener {
        void onCategoryClicked(String categoryId, String parentId);
    }
    public Exam_Day_ListAdapter(EventListener listener){
        this.listener =listener;
    }

    public  class MainView extends RecyclerView.ViewHolder {
        TextView textViewTitle,cat_id,textViewDescription;
        ImageView thumbnailCat;
        CardView cardClickView;
        LinearLayout layout123;
        public MainView(View v) {
            super(v);
            this.cat_id = (TextView) v.findViewById(R.id.id);
            this.textViewTitle = (TextView) v.findViewById(R.id.textViewTitle);
            this.textViewDescription = (TextView) v.findViewById(R.id.textViewDescription);
            this.thumbnailCat = (ImageView) v.findViewById(R.id.thumbnailCat);
            this.cardClickView = (CardView) v.findViewById(R.id.cardClickView);
            this.layout123 = (LinearLayout) v.findViewById(R.id.layout123);

            this.layout123.setVisibility(View.VISIBLE);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  String catId = ((TextView) v.findViewById(R.id.id)).getText().toString();

                    if(listener != null){
                      //  listener.onEvent(Integer.parseInt(catId));
                        //listener.onCategoryClicked(cat);
                    }else {
                     //   VideosCategoryAdapter (LD)
                        Toast.makeText(mContext, "NULL", Toast.LENGTH_SHORT).show();
                    }

                }
            });


        }
    }

    public class SportAudView extends  RecyclerView.ViewHolder {
        public SportAudView(View v) {
            super(v);
        }
    }

    public  class AdsView extends  RecyclerView.ViewHolder {
        public AdsView(View v) {
            super(v);
        }
    }

    public void showImage(String IMAGE_URL, ImageView imageView){

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_cash);
        requestOptions.error(R.drawable.ic_cash);

        Glide.with(mContext)
                .applyDefaultRequestOptions(requestOptions)
                .load(IMAGE_URL)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        int i=0;
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        return false;
                    }
                }).into(imageView)
        ;

    }

    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_layout_days, parent, false);
            return new MainView(itemView
            );




    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Days_Model catObj = daysList.get(position);
        if(holder.getItemViewType()==1)
        {

            ((MainView)holder).cardClickView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCategoryClicked(catObj.getCategory_id(),catObj.getCategory_id());
                }
            });


          //  ((MainView)holder).cat_id.setText(catObj.getCategory_id());
            ((MainView)holder).textViewTitle.setText(catObj.getName());
           // ((MainView)holder).textViewDescription.setText(catObj.getDescription());
            Log.d("IMAGE_URL23", "URL : "+catObj.getImage());

            showImage(catObj.getImage(),((MainView)holder).thumbnailCat);

        }
        else if(holder.getItemViewType()==2)
        {

        }
        else {

            final AdsView third_holder = (AdsView)holder;
            //third_holder.third_categoryList_tv.setText(catObj.getName());
        }

    }




    @Override
    public int getItemViewType(int position) {
        return daysList.get(position).getCAT_TYPE();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return daysList.size();
    }
}
