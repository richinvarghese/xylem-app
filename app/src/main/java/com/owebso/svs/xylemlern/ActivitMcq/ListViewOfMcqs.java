package com.owebso.svs.xylemlern.ActivitMcq;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.adapters.CategoryAdapter22;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.functionUtils.ZonStringFunctions;
import com.owebso.svs.xylemlern.models.Category;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ListViewOfMcqs extends AppCompatActivity {

    RecyclerView recyclerView;
    Context mContext = ListViewOfMcqs.this;
    private String API_RESPONSE_GET_CATEGORY="";
    ArrayList<Category> categoryList;
    ArrayList<Category> firstYearCatList;
    ArrayList<Category> secondYearCatList;
    ArrayList<String> moduleList;
    boolean isModule=false;
    CategoryAdapter22 adapter22;

    String CURRENT_MODULE_ID="";
    boolean isModuleTitleSet=false;
    String LIST_OF_MODULES="";
    UserSession userSession;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_mcq_list_view_with_tabs);

        initActionBar();
        changeStatusBarColor();
        initViews();

        LIST_OF_MODULES = getIntent().getStringExtra("MODULE_LIST");

        Log.d("1235636", "LIST_OF_MODULES : "+LIST_OF_MODULES);

        setupTabLayout();
        //getSupportActionBar().hide();
        getCategories();
    }


    public void changeStatusBarColor(){
        Window window = ListViewOfMcqs.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(mContext,R.color.accentColor1));
    }
    public  void initActionBar(){
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.accentColor1);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent =(Toolbar) actionBarLayout.getParent();
       // parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
      //  parent.setContentInsetsAbsolute(0,0);
        TextView actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("MCQ");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    public void initViews(){

        ProfileUtils profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);
        moduleList = new ArrayList<>();

    }

    public void getCategories(){

        categoryList = new ArrayList<>();
        firstYearCatList = new ArrayList<>();
        secondYearCatList = new ArrayList<>();

        categoryList.clear();
        firstYearCatList.clear();
        secondYearCatList.clear();

        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("parent_id", ZonStringFunctions.stripCommasFromString(LIST_OF_MODULES));
        Log.d("LIST_OF_MODULES", "LIST_OF_MODULES : "+LIST_OF_MODULES);
        post_data.put("section","mcq");
        post_data.put("login_id",userSession.getUSER_SESSION_ID());

        //Log.d("CATEGORY_ID", "ID: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_CATEGORY_IN;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CATEGORY = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.d("API_RESPONSE", ""+API_RESPONSE_GET_CATEGORY);
                        //Log.d("API_RESPONSE", "CAT_ID="+CATEGORY_ID);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
                            String status = jsonObject1.getString("status");
                           // String video_exist = jsonObject1.getString("video_exist");

                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject catObj = jsonArray.getJSONObject(i);
                                String category_id = catObj.getString("category_id");
                                String is_module = catObj.getString("is_module");
                                String category_name = catObj.getString("category_name");
                                String description = catObj.getString("description");
                                String parent_id = catObj.getString("parent_id");

                                String category_section = catObj.getString("category_section");
                                String category_status = catObj.getString("category_status");
                                String category_image = catObj.getString("category_image");
                                String created_at = catObj.getString("created_at");
                                String year = catObj.getString("year");

                                category_image = ConfigServer.PATH_CATEGORY_THUMB+category_image;


                                Log.d("tesqwewq", "PARENT : "+parent_id);

                                if(!CURRENT_MODULE_ID.equals(parent_id)){
                                    CURRENT_MODULE_ID =parent_id;
                                    if(!isModuleTitleSet){
                                        Category category1 = new Category(i,category_id,category_name,description,parent_id,category_section,category_status,category_image,created_at,0);
                                        category1.setYear("0");
                                        categoryList.add(category1);
                                    }
                                    Category category2 = new Category(i,category_id,category_name,description,parent_id,category_section,category_status,category_image,created_at,1);
                                    category2.setYear(year);
                                    categoryList.add(category2);
                                    isModuleTitleSet=true;
                                }else {
                                    isModuleTitleSet=false;
                                    Category category3 = new Category(i,category_id,category_name,description,parent_id,category_section,category_status,category_image,created_at,1);
                                    category3.setYear(year);
                                    categoryList.add(category3);
                                }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                       // getSortedList(); //CREATING SORTED LIST
                        getnerateYearLists();

                        recyclerView.setAdapter(null);
                        adapter22 = new CategoryAdapter22(mContext, categoryList, new CategoryAdapter22.EventListener() {
                            @Override
                            public void onCategoryClicked(String categoryId, String parentId) {
                                gotoCategory(categoryId);
                            }
                        },0);
                        Log.d("fdked", "list size: "+categoryList.size());
                        Log.d("fdked", "LIST_OF_MODULES "+LIST_OF_MODULES);


                        recyclerView.setAdapter(adapter22);

                    }
                });
            }
        };

        new Thread(runnable).start();
    }

public void gotoCategory(String categoryId){

    Intent in = new Intent(mContext, LastSubCategoryMcqLists.class);
    in.putExtra("CATEGORY_ID",categoryId);
    startActivity(in);
    //Toast.makeText(mContext, ""+categoryId, Toast.LENGTH_SHORT).show();
}

    public void getnerateYearLists(){

        firstYearCatList.clear();
        secondYearCatList.clear();

        for(int i=0;i<categoryList.size();i++){
            if(categoryList.get(i).getYear().equals("1")){
                firstYearCatList.add(categoryList.get(i));
            }else if(categoryList.get(i).getYear().equals("2")){
                secondYearCatList.add(categoryList.get(i));
            }else {

                firstYearCatList.add(categoryList.get(i));
                secondYearCatList.add(categoryList.get(i));
            }
        }

        Log.d("TESTSUT7", "firstYearCatList SIZE: "+firstYearCatList.size());
        Log.d("TESTSUT7", "secondYearCatList : SIZE "+secondYearCatList.size());



        getTrimList();
    }

    public void getTrimList(){
      //  ArrayList<Category> cat123 = new ArrayList<>();

        for(int i=0;i<firstYearCatList.size();i++){
            try{
                if(firstYearCatList.get(i).getYear().equals("0") && firstYearCatList.get(i+1).getYear().equals("0")){
                    firstYearCatList.remove(i);
                    firstYearCatList.remove(i+1);
                }
            }catch (IndexOutOfBoundsException e){
                firstYearCatList.remove(i);
            }

        }
        for(int i=0;i<secondYearCatList.size();i++){
            try {
                if (secondYearCatList.get(i).getYear().equals("0") && secondYearCatList.get(i + 1).getYear().equals("0")) {
                    secondYearCatList.remove(i);
                    secondYearCatList.remove(i + 1);
                }
            }catch (IndexOutOfBoundsException e){
                secondYearCatList.remove(i);
            }
        }

        try{

        }catch (ArrayIndexOutOfBoundsException  e){
            if(secondYearCatList.get(secondYearCatList.size()-1).getYear().equals("0")){
                secondYearCatList.remove(secondYearCatList.size());
            }
        }

    }

    public void setupTabLayout() {
        TabLayout mTabLayout = (TabLayout) findViewById(R.id.tabs);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()){
                    case 0:
                        recyclerView.setAdapter(null);
                        adapter22 = new CategoryAdapter22(mContext, categoryList, new CategoryAdapter22.EventListener() {
                            @Override
                            public void onCategoryClicked(String categoryId, String parentId) {
                                gotoCategory(categoryId);
                            }
                        },0);
                        recyclerView.setAdapter(adapter22);
                        break;

                        case 1:
                            recyclerView.setAdapter(null);
                            adapter22 = new CategoryAdapter22(mContext, firstYearCatList, new CategoryAdapter22.EventListener() {
                                @Override
                                public void onCategoryClicked(String categoryId, String parentId) {
                                    gotoCategory(categoryId);
                                }
                            },1);
                            recyclerView.setAdapter(adapter22);
                        break;

                        case 2:
                            recyclerView.setAdapter(null);
                            adapter22 = new CategoryAdapter22(mContext, secondYearCatList, new CategoryAdapter22.EventListener() {
                                @Override
                                public void onCategoryClicked(String categoryId, String parentId) {
                                    gotoCategory(categoryId);
                                }
                            },2);
                            recyclerView.setAdapter(adapter22);
                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void onTabSelected1(int pos) {
        switch (pos) {
            case 0:
                Toast.makeText(this, "Tapped " + pos, Toast.LENGTH_SHORT);
                break;
            default:
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();

    }
}
