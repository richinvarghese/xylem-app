package com.owebso.svs.xylemlern.login;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.owebso.svs.xylemlern.MainActivity;
import com.owebso.svs.xylemlern.R;
public class VerifyAccount extends AppCompatActivity {

    Context mContext =VerifyAccount.this ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifiy_account);
        initViews();
    }
    public void initViews(){
        Intent in = new Intent(mContext, MainActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(in);
        finish();

    }

}
