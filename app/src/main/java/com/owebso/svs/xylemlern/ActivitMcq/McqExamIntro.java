package com.owebso.svs.xylemlern.ActivitMcq;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.functionUtils.WebServiceApi;
import com.owebso.svs.xylemlern.models.McqAttendedData;
import com.owebso.svs.xylemlern.models.McqQuestion;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class McqExamIntro extends AppCompatActivity {

    String CATEGORY_ID="",API_RESPONSE="",TITLE="";
    ArrayList<McqQuestion> questionArrayList;
    LinearLayout loadingLayout;
    LinearLayout fullLayout;
    Context mContext =  McqExamIntro.this;
    TextView textViewCount;
    Button buttonProceed;
    TextView textViewTitle;
    WebServiceApi webServiceApi;
    ProfileUtils profileUtils;
    UserSession userSession;
    int GOTO_ACTIITY_NO=1;
    TextView actionBarTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcq_exam_intro);


        CATEGORY_ID = getIntent().getStringExtra("CATEGORY_ID");
        TITLE = getIntent().getStringExtra("TITLE");

        Log.d("DJFHEKH", " ID IDIDID "+CATEGORY_ID);


        initActionBar();
        changeStatusBarColor();

        initViews();

        textViewTitle.setText(TITLE);
        actionBarTitle.setText(TITLE.toUpperCase());

        checkIfMcqAlreadyAttended();

    }

    private void initViews() {

        profileUtils = new ProfileUtils(mContext);
        userSession = profileUtils.getUserSession();

        buttonProceed = (Button)findViewById(R.id.buttonProceed);
        loadingLayout = (LinearLayout)findViewById(R.id.loadingLayout);
        fullLayout = (LinearLayout)findViewById(R.id.fullLayout);
        textViewCount = (TextView)findViewById(R.id.textViewCount);
        textViewTitle = (TextView)findViewById(R.id.textViewTitle);
        getQuestionListCount();

        buttonProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //   Toast.makeText(mContext, "hello", Toast.LENGTH_SHORT).show();

                if(GOTO_ACTIITY_NO==1 || GOTO_ACTIITY_NO==2){
                    Intent in = new Intent(mContext,McqExams.class);
                    in.putExtra("CATEGORY_ID",CATEGORY_ID);
                    startActivity(in);
                    finish();
                }else {
                    Intent in = new Intent(mContext,ReviewMcq.class);
                    in.putExtra("CATEGORY_ID",CATEGORY_ID);
                    startActivity(in);
                    finish();
                }

            }
        });
    }

    public  void initActionBar(){
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.accentColor1);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent =(Toolbar) actionBarLayout.getParent();
//        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);

        actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("MCQ");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
    public void changeStatusBarColor(){
        Window window = McqExamIntro.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(McqExamIntro.this,R.color.accentColor1));
    }
    public void showLoading(boolean stat){
        if(stat){
            loadingLayout.setVisibility(View.VISIBLE);
            fullLayout.setVisibility(View.GONE);
        }else {
            loadingLayout.setVisibility(View.GONE);
            fullLayout.setVisibility(View.VISIBLE);
        }

    }
    public void getQuestionListCount(){
        showLoading(true);
        questionArrayList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("category_id",CATEGORY_ID);
        String URL = ConfigServer.API_GET_MCQ_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLog("API_RESPONSE",API_RESPONSE);
                        try{
                            //showLoading(false);
                            JSONObject jsonObject = new JSONObject(API_RESPONSE);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");

                            textViewCount.setText(""+jsonArray.length());


                        }catch (JSONException e){

                        }
                       // showLoading(false);
                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    private void showLog(String tag, String api_response1) {
        Log.d(tag ,api_response1);
    }

    public void checkIfMcqAlreadyAttended(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigServer.API_CHECK_MCQ_ATTENDANCE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        webServiceApi = retrofit.create(WebServiceApi.class);

        Call<String> call = webServiceApi.checkMcqAttended(ConfigServer.API_HASH,CATEGORY_ID,userSession.getUSER_SESSION_ID());

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d("djfhkef", "onResponse: CAT = "+CATEGORY_ID+" USERID :"+userSession.getUSER_SESSION_ID()+"   body ="+response.body());

                showLoading(false);

                try{
                    JSONObject jsonObject = new JSONObject(response.body());

                    int code = Integer.parseInt(jsonObject.getString("code"));

                    if(code==1){
                        JSONObject result = jsonObject.getJSONObject("result");
                        Gson gson = new Gson();
                        McqAttendedData mcqAttendedData = gson.fromJson(result.toString(),McqAttendedData.class);

                      //  Toast.makeText(McqExamIntro.this, ""+mcqAttendedData.getFinish(), Toast.LENGTH_SHORT).show();

                        switch (mcqAttendedData.getFinish()){
                            case ConfigServer.MCQ_STATUS_FRESH:
                                // I don't think this condition will ever occur :|
                                GOTO_ACTIITY_NO=1;
                                buttonProceed.setText("Solve");
                              //  Toast.makeText(McqExamIntro.this, "Fresh", Toast.LENGTH_SHORT).show();
                                break;
                            case ConfigServer.MCQ_STATUS_PAUSED:
                                GOTO_ACTIITY_NO=2;
                                buttonProceed.setText("Resume");
                             //   Toast.makeText(McqExamIntro.this, "Paused", Toast.LENGTH_SHORT).show();
                                break;
                            case ConfigServer.MCQ_STATUS_FINISHED:
                                GOTO_ACTIITY_NO=3;
                                buttonProceed.setText("Review");
                             //   Toast.makeText(McqExamIntro.this, "finished", Toast.LENGTH_SHORT).show();
                                break;
                                default:
                                    GOTO_ACTIITY_NO=1;
                        }

                    }else {

                    }

                }catch (JSONException je){
                    je.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(McqExamIntro.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("ejfdhek", "onFailure: "+t.getMessage());

            }
        });

    }


}
