package com.owebso.svs.xylemlern.models;

public class Slider_Model {
    String sid;
    String title;
    String category_id;
    String video_id;
    String description;
    String image;
    String video_path;

    public Slider_Model(String sid, String title, String category_id, String video_id,
                        String description, String image, String video_path) {
        this.sid = sid;
        this.title = title;
        this.category_id = category_id;
        this.category_id = category_id;
        this.video_id = video_id;
        this.description = description;
        this.image = image;
        this.video_path = video_path;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo_path() {
        return video_path;
    }

    public void setVideo_path(String video_path) {
        this.video_path = video_path;
    }
}
