package com.owebso.svs.xylemlern.adapters;

import android.app.Activity;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.models.Rank_Model;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Prafull Kumhar on.
 */

public class Rank_Adapter extends RecyclerView.Adapter<Rank_Adapter.MyViewHolder> {
    private Activity context;
    private View.OnClickListener onClickListener;
    private ArrayList<Rank_Model> rank_list;
    int points;
    int pos;
    public Rank_Adapter(Activity context, ArrayList<Rank_Model> rank_list) {
        this.context = context;
        this.rank_list = rank_list;
    }

    @Override
    public Rank_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_layout_rank_list, parent, false);
        return new Rank_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Rank_Adapter.MyViewHolder holder, int position) {


        if(position==0){
            pos = position+1;
            holder.tv_rank.setText(""+pos);
            points = Integer.parseInt(rank_list.get(position).getPoints());
        }else {
            if (points==Integer.parseInt(rank_list.get(position).getPoints())){
                holder.tv_rank.setText(""+pos);
            }else {
                pos = pos+1;
                holder.tv_rank.setText(""+pos);
                points = Integer.parseInt(rank_list.get(position).getPoints());
            }
        }

        holder.textViewname.setText(rank_list.get(position).getStname());
        if (Integer.parseInt(rank_list.get(position).getPoints())<=0){
            holder.textmark.setText("0");
        }else {
            holder.textmark.setText(rank_list.get(position).getPoints());
        }
        holder.tv_no_currect.setText(rank_list.get(position).getRight_answer());
        holder.tv_no_wrong.setText(rank_list.get(position).getWrong_answer());
        holder.tv_no_skipped.setText(rank_list.get(position).getSkipped());

    }

    @Override
    public int getItemCount() {
        return rank_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_rank,textViewname,textmark,tv_no_currect,tv_no_wrong,tv_no_skipped;
        private CircleImageView image;
        private TextView tv_title;


        public MyViewHolder(View view) {
            super(view);

            image = view.findViewById(R.id.image);
            tv_rank = view.findViewById(R.id.tv_rank);
            textViewname = view.findViewById(R.id.textViewname);
            textmark = view.findViewById(R.id.textmark);
            tv_no_currect = view.findViewById(R.id.tv_no_currect);
            tv_no_wrong = view.findViewById(R.id.tv_no_wrong);
            tv_no_skipped = view.findViewById(R.id.tv_no_skipped);


        }
    }
}
