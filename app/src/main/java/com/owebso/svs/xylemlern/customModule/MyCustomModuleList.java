package com.owebso.svs.xylemlern.customModule;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cunoraz.tagview.TagView;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.McqQuestion;
import com.owebso.svs.xylemlern.profile.ProfileUtils;
import com.owebso.svs.xylemlern.profile.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyCustomModuleList extends AppCompatActivity {
    TextView actionBarTitle;

    ArrayList<McqQuestion> questionArrayList;
    UserSession userSession;
    ProfileUtils profileUtils;
    String API_RESPONSE="";
    Context mContext = MyCustomModuleList.this;
    //@BindView(R.id.tag_group)TagView tagGroup;
    @BindView(R.id.textViewCount) TextView textViewCount;
    @BindView(R.id.tvCreateNew) TextView tvCreateNew;
    @BindView(R.id.layoutExists) CardView layoutExists;
    //@BindView(R.id.layoutCreateCm) CardView layoutCreateCm;
    @BindView(R.id.fullLayout) LinearLayout fullLayout;
    @BindView(R.id.layoutLoading) RelativeLayout layoutLoading;
    @BindView(R.id.buttonStartCM) Button buttonStartCM;
    TagView tagGroup;

    String EXAM_MODE = "mcq";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_custom_module_list);
        ButterKnife.bind(this);

        initActionBar();
        changeStatusBarColor();
        showLoading(false);

        init();
        showCustomModuleData();

    }

    public void init(){
        profileUtils = new ProfileUtils(getApplicationContext());
        userSession = profileUtils.getUserSession();
        tagGroup  = (TagView)findViewById(R.id.tag_group);

        tvCreateNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCreateNewCM(v);
            }
        });

        buttonStartCM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (EXAM_MODE){
                    case "exam":
                        Intent exam = new Intent(mContext,CmExam.class);
                        startActivity(exam);
                        finish();
                        break;
                    case "mcq":
                        Intent mcq = new Intent(mContext,CmMcq.class);
                        startActivity(mcq);
                        finish();
                        break;
                }
            }
        });
    }

    public void showCustomModuleData(){

        showLoading(false);
        getCustomModuleQuestions();
    }

    public void setTags(String[] tagsList){
        String[] temp = new String[tagsList.length];
        for(int i=0;i<tagsList.length;i++){
            temp[i]="#"+tagsList[i];
        }
        tagGroup.addTags(temp);
    }


    public void getCustomModuleQuestions(){
        showLoading(true);
        questionArrayList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("login_id",userSession.getUSER_SESSION_ID());
        String URL = ConfigServer.API_GET_CUSTOM_MODULE;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLog("API_RESPONSE45345",API_RESPONSE);
                        try{
                            JSONObject jsonObject = new JSONObject(API_RESPONSE);
                            int status = jsonObject.getInt("status");
                            int code = jsonObject.getInt("code");
                            if(status==200 && code==1){
                                JSONObject resultO = jsonObject.getJSONObject("result");
                                JSONArray questions = resultO.getJSONArray("questions");

                                String tags_list = jsonObject.getString("tags");
                                setTags(tags_list.split(","));

                                EXAM_MODE = resultO.getString("exam_mode");
                                showLoading(false);
                            }else if(status==404 && code==2){
                                Intent in = new Intent(mContext,SubjectListsCM.class);
                                startActivity(in);
                                finish();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(mContext, "Try Again Later", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    private void showLog(String name, String value) {
        Log.d(name,value);
    }



    public void showCreateNewCM(View view) {

        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.poupup_layout_alert_create_custom_module, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        Button buttonProceed2 = (Button) popupView.findViewById(R.id.buttonProceed2);

        buttonProceed2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(mContext,SubjectListsCM.class);
                startActivity(in);
                finish();
            }
        });

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }



    public void showLoading(boolean stat){
        if(stat){
            layoutLoading.setVisibility(View.VISIBLE);
           // fullLayout.setVisibility(View.GONE);
        }else {
            layoutLoading.setVisibility(View.GONE);
          // fullLayout.setVisibility(View.VISIBLE);
        }
    }

    public  void initActionBar(){
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.accentColor1);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent =(Toolbar) actionBarLayout.getParent();
//        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);

        actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("Customized Module");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
    public void changeStatusBarColor(){
        Window window = MyCustomModuleList.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(MyCustomModuleList.this,R.color.accentColor1));
    }

}
