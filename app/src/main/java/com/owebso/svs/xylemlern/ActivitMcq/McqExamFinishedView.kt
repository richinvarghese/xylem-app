package com.owebso.svs.xylemlern.ActivitMcq

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.iarcuschin.simpleratingbar.SimpleRatingBar
import com.owebso.svs.xylemlern.R
import com.owebso.svs.xylemlern.functionUtils.AppUtils
import com.owebso.svs.xylemlern.models.ResultModelMcq
import kotlinx.android.synthetic.main.activity_mcq_exam_finished_view.*
import java.text.DecimalFormat

class McqExamFinishedView : AppCompatActivity() {
    var progressBar6: ProgressBar? = null

    var TOTAL_NUM_QUESTIONS = ""
    var TOTAL_NUM_RIGHT_ANSWERS = ""
    var TOTAL_NUM_WRONG_ANSWERS = ""
    var TOTAL_NUM_SKIPPED_ANSWERS = ""
    var TOTAL_SCORE_FROM_CURRENT_EXAM = ""
    var EXAM_TOTAL_MARK = ""
    var simpleRatingBar: SimpleRatingBar? = null
    var CURRENT_MCQ_ID = ""
    var gson:Gson = Gson()
    lateinit var appUtils:AppUtils
    lateinit var mcqResultModelMcq:ResultModelMcq
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mcq_exam_finished_view)
        appUtils = AppUtils(applicationContext)

        val MCQ_RESULT_DATA = intent.getStringExtra("MCQ_RESULT_DATA")
        mcqResultModelMcq = gson.fromJson(MCQ_RESULT_DATA,ResultModelMcq::class.java)
        getData()

        initialize()


    }

    fun initialize(){
        appUtils = AppUtils(applicationContext)
        initActionBar()
        initViews()
        changeStatusBarColor()
       // intentValues
        setValues()
    }

    fun initActionBar() {
        val actionBarLayout = layoutInflater.inflate(
                R.layout.action_bar_04,
                null) as ViewGroup
        // Set up your ActionBar
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(false)
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.customView = actionBarLayout
        // You customization
        val actionBarColor = resources.getColor(R.color.accentColor1)
        actionBar.setBackgroundDrawable(ColorDrawable(actionBarColor))
        val parent = actionBarLayout.parent as Toolbar
        //        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);
        val actionBarTitle = findViewById<View>(R.id.actionBarTitle) as TextView
        actionBarTitle.text = "MCQ"
        val togBackButton = findViewById<View>(R.id.togBackButton) as ImageView
        togBackButton.setOnClickListener { finish() }
    }

    fun changeStatusBarColor() {
        val window = this@McqExamFinishedView.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@McqExamFinishedView, R.color.accentColor1)
    }

    private fun getData(){
        CURRENT_MCQ_ID = mcqResultModelMcq.CATEGORY_ID
        TOTAL_NUM_WRONG_ANSWERS = mcqResultModelMcq.TOTAL_NUM_WRONG_ANSWERS.toString()
        TOTAL_NUM_RIGHT_ANSWERS = mcqResultModelMcq.TOTAL_NUM_RIGHT_ANSWERS.toString()
        TOTAL_NUM_QUESTIONS = mcqResultModelMcq.TOTAL_NUM_QUESTIONS.toString()
        TOTAL_NUM_SKIPPED_ANSWERS = mcqResultModelMcq.TOTAL_NUM_SKIPPED_ANSWERS.toString()
        TOTAL_SCORE_FROM_CURRENT_EXAM = mcqResultModelMcq.TOTAL_SCORE_FROM_CURRENT_EXAM.toString()
        EXAM_TOTAL_MARK = mcqResultModelMcq.TOTAL_QBANK_SCORE.toString()
    }

    private fun setValues() {
        val COUNT_ANSWERED = TOTAL_NUM_RIGHT_ANSWERS.toInt() + TOTAL_NUM_WRONG_ANSWERS.toInt()
        //  Log.d("dkjfgewk", "setValues: COUNT_ANSWERED : " +COUNT_ANSWERED);
        tvTotalQuestions!!.text = TOTAL_NUM_QUESTIONS
        tvAnswered!!.text = "" + COUNT_ANSWERED
        tvRightAnswer!!.text = TOTAL_NUM_RIGHT_ANSWERS
        tvWrongAnswers!!.text = TOTAL_NUM_WRONG_ANSWERS
        tvSkipped!!.text = TOTAL_NUM_SKIPPED_ANSWERS
        tvPoints!!.text = ""
        tvTotalScore!!.text = EXAM_TOTAL_MARK
        tvYourScore!!.text = TOTAL_SCORE_FROM_CURRENT_EXAM
        bv_tv_Correct!!.text = addzero(TOTAL_NUM_RIGHT_ANSWERS.toInt())
        bv_tv_Skipped!!.text = addzero(TOTAL_NUM_SKIPPED_ANSWERS.toInt())
        bv_tv_InCorrect!!.text = addzero(TOTAL_NUM_WRONG_ANSWERS.toInt())
        val right_ans_count = TOTAL_NUM_RIGHT_ANSWERS.toInt()
        val accuracyD = TOTAL_NUM_RIGHT_ANSWERS.toDouble() / ("" + COUNT_ANSWERED).toDouble()
        val percentage_accuracy = accuracyD * 100
        Log.d("accuracy123", "right_ans_count : $right_ans_count")
        Log.d("accuracy123", "COUNT_ANSWERED : $COUNT_ANSWERED")
        Log.d("accuracy123", "accuracyD : $percentage_accuracy %")
      //  tvAccuracy = findViewById<View>(R.id.tvAccuracy) as TextView
        tvAccuracy!!.text = DecimalFormat("###").format(percentage_accuracy) + "%"
    }

    fun addzero(number: Int): String {
        return if (number <= 9) "0$number" else number.toString()
    }

    fun initViews() { // progressBar6 = (ProgressBar)findViewById(R.id.progressBar6);
        simpleRatingBar = findViewById<View>(R.id.simpleRatingBar) as SimpleRatingBar
//        tvTotalQuestions = findViewById<View>(R.id.tvTotalQuestions) as TextView
//        tvAnswered = findViewById<View>(R.id.tvAnswered) as TextView
//        tvSkipped = findViewById<View>(R.id.tvSkipped) as TextView
//        tvTotalScore = findViewById<View>(R.id.tvTotalScore) as TextView
//        tvYourScore = findViewById<View>(R.id.tvYourScore) as TextView
//        tvRightAnswer = findViewById<View>(R.id.tvRightAnswer) as TextView
//        tvWrongAnswers = findViewById<View>(R.id.tvWrongAnswers) as TextView
//        tvPoints = findViewById<View>(R.id.tvPoints) as TextView
//        bv_tv_Correct = findViewById<View>(R.id.bv_tv_Correct) as TextView
//        bv_tv_Skipped = findViewById<View>(R.id.bv_tv_Skipped) as TextView
//        bv_tv_InCorrect = findViewById<View>(R.id.bv_tv_InCorrect) as TextView
//        buttonReviewMcq = findViewById<View>(R.id.buttonReviewMcq) as Button
        buttonReviewMcq!!.setOnClickListener {
            val `in` = Intent(this@McqExamFinishedView, ReviewMcq::class.java)
            `in`.putExtra("CATEGORY_ID", CURRENT_MCQ_ID)
            startActivity(`in`)
            finish()
        }
        simpleRatingBar!!.setOnRatingBarChangeListener { simpleRatingBar, rating, fromUser -> Toast.makeText(this@McqExamFinishedView, "" + rating, Toast.LENGTH_SHORT).show() }
    }

    companion object {
        private const val TAG = "sjdfehw"
    }
}