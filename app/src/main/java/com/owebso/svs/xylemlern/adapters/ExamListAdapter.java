package com.owebso.svs.xylemlern.adapters;

import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.models.Exam;

import java.util.ArrayList;
/**
 * Created by Sarath on 07/06/19.
 */

public class ExamListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<Exam> examList;
    private EventListener listener;

    public ExamListAdapter(Context mContext, ArrayList<Exam> examList,EventListener listener) {
        this.mContext = mContext;
        this.examList = examList;
        this.listener =listener;
        setHasStableIds(true);
    }

    public interface EventListener {
        void onExamClicked(Exam exam);
    }
    public ExamListAdapter(EventListener listener){
        this.listener =listener;
    }

    public  class MainView extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        ImageView thumbnailCat;
        CardView cardClickView;
        public MainView(View v) {
            super(v);
            this.cardClickView = (CardView)v.findViewById(R.id.cardClickView);
            this.textViewTitle = (TextView) v.findViewById(R.id.textViewTitle);

        }
    }
    
    public  class AdsView extends  RecyclerView.ViewHolder {
        public AdsView(View v) {
            super(v);
        }
    }


    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
        if(viewType==1)
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_layout_exam_card, parent, false);
            return new MainView(itemView);
        }

        else
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_layout_exam_card, parent, false);
            return new AdsView(itemView);
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Exam examobj = examList.get(position);
        if(holder.getItemViewType()==1)
        {

            ((MainView)holder).cardClickView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onExamClicked(examobj);
                }
            });

            ((MainView)holder).textViewTitle.setText(examobj.getExam_title());


        }


    }




    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return examList.size();
    }
}
