package com.owebso.svs.xylemlern.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.owebso.svs.xylemlern.MainActivity
import com.owebso.svs.xylemlern.R

import org.json.JSONObject
import java.util.*

class MyFirebaseMessagingService : FirebaseMessagingService() {
    val TAG = "Service"
    var jsonMsgData = JSONObject()
    lateinit var intent: Intent
    var gson = Gson()
    companion object {

     //   private lateinit var badgeListener: BadgeListener
       // fun setBadgeListener(listener: BadgeListener) {
       //     badgeListener = listener
      //  }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }



    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.e("slkdjlhkf1",remoteMessage.data.toString())
        Log.e("slkdjlhkf2",remoteMessage.notification?.body.toString())
        Log.e("slkdjlhkf3",remoteMessage.ttl.toString())

        try {// Handle FCM messages here.


             remoteMessage.data.toString()

         //   var fbNotification = gson.fromJson(remoteMessage.data.toString(), FbNotification::class.java)

            Log.e("slkdjlhkf",remoteMessage.data.toString())

            intent = Intent(applicationContext, MainActivity::class.java)



//            if (remoteMessage.data.get("info") != null) {
//                try {
//                    jsonMsgData = JSONObject(remoteMessage.data.get("info"))
//                    var msg = jsonMsgData["type"]
//                    Log.e(TAG, "Type : " + msg)
//
//                    if (msg == "AdminMessage") {
//                        intent.putExtra("NextPage", "Message")
//                    } else if (msg == "Property_assigned") {
//                        intent.putExtra("NextPage", "Assigned_Properties")
//                    } else {
//                        intent.putExtra("NextPage", "")
//                    }
//                } catch (e: Exception) {
//                    Log.e("Here is Exception  :: ", e.message);
//                }
//            }

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val NOTIFICATION_CHANNEL_ID = "My_Channel"

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notificationChannel =
                    NotificationChannel(
                        NOTIFICATION_CHANNEL_ID,
                        "User Notifications",
                        NotificationManager.IMPORTANCE_HIGH
                    )

                notificationChannel.description = "Description"
                notificationChannel.enableLights(true)
                notificationChannel.lightColor = Color.RED
                notificationChannel.vibrationPattern = longArrayOf(0, 1000, 500, 1000)
                notificationChannel.enableVibration(true)
                notificationManager.createNotificationChannel(notificationChannel)
            }

            // to diaplay notification in DND Mode
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel =
                    notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID)
                channel.canBypassDnd()
            }

            val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            val pendingIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT)
            //val count = AppPreferences.getIntValue(AppContants.BADGECOUNT, 0) + 1
            //AppPreferences.setIntValue(AppContants.BADGECOUNT, count)
//            if (badgeListener != null) {
//                 runOnUiThread(Runnable {
//                badgeListener.countIncrement(count)
//                 })
//            }
            val notificationId = Random().nextInt(60000)
            notificationBuilder
                //.setContentIntent(resultPendingIntent)
                .setContentTitle("Xylem")
                .setStyle(NotificationCompat.BigTextStyle()
                    .bigText(remoteMessage.notification?.body.toString()))
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.app_icon_100_white)
                .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)

            notificationManager.notify(notificationId, notificationBuilder.build())
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Exception here :: ", e.message);
        }
    }

}
