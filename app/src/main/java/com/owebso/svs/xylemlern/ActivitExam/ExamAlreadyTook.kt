package com.owebso.svs.xylemlern.ActivitExam

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.owebso.svs.xylemlern.R
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer
import com.owebso.svs.xylemlern.functionUtils.DateTimeConverter
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions
import com.owebso.svs.xylemlern.profile.ProfileUtils
import com.owebso.svs.xylemlern.profile.UserSession
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ExamAlreadyTook : AppCompatActivity() {
    var layoutNoResult: LinearLayout? = null
    var layoutResultPublished: LinearLayout? = null
    var EXAM_ID = ""
    var mContext: Context = this@ExamAlreadyTook
    var API_RESPONSE = ""
    var API_RESPONSE_GET_CATEGORY = ""
    var API_RESPONSE_GET_EXAM_PUBLISH_CHECK = ""
    var textViewResultMsg: TextView? = null
    var tvExamTitle: TextView? = null
    var tvDateExam: TextView? = null
    var tvTimeExam: TextView? = null
    var API_RESPONSE_CHECK_EXAM_ATT = ""
    var TOTAL_SCORE_FROM_CURRENT_EXAM = ""
    var buttonViewResult: Button? = null
    var userSession: UserSession? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam_already_took)
        initActionBar()
        initViews2()
        changeStatusBarColor()
        intentValues
        loadMainView()
    }

    fun changeStatusBarColor() {
        val window = this@ExamAlreadyTook.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@ExamAlreadyTook, R.color.accentColor1)
    }

    val intentValues: Unit
        get() {
            EXAM_ID = intent.getStringExtra("EXAM_ID")
        }

    fun loadMainView() {
        checkIfExamResultAvailable() //CHECKING WHETHER EXAM RESULT IS PUBLISHED OR NOT
        checkExamAttendance() //CHECKING EAM
    }

    fun initActionBar() {
        val actionBarLayout = layoutInflater.inflate(
                R.layout.action_bar_04,
                null) as ViewGroup
        // Set up your ActionBar
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(false)
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.customView = actionBarLayout
        // You customization
        val actionBarColor = resources.getColor(R.color.accentColor1)
        actionBar.setBackgroundDrawable(ColorDrawable(actionBarColor))
        val parent = actionBarLayout.parent as Toolbar
        //        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0,0);
        val actionBarTitle = findViewById<View>(R.id.actionBarTitle) as TextView
        actionBarTitle.text = "Exam"
        val togBackButton = findViewById<View>(R.id.togBackButton) as ImageView
        togBackButton.setOnClickListener { finish() }
    }

    fun initViews2() {
        val profileUtils = ProfileUtils(mContext)
        userSession = profileUtils.userSession
        layoutNoResult = findViewById<View>(R.id.layoutNoResult) as LinearLayout
        layoutResultPublished = findViewById<View>(R.id.layoutResultPublished) as LinearLayout
        textViewResultMsg = findViewById<View>(R.id.textView32) as TextView
        tvExamTitle = findViewById<View>(R.id.tvExamTitle) as TextView
        tvTimeExam = findViewById<View>(R.id.tvTimeExam) as TextView
        tvDateExam = findViewById<View>(R.id.tvDateExam) as TextView
        buttonViewResult = findViewById<View>(R.id.buttonViewResult) as Button
        buttonViewResult!!.setOnClickListener {
            val `in` = Intent(mContext, ExamResultsView::class.java)
            `in`.putExtra("EXAM_ID", EXAM_ID)
            `in`.putExtra("TOTAL_SCORE", TOTAL_SCORE_FROM_CURRENT_EXAM)
            startActivity(`in`)
        }
    }

    fun showResultsPublished(stat: Boolean) {
        if (stat) {
            layoutNoResult!!.visibility = View.GONE
            layoutResultPublished!!.visibility = View.VISIBLE
        } else {
            layoutNoResult!!.visibility = View.VISIBLE
            layoutResultPublished!!.visibility = View.GONE
        }
    }

    fun checkIfExamResultAvailable() {
        val post_data = HashMap<String, String>()
        post_data["hash_code"] = ConfigServer.API_HASH
        post_data["exam_id"] = EXAM_ID
        post_data["student_id"] =  userSession!!.useR_SESSION_ID
        val URL = ConfigServer.API_CHECK_EXAM_PUBLISH_DATE
        val olf = OnlineFunctions(mContext, URL, post_data)
        val handler = Handler()
        val runnable = Runnable {
            API_RESPONSE_GET_EXAM_PUBLISH_CHECK = olf.pOstResult
            handler.post {
                Log.d("sfjhsd", "run: $API_RESPONSE_GET_EXAM_PUBLISH_CHECK")
                try {
                    val jsonObject1 = JSONObject(API_RESPONSE_GET_EXAM_PUBLISH_CHECK)
                    val status = jsonObject1.getString("status")
                    val code = jsonObject1.getString("code")
                    if (code == "1") {
                        showResultsPublished(true)
                    } else if (code == "2") {
                        showResultsPublished(false)
                    }
                    val array1 = jsonObject1.getJSONArray("response")
                    val jsonObject = array1.getJSONObject(0)
                    val exam_id = jsonObject.getString("exam_id")
                    val exam_title = jsonObject.getString("exam_title")
                    val description = jsonObject.getString("description")
                    val category_id = jsonObject.getString("category_id")
                    val university_id = jsonObject.getString("university_id")
                    val coursetype_id = jsonObject.getString("coursetype_id")
                    val course_id = jsonObject.getString("course_id")
                    val subject_id = jsonObject.getString("subject_id")
                    val instruction_id = jsonObject.getString("instruction_id")
                    val examtype = jsonObject.getString("examtype")
                    val start_date = jsonObject.getString("start_date")
                    val end_date = jsonObject.getString("end_date")
                    val duration = jsonObject.getString("duration")
                    val total_mark = jsonObject.getString("total_mark")
                    val pass_mark = jsonObject.getString("pass_mark")
                    val negative_mark = jsonObject.getString("negative_mark")
                    val published = jsonObject.getString("published")
                    val exam_status = jsonObject.getString("exam_status")
                    val questions = jsonObject.getString("questions")
                    val created_at = jsonObject.getString("created_at")
                    val updated_at = jsonObject.getString("updated_at")
                    var publish_date = jsonObject.getString("publish_date")
                    TOTAL_SCORE_FROM_CURRENT_EXAM = total_mark
                    val dtc = DateTimeConverter()
                    publish_date = dtc.formateDateFromstring("yyyy-MM-dd", "dd MMM YYYY", publish_date)
                    textViewResultMsg!!.text = "Exam result will be published on $publish_date"
                    tvExamTitle!!.text = exam_title
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
        Thread(runnable).start()
    }

    fun checkExamAttendance() {
        val post_data = HashMap<String, String>()
        post_data["hash_code"] = ConfigServer.API_HASH
        post_data["exam_id"] = EXAM_ID
        post_data["student_id"] = userSession!!.useR_SESSION_ID
        val URL = ConfigServer.API_CHECK_EXAM_ATTENDANCE
        val olf = OnlineFunctions(mContext, URL, post_data)
        val handler = Handler()
        val runnable = Runnable {
            API_RESPONSE_CHECK_EXAM_ATT = olf.pOstResult
            handler.post {
                try {
                    val jsonObject = JSONObject(API_RESPONSE_CHECK_EXAM_ATT)
                    val status = jsonObject.getString("status")
                    if (status == "200") {
                        val code = jsonObject.getString("code")
                        val resultObj = jsonObject.getJSONObject("result")
                        val created_at = resultObj.getString("created_at")
                        val dtc = DateTimeConverter()
                        val exam_date = dtc.formateDateFromstring("yyyy-MM-dd hh:mm:ss", "dd MMM", created_at)
                        val exam_time = dtc.formateDateFromstring("yyyy-MM-dd hh:mm:ss", "hh:mm a", created_at)
                        tvDateExam!!.text = exam_date
                        tvTimeExam!!.text = exam_time
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
        Thread(runnable).start()
    }
}