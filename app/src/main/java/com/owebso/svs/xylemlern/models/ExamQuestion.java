package com.owebso.svs.xylemlern.models;

public class ExamQuestion {
    private String index;
    private String questionbanks_id;
    private String category_id;
    private String question;
    private String question_image;
    private String explanation;
    private String hints;
    private String mark;
    private String questiontype;
    private String year;
    private String usertype;
    private String questionbanks_status;
    private String no_of_options;
    private String options;
    private String answer;

    public ExamQuestion(String index, String questionbanks_id, String category_id, String question, String question_image, String explanation, String hints, String mark, String questiontype, String year, String usertype, String questionbanks_status, String no_of_options, String options, String answer) {
        this.index = index;
        this.questionbanks_id = questionbanks_id;
        this.category_id = category_id;
        this.question = question;
        this.question_image = question_image;
        this.explanation = explanation;
        this.hints = hints;
        this.mark = mark;
        this.questiontype = questiontype;
        this.year = year;
        this.usertype = usertype;
        this.questionbanks_status = questionbanks_status;
        this.no_of_options = no_of_options;
        this.options = options;
        this.answer = answer;
    }


    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getQuestionbanks_id() {
        return questionbanks_id;
    }

    public void setQuestionbanks_id(String questionbanks_id) {
        this.questionbanks_id = questionbanks_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestion_image() {
        return question_image;
    }

    public void setQuestion_image(String question_image) {
        this.question_image = question_image;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getHints() {
        return hints;
    }

    public void setHints(String hints) {
        this.hints = hints;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getQuestiontype() {
        return questiontype;
    }

    public void setQuestiontype(String questiontype) {
        this.questiontype = questiontype;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getQuestionbanks_status() {
        return questionbanks_status;
    }

    public void setQuestionbanks_status(String questionbanks_status) {
        this.questionbanks_status = questionbanks_status;
    }

    public String getNo_of_options() {
        return no_of_options;
    }

    public void setNo_of_options(String no_of_options) {
        this.no_of_options = no_of_options;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getIndexInInteger() {
        return Integer.parseInt(index);
    }
}
