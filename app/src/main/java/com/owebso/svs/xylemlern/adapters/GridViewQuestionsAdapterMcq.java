package com.owebso.svs.xylemlern.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.models.McqQuestion;

import java.util.ArrayList;

/**
 * Created by Sarath on 07/06/19.
 */

public class GridViewQuestionsAdapterMcq extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<McqQuestion> indexList;
    EventListener listener;
    int SORT_TYPE=0;
    RelativeLayout cardOldSelection;
    RecyclerView.ViewHolder myHolder1;
    String[] scoreBoardArray;

    public GridViewQuestionsAdapterMcq(Context mContext, ArrayList<McqQuestion> indexList,String[] scoreBoardArray
            , EventListener listener, int SORT_TYPE) {
        this.mContext = mContext;
        this.indexList = indexList;
        this.listener =listener;
        this.SORT_TYPE =SORT_TYPE;
        this.scoreBoardArray = scoreBoardArray;
    }

    public interface EventListener {
        void onCategoryClicked(McqQuestion question, int index);
    }
    public GridViewQuestionsAdapterMcq(EventListener listener){
        this.listener =listener;
    }

    public  class MainView extends RecyclerView.ViewHolder {
        TextView textViewIndex;
        RelativeLayout fullView;
        ImageView imageMarked;
 
        public MainView(View v) {
            super(v);
            this.textViewIndex = (TextView) v.findViewById(R.id.textViewIndex);
            this.fullView = (RelativeLayout)v.findViewById(R.id.fullView);
            this.imageMarked = (ImageView) v.findViewById(R.id.imageMarked);

        }
    }

//    public class TitleView extends  RecyclerView.ViewHolder {
//        TextView tvModuleTitle;
//        public TitleView(View v) {
//            super(v);
//            
//        }
//    }

    public  class AdsView extends  RecyclerView.ViewHolder {
        public AdsView(View v) {
            super(v);
        }
    }


    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
        if(viewType==0)
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_view_grid_view_01, parent, false);
            return new MainView(itemView);
        }
      
        else
        {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_layout_video_category, parent, false);
            return new AdsView(itemView);
        }



    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final McqQuestion question = indexList.get(position);
      if(holder.getItemViewType()==0)
        {

            chnageStyle(((MainView)holder).fullView,scoreBoardArray[position]);

            if(position==0){
                setSelection(((MainView)holder).fullView);
            }

            ((MainView)holder).imageMarked.setVisibility(View.GONE);

            int index = Integer.parseInt(question.getIndex());
            index++;
            ((MainView)holder).textViewIndex.setText(""+index);
            ((MainView)holder).fullView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCategoryClicked(question,position)  ;
                   // ((MainView)holder).fullView.setBackgroundColor(mContext.getResources().getColor(R.color.right_answer));
                    setSelection(((MainView)holder).fullView);
                }
            });
            
        }
        else {

            final AdsView third_holder = (AdsView)holder;
            //third_holder.third_indexList_tv.setText(catObj.getName());
        }

    }


    public void setSelection(RelativeLayout view){

        if(cardOldSelection!=null){
          //  cardOldSelection.setBackgroundColor(mContext.getResources().getColor(R.color.white));
         //   cardOldSelection.setBackground(mContext.getResources().getDrawable(R.drawable.numbering_layout));

        }
      //  view.setBackgroundColor(mContext.getResources().getColor(R.color.right_answer));
      //  view.setBackground(mContext.getResources().getDrawable(R.drawable.numbering_layout_current_selection));
      //  cardOldSelection = view;
    }

    public void chnageStyle(RelativeLayout view,String style){
        // 0 SKIPPED
        // 1 RIGHT
        // 2 WRONG
        int styleInt = Integer.parseInt(style);

        switch (styleInt){
            case 0:
                //skipped
                view.setBackground(mContext.getResources().getDrawable(R.drawable.exam_skipped_rounded_corner_01));
                break;
            case 1:
                //right
                view.setBackground(mContext.getResources().getDrawable(R.drawable.exam_attended_right_answer_rounded_corner_01));
                break;
            case 2:
                //wrong
                view.setBackground(mContext.getResources().getDrawable(R.drawable.exam_attended_wrong_answer_rounded_corner_01));
                break;
            case 9:
                //unattended
                view.setBackground(mContext.getResources().getDrawable(R.drawable.exam_un_attended_rounded_corner_01));
                break;

        }

    }




    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return indexList.size();
    }


}
