package com.owebso.svs.xylemlern.models;

public class VidTimeLine {
    private String id;
    private String title;
    private String seconds;
    private String imagePath;

    public VidTimeLine(String id, String title, String seconds, String imagePath) {
        this.id = id;
        this.title = title;
        this.seconds = seconds;
        this.imagePath = imagePath;
    }


    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSeconds() {
        return seconds;
    }

    public void setSeconds(String seconds) {
        this.seconds = seconds;
    }
}
