package com.owebso.svs.xylemlern.test;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.owebso.svs.xylemlern.MainActivity;
import com.owebso.svs.xylemlern.R;
import com.owebso.svs.xylemlern.ServerConfig.ConfigServer;
import com.owebso.svs.xylemlern.adapters.VideosCategoryAdapter;
import com.owebso.svs.xylemlern.adapters.VideosListAdapter;
import com.owebso.svs.xylemlern.functionUtils.OnlineFunctions;
import com.owebso.svs.xylemlern.models.Category;
import com.owebso.svs.xylemlern.models.VideoX;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class VideosListView extends AppCompatActivity {
    private Context mContext = VideosListView.this;
    String API_RESPONSE_GET_CATEGORY="",API_RESPONSE_GET_VIDEOS;
    ArrayList<Category> categoryList;
    VideosCategoryAdapter adapter;
    RecyclerView recyclerview;
    String CATEGORY_ID="1",API_RESPONSE_GET_CAT_SINGLE;
    LinearLayout loadingLayout,mainView;
    ArrayList<Integer> catParent;
    ArrayList<VideoX> videoXArrayList;
    RecyclerView recyclerView123;
    VideosListAdapter videoAdapter;
    LinearLayout videosLayout;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager videoLayoutmanager;
    BottomNavigationView bottomNavigationView;
    LinearLayout noVidLayoutAlert;
    ImageView backActionB;
    TextView actionBarTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos_list_view);

        changeStatusBarColor();
        initActionBar();
        initViews();
        initBottomNavigation();
        getCategories();


    }

    public void initViews(){
        catParent = new ArrayList<>();
        catParent.add(Integer.parseInt(CATEGORY_ID));

        noVidLayoutAlert = (LinearLayout)findViewById(R.id.noVidLayoutAlert);
        noVidLayoutAlert.setVisibility(View.GONE);

        if(!isTablet()){
            gridLayoutManager = new GridLayoutManager(getApplicationContext(),1);
            videoLayoutmanager = new GridLayoutManager(getApplicationContext(),1);
        }else{
            gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
            videoLayoutmanager = new GridLayoutManager(getApplicationContext(),2);
        }


        recyclerview = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView123 = (RecyclerView)findViewById(R.id.recyclerView123);
        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerView123.setLayoutManager(videoLayoutmanager);

        loadingLayout = (LinearLayout)findViewById(R.id.loadingLayout);
        videosLayout = (LinearLayout)findViewById(R.id.videosLayout);
        loadingLayout.setVisibility(View.GONE);
        videosLayout.setVisibility(View.GONE);

        mainView = (LinearLayout)findViewById(R.id.mainView);

    }

    public void changeStatusBarColor(){
        Window window = VideosListView.this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(VideosListView.this,R.color.accentColor1));
    }

    public  void initActionBar(){
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_04,
                null);

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        // You customization
        final int actionBarColor = getResources().getColor(R.color.accentColor1);
        actionBar.setBackgroundDrawable(new ColorDrawable(actionBarColor));

        Toolbar parent =(Toolbar) actionBarLayout.getParent();
      //  parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
       // parent.setContentInsetsAbsolute(0,0);

        actionBarTitle = (TextView) findViewById(R.id.actionBarTitle);
        actionBarTitle.setText("VIDEOS");

        ImageView togBackButton = (ImageView) findViewById(R.id.togBackButton);
        togBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    public boolean isTablet(){
        boolean isTablet;
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float yInches= metrics.heightPixels/metrics.ydpi;
        float xInches= metrics.widthPixels/metrics.xdpi;
        double diagonalInches = Math.sqrt(xInches*xInches + yInches*yInches);
        if (diagonalInches>=8){
            // 6.5inch device or bigger
            isTablet=true;
        }else{
            // smaller device
            isTablet=false;
        }

        return isTablet;
    }
    public void getCategories(){
        showLoading(true);
        categoryList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("parent_id",CATEGORY_ID);
        String URL = ConfigServer.API_GET_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CATEGORY = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showLoading(false);
                        noVidLayoutAlert.setVisibility(View.GONE);
                        Log.d("API_RESPONSE", ""+API_RESPONSE_GET_CATEGORY);
                        try {
                            JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_CATEGORY);
                            String status = jsonObject1.getString("status");

                            if(status.equals("404")){
                                noVidLayoutAlert.setVisibility(View.VISIBLE);
                            }else {
                                noVidLayoutAlert.setVisibility(View.GONE);
                            }

                            JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject catObj = jsonArray.getJSONObject(i);
                                String category_id = catObj.getString("category_id");
                                String category_name = catObj.getString("category_name");
                                String description = catObj.getString("description");
                                String parent_id = catObj.getString("parent_id");
                                String category_section = catObj.getString("category_section");
                                String category_status = catObj.getString("category_status");
                                String category_image = catObj.getString("category_image");
                                String created_at = catObj.getString("created_at");

                                categoryList.add(new Category(i,category_id,category_name,description,parent_id,category_section,category_status,category_image,created_at,1));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapter = new VideosCategoryAdapter(mContext, categoryList, new VideosCategoryAdapter.EventListener() {
                            @Override
                            public void onCategoryClicked(String categoryId, String parentId) {

                            }
                        });
                        recyclerview.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        videosLayout.setVisibility(View.GONE);
                    }
                });
            }
        };

        new Thread(runnable).start();
    }

    public void showLoading(boolean stat){
        if(stat){
            mainView.setVisibility(View.GONE);
            loadingLayout.setVisibility(View.VISIBLE);
        }else {
            mainView.setVisibility(View.VISIBLE);
            loadingLayout.setVisibility(View.GONE);
        }
    }

    public void callRefresh(String CAT){
            showLoading(true);
          this.CATEGORY_ID= CAT;

    }

    public void setParentArray(int i){

        if(catParent.size()>=4){
            this.CATEGORY_ID=String.valueOf(i);
           // Toast.makeText(mContext, "SHOW VIDEOS", Toast.LENGTH_SHORT).show();
            getVideos();
        }else {
            catParent.add(i);
            this.CATEGORY_ID=String.valueOf(i);
            getCategories();
        }
    }

    @Override
    public void onBackPressed() {

        if(catParent.size()!=0){
            try{
                Log.d("PARENT_ARRAY", "size: "+catParent.size());
                catParent.remove(catParent.get(catParent.size()-1));
                //  Log.d("PARENT_ARRAY", "removed : "+catParent.get(catParent.get(catParent.size()-1)));
                this.CATEGORY_ID=String.valueOf(catParent.get(catParent.size()-1));
                getCategories();
            }catch (ArrayIndexOutOfBoundsException e){
                super.onBackPressed();
            }

        }else {
            super.onBackPressed();
        }
    }



    public void getVideos(){
        showLoading(true);
        videoXArrayList = new ArrayList<>();
        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code",ConfigServer.API_HASH);
        post_data.put("category_id",CATEGORY_ID);
        Log.d("API_RESPONSE", "current_id: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_VIDEOS_CATEGORY;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_VIDEOS = olf.getPOstResult();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            showLoading(false);
                            Log.d("API_RESPONSE", "run: "+API_RESPONSE_GET_VIDEOS);
                            try {
                                JSONObject jsonObject1 = new JSONObject(API_RESPONSE_GET_VIDEOS);
                                String status  = jsonObject1.getString("status");
                                if(!status.equals("404")){
                                    noVidLayoutAlert.setVisibility(View.GONE);
                                    JSONArray jsonArray = new JSONArray(jsonObject1.getString("result"));
                                    for(int i =0;i<jsonArray.length();i++){
                                        JSONObject videoObj = jsonArray.getJSONObject(i);
                                        String video_id = videoObj.getString("video_id");
                                        String video_title = videoObj.getString("video_title");
                                        String category_id = videoObj.getString("category_id");
                                        String description = videoObj.getString("description");
                                        String thumb_image = videoObj.getString("thumb_image");
                                        String video_path = videoObj.getString("video_path");
                                        String str_paid = videoObj.getString("paid");
                                        String str_unpaid = videoObj.getString("unpaid");
                                        String str_favourite = videoObj.getString("favourite");
                                        String video_status = videoObj.getString("video_status");
                                        String str_menu_order = videoObj.getString("menu_order");
                                        String created_at = videoObj.getString("created_at");
                                        String str_watch_status = videoObj.getString("watch_status");
                                        String str_time = videoObj.getString("time");
                                        String IMAGE_URL_THUMB = ConfigServer.PATH_VIDEO_THUMB+thumb_image;
                                        video_path = ConfigServer.PATH_VIDEO+video_path;

                                        videoXArrayList.add(new VideoX(video_id,video_title,category_id,description,IMAGE_URL_THUMB,video_path,str_paid,str_unpaid,str_favourite,video_status,
                                                str_menu_order,created_at,str_watch_status,str_time,1));
                                    }
                                }else {
                                    Log.d("API_RESPONSE", "NO VIDEOS");
                                    noVidLayoutAlert.setVisibility(View.VISIBLE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.d("API_RESPONSE", "exception");

                            }

                            videoAdapter = new VideosListAdapter(mContext, videoXArrayList, new VideosListAdapter.EventListener() {
                                @Override
                                public void onVideoClicked(VideoX video,String pay_sts) {

                                }
                            });
                            recyclerView123.setAdapter(videoAdapter);
                            mainView.setVisibility(View.GONE);
                            videosLayout.setVisibility(View.VISIBLE);


                        }
                    });
            }
        };

        new Thread(runnable).start();
    }

    public void openFragment(int FRAGMENT_ID){
        Intent in = new Intent(mContext, MainActivity.class);
        Log.d("FRAGMENTSSFD", "openFragment:"+FRAGMENT_ID);
        in.putExtra("FRAGMENT_ID",FRAGMENT_ID);
        in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(in);
        finish();
    }

    public void initBottomNavigation(){
        bottomNavigationView = (BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.action_videos);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.action_home:
                        openFragment(1);
                        break;
                    case R.id.action_videos:
                        Intent in = new Intent(mContext,VideosListView.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(in);
                        finish();
                        break;
                    case R.id.action_mcq_mcq:
                        openFragment(3);
                        break;
                    case R.id.action_exams:
                        openFragment(4);
                        break;
                }
                return true;
            }
        });
    }

    public void setModuleTitle(String PARENT_CATEGORY_ID){

        final HashMap<String, String> post_data = new HashMap<>();
        post_data.put("hash_code", ConfigServer.API_HASH);
        post_data.put("category_id",PARENT_CATEGORY_ID);
        //Log.d("CATEGORY_ID", "ID: "+CATEGORY_ID);
        String URL = ConfigServer.API_GET_CATEGORY_SINGLE;
        final OnlineFunctions olf = new OnlineFunctions(mContext,URL,post_data);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                API_RESPONSE_GET_CAT_SINGLE = olf.getPOstResult();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("API_RESPONSE_GET16567", ""+API_RESPONSE_GET_CAT_SINGLE);

                        try{
                            JSONObject jsonObject =  new JSONObject(API_RESPONSE_GET_CAT_SINGLE);
                            String status = jsonObject.getString("status");

                            if(status.equals("200")){
                                JSONArray jsonArray = jsonObject.getJSONArray("result");
                                JSONObject category = jsonArray.getJSONObject(0);
                                String category_name = category.getString("category_name");
                                // MODULE_TITLE = category_name;
                                actionBarTitle.setText(category_name);
                            }

                        }catch (JSONException e){

                        }
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

}
