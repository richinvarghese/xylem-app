package com.owebso.svs.xylemlern.functionUtils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.owebso.svs.xylemlern.R;

public class ImageUtils {

    private Context mContext;

    public ImageUtils(Context mContext) {
        this.mContext = mContext;
    }

    public void showImage(String IMAGE_URL, final ImageView imageView){
        Log.d("image_url", "URL : "+ IMAGE_URL);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.placeholder_01);
        requestOptions.error(R.drawable.placeholder_01);

        Glide.with(mContext)
                .applyDefaultRequestOptions(requestOptions)
                .load(IMAGE_URL)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        int i=0;
                      //  Toast.makeText(mContext, "error", Toast.LENGTH_SHORT).show();
                       // imageView.setVisibility(View.GONE);
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                      //  Toast.makeText(mContext, "success", Toast.LENGTH_SHORT).show();

                        return false;
                    }
                }).into(imageView)
        ;

    }

}
